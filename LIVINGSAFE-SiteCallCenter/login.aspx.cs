﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Net.Mail;
using LIVINGSAFE_SiteCallCenter.App_Code;
using System.Web.UI.WebControls;
using System.Collections;
using System.Threading;

namespace LIVINGSAFE_SiteCallCenter
{
  public partial class login : System.Web.UI.Page
  {
    protected int IdISP = -1;
    cUtil Utilidad = new cUtil();
    cSesiones Sesion = new cSesiones();
    cIdiomas Idiomas = new cIdiomas();

    #region Eventos

    protected void Page_Load(object sender, EventArgs e)
    {
      if (User.Identity.IsAuthenticated)
        Response.Redirect("~/res/landing.aspx");

      Utilidad.InsertarCabeceras();

      try
      {
        if (!IsPostBack)
        {
          ComprobarDominio();
          txtNIF.Focus();
          CargarListadoIdiomas();
        }

        //Siempre que se recargue la página ocultamos el aviso de error. Es una manera de resetear la capa de alerta.
        divAlerta.Attributes["class"] = "alert alert-danger fade collapse";

				if (ViewState["NombreISP"] == null || ViewState["NombreISP"].ToString() == string.Empty)
					ViewState.Add("NombreISP", Utilidad.DameDatoISP(IdISP, "Nombre"));

				Page.Title = "LivingSafe - " + ViewState["NombreISP"];
			}

      catch (Exception Exc)
      {
        string Msg = string.Empty;

        if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);
        else
          Msg = Exc.Message.ToString();


        if (Utilidad.EsEntornoDesarrollo())
          lblResultado.Text = Msg + "<br /><br />" + Exc.ToString();
        else
          lblResultado.Text = Msg;

        divAlerta.Attributes["class"] = "alert alert-danger alert-dismissible fade show container-fluid";
      }
    }

    protected void lbSiguiente_Click(object sender, EventArgs e)
    {
      string Login = txtNIF.Text.Replace("'", "").Replace(" ", "").Replace("\\", "").Trim();
      string URL = HttpContext.Current.Request.Url.Host;
      int IdISPDetectado = Utilidad.DameIdISPPorDominio(URL);
      ActivaDesactivaCaptcha(Login, IdISPDetectado);

      lbUsuario.Text = txtNIF.Text;
      divPassword.Visible = true;
      divUsuario.Visible = false;

      txtPassword.Focus();
    }

    protected void lbVolver_Click(object sender, EventArgs e)
    {
      divPassword.Visible = false;
      divUsuario.Visible = true;
      txtNIF.Focus();
    }

    protected void lbLogin_Click(object sender, EventArgs e)
    {
      if (Page.IsValid)
      {
        try
        {
          Page.Validate();

          if (!rfvUsuario.IsValid || !rfvPassword.IsValid)
            return;

          //Autenticar devuelve true o false en función de si hay que llamar a activar o desactivar captcha.
          bool ActivarDesactivarCAPTCHA = Sesion.Autenticar(txtNIF.Text, txtPassword.Text, chkRecordar.Checked, "(Pagina de login)");

          if (ActivarDesactivarCAPTCHA)
          {
            string Login = txtNIF.Text.Replace("'", "").Replace(" ", "").Replace("\\", "").Trim();
            string URL = HttpContext.Current.Request.Url.Host;
            int IdISPDetectado = Utilidad.DameIdISPPorDominio(URL);
            ActivaDesactivaCaptcha(Login, IdISPDetectado);
          }
        }

        catch (Exception Exc)
        {
          string Msg = string.Empty;

          if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
            Msg = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);

          if (Resources.login.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null && Msg == string.Empty)
            Msg = Resources.login.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);

					if (Msg == string.Empty)
						Msg = Resources.login.ResourceManager.GetString("ErrorGenerico", Thread.CurrentThread.CurrentCulture);



					if (Utilidad.EsEntornoDesarrollo())
            lblMensaje.Text = Msg + "<br /><br />" + Exc.ToString();
          else
            lblMensaje.Text = Msg;

          divAlerta.Attributes["class"] = "alert alert-danger alert-dismissible fade show container-fluid";
        }
      }
      else
      {
        lblMensaje.Text = "O CAPTCHA &eacute; inv&aacute;lido ou est&aacute; vazio.";

        divAlerta.Attributes["class"] = "alert alert-danger alert-dismissible fade show container-fluid";
      }
    }

    protected void lbCambiarPassword_Click(object sender, EventArgs e)
    {
      txtLogin.Text = string.Empty;
      divAlertaPassword.Attributes["class"] = "alert alert-success fade collapse container-fluid";
      Page.ClientScript.RegisterStartupScript(this.GetType(), "Show", "$(document).ready(function() {$('#divRecuperar').modal('show');$('#" + txtLogin.ClientID + "').focus();});", true);
    }

    protected void btnSolicitar_Click(object sender, EventArgs e)
    {
      try
      {
        ComprobarDominio();

        divAlertaPassword.Attributes["class"] = "alert alert-success fade show container-fluid";

        string EMail = Sesion.SolicitarCambioPassword(txtLogin.Text, IdISP);

        if (EMail != null && EMail != string.Empty)
        { 
          string[] Partes = EMail.Split('@');
          string CadenaReemplazada = Partes[0].Substring(0, Partes[0].Length / 2);

          CadenaReemplazada = CadenaReemplazada + "*****" + "@" + Partes[1];

          lblResultado.Text = Resources.login.MsgCambioOK1 + " " + CadenaReemplazada + ". " + Resources.login.MsgCambioOK2;
        }
        else
        {
          lblResultado.Text = Resources.login.ResourceManager.GetString("ERRNUM0002", Thread.CurrentThread.CurrentCulture);
          divAlertaPassword.Attributes["class"] = "alert alert-error alert-danger fade show container-fluid";
          string Escript = "$(document).ready(function() {$('#divAlertaPassword').modal('show');});";
          Page.ClientScript.RegisterStartupScript(Page.GetType(), "MostrarErrorLogin", Escript, true);
        }
      }

      catch (Exception Exc)
      {
        string Msg = string.Empty;

        if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);
        else
          Msg = Exc.Message.ToString();


        if (Utilidad.EsEntornoDesarrollo())
          lblResultado.Text = Msg + "<br /><br />" + Exc.ToString();
        else
          lblResultado.Text = Msg;

        divAlertaPassword.Attributes["class"] = "alert alert-error alert-danger fade show container-fluid";
        string Escript = "$(document).ready(function() {$('#divAlertaPassword').modal('show');});";
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "MostrarErrorLogin", Escript, true);
      }
    }

    protected void ddlIdioma_SelectedIndexChanged(object sender, EventArgs e)
    {
      DropDownList ddlIdiomas = (DropDownList)sender;
      Idiomas.GrabarCookieIdioma(ddlIdiomas.SelectedItem.Value);
      Response.Redirect(Request.RawUrl);
    }

    #endregion


    #region Métodos

    protected void ComprobarDominio()
    {
      string URL = HttpContext.Current.Request.Url.Host;
      string DominioPrincipal = Utilidad.DameValorConfiguracion("DominioPrincipal");
      string Subdominio = Utilidad.DameSubdominio(URL, DominioPrincipal);
      string RutaCSS = Utilidad.CargarISP(Subdominio, ref IdISP);
      cssCustom.Href = RutaCSS;
    }

    protected void ValidateCaptcha(object sender, ServerValidateEventArgs e)
    {
      Captcha1.ValidateCaptcha(txtCaptcha.Text.Trim());
      e.IsValid = Captcha1.UserValidated;
    }

    protected void CargarListadoIdiomas()
    {
      string CulturaDetectada = Idiomas.DevolverCulturaDetectada();
      string URL = HttpContext.Current.Request.Url.Host;
      int IdISPDetectado = Utilidad.DameIdISPPorDominio(URL);
      ArrayList Lista = Utilidad.DameIdiomasISP(IdISPDetectado);
      ListItem Idioma = null;

      Idioma = new ListItem();
      Idioma.Text = Resources.login.ResourceManager.GetString("Seleccionar");
      Idioma.Value = string.Empty;
      Idioma.Selected = true;
      ddlIdioma.Items.Add(Idioma);

      for (int i = 0; i < Lista.Count; i++)
      {
        Idioma = new ListItem();
        Idioma.Text = HttpUtility.HtmlDecode(Utilidad.DameDato((ArrayList)Lista[i], "Idioma") + " (" + Utilidad.DameDato((ArrayList)Lista[i], "CultureCode") + ")");
        Idioma.Value = Utilidad.DameDato((ArrayList)Lista[i], "CultureCode");
        ddlIdioma.Items.Add(Idioma);
      }
    }

    // Control CAPTCHA (Intentos)
    protected void ActivaDesactivaCaptcha(string Login, int IdISP)
    {
      //Calculamos si tenemos que mostrar el Captcha o no
      cSesiones Sesiones = new cSesiones();
      string IP = Utilidad.DameIP();

      int Contador = 0;
      Contador = Sesiones.ContarIntentosCAPTCHA(Login, IdISP, IP);

      if (Contador >= 3)
      {
        divCaptcha.Visible = true;
        txtCaptcha.Visible = true;
        cvCaptcha.Visible = true;
        Captcha1.Visible = true;
      }
      else
      {
        divCaptcha.Visible = false;
        txtCaptcha.Visible = false;
        cvCaptcha.Visible = false;
        Captcha1.Visible = false;
      }
    }

    #endregion
  }
}