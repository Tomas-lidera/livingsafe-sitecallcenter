﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LIVINGSAFE_SiteCallCenter.errors
{
  public partial class error : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      switch (Request.QueryString["code"])
      {
        case "0":
          lblMsgError.Text = "Unexpected error";
          lblCodError.Text = "???";
          break;

        case "400":
          lblMsgError.Text = "Bad request";
          lblCodError.Text = "400";
          break;

        case "401":
          lblMsgError.Text = "Unauthorized";
          lblCodError.Text = "401";
          break;

        case "403":
          lblMsgError.Text = "Forbidden";
          lblCodError.Text = "403";
          break;

        case "404":
          lblMsgError.Text = "Page not found";
          lblCodError.Text = "404";
          break;

        case "500":
          lblMsgError.Text = "Internal server error";
          lblCodError.Text = "500";
          break;

        default:
          lblMsgError.Text = "Unexpected error";
          lblCodError.Text = "???";
          break;
      }
    }
  }
}