﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="error.aspx.cs" Inherits="LIVINGSAFE_SiteCallCenter.errors.error" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <title>LivingSafe - Error</title>
  <link runat="server" rel="stylesheet" type="text/css" href="~/style/bootstrap.css" />
  <link runat="server" rel="stylesheet" type="text/css" href="~/style/generalstyle.css" />
  <link runat="server" rel="stylesheet" type="text/css" href="~/style/custom/personalstyle.css" id="cssCustom" />
  <script type="text/javascript" src="/js/jquery-3.3.1.slim.min.js"></script>
  <script type="text/javascript" src="/js/bootstrap.bundle.js"></script>
  <script type="text/javascript" src="/js/bootstrap.js"></script>
  <link rel="apple-touch-icon" sizes="180x180" href="~/apple-touch-icon.png" runat="server" />
  <link rel="icon" type="image/png" sizes="32x32" href="~/favicon-32x32.png" runat="server" />
  <link rel="icon" type="image/png" sizes="16x16" href="~/favicon-16x16.png" runat="server" />
  <link rel="mask-icon" href="~/safari-pinned-tab.svg" color="#5bbad5" runat="server" />
</head>
<body class="body_login">
  <form id="form1" runat="server" >
    <div class="container">
      
    </div>

    <div style="display:block; text-align:center; margin:auto 0px;">
      <span class="txt_error_big"><asp:Label ID="lblCodError" runat="server"></asp:Label></span>
      <span class="txt_error_peq"><asp:Label ID="lblMsgError" runat="server"></asp:Label></span>
      <span class="txt_error_peq" style="margin-top:20px;"><a href="~/login.aspx" runat="server"  enableviewstate="false" class="lnk_vermasblanco">VOLVER</a></span>
    </div>

  </form>
</body>
</html>
