﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LIVINGSAFE_SiteCallCenter.App_Code;

namespace LIVINGSAFE_SiteCallCenter
{
  public partial class change_password : System.Web.UI.Page
  {
    int IdISP = -1;
    int IdOperador = -1;
    cIdiomas Idiomas = new cIdiomas();
    cUtil Utilidad = new cUtil();

    protected void Page_Load(object sender, EventArgs e)
    {
      if (!IsPostBack)
      {
        txtPassword1.Focus();
        CargarListadoIdiomas();
      }

      try
      {
        Utilidad.InsertarCabeceras();

        int isd = -1;
        DateTime Fecha = DateTime.Now;
        string IdOperadorCifrado = string.Empty;
        string IdISPCifrado = string.Empty;

        if (Request.QueryString["i"] != null && Request.QueryString["i"] != string.Empty && Request.QueryString["p"] != null && Request.QueryString["p"] != string.Empty && Request.QueryString["f"] != null && Request.QueryString["f"] != string.Empty)
        {
          IdOperadorCifrado = Request.QueryString["i"];
          IdISPCifrado = Request.QueryString["p"];
          //Como el parámetro "isd" (que es el IdISP) me llega en plano, lo convierto a entero para limpiar posible código inyectado.
          //Necesito el IdISP en plano porque he de localizar la clave de cifrado AES de ese IdISP.
          isd = Convert.ToInt32(Request.QueryString["pd"]);
        }
        else
        {
          Response.Write(Resources.change_password.ResourceManager.GetString("ERRNUM0003"));
          //Response.End(); //Da error. En vez de eso, ponemos estas tres líneas.
          HttpContext.Current.Response.Flush();
          HttpContext.Current.Response.SuppressContent = true;
          HttpContext.Current.ApplicationInstance.CompleteRequest();
        }

        //Si tiene mas de 50 caracteres
        if (IdOperadorCifrado.Length > 50 && IdISPCifrado.Length > 50)
        {
          cEncriptacion AES = new cEncriptacion(Utilidad.DameClaveCifradoISP(isd));

          string SC = IdISPCifrado.Substring(0, 44);
          string IdISPDescifrado = IdISPCifrado.Substring(SC.Length, IdISPCifrado.Length - SC.Length);
          string IdISPDescifradoFinal = AES.DescifrarAES(isd, IdISPCifrado);

          //Como el IdISP me llega cifrado y en plano miro que sean el mismo, aunque si ha sido capaz 
          //de descrifrarlo es que es el ISP correcto, el que se usó en origen.
          if (Convert.ToInt32(IdISPDescifradoFinal) != isd)
            throw new Exception(Resources.change_password.ResourceManager.GetString("ERRNUM0001"));


          SC = IdOperadorCifrado.Substring(0, 44);
          string IdOperadorFinalDescifrado = IdOperadorCifrado.Substring(SC.Length, IdOperadorCifrado.Length - SC.Length);
          string IdOperadorDescifradoFinal = AES.DescifrarAES(IdOperadorFinalDescifrado, SC);

          Fecha = new DateTime((long)Convert.ToInt64(Request.QueryString["f"]));

          if (Fecha.AddMinutes(Convert.ToInt32(Utilidad.DameValorConfiguracion("CaducidadEnlaceCambioPassword"))) < DateTime.Now)
          {
            Response.Write(Resources.change_password.ResourceManager.GetString("ERRNUM0002"));
            //Response.End(); //Da error. En vez de eso, ponemos estas tres líneas.
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.SuppressContent = true;
            HttpContext.Current.ApplicationInstance.CompleteRequest();
          }

          //Si llega aquí es que está todo correcto. Le permito cambiar la contraseña.
          IdISP = Convert.ToInt32(IdISPDescifradoFinal);
          IdOperador = Convert.ToInt32(IdOperadorDescifradoFinal);

					if (ViewState["NombreISP"] == null || ViewState["NombreISP"].ToString() == string.Empty)
						ViewState.Add("NombreISP", Utilidad.DameDatoISP(IdISP, "Nombre"));

					Page.Title = "LivingSafe - " + ViewState["NombreISP"];
				}
      }

      catch (Exception Exc)
      {
        Response.Write(Exc.ToString());
        //Response.End(); //Da error. En vez de eso, ponemos estas tres líneas.
        HttpContext.Current.Response.Flush();
        HttpContext.Current.Response.SuppressContent = true;
        HttpContext.Current.ApplicationInstance.CompleteRequest();
      }
    }

    protected void btnCambioPassword_Click(object sender, EventArgs e)
    {
      Page.Validate();

      if (!reqPassword1.IsValid || !regPassword1.IsValid || !reqPassword2.IsValid || !reqcomPasswords.IsValid)
        return;

      CambiarPassword();
    }

    protected void ddlIdioma_SelectedIndexChanged(object sender, EventArgs e)
    {
      DropDownList ddlIdiomas = (DropDownList)sender;
      Idiomas.GrabarCookieIdioma(ddlIdiomas.SelectedItem.Value);
      Response.Redirect(Request.RawUrl);
    }

    protected void CambiarPassword()
    {
      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      cEncriptacion AES = new cEncriptacion(Utilidad.DameClaveCifradoISP(IdISP));

      try
      {
        string Password = txtPassword1.Text;
        string SC = AES.CrearSecretoCompartido(Password);
        string PasswordCifrada = AES.CifrarAES(Password, SC);
        string PasswordCifradaFinal = SC + PasswordCifrada;

        Con.Open();

        SqlCommand Com = Con.CreateCommand();

        Com.Parameters.Clear();

        Com.CommandText = "Operadores_CambiarPassword";
        Com.CommandTimeout = 300;
        Com.CommandType = System.Data.CommandType.StoredProcedure;
        Com.Parameters.Add("@IdOperador", SqlDbType.Int).Value = IdOperador;
        Com.Parameters.Add("@Password", SqlDbType.VarChar, -1).Value = PasswordCifradaFinal;

        Com.ExecuteNonQuery();

        txtPassword1.Text = txtPassword2.Text = string.Empty;

        lblError.Text = Resources.change_password.ResourceManager.GetString("PasswordCambiada");
        divAlerta.Attributes["class"] = "alert alert-success fade show container-fluid";

        //Registramos evento de cambio de contraseña
        Utilidad.EscribeLOG(IdOperador, "Operador", "Cambio de contraseña realizado desde la página de LOGIN (CALLCENTER)");


        //Tras cambio de contraseña caducamos la sesion para forzar el volver a logar
        string TipoUsuario = string.Empty;

        if (Request.Cookies["SesionTipo"] != null)
          TipoUsuario = AES.DescifrarAES(IdISP, Request.Cookies["SesionTipo"].Value.ToString());
        else
          TipoUsuario = "Desconocido";

        Utilidad.EscribeLOG(IdOperador, TipoUsuario, "Forzamos caducidad de sesión para obligarle a volver a logar (CALLCENTER)");

        cSesiones Sesion = new cSesiones();
        Sesion.CaducarSesion(IdOperador, TipoUsuario, IdISP);
        Sesion.CaducaOperador();

        //Redirigimos al login transcurridos 5.000 milisegundos
        string Escript = "setTimeout(function () {window.location.href = \"/login.aspx\";}, 5000);";
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Redireccion", Escript, true);
      }

      catch (Exception Exc)
      {
        string Msg = string.Empty;

        if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);
        else
          Msg = Exc.Message.ToString();


        if (Utilidad.EsEntornoDesarrollo())
          lblError.Text = Msg + "<br /><br />" + Exc.ToString();
        else
          lblError.Text = Msg;

        divAlerta.Attributes["class"] = "alert alert-danger fade show container-fluid";
      }

      finally
      {
        if (Con.State == ConnectionState.Open)
          Con.Close();

        Con.Dispose();
      }
    }

    protected void CargarListadoIdiomas()
    {
      string CulturaDetectada = Idiomas.DevolverCulturaDetectada();
      string URL = HttpContext.Current.Request.Url.Host;
      int IdISPDetectado = Utilidad.DameIdISPPorDominio(URL);
      ArrayList Lista = Utilidad.DameIdiomasISP(IdISPDetectado);
      ListItem Idioma = null;

      Idioma = new ListItem();
      Idioma.Text = Resources.login.ResourceManager.GetString("Seleccionar");
      Idioma.Value = string.Empty;
      Idioma.Selected = true;
      ddlIdioma.Items.Add(Idioma);

      for (int i = 0; i < Lista.Count; i++)
      {
        Idioma = new ListItem();
        Idioma.Text = Utilidad.DameDato((ArrayList)Lista[i], "Idioma") + " (" + Utilidad.DameDato((ArrayList)Lista[i], "CultureCode") + ")";
        Idioma.Value = Utilidad.DameDato((ArrayList)Lista[i], "CultureCode");

        //if (Idioma.Value.ToLower() == CulturaDetectada.ToLower())
        //  Idioma.Selected = true;

        ddlIdioma.Items.Add(Idioma);
      }
    }
  }
}