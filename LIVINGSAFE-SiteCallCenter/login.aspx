﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="LIVINGSAFE_SiteCallCenter.login" %>
<%@ Register Assembly="MSCaptcha" Namespace="MSCaptcha" TagPrefix="cc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <title>LivingSafe</title>
  
  <link runat="server" rel="stylesheet" type="text/css" href="~/style/bootstrap.css" />
  <link runat="server" rel="stylesheet" type="text/css" href="~/style/generalstyle.css" />
  <link runat="server" rel="stylesheet" type="text/css" href="~/style/custom/personalstyle.css" id="cssCustom" />

  <script type="text/javascript" src="/js/jquery-3.3.1.slim.min.js"></script>
  <script type="text/javascript" src="/js/bootstrap.bundle.js"></script>
  <script type="text/javascript" src="/js/bootstrap.js"></script>
  <script type="text/javascript" src="/js/jquery.mask.js"></script>
  <script type="text/javascript" src="/js/jquery.mask.min.js"></script>

  <link rel="apple-touch-icon" sizes="180x180" href="~/apple-touch-icon.png" runat="server" />
  <link rel="icon" type="image/png" sizes="32x32" href="~/favicon-32x32.png" runat="server" />
  <link rel="icon" type="image/png" sizes="16x16" href="~/favicon-16x16.png" runat="server" />
  <link rel="mask-icon" href="~/safari-pinned-tab.svg" color="#5bbad5" runat="server" />
</head>
<body class="body_login">
  <form id="form1" runat="server" >
    <asp:ScriptManager ID="smLogin" runat="server"></asp:ScriptManager>
    <div class="container">

      <div id="divUsuario" runat="server" class="cuadrologin" style="margin: 0px auto;">
        <div class="logo_login">&nbsp;</div>
        <asp:Panel ID="pnlUsuario" runat="server" DefaultButton="lbSiguiente" Width="100%">
          <div class="login-form">
            <div class="div_selector_idioma">
              <a class="login_selector_idiomas" data-target="#CapaIdiomas" onclick="$(document).ready(function() {$('#CapaIdiomas').modal('show');});" onmouseover="this.style.cursor='pointer';">&nbsp;</a>
            </div>
            <span class="span_login"><asp:Literal id="litUtilizador" Text='<%$ Resources:login,Usuario %>' runat="server" EnableViewState="false" /></span>
            <span class="span_login">
              <asp:TextBox ID="txtNIF" runat="server" CssClass="input_login" ValidationGroup="usuario" ToolTip='<%$ Resources:login,ErrorLogin %>' placeholder='<%$ Resources:login,PlaceHolderLogin %>' />
            </span>
            <span class="span_login txt_error">
              <asp:RequiredFieldValidator runat="server" ID="rfvUsuario" ControlToValidate="txtNIF" Display="Dynamic" CssClass="span_login txt_error" ValidationGroup="usuario" ErrorMessage='<%$ Resources:login,Obligatorio %>'  />
            </span>
          </div>
          <div class="login-btn">
            <asp:LinkButton ID="lbSiguiente" runat="server" CssClass="btn_acceder" OnClick="lbSiguiente_Click" Text='<%$ Resources:login,Siguiente %>' ValidationGroup="usuario"  />
          </div>
        </asp:Panel>
      </div>


      <div id="divPassword" runat="server" class="cuadrologin" style="margin: 0px auto;" visible="false">
        <div class="logo_login">&nbsp;</div>
        <asp:Panel ID="pnlPassword" runat="server" DefaultButton="lbLogin" Width="100%">

          <div class="login-form">
            <span class="span_login"><asp:Literal id="litUtilizador2" Text='<%$ Resources:login,Usuario %>' runat="server" EnableViewState="false" />
              <asp:LinkButton ID="lbVolver" runat="server" EnableViewState="false" CssClass="login_volver" OnClick="lbVolver_Click"></asp:LinkButton>
            </span>
            <span class="span_login"><asp:Label ID="lbUsuario" runat="server" CssClass="label_login" /></span>
            <span class="span_login" style="padding-top:30px;"><asp:Literal id="litPassword" Text='<%$ Resources:login,Password %>' runat="server" EnableViewState="false" /></span>
            <span class="span_login"><asp:TextBox ID="txtPassword" TextMode="Password" runat="server" CssClass="input_login" ValidationGroup="login" AutoCompleteType="None" autocomplete="off" /></span>
            <span class="span_login txt_error"><asp:RequiredFieldValidator runat="server" ID="rfvPassword" ControlToValidate="txtPassword" Display="Dynamic" ErrorMessage='<%$ Resources:login,Obligatorio %>' CssClass="span_login txt_error" ValidationGroup="login" /></span>
            <span class="span_login2"><asp:CheckBox ID="chkRecordar" Text='<%$ Resources:login,Recuerdame %>' runat="server" ValidationGroup="login" /></span>

            <div id="divCaptcha" runat="server" visible="false">
              <span class="span_login2"><cc1:CaptchaControl ID="Captcha1" runat="server" CaptchaBackgroundNoise="High" CaptchaLength="5"
                CaptchaHeight="31" CaptchaWidth="190" CaptchaMinTimeout="5" CaptchaMaxTimeout="240"
                FontColor="#0057AE" NoiseColor="#0057AE" BackColor="#7FCFFF" ValidationGroup="login" 
                 CssClass="span_login3" Visible="false" /></span>

              <span class="span_login2">
                <asp:TextBox ID="txtCaptcha" runat="server" CssClass="captcha" ValidationGroup="login" MaxLength="5" style="text-transform:uppercase" Visible="false"></asp:TextBox>
                <asp:CustomValidator runat="server" ID="cvCaptcha"  OnServerValidate="ValidateCaptcha" ValidationGroup="login" Display="Dynamic" Visible="false" />
              </span>
            </div>
          </div>
          <div class="login-btn">
            <asp:LinkButton ID="lbLogin" runat="server" CssClass="btn_acceder" OnClick="lbLogin_Click" Text='<%$ Resources:login,Acceder %>' ValidationGroup="login"  />
          </div> 
        </asp:Panel>
      </div>

      <div class="cuadrologin" style="margin: 0px auto;">
        <div class="pie_login">
          <asp:LinkButton ID="lbCambiarPassword" runat="server" CssClass="link_pielogin" Text='<%$ Resources:login,Olvido %>' OnClick="lbCambiarPassword_Click" CausesValidation="false" />
        </div>
      </div>


      <div id="divAlerta" runat="server" class="alert alert-danger fade collapse" role="alert" style="margin-top:10px;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close" onclick="this.className='lert alert-danger fade collapse';"><span aria-hidden="true">&times;</span></button>
        <asp:Label ID="lblMensaje" runat="server" />
      </div>
      <div class="logo_login_pie"></div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="Capa cambio password" id="divRecuperar" aria-hidden="true" runat="server">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title txt_titulopagina_med" id="TituloCambioPassword"><asp:Literal id="litTituloCambioPassword" Text='<%$ Resources:login,TitCambioPassword %>' runat="server" EnableViewState="false" /></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <div class="modal-body">
            <div class="form-row">
              <div class="form-group col-md-12">
                <label for="Login"><asp:Literal id="litInstrucciones" Text='<%$ Resources:login,InfoCambioPassword %>' runat="server" EnableViewState="false" /></label>
                <asp:TextBox runat="server" PlaceHolder='<%$ Resources:login,PlaceHolderLogin %>' ID="txtLogin" class="form-control" ValidationGroup="Recordar"
                  ToolTip='<%$ Resources:login,ErrorLogin %>'></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ID="reqLogin" Text='<%$ Resources:login,Obligatorio %>' ControlToValidate="txtLogin" ValidationGroup="Recordar"
                  ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
              </div>
            </div>
            <div class="modal-footer">
                <asp:Button ID="btnSolicitar" runat="server" Text='<%$ Resources:login,Solicitar %>' OnClick="btnSolicitar_Click" class="lnk_passchange" ValidationGroup="Recordar" />
            </div>
            <asp:UpdatePanel runat="server" ID="upCambioPassword">
              <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnSolicitar" />
              </Triggers>
              <ContentTemplate>
                <div class="alert alert-danger fade collapse container-fluid" style="margin-top:20px;" role="alert" runat="server" id="divAlertaPassword">
                  <asp:Label runat="server" ID="lblResultado" />
                </div>
              </ContentTemplate>
            </asp:UpdatePanel>
          </div>
        </div>
      </div>
    </div>

    <div id="CapaIdiomas" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" runat="server">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title txt_titulopagina_peq" id="exampleModalLabel">
              <asp:Literal id="litSeleccionarIdioma" Text='<%$ Resources:login,TitSeleccionarIdioma %>' runat="server" EnableViewState="false" />
            </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <div class="modal-body">
            <asp:DropDownList ID="ddlIdioma" runat="server" AutoPostBack="True" class="form-control" OnSelectedIndexChanged="ddlIdioma_SelectedIndexChanged"></asp:DropDownList>
          </div>
        </div>
      </div>
    </div>
  </form>
</body>
</html>
