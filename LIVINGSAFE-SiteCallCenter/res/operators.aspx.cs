﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LIVINGSAFE_SiteCallCenter.App_Code;
using LIVINGSAFE_SiteCallCenter.Clases;

namespace LIVINGSAFE_SiteCallCenter.res
{
	public partial class operators : System.Web.UI.Page
	{
		cUtil Utilidad = new cUtil();
		cPermisos Permisos = new cPermisos();
		int Entidad = Convert.ToInt32(HttpContext.Current.User.Identity.Name);

		protected void Page_Load(object sender, EventArgs e)
		{
			try
			{
				if (!Permisos.TienePermiso("PAGINAOPERADORES", Entidad))
					Response.Redirect("~/forbidden.aspx");

				if (!IsPostBack)
				{
					if (Utilidad.DameDatoOperador(Entidad, "NombreISP") != null && Utilidad.DameDatoOperador(Entidad, "NombreISP") != string.Empty)
						txtEmpresa.Text = HttpUtility.HtmlDecode(Utilidad.DameDatoOperador(Entidad, "NombreISP"));

					CargarOperadores(tvOperadores,true);
					CargarOperadores(tvOperadoresDestino,false);
					CargarDesplegableTiposOperadores();
				}
			}

			catch (Exception Exc)
			{
				if (Utilidad.EsEntornoDesarrollo())
				{
					lblResultado.Text = Exc.ToString();
				}
				else
				{
					if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
						lblResultado.Text = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
					else if (Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
						lblResultado.Text = Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
					else
						lblResultado.Text = Exc.Message.ToString() + "<br /><br />";
				}

				string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
				ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MostrarErrorPedidos", Escript, true);
			}
		}

		protected void lbGrabarLogin_Click(object sender, EventArgs e)
		{
			if (txtLogin.Text != string.Empty)
			{
				txtLogin.Enabled = true;
				lbGrabarLogin.Visible = false;
				lbGrabarLoginNOK.Visible = true;
				lbGrabarLoginOK.Visible = true;
			}
		}

		protected void lbGrabarLoginOK_Click(object sender, EventArgs e)
		{
			
			txtLogin.Enabled = false;
			lbGrabarLogin.Visible = true;
			lbGrabarLoginNOK.Visible = false;
			lbGrabarLoginOK.Visible = false;
			try
			{
				string[] valores = tvOperadores.SelectedNode.Value.Split(';');
				GrabarDatoOperador(Convert.ToInt32(valores[0]), "Login", txtLogin.Text);

				//Refrescamos los árboles. 
				CargarOperadores(tvOperadores, true);
				CargarOperadores(tvOperadoresDestino, false);
				//Cuando se refrescan los árboles hay que cerrar la ficha porque se pierde el foco del nodo seleccionado y ya no se puede operar con él.
				RestablecerFormularioNuevoOperador();
			}

			catch (Exception Exc)
			{
				string[] valores = tvOperadores.SelectedNode.Value.Split(';');
				CargarDatosOperador(Convert.ToInt32(valores[0]));

				if (Utilidad.EsEntornoDesarrollo())
				{
					lblResultado.Text = Exc.ToString();
				}
				else
				{
					if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
						lblResultado.Text = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
					else if (Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
						lblResultado.Text = Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
					else
						lblResultado.Text = Exc.Message.ToString() + "<br /><br />";
				}

				string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
				ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MostrarErrorPedidos", Escript, true);
			}
		}

		protected void lbGrabarLoginNOK_Click(object sender, EventArgs e)
		{
			txtLogin.Enabled = false;
			lbGrabarLogin.Visible = true;
			lbGrabarLoginNOK.Visible = false;
			lbGrabarLoginOK.Visible = false;

			string[] valores = tvOperadores.SelectedNode.Value.Split(';');
			CargarDatosOperador(Convert.ToInt32(valores[0]));
		}

		protected void lbGrabarContactoNombre_Click(object sender, EventArgs e)
		{
			if (txtContactoNombre.Text != string.Empty || txtContactoApellidos.Text != string.Empty)
			{
				txtContactoNombre.Enabled = true;
				txtContactoApellidos.Enabled = true;
				lbGrabarContactoNombre.Visible = false;
				lbGrabarContactoNombreNOK.Visible = true;
				lbGrabarContactoNombreOK.Visible = true;
			}
		}

		protected void lbGrabarContactoNombreOK_Click(object sender, EventArgs e)
		{
			txtContactoNombre.Enabled = false;
			txtContactoApellidos.Enabled = false;
			lbGrabarContactoNombre.Visible = true;
			lbGrabarContactoNombreNOK.Visible = false;
			lbGrabarContactoNombreOK.Visible = false;

			try
			{
				string[] valores = tvOperadores.SelectedNode.Value.Split(';');
				GrabarDatoOperador(Convert.ToInt32(valores[0]), "Nombre", txtContactoNombre.Text);
				GrabarDatoOperador(Convert.ToInt32(valores[0]), "Apellidos", txtContactoApellidos.Text);
			}

			catch (Exception Exc)
			{
				if (Utilidad.EsEntornoDesarrollo())
				{
					lblResultado.Text = Exc.ToString();
				}
				else
				{
					if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
						lblResultado.Text = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
					else if (Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
						lblResultado.Text = Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
					else
						lblResultado.Text = Exc.Message.ToString() + "<br /><br />";
				}

				string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
				ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MostrarErrorPedidos", Escript, true);
			}
		}

		protected void lbGrabarContactoNombreNOK_Click(object sender, EventArgs e)
		{
			txtContactoNombre.Enabled = false;
			txtContactoApellidos.Enabled = false;
			lbGrabarContactoNombre.Visible = true;
			lbGrabarContactoNombreNOK.Visible = false;
			lbGrabarContactoNombreOK.Visible = false;

			string[] valores = tvOperadores.SelectedNode.Value.Split(';');
			CargarDatosOperador(Convert.ToInt32(valores[0]));
		}

		protected void lbGrabarEmailComunicaciones_Click(object sender, EventArgs e)
		{
			if (txtEmailComunicaciones.Text != string.Empty)
			{
				txtEmailComunicaciones.Enabled = true;
				lbGrabarEmailComunicaciones.Visible = false;
				lbGrabarEmailComunicacionesNOK.Visible = true;
				lbGrabarEmailComunicacionesOK.Visible = true;
			}
		}

		protected void lbGrabarEmailComunicacionesOK_Click(object sender, EventArgs e)
		{
			try
			{
				txtEmailComunicaciones.Enabled = false;
				lbGrabarEmailComunicaciones.Visible = true;
				lbGrabarEmailComunicacionesNOK.Visible = false;
				lbGrabarEmailComunicacionesOK.Visible = false;

				string[] valores = tvOperadores.SelectedNode.Value.Split(';');
				GrabarDatoOperador(Convert.ToInt32(valores[0]), "Email", txtEmailComunicaciones.Text);
			}

			catch (Exception Exc)
			{
				if (Utilidad.EsEntornoDesarrollo())
				{
					lblResultado.Text = Exc.ToString();
				}
				else
				{
					if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
						lblResultado.Text = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
					else if (Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
						lblResultado.Text = Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
					else
						lblResultado.Text = Exc.Message.ToString() + "<br /><br />";
				}

				string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
				ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MostrarErrorPedidos", Escript, true);
			}
		}

		protected void lbGrabarEmailComunicacionesNOK_Click(object sender, EventArgs e)
		{
			txtEmailComunicaciones.Enabled = false;
			lbGrabarEmailComunicaciones.Visible = true;
			lbGrabarEmailComunicacionesNOK.Visible = false;
			lbGrabarEmailComunicacionesOK.Visible = false;

			string[] valores = tvOperadores.SelectedNode.Value.Split(';');
			CargarDatosOperador(Convert.ToInt32(valores[0]));
		}

		protected void lbGrabarTelefonoComunicaciones_Click(object sender, EventArgs e)
		{
			if (txtTelefonoComunicaciones.Text != string.Empty)
			{
				txtTelefonoComunicaciones.Enabled = true;
				lbGrabarTelefonoComunicaciones.Visible = false;
				lbGrabarTelefonoComunicacionesNOK.Visible = true;
				lbGrabarTelefonoComunicacionesOK.Visible = true;
			}
		}

		protected void lbGrabarTelefonoComunicacionesOK_Click(object sender, EventArgs e)
		{
			try
			{
				txtTelefonoComunicaciones.Enabled = false;
				lbGrabarTelefonoComunicaciones.Visible = true;
				lbGrabarTelefonoComunicacionesNOK.Visible = false;
				lbGrabarTelefonoComunicacionesOK.Visible = false;

				string[] valores = tvOperadores.SelectedNode.Value.Split(';');
				GrabarDatoOperador(Convert.ToInt32(valores[0]), "Telefono", txtTelefonoComunicaciones.Text);
			}

			catch (Exception Exc)
			{
				if (Utilidad.EsEntornoDesarrollo())
				{
					lblResultado.Text = Exc.ToString();
				}
				else
				{
					if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
						lblResultado.Text = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
					else if (Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
						lblResultado.Text = Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
					else
						lblResultado.Text = Exc.Message.ToString() + "<br /><br />";
				}

				string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
				ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MostrarErrorPedidos", Escript, true);
			}
		}

		protected void lbGrabarTelefonoComunicacionesNOK_Click(object sender, EventArgs e)
		{
			txtTelefonoComunicaciones.Enabled = false;
			lbGrabarTelefonoComunicaciones.Visible = true;
			lbGrabarTelefonoComunicacionesNOK.Visible = false;
			lbGrabarTelefonoComunicacionesOK.Visible = false;

			string[] valores = tvOperadores.SelectedNode.Value.Split(';');
			CargarDatosOperador(Convert.ToInt32(valores[0]));
		}

		protected void lbCambiarTipoOperador_Click(object sender, EventArgs e)
		{
			ddlTipoOperador.Enabled = true;
			lbCambiarTipoOperador.Visible = false;
			lbCambiarTipoOperadorNOK.Visible = true;
			lbCambiarTipoOperadorOK.Visible = true;
		}

		protected void lbCambiarTipoOperadorOK_Click(object sender, EventArgs e)
		{
			try
			{
				string[] valores = tvOperadores.SelectedNode.Value.Split(';');
				GrabarDatoOperador(Convert.ToInt32(valores[0]), "tipooperador", ddlTipoOperador.SelectedValue);

				ddlTipoOperador.Enabled = false;
				lbCambiarTipoOperador.Visible = true;
				lbCambiarTipoOperadorNOK.Visible = false;
				lbCambiarTipoOperadorOK.Visible = false;

				//Refrescamos los árboles. 
				CargarOperadores(tvOperadores, true);
				CargarOperadores(tvOperadoresDestino, false);
				//Cuando se refrescan los árboles hay que cerrar la ficha porque se pierde el foco del nodo seleccionado y ya no se puede operar con él.
				RestablecerFormularioNuevoOperador();
			}

			catch (Exception Exc)
			{
				if (Utilidad.EsEntornoDesarrollo())
				{
					lblResultado.Text = Exc.ToString();
				}
				else
				{
					if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
						lblResultado.Text = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
					else if (Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
						lblResultado.Text = Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
					else
						lblResultado.Text = Exc.Message.ToString() + "<br /><br />";
				}

				string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
				ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MostrarErrorPedidos", Escript, true);
			}
		}

		protected void lbCambiarTipoOperadorNOK_Click(object sender, EventArgs e)
		{
			ddlTipoOperador.Enabled = false;
			lbCambiarTipoOperador.Visible = true;
			lbCambiarTipoOperadorNOK.Visible = false;
			lbCambiarTipoOperadorOK.Visible = false;

			string[] valores = tvOperadores.SelectedNode.Value.Split(';');
			CargarDatosOperador(Convert.ToInt32(valores[0]));
		}

		protected void lbCambiarPadre_Click(object sender, EventArgs e)
		{
			if (ddlTipoOperador.SelectedIndex > 0)
			{
				ViewState["NodosAMover"] = null;

				string Escript = "$(document).ready(function() {$('#divAsignar').modal('show');});";
				ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MostrarAsignar", Escript, true);
			}
			else
			{
				lblResultado.Text = Resources.errores.ResourceManager.GetString("ERRNUM0039", Thread.CurrentThread.CurrentCulture) + "<br /><br />";
				string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
				ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MostrarErrorPedidos", Escript, true);
			}
		}

		protected void tvOperadores_SelectedNodeChanged(object sender, EventArgs e)
		{
			try
			{
				ViewState["NodosAMover"] = null;
				string[] valores = tvOperadores.SelectedNode.Value.Split(';');

				//Sólo cargamos el formulario si el nodo seleccionado no es el raíz (que no existe)
				if (Convert.ToInt32(valores[0]) > -1 && Convert.ToInt32(valores[1]) > 0)
				{
					divFicha.Visible = true;
					divInstrucciones.Visible = false;
					lbGrabarLogin.Visible = true;
					lbGrabarLoginOK.Visible = lbGrabarLoginNOK.Visible = false;
					lbGrabarContactoNombre.Visible = true;
					lbGrabarContactoNombreOK.Visible = lbGrabarContactoNombreNOK.Visible = false;
					lbGrabarEmailComunicaciones.Visible = true;
					lbGrabarEmailComunicacionesOK.Visible = lbGrabarEmailComunicacionesNOK.Visible = false;
					lbGrabarTelefonoComunicaciones.Visible = true;
					lbGrabarTelefonoComunicacionesOK.Visible = lbGrabarTelefonoComunicacionesNOK.Visible = false;
					lbCambiarTipoOperador.Visible = true;
					lbCambiarTipoOperadorOK.Visible = lbCambiarTipoOperadorNOK.Visible = false;
					lbCambiarPadre.Visible = false;
					lbGuardarPerfil.Visible = false;

					txtContactoApellidos.Enabled = txtContactoNombre.Enabled = txtEmailComunicaciones.Enabled = txtTelefonoComunicaciones.Enabled = txtLogin.Enabled = ddlTipoOperador.Enabled = false;

					CargarDatosOperador(Convert.ToInt32(valores[0]));
				}
			}

			catch (Exception Exc)
			{
				if (Utilidad.EsEntornoDesarrollo())
				{
					lblResultado.Text = Exc.ToString();
				}
				else
				{
					if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
						lblResultado.Text = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
					else if (Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
						lblResultado.Text = Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
					else
						lblResultado.Text = Exc.Message.ToString() + "<br /><br />";
				}

				string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
				ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MostrarErrorPedidos", Escript, true);
			}
		}

		protected void tvOperadoresDestino_SelectedNodeChanged(object sender, EventArgs e)
		{
			try
			{
				TreeView Arbol = (TreeView)sender;
				string[] valores = Arbol.SelectedNode.Value.Split(';');

				//Si este ViewState está vacío es que se está cambiado el padre o seleccionando el padre de un nodo nuevo.
				if (ViewState["NodosAMover"] == null)
				{
					if (valores[0] == "-1")
					{
						txtPadre.Text = string.Empty;
						ViewState.Add("NuevoIdPadre", Convert.ToInt32(valores[0]));
					}
					else //Si está relleno es que se está moviendo un nodo de padre.
					{
						if (Convert.ToInt32(Utilidad.DameDatoTipoOperador(Convert.ToInt32(ddlTipoOperador.SelectedValue), "Nivel")) > Convert.ToInt32(Utilidad.DameDatoTipoOperador(Convert.ToInt32(Utilidad.DameDatoOperador(Convert.ToInt32(valores[0]), "IdTipoOperador")), "Nivel")))
						{
							txtPadre.Text = Utilidad.DameDatoOperador(Convert.ToInt32(valores[0]), "LoginOperador");
							ViewState.Add("NuevoIdPadre", Convert.ToInt32(valores[0]));
						}
						else
						{
							txtPadre.Text = string.Empty;
							ViewState["NuevoIdPadre"] = null;
							throw new Exception("ERRNUM0037");
						}
					}
				}
				else
				{
					ArrayList Seleccionados = (ArrayList)ViewState["NodosAMover"];

					if (Seleccionados.Count > 0)
					{
						if (Convert.ToInt32(((ArrayList)Seleccionados[0])[1]) <= Convert.ToInt32(valores[1]))
							throw new Exception("ERRNUM0035");

						CambiarPadres(Seleccionados, valores);

						//Ocultamos el árbol de destino
						string Escript = "$(document).ready(function() {$('#divAsignar').modal('hide');});";
						ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "OcultarAsignar", Escript, true);

						//Refrescamos los árboles
						CargarOperadores(tvOperadores, true);
						CargarOperadores(tvOperadoresDestino, false);
						//Cuando se refrescan los árboles hay que cerrar la ficha porque se pierde el foco del nodo seleccionado y ya no se puede operar con él.
						RestablecerFormularioNuevoOperador();
					}
				}
			}

			catch (Exception Exc)
			{
				if (Utilidad.EsEntornoDesarrollo())
				{
					lblResultado.Text = Exc.ToString();
				}
				else
				{
					if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
						lblResultado.Text = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
					else if (Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
						lblResultado.Text = Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
					else
						lblResultado.Text = Exc.Message.ToString() + "<br /><br />";
				}

				string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
				ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MostrarErrorPedidos", Escript, true);
			}
		}

		protected void lbMover_Click(object sender, EventArgs e)
		{
			try
			{
				ArrayList Seleccionados = DameNodosSeleccionados(tvOperadores);

				if (Seleccionados.Count > 0)
				{
					ViewState.Add("NodosAMover", Seleccionados);

					if (TieneElementosDeDistintoNivel(Seleccionados))
						throw new Exception("ERRNUM0034");

					string Escript = "$(document).ready(function() {$('#divAsignar').modal('show');});";
					ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MostrarAsignar", Escript, true);
				}
			}

			catch (Exception Exc)
			{
				if (Utilidad.EsEntornoDesarrollo())
				{
					lblResultado.Text = Exc.ToString();
				}
				else
				{
					if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
						lblResultado.Text = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
						else if (Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
							lblResultado.Text = Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
							else
								lblResultado.Text = Exc.Message.ToString() + "<br /><br />";
				}

				string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
				ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MostrarErrorPedidos", Escript, true);
			}
		}

		protected void lbBorrar_Click(object sender, EventArgs e)
		{
			try
			{
				ArrayList Seleccionados = DameNodosSeleccionadosConHijos(tvOperadores);
				int Cuantos = Seleccionados.Count;

				if (Cuantos > 0)
				{
					ViewState.Add("NodosABorrar", Seleccionados);

					lblAvisoBorrar.Text = Resources.operators.ResourceManager.GetString("TextoConfirmacion", Thread.CurrentThread.CurrentCulture).Replace("[CUANTOS]", Cuantos.ToString());

					string Escript = "$(document).ready(function() {$('#divBorrar').modal('show');});";
					ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MostrarBorrar", Escript, true);
				}
			}

			catch (Exception Exc)
			{
				if (Utilidad.EsEntornoDesarrollo())
				{
					lblResultado.Text = Exc.ToString();
				}
				else
				{
					if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
						lblResultado.Text = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
					else if (Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
						lblResultado.Text = Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
					else
						lblResultado.Text = Exc.Message.ToString() + "<br /><br />";
				}

				string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
				ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MostrarErrorPedidos", Escript, true);
			}
		}

		protected void btnConfirmar_Click(object sender, EventArgs e)
		{
			try
			{
				ArrayList Seleccionados = (ArrayList)ViewState["NodosABorrar"];

				if (Seleccionados.Count > 0)
				{
					EliminarPerfiles(Seleccionados);

					//Ocultamos la capa
					string Escript = "$(document).ready(function() {$('#divBorrar').modal('hide');});";
					ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "OcultarBorrar", Escript, true);

					//Refrescamos los árboles
					CargarOperadores(tvOperadores, true);
					CargarOperadores(tvOperadoresDestino, false);
					//Cuando se refrescan los árboles hay que cerrar la ficha porque se pierde el foco del nodo seleccionado y ya no se puede operar con él.
					RestablecerFormularioNuevoOperador();
				}
			}

			catch (Exception Exc)
			{
				if (Utilidad.EsEntornoDesarrollo())
				{
					lblResultado.Text = Exc.ToString();
				}
				else
				{
					if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
						lblResultado.Text = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
					else if (Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
						lblResultado.Text = Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
					else
						lblResultado.Text = Exc.Message.ToString() + "<br /><br />";
				}

				string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
				ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MostrarErrorPedidos", Escript, true);
			}
		}

		protected void lbNuevoPerfil_Click(object sender, EventArgs e)
		{
			try
			{
				//Seleccionamos el raíz porque si se ha pulsado NUEVO PERFIL tras haber seleccionado un nodo, si luego se vuelve a pulsar ese nodo, no 
				//salta el evento NodeChanged del TreeView (porque ya está seleccionado) y no se cargan sus datos en el formulario. Con esto "limpiamos" 
				//la seleccionad cada vez que se pulsa el botón.
				tvOperadores.Nodes[0].Select();

				divFicha.Visible = true;
				divInstrucciones.Visible = false;
				lbGrabarLogin.Visible = false;
				lbGrabarLoginOK.Visible = lbGrabarLoginNOK.Visible = false;
				lbGrabarContactoNombre.Visible = false;
				lbGrabarContactoNombreOK.Visible = lbGrabarContactoNombreNOK.Visible = false;
				lbGrabarEmailComunicaciones.Visible = false;
				lbGrabarEmailComunicacionesOK.Visible = lbGrabarEmailComunicacionesNOK.Visible = false;
				lbGrabarTelefonoComunicaciones.Visible = false;
				lbGrabarTelefonoComunicacionesOK.Visible = lbGrabarTelefonoComunicacionesNOK.Visible = false;
				lbCambiarTipoOperador.Visible = false;
				lbCambiarTipoOperadorOK.Visible = lbCambiarTipoOperadorNOK.Visible = false;
				lbCambiarPadre.Visible = true;
				lbGuardarPerfil.Visible = true;

				txtContactoApellidos.Text = txtContactoNombre.Text = txtEmailComunicaciones.Text = txtTelefonoComunicaciones.Text = txtLogin.Text = txtPadre.Text = string.Empty;
				txtContactoApellidos.Enabled = txtContactoNombre.Enabled = txtEmailComunicaciones.Enabled = txtTelefonoComunicaciones.Enabled = txtLogin.Enabled = ddlTipoOperador.Enabled = true;
				ddlTipoOperador.ClearSelection();

				ViewState["NuevoIdPadre"] = null;
				ViewState["NodosAMover"] = null;
				ViewState["NodosABorrar"] = null;
			}

			catch (Exception Exc)
			{
				if (Utilidad.EsEntornoDesarrollo())
				{
					lblResultado.Text = Exc.ToString();
				}
				else
				{
					if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
						lblResultado.Text = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
					else if (Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
						lblResultado.Text = Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
					else
						lblResultado.Text = Exc.Message.ToString() + "<br /><br />";
				}

				string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
				ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MostrarErrorPedidos", Escript, true);
			}
		}

		protected void lbRestablecer_Click(object sender, EventArgs e)
		{
			RestablecerFormularioNuevoOperador();
		}

		protected void lbGuardarPerfil_Click(object sender, EventArgs e)
		{
			try
			{
				int IdPadre = Convert.ToInt32(ViewState["NuevoIdPadre"]);

				if (GrupoEsValido("NombreApellidos") && GrupoEsValido("Email") && GrupoEsValido("Telefono") && GrupoEsValido("Login") && GrupoEsValido("TipoOperador") && GrupoEsValido("PerfilPadre"))
				{
					GrabarNuevoOperador();

					//Refrescamos los árboles
					CargarOperadores(tvOperadores, true);
					CargarOperadores(tvOperadoresDestino, false);
					//Cuando se refrescan los árboles hay que cerrar la ficha porque se pierde el foco del nodo seleccionado y ya no se puede operar con él.
					RestablecerFormularioNuevoOperador();

					lblResultado.Text = Resources.operators.ResourceManager.GetString("LiteralNuevoOperadorCreado", Thread.CurrentThread.CurrentCulture);

					string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
					ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MostrarErrorPedidos", Escript, true);
				}
				else
				{
					reqLogin.Validate();
					reqContactoApellidos.Validate();
					reqContactoNombre.Validate();
					reqEmailComunicaciones.Validate();
					reqTelefonoComunicaciones.Validate();
					reqddlTipoOperador.Validate();
					regLogin.Validate();
					regEmailComunicaciones.Validate();
					regTelefonoComunicaciones.Validate();
				}
			}

			catch (Exception Exc)
			{
				if (Utilidad.EsEntornoDesarrollo())
				{
					lblResultado.Text = Exc.ToString();
				}
				else
				{
					if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
						lblResultado.Text = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
					else if (Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
						lblResultado.Text = Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
					else
						lblResultado.Text = Exc.Message.ToString() + "<br /><br />";
				}

				string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
				ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MostrarErrorPedidos", Escript, true);
			}
		}

		protected void ddlTipoOperador_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				//Si el botón de OK tras pulsar EDITAR es visible es que se está cambiando a un nodo de tipo. Si no está visible es que se está creando un nuevo perfil y no hemos de hacer caso del cambio de opción del desplegable.
				if (lbCambiarTipoOperadorOK.Visible == true)
				{
					string[] valores = tvOperadores.SelectedNode.Value.Split(';');

					//Si el nuevo tipo de perfil para el operador es igual o superior al de su padre, error.
					if (Utilidad.DameDatoTipoOperador(Convert.ToInt32(Utilidad.DameDatoOperador(Convert.ToInt32(valores[0]), "IdTipoOperadorSuperior")), "Nivel")!=string.Empty && Convert.ToInt32(Utilidad.DameDatoTipoOperador(Convert.ToInt32(ddlTipoOperador.SelectedValue), "Nivel")) <= Convert.ToInt32(Utilidad.DameDatoTipoOperador(Convert.ToInt32(Utilidad.DameDatoOperador(Convert.ToInt32(valores[0]), "IdTipoOperadorSuperior")), "Nivel")))
					{
						//Si el tipo de perfil seleccionado no es válido, reiniciamos el DDL.
						ListItem Elemento = ddlTipoOperador.Items.FindByValue(Utilidad.DameDatoOperador(Convert.ToInt32(valores[0]), "IdTipoOperador"));

						if (Elemento != null)
						{
							ddlTipoOperador.ClearSelection();
							Elemento.Selected = true;
						}

						throw new Exception("ERRNUM0038");
					}
				}
			}

			catch (Exception Exc)
			{
				if (Utilidad.EsEntornoDesarrollo())
				{
					lblResultado.Text = Exc.ToString();
				}
				else
				{
					if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
						lblResultado.Text = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
					else if (Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
						lblResultado.Text = Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
					else
						lblResultado.Text = Exc.Message.ToString() + "<br /><br />";
				}

				string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
				ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MostrarErrorPedidos", Escript, true);
			}
		}




		protected void CargarOperadores(TreeView Arbol,bool MostrarCheck)
		{
			int IdISP = Convert.ToInt32(Utilidad.DameDatoOperador(Entidad, "IdISPOperador"));
			int idCliente = Convert.ToInt32(User.Identity.Name);
			string IdPadre = String.Empty;
			SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ConnectionString);

			try
			{
				Con.Open();
				SqlCommand Com = Con.CreateCommand();
				Com.CommandText = "Operadores_DevolverOperadoresISP";
        Com.CommandTimeout = 300;
        Com.CommandType = CommandType.StoredProcedure;
				Com.Parameters.Add("@IdISP", SqlDbType.Int).Value = IdISP;
				SqlDataReader Rst = Com.ExecuteReader();

				Arbol.Nodes.Clear();

				Arbol.Nodes.Add(new TreeNode());
				Arbol.Nodes[0].Text = Resources.operators.ResourceManager.GetString("Perfiles", Thread.CurrentThread.CurrentCulture);
				Arbol.Nodes[0].Value = "-1;0";
				Arbol.Nodes[0].ShowCheckBox = false;

				while (Rst.Read())
				{
          //El perfil de Análisis sólo se ve si se tiene permiso
          if (Rst["Siglas"].ToString().ToUpper() != "AN" || (Rst["Siglas"].ToString().ToUpper() == "AN" && Permisos.TienePermiso("GESTIONATIPOOPERADORANALISIS", Entidad)))
          {
            if (IdPadre == string.Empty)
              IdPadre = Rst["IdSupervisor"].ToString();

            TreeNode Tn = new TreeNode();
            Tn.Text = Rst["LoginOperador"].ToString() + " (" + Rst["Siglas"].ToString() + ")";
            Tn.ToolTip = Rst["ApellidosOperador"].ToString() + ", " + Rst["NombreOperador"] + " - (" + Rst["TipoOperador"].ToString() + ")";
            Tn.Value = Rst["IdOperador"].ToString() + ";" + Rst["Nivel"].ToString();
            Tn.ShowCheckBox = MostrarCheck;

            if (Rst["IdSupervisor"].ToString() != IdPadre)
            {
              TreeNode Padre = DameNodoPorId(Arbol.Nodes[0], Rst["IdSupervisor"].ToString());
              Padre.ChildNodes.Add(Tn);
            }
            else
            {
              Arbol.Nodes[0].ChildNodes.Add(Tn);
            }
          }
				}

				if (Arbol.Nodes.Count > 0)
				{
					Arbol.CollapseAll();
					Arbol.Nodes[0].Expand();
					Arbol.ExpandAll();
				}

				Rst.Close();
				Com.Dispose();
			}

			finally
			{
				if (Con.State == ConnectionState.Open)
					Con.Close();
				Con.Dispose();
			}
		}

		protected TreeNode DameNodoPorId(TreeNode Nodo, string Id)
		{
			string[] valores = Nodo.Value.Split(';');

			if (valores[0].ToString() == Id)
				return Nodo;

			for (int i = 0; i < Nodo.ChildNodes.Count; i++)
			{
				valores = Nodo.ChildNodes[i].Value.Split(';');

				if (valores[0].ToString() == Id)
					return Nodo.ChildNodes[i];

				TreeNode RetNodo = DameNodoPorId(Nodo.ChildNodes[i], Id);

				if (RetNodo != null)
					return RetNodo;
			}

			return null;
		}

		protected void CargarDatosOperador(int IdOperador)
		{
			txtLogin.Text = Utilidad.DameDatoOperador(IdOperador, "LoginOperador");
			txtContactoNombre.Text = HttpUtility.HtmlDecode(Utilidad.DameDatoOperador(IdOperador, "NombreOperador"));
			txtContactoApellidos.Text = HttpUtility.HtmlDecode(Utilidad.DameDatoOperador(IdOperador, "ApellidosOperador"));
			txtEmailComunicaciones.Text = Utilidad.DameDatoOperador(IdOperador, "EmailOperador");
			txtTelefonoComunicaciones.Text = Utilidad.DameDatoOperador(IdOperador, "TelefonoOperador");
			ListItem Elemento = ddlTipoOperador.Items.FindByValue(Utilidad.DameDatoOperador(IdOperador, "IdTipoOperador"));
			if (Utilidad.DameDatoOperador(IdOperador, "SiglasSuperior") != "SA")
				txtPadre.Text = Utilidad.DameDatoOperador(IdOperador, "LoginSuperior");
			else
				txtPadre.Text = string.Empty;


			if (Elemento != null)
			{
				ddlTipoOperador.ClearSelection();
				Elemento.Selected = true;
			}
		}

		protected ArrayList DameNodosSeleccionados(TreeView Arbol)
		{
			int Cuantos = Arbol.CheckedNodes.Count;
			ArrayList Lista = new ArrayList();
			ArrayList Dupla = new ArrayList();

			if (Cuantos > 0)
			{
				for (int i = 0; i < Cuantos; i++)
				{
					string[] valores = Arbol.CheckedNodes[i].Value.Split(';');
					Dupla = new ArrayList();
					Dupla.Add(Convert.ToInt32(valores[0]));
					Dupla.Add(Convert.ToInt32(valores[1]));
					Lista.Add(Dupla);
				}
			}

			return Lista;
		}

		protected ArrayList DameNodosSeleccionadosConHijos(TreeView Arbol)
		{
			int Cuantos = Arbol.CheckedNodes.Count;
			ArrayList Lista = new ArrayList();
			ArrayList Dupla = new ArrayList();
			string[] valores = null;

			if (Cuantos > 0)
			{
				for (int i = 0; i < Cuantos; i++)
				{
					valores = Arbol.CheckedNodes[i].Value.Split(';');
					Dupla = new ArrayList();
					Dupla.Add(Convert.ToInt32(valores[0]));
					Dupla.Add(Convert.ToInt32(valores[1]));

					if (!ExisteEnLista(Lista, Dupla))
						Lista.Add(Dupla);

					if(Arbol.CheckedNodes[i].ChildNodes.Count>0)
					{
						TreeNode Nodo = BorrarCategorias(Arbol.CheckedNodes[i], ref Lista);
					}
				}
			}

			return Lista;
		}

		protected TreeNode BorrarCategorias(TreeNode Nodo, ref ArrayList Lista)
		{
			TreeNode RetNodo = null;
			// Recorremos los nodos hijo del seleccionado para eliminarlos también
			for (int i = 0; i < Nodo.ChildNodes.Count; i++)
			{
				RetNodo = BorrarCategorias(Nodo.ChildNodes[i], ref Lista);

				if (RetNodo != null)
				{
					
					string[] valores = RetNodo.Value.Split(';');
					ArrayList Dupla = new ArrayList();
					Dupla.Add(Convert.ToInt32(valores[0]));
					Dupla.Add(Convert.ToInt32(valores[1]));

					//Sólo lo añadimos si no está ya en la lista
					if (!ExisteEnLista(Lista,Dupla))
						Lista.Add(Dupla);
				}
			}

			return Nodo;
		}

		protected bool TieneElementosDeDistintoNivel(ArrayList Lista)
		{
			bool Tiene = false;
			int NivelTemp = Convert.ToInt32(((ArrayList)Lista[0])[1]);


			for(int i=0; i<Lista.Count; i++)
			{
				if (NivelTemp != Convert.ToInt32(((ArrayList)Lista[i])[1]))
					Tiene = true;
			}

			return Tiene;
		}

		protected void CambiarPadres(ArrayList Seleccionados,string[] Padre)
		{
			SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ConnectionString);
			Con.Open();
			SqlTransaction Trans = null;
			Trans = Con.BeginTransaction();

			try
			{
				SqlCommand Com = Con.CreateCommand();
				Com.Transaction = Trans;
				Com.CommandType = CommandType.StoredProcedure;
				Com.CommandText = "Operadores_CambiarPadreOperador";
        Com.CommandTimeout = 300;

        for (int i=0; i<Seleccionados.Count;i++)
				{
					Com.Parameters.Clear();
					Com.Parameters.Add("@IdOperadorPadre", SqlDbType.Int).Value = Convert.ToInt32(Padre[0]);
					Com.Parameters.Add("@IdOperadorHijo", SqlDbType.Int).Value = Convert.ToInt32(((ArrayList)Seleccionados[i])[0]);
					Com.ExecuteNonQuery();
				}

				Trans.Commit();

				Com.Dispose();
			}

			catch
			{
				if (Trans != null)
					Trans.Rollback();

				throw;
			}

			finally
			{
				if (Con.State == ConnectionState.Open)
					Con.Close();

				Trans.Dispose();
				Con.Dispose();
			}
		}

		protected void EliminarPerfiles(ArrayList Seleccionados)
		{
			SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ConnectionString);
			Con.Open();
			SqlTransaction Trans = null;
			Trans = Con.BeginTransaction();

			try
			{
				SqlCommand Com = Con.CreateCommand();
				Com.Transaction = Trans;
				Com.CommandType = CommandType.StoredProcedure;
				Com.CommandText = "Operadores_EliminarOperador";
        Com.CommandTimeout = 300;

        for (int i = 0; i < Seleccionados.Count; i++)
				{
					Com.Parameters.Clear();
					Com.Parameters.Add("@IdOperador", SqlDbType.Int).Value = Convert.ToInt32(((ArrayList)Seleccionados[i])[0]);
					Com.Parameters.Add("@IdISP", SqlDbType.Int).Value = Convert.ToInt32(Utilidad.DameDatoOperador(Entidad, "IdISPOperador"));
					Com.ExecuteNonQuery();
				}

				Trans.Commit();
				Com.Dispose();
			}

			catch
			{
				if (Trans != null)
					Trans.Rollback();

				throw;
			}

			finally
			{
				if (Con.State == ConnectionState.Open)
					Con.Close();

				Trans.Dispose();
				Con.Dispose();
			}
		}

		protected bool ExisteEnLista(ArrayList Lista,ArrayList Dupla)
		{
			bool Existe = false;

			for(int i=0; i<Lista.Count;i++)
			{
				if (Convert.ToInt32(((ArrayList)Lista[i])[0]) == Convert.ToInt32(Dupla[0]))
					Existe = true;
			}

			return Existe;
		}

		protected void GrabarDatoOperador(int IdOperador, string Campo, string Valor)
		{
			SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
			SqlCommand Com = Con.CreateCommand();

			try
			{
				Con.Open();
				Com.CommandType = System.Data.CommandType.StoredProcedure;
				Com.CommandText = "Operadores_ActualizarDato";
        Com.CommandTimeout = 300;
        Com.Parameters.Add("@IdOperador", SqlDbType.Int).Value = IdOperador;
				Com.Parameters.Add("@Campo", SqlDbType.VarChar, 500).Value = Campo;
				Com.Parameters.Add("@Valor", SqlDbType.VarChar, 500).Value = Valor;

				Com.ExecuteNonQuery();

				Utilidad.EscribeLOG(Entidad, "Operador", "Solicitud de modificación de dato del perfil del operador (Operador: " + Entidad.ToString() + ", Dato: " + Campo + ", Valor: " + Valor + ") desde gestión de perfiles (CALLCENTER)");
			}

			finally
			{
				if (Con.State == ConnectionState.Open)
					Con.Close();

				Com.Dispose();
				Con.Dispose();
			}
		}

		protected void GrabarNuevoOperador()
		{
			SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
			SqlCommand Com = Con.CreateCommand();

			try
			{
				Con.Open();
				Com.CommandType = System.Data.CommandType.StoredProcedure;
				Com.CommandText = "Operadores_CrearOperador";
        Com.CommandTimeout = 300;
        Com.Parameters.Add("@IdISP", SqlDbType.Int).Value = Convert.ToInt32(Utilidad.DameDatoOperador(Entidad, "IdISPOperador"));
				Com.Parameters.Add("@IdTipoOperador", SqlDbType.Int).Value = Convert.ToInt32(ddlTipoOperador.SelectedValue);

				if(ViewState["NuevoIdPadre"] !=null && ViewState["NuevoIdPadre"].ToString() !=string.Empty && Convert.ToInt32(ViewState["NuevoIdPadre"]) !=-1)
					Com.Parameters.Add("@IdSupervisor", SqlDbType.Int).Value = Convert.ToInt32(ViewState["NuevoIdPadre"]);

				Com.Parameters.Add("@Login", SqlDbType.VarChar,500).Value = txtLogin.Text;
				Com.Parameters.Add("@Nombre", SqlDbType.VarChar,500).Value = txtContactoNombre.Text;
				Com.Parameters.Add("@Apellidos", SqlDbType.VarChar,500).Value = txtContactoApellidos.Text;
				Com.Parameters.Add("@Email", SqlDbType.VarChar,500).Value = txtEmailComunicaciones.Text;
				Com.Parameters.Add("@Telefono", SqlDbType.VarChar,25).Value = txtTelefonoComunicaciones.Text;

				Com.ExecuteNonQuery();

				Utilidad.EscribeLOG(Entidad, "Operador", "Solicitud de creación de nuevo operador (Login: " + txtLogin.ToString() + ") desde gestión de perfiles (CALLCENTER)");
			}

			finally
			{
				if (Con.State == ConnectionState.Open)
					Con.Close();

				Com.Dispose();
				Con.Dispose();
			}
		}

		protected bool GrupoEsValido(string NombreGrupo)
		{
			foreach (BaseValidator Validador in Page.Validators)
			{
				if (Validador.ValidationGroup == NombreGrupo)
				{
					bool EsValido = Validador.IsValid;
					if (EsValido)
					{
						Validador.Validate();
						EsValido = Validador.IsValid;
						Validador.IsValid = true;
					}

					if (!EsValido)
						return false;
				}
			}

			return true;
		}

		protected void CargarDesplegableTiposOperadores()
		{
			SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
			SqlCommand Com = Con.CreateCommand();

			try
			{
				Con.Open();
				Com.CommandType = System.Data.CommandType.StoredProcedure;
				Com.CommandText = "Operadores_DevolverOperadoresTipos";
        Com.CommandTimeout = 300;

        ddlTipoOperador.Items.Clear();

				ListItem Elemento = new ListItem();
				Elemento.Value = "-1";
				Elemento.Text = Resources.operators.ResourceManager.GetString("LiteralSeleccionar", Thread.CurrentThread.CurrentCulture);
				ddlTipoOperador.Items.Add(Elemento);

				SqlDataReader Rst = Com.ExecuteReader();

        while (Rst.Read())
        {
          if (Rst["Siglas"].ToString().ToUpper() != "AN" || (Rst["Siglas"].ToString().ToUpper() == "AN" && Permisos.TienePermiso("GESTIONATIPOOPERADORANALISIS", Entidad)))
          { 
            Elemento = new ListItem();
            Elemento.Value = Rst["IdTipoOperador"].ToString();
            Elemento.Text = Rst["TipoOperador"].ToString();
            ddlTipoOperador.Items.Add(Elemento);
          }
				}
			}

			finally
			{
				if (Con.State == ConnectionState.Open)
					Con.Close();

				Com.Dispose();
				Con.Dispose();
			}
		}

		protected void RestablecerFormularioNuevoOperador()
		{
			//Seleccionamos el raíz porque si se ha pulsado NUEVO PERFIL tras haber seleccionado un nodo, si luego se vuelve a pulsar ese nodo, no 
			//salta el evento NodeChanged del TreeView (porque ya está seleccionado) y no se cargan sus datos en el formulario. Con esto "limpiamos" 
			//la seleccionad cada vez que se pulsa el botón.
			tvOperadores.Nodes[0].Select();

			divFicha.Visible = false;
			divInstrucciones.Visible = true;
			lbGrabarLogin.Visible = true;
			lbGrabarLoginOK.Visible = lbGrabarLoginNOK.Visible = false;
			lbGrabarContactoNombre.Visible = true;
			lbGrabarContactoNombreOK.Visible = lbGrabarContactoNombreNOK.Visible = false;
			lbGrabarEmailComunicaciones.Visible = true;
			lbGrabarEmailComunicacionesOK.Visible = lbGrabarEmailComunicacionesNOK.Visible = false;
			lbGrabarTelefonoComunicaciones.Visible = true;
			lbGrabarTelefonoComunicacionesOK.Visible = lbGrabarTelefonoComunicacionesNOK.Visible = false;
			lbCambiarTipoOperador.Visible = true;
			lbCambiarTipoOperadorOK.Visible = lbCambiarTipoOperadorNOK.Visible = false;
			lbGuardarPerfil.Visible = false;
			lbCambiarPadre.Visible = false;

			txtContactoApellidos.Text = txtContactoNombre.Text = txtEmailComunicaciones.Text = txtTelefonoComunicaciones.Text = txtLogin.Text = txtPadre.Text = string.Empty;
			txtContactoApellidos.Enabled = txtContactoNombre.Enabled = txtEmailComunicaciones.Enabled = txtTelefonoComunicaciones.Enabled = txtLogin.Enabled = ddlTipoOperador.Enabled = false;
			ddlTipoOperador.ClearSelection();

			ViewState["NuevoIdPadre"] = null;
			ViewState["NodosAMover"] = null;
			ViewState["NodosABorrar"] = null;
		}
  }
}