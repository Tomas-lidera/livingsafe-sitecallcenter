﻿using System;
using System.Web;
using LIVINGSAFE_SiteCallCenter.App_Code;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Threading;
using System.Web.UI;

namespace LIVINGSAFE_SiteCallCenter.res
{
  public partial class profile : System.Web.UI.Page
  {
    cUtil Utilidad = new cUtil();
    int Entidad = Convert.ToInt32(HttpContext.Current.User.Identity.Name);

    protected void Page_Load(object sender, EventArgs e)
    {
      try
      {
        if (!IsPostBack)
          CargarDatosOperador();
      }

      catch (Exception Exc)
      {
        string Msg = string.Empty;

        if (Resources.errores.ResourceManager.GetString("ERRNUM0001", Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.errores.ResourceManager.GetString("ERRNUM0001", Thread.CurrentThread.CurrentCulture);
        else
          Msg = Exc.Message.ToString();


        if (Utilidad.EsEntornoDesarrollo())
          lblMensaje.Text = Msg + "<br /><br />" + Exc.ToString();
        else
          lblMensaje.Text = Msg;

        string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "MostrarErrorProfile", Escript, true);
      }
    }

    protected void lbCambiarPassword_Click(object sender, EventArgs e)
    {
      try
      {
        cSesiones Sesion = new cSesiones();
        string EMail = Sesion.SolicitarCambioPassword(lblIdUsuario.Text, Convert.ToInt32(Utilidad.DameDatoOperador(Entidad, "IdISPOperador")));

        lblMensaje.Text = Resources.profile.ResourceManager.GetString("MensajeCambioPassword", Thread.CurrentThread.CurrentCulture);

        string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MostrarCambioPassword", Escript, true);

        Utilidad.EscribeLOG(Entidad, "Operador", "Solicitud de cambio de contraseña del operador (Operador: " + Entidad.ToString() + ") (CALLCENTER)");
      }

      catch (Exception Exc)
      {
        string Msg = string.Empty;

        if (Resources.errores.ResourceManager.GetString("ERRNUM0001", Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.errores.ResourceManager.GetString("ERRNUM0001", Thread.CurrentThread.CurrentCulture);
        else
          Msg = Exc.Message.ToString();


        if (Utilidad.EsEntornoDesarrollo())
          lblMensaje.Text = Msg + "<br /><br />" + Exc.ToString();
        else
          lblMensaje.Text = Msg;

        string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "MostrarErrorProfile", Escript, true);
      }
    }


    #region Nombre y Apellidos

    protected void lbGrabarContactoNombre_Click(object sender, EventArgs e)
    {
      txtContactoNombre.Enabled = true;
      txtContactoApellidos.Enabled = true;
      lbGrabarContactoNombre.Visible = false;
      lbGrabarContactoNombreNOK.Visible = true;
      lbGrabarContactoNombreOK.Visible = true;
    }

    protected void lbGrabarContactoNombreOK_Click(object sender, EventArgs e)
    {
      txtContactoNombre.Enabled = false;
      txtContactoApellidos.Enabled = false;
      lbGrabarContactoNombre.Visible = true;
      lbGrabarContactoNombreNOK.Visible = false;
      lbGrabarContactoNombreOK.Visible = false;

      GrabarDatoOperador(Entidad, "Nombre", txtContactoNombre.Text);
      GrabarDatoOperador(Entidad, "Apellidos", txtContactoApellidos.Text);
    }

    protected void lbGrabarContactoNombreNOK_Click(object sender, EventArgs e)
    {
      txtContactoNombre.Enabled = false;
      txtContactoApellidos.Enabled = false;
      lbGrabarContactoNombre.Visible = true;
      lbGrabarContactoNombreNOK.Visible = false;
      lbGrabarContactoNombreOK.Visible = false;
      CargarDatosOperador();
    }

    #endregion


    #region Email Comunicaciones

    protected void lbGrabarEmailComunicaciones_Click(object sender, EventArgs e)
    {
      txtEmailComunicaciones.Enabled = true;
      lbGrabarEmailComunicaciones.Visible = false;
      lbGrabarEmailComunicacionesNOK.Visible = true;
      lbGrabarEmailComunicacionesOK.Visible = true;
    }

    protected void lbGrabarEmailComunicacionesOK_Click(object sender, EventArgs e)
    {
      txtEmailComunicaciones.Enabled = false;
      lbGrabarEmailComunicaciones.Visible = true;
      lbGrabarEmailComunicacionesNOK.Visible = false;
      lbGrabarEmailComunicacionesOK.Visible = false;

      GrabarDatoOperador(Entidad, "Email", txtEmailComunicaciones.Text);
    }

    protected void lbGrabarEmailComunicacionesNOK_Click(object sender, EventArgs e)
    {
      txtEmailComunicaciones.Enabled = false;
      lbGrabarEmailComunicaciones.Visible = true;
      lbGrabarEmailComunicacionesNOK.Visible = false;
      lbGrabarEmailComunicacionesOK.Visible = false;
      CargarDatosOperador();
    }

    #endregion


    #region Telefono Comunicaciones

    protected void lbGrabarTelefonoComunicaciones_Click(object sender, EventArgs e)
    {
      txtTelefonoComunicaciones.Enabled = true;
      lbGrabarTelefonoComunicaciones.Visible = false;
      lbGrabarTelefonoComunicacionesNOK.Visible = true;
      lbGrabarTelefonoComunicacionesOK.Visible = true;
    }

    protected void lbGrabarTelefonoComunicacionesOK_Click(object sender, EventArgs e)
    {
      txtTelefonoComunicaciones.Enabled = false;
      lbGrabarTelefonoComunicaciones.Visible = true;
      lbGrabarTelefonoComunicacionesNOK.Visible = false;
      lbGrabarTelefonoComunicacionesOK.Visible = false;

      GrabarDatoOperador(Entidad, "Telefono", txtTelefonoComunicaciones.Text);
    }

    protected void lbGrabarTelefonoComunicacionesNOK_Click(object sender, EventArgs e)
    {
      txtTelefonoComunicaciones.Enabled = false;
      lbGrabarTelefonoComunicaciones.Visible = true;
      lbGrabarTelefonoComunicacionesNOK.Visible = false;
      lbGrabarTelefonoComunicacionesOK.Visible = false;
      CargarDatosOperador();
    }

    #endregion


    protected void CargarDatosOperador()
    {
      lblIdUsuario.Text = Utilidad.DameDatoOperador(Entidad,"LoginOperador");
      txtContactoNombre.Text = HttpUtility.HtmlDecode(Utilidad.DameDatoOperador(Entidad, "NombreOperador"));
      txtContactoApellidos.Text = HttpUtility.HtmlDecode(Utilidad.DameDatoOperador(Entidad, "ApellidosOperador"));

      if (Utilidad.DameDatoOperador(Entidad, "NombreISP") != null && Utilidad.DameDatoOperador(Entidad, "NombreISP") !=string.Empty)
        txtEmpresa.Text = HttpUtility.HtmlDecode(Utilidad.DameDatoOperador(Entidad, "NombreISP"));

      txtEmailComunicaciones.Text = Utilidad.DameDatoOperador(Entidad, "EmailOperador");
      txtTelefonoComunicaciones.Text = Utilidad.DameDatoOperador(Entidad, "TelefonoOperador");
    }

    protected void GrabarDatoOperador(int IdOperador, string Campo, string Valor)
    {
      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = Con.CreateCommand();

      try
      {
        Con.Open();
        Com.CommandType = System.Data.CommandType.StoredProcedure;
        Com.CommandText = "Operadores_ActualizarDato";
        Com.CommandTimeout = 300;
        Com.Parameters.Add("@IdOperador", SqlDbType.Int).Value = IdOperador;
        Com.Parameters.Add("@Campo", SqlDbType.VarChar,500).Value = Campo;
        Com.Parameters.Add("@Valor", SqlDbType.VarChar,500).Value = Valor;

        Com.ExecuteNonQuery();

        Utilidad.EscribeLOG(Entidad, "Operador", "Solicitud de modificación de dato del perfil del operador (Operador: " + Entidad.ToString() + ", Dato: " + Campo + ", Valor: " + Valor + ") (CALLCENTER)");
      }

      catch (Exception Exc)
      {
        string Msg = string.Empty;

        if (Resources.errores.ResourceManager.GetString(Exc.Message, Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.errores.ResourceManager.GetString(Exc.Message, Thread.CurrentThread.CurrentCulture);
        else
          Msg = Exc.Message.ToString();


        if (Utilidad.EsEntornoDesarrollo())
          lblMensaje.Text = Msg + "<br /><br />" + Exc.ToString();
        else
          lblMensaje.Text = Msg;

        string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "MostrarErrorProfile", Escript, true);
      }

      finally
      {
        if (Con.State == ConnectionState.Open)
          Con.Close();

        Com.Dispose();
        Con.Dispose();
      }
    }
 
  }
}