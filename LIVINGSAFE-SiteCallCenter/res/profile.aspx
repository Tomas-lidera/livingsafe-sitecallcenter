﻿<%@ Page Title="" Language="C#" MasterPageFile="~/res/LIVINGSAFE.Master" AutoEventWireup="true" CodeBehind="profile.aspx.cs" Inherits="LIVINGSAFE_SiteCallCenter.res.profile" %>
<asp:Content ID="cProfileHead" ContentPlaceHolderID="cpHead" runat="server">
</asp:Content>
<asp:Content ID="cProfileBody" ContentPlaceHolderID="cpBody" runat="server">
  <asp:ScriptManager ID="smPerfil" runat="server"></asp:ScriptManager>

  <asp:UpdatePanel runat="server" ID="upPerfil">
    <Triggers>
      <asp:PostBackTrigger ControlID="lbCambiarPassword" />
      <asp:PostBackTrigger ControlID="lbGrabarContactoNombreOK" />
    </Triggers>
    <ContentTemplate>
      <%//Este código tiene que ir dentro del UpdatePanel porque si no la máscara no se aplica %>
      <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(function () {
          $(document).ready(function () {
            var SPMaskBehavior = function (val) {
              return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
            },
            spOptions = {
              onKeyPress: function (val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
              }
            };

            $('#<%=txtTelefonoComunicaciones.ClientID%>').mask(SPMaskBehavior, spOptions);
          });
        });
      </script>
      <div class="container portada_servicios">
	      <div class="row">
          <div class="col-md-12 txt_titulopagina" style="padding-left:10px; margin-bottom:15px;">
     	      <asp:Literal id="litRecientes" Text='<%$ Resources:profile,TituloMiPerfil %>' runat="server" EnableViewState="false" />
          </div>

          <div class="col-12" style="padding-bottom:20px;">
              <asp:Literal id="litTextoMiPerfil01" Text='<%$ Resources:profile,TextoMiPerfil01 %>' runat="server" EnableViewState="false" />
          </div>
     
          <div class="col-md-12" style="margin-bottom:40px;">
            <%-- ID Usuario --%>
            <b><asp:Literal id="litIDUsuario" Text='<%$ Resources:profile,IDUsuario %>' runat="server" EnableViewState="false" /></b>
          
            <asp:Label ID="lblIdUsuario" runat="server" placeholder='<%$ Resources:profile,PlaceholderId %>' 
            CssClass="textbox_editar_perfil" style="margin-left:10px; margin-right:30px; padding-left:5px; padding-right:5px;"></asp:Label>

            <asp:LinkButton ID="lbCambiarPassword" runat="server" Text='<%$ Resources:profile,BotonCambiarPassword %>' CssClass="lnk_passchange"
            OnClick="lbCambiarPassword_Click" CausesValidation="false"></asp:LinkButton>
          </div> 


          <div class="col-md-6" style="margin-bottom:20px;">
            <div class="row">
              <div class="col-12 txt_perfiltit1"><asp:Literal id="litDatosContacto" Text='<%$ Resources:profile,TituloDatosContacto %>' runat="server" EnableViewState="false" /></div>

              <%-- Persona de contacto --%>
              <div class="col-12 txt_perfilsubtit">
                <asp:Label id="lblPersonaContacto" Text='<%$ Resources:profile,PersonaContacto %>' runat="server" EnableViewState="false" />
              </div>
              <div class="col-9 text-left" style="margin-top:5px;">
                <asp:TextBox ID="txtContactoNombre" runat="server" placeholder='<%$ Resources:profile,PlaceholderNombre %>' BackColor="White" Enabled="false" CssClass="textbox_editar_perfil" ValidationGroup="NombreApellidos"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ID="reqContactoNombre" Text='<%$ Resources:profile,Obligatorio %>' ControlToValidate="txtContactoNombre" ForeColor="Red" Display="Dynamic" ValidationGroup="NombreApellidos"></asp:RequiredFieldValidator>
              </div>
              <div class="col-9 text-left" style="margin-top:5px;">
                <asp:TextBox ID="txtContactoApellidos" runat="server" placeholder='<%$ Resources:profile,PlaceholderApellidos %>' BackColor="White" Enabled="false" CssClass="textbox_editar_perfil" ValidationGroup="NombreApellidos"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ID="reqContactoApellidos" Text='<%$ Resources:profile,Obligatorio %>' ControlToValidate="txtContactoApellidos" ForeColor="Red" Display="Dynamic" ValidationGroup="NombreApellidos"></asp:RequiredFieldValidator>
              </div>
              <div class="col-3 text-left">
                <asp:LinkButton ID="lbGrabarContactoNombre"     runat="server" CssClass="lnk_editar" Text='<%$ Resources:profile,BotonEditar %>'  OnClick="lbGrabarContactoNombre_Click"    Visible="true"  CausesValidation="false"></asp:LinkButton>
                <asp:LinkButton ID="lbGrabarContactoNombreOK"   runat="server" CssClass="lnk_editar" Text='<%$ Resources:profile,BotonOK %>'      OnClick="lbGrabarContactoNombreOK_Click"  Visible="false" ValidationGroup="NombreApellidos"></asp:LinkButton>
                <asp:LinkButton ID="lbGrabarContactoNombreNOK"  runat="server" CssClass="lnk_editar" Text='<%$ Resources:profile,BotonNOK %>'     OnClick="lbGrabarContactoNombreNOK_Click" Visible="false" CausesValidation="false"></asp:LinkButton>
              </div>      

              <%-- Empresa --%>
              <div class="col-12 txt_perfilsubtit">
                <asp:Label id="lblEmpresa" Text='<%$ Resources:profile,Empresa %>' runat="server" EnableViewState="false" />
              </div>

              <div class="col-9 text-left" style="margin-top:5px;">
                <asp:TextBox ID="txtEmpresa" runat="server" placeholder='<%$ Resources:profile,PlaceholderEmpresa %>' BackColor="White" Enabled="false" CssClass="textbox_editar_perfil"></asp:TextBox>
              </div>
            </div>
          </div>
      
      
          <div class="col-md-6" style="margin-bottom:20px;">
            <div class="row">
              <div class="col-12 txt_perfiltit2"><asp:Literal id="litComunicaciones" Text='<%$ Resources:profile,TituloComunicaciones %>' runat="server" EnableViewState="false" /></div>

              <%-- E-mail comunicaciones --%>
              <div class="col-12 txt_perfilsubtit">
                <asp:Literal id="litEmailComunicaciones" Text='<%$ Resources:profile,EmailComunicaciones %>' runat="server" EnableViewState="false" />
              </div>

              <div class="col-9 text-left" style="margin-top:5px;">
                <asp:TextBox ID="txtEmailComunicaciones" runat="server" placeholder='<%$ Resources:profile,PlaceholderEmail %>' BackColor="White" Enabled="false" CssClass="textbox_editar_perfil" ValidationGroup="Email"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ID="reqEmailComunicaciones" Text='<%$ Resources:profile,Obligatorio %>' ControlToValidate="txtEmailComunicaciones" ForeColor="Red" Display="Dynamic" ValidationGroup="Email"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator runat="server" ID="regEmailComunicaciones" ControlToValidate="txtEmailComunicaciones" ErrorMessage='<%$ Resources:profile,RegexMail %>' ForeColor="Red" ValidationExpression="^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)+$" Display="Dynamic" ValidationGroup="Email"></asp:RegularExpressionValidator>
                                                                                                                                                                                                                             
              </div>

              <div class="col-3 text-left">
                <asp:LinkButton ID="lbGrabarEmailComunicaciones"     runat="server" CssClass="lnk_editar" Text='<%$ Resources:profile,BotonEditar %>'  OnClick="lbGrabarEmailComunicaciones_Click"    Visible="true"  CausesValidation="false"></asp:LinkButton>
                <asp:LinkButton ID="lbGrabarEmailComunicacionesOK"   runat="server" CssClass="lnk_editar" Text='<%$ Resources:profile,BotonOK %>'      OnClick="lbGrabarEmailComunicacionesOK_Click"  Visible="false" ValidationGroup="Email"></asp:LinkButton>
                <asp:LinkButton ID="lbGrabarEmailComunicacionesNOK"  runat="server" CssClass="lnk_editar" Text='<%$ Resources:profile,BotonNOK %>'     OnClick="lbGrabarEmailComunicacionesNOK_Click" Visible="false" CausesValidation="false"></asp:LinkButton>
              </div>

              <%-- Teléfono comunicaciones --%>
              <div class="col-12 txt_perfilsubtit">
                <asp:Literal id="litTelefonoComunicaciones" Text='<%$ Resources:profile,TelefonoComunicaciones %>' runat="server" EnableViewState="false" />
              </div>

              <div class="col-9 text-left" style="margin-top:5px;">
                <asp:TextBox ID="txtTelefonoComunicaciones" runat="server" placeholder='<%$ Resources:profile,PlaceholderTelefono %>' BackColor="White" Enabled="false" CssClass="textbox_editar_perfil" ValidationGroup="Telefono"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ID="reqTelefonoComunicaciones" ControlToValidate="txtTelefonoComunicaciones" Text='<%$ Resources:profile,Obligatorio %>' ForeColor="Red" Display="Dynamic" ValidationGroup="Telefono"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator runat="server" ID="regTelefonoComunicaciones" ControlToValidate="txtTelefonoComunicaciones" ErrorMessage='<%$ Resources:profile,RegexMovil %>' ForeColor="Red" ValidationExpression='<%$ Resources:profile,ValidatorTelefonoBR %>' Display="Dynamic" ValidationGroup="Telefono"></asp:RegularExpressionValidator>
              </div>

              <div class="col-3 text-left">
                <asp:LinkButton ID="lbGrabarTelefonoComunicaciones"     runat="server" CssClass="lnk_editar" Text='<%$ Resources:profile,BotonEditar %>'  OnClick="lbGrabarTelefonoComunicaciones_Click"    Visible="true"  CausesValidation="false"></asp:LinkButton>
                <asp:LinkButton ID="lbGrabarTelefonoComunicacionesOK"   runat="server" CssClass="lnk_editar" Text='<%$ Resources:profile,BotonOK %>'      OnClick="lbGrabarTelefonoComunicacionesOK_Click"  Visible="false" ValidationGroup="Telefono"></asp:LinkButton>
                <asp:LinkButton ID="lbGrabarTelefonoComunicacionesNOK"  runat="server" CssClass="lnk_editar" Text='<%$ Resources:profile,BotonNOK %>'     OnClick="lbGrabarTelefonoComunicacionesNOK_Click" Visible="false" CausesValidation="false"></asp:LinkButton>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div id="divAlerta" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md">
          <div class="modal-content">
            <div class="modal-body">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div class="modal-body">
              <asp:Label ID="lblMensaje" runat="server" />
            </div>
          </div>
        </div>
      </div>
    </ContentTemplate>
  </asp:UpdatePanel>
</asp:Content>
