﻿using LIVINGSAFE_Billing;
using LIVINGSAFE_SiteCallCenter.App_Code;
using LIVINGSAFE_SiteCallCenter.Clases;
using OfficeOpenXml;
using System;
using System.Collections;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace LIVINGSAFE_SiteCallCenter.res
{
  public partial class billing : System.Web.UI.Page
  {
    cUtil Utilidad = new cUtil();
    cPermisos Permisos = new cPermisos();
    cIdiomas Idioma = new cIdiomas();
    cEncriptacion Encriptacion = new cEncriptacion();
    int Entidad = Convert.ToInt32(HttpContext.Current.User.Identity.Name);

    protected void Page_Load(object sender, EventArgs e)
    {
      try
      {
        int IdISPOperador = Convert.ToInt32(Utilidad.DameDatoOperador(Entidad, "IdISPOperador"));
        string Siglas = Utilidad.DameDatoISP(IdISPOperador, "Siglas");

        if (!Permisos.TienePermiso("PAGINADASHBOARD", Entidad) || Siglas.ToUpper() != "LIVINGSAFE")
          Response.Redirect("~/res/landing.aspx");

        CargarCapasProgreso();

        if(!IsPostBack)
          Resumen();
      }

      catch (Exception Exc)
      {
        string Msg = string.Empty;

        if (Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);
        else if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
            Msg = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);
          else
          {
            if (Utilidad.EsEntornoDesarrollo())
              Msg = Exc.ToString();
            else
              Msg = Exc.Message.ToString();
          }

        lblResultado.Text = Msg;

        string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "MostrarErrorModif", Escript, true);
      }
    }

    protected void lbPaso1Seleccionar_Click(object sender, EventArgs e)
    {
      string Tipo = ((LinkButton)sender).CommandArgument.ToString().Trim().ToUpper();

      ViewState["Tipo"] = null;
      ViewState.Add("Tipo", Tipo);

      switch(ViewState["Tipo"].ToString())
      {
        case "ISP":
          lblPaso2Entidad.Text = Resources.billing.ResourceManager.GetString("Paso1EntidadISP", Thread.CurrentThread.CurrentCulture);
          break;

        case "PARTNER":
          lblPaso2Entidad.Text = Resources.billing.ResourceManager.GetString("Paso1EntidadPartner", Thread.CurrentThread.CurrentCulture);
          break;

        case "PROVEEDOR":
          lblPaso2Entidad.Text = Resources.billing.ResourceManager.GetString("Paso1EntidadProveedor", Thread.CurrentThread.CurrentCulture);
          break;
      }

      sqlDatosEntidad.SelectParameters["Tipo"].DefaultValue = ViewState["Tipo"].ToString();
      lvEntidad.DataBind();

      Resumen();
      Reiniciar(2);
    }

    protected void chkPaso2EntidadSeleccionarTodos_CheckedChanged(object sender, EventArgs e)
    {
      bool Seleccionado = ((CheckBox)sender).Checked;

      for (int i = 0; i < lvEntidad.Items.Count; i++)
        ((HtmlInputCheckBox)lvEntidad.Items[i].FindControl("chkEntidad")).Checked = Seleccionado;
    }

    protected void lbPaso2Siguiente_Click(object sender, EventArgs e)
    {
      try
      {
        ArrayList EntidadesSeleccionadas = new ArrayList();

        for (int i = 0; i < lvEntidad.Items.Count; i++)
        {
          if (((HtmlInputCheckBox)lvEntidad.Items[i].FindControl("chkEntidad")).Checked)
          {
            EntidadesSeleccionadas.Add(Convert.ToInt32(((HtmlInputCheckBox)lvEntidad.Items[i].FindControl("chkEntidad")).Value));
          }
        }

        if (EntidadesSeleccionadas.Count == 0)
          throw new Exception("BILLERRNUM0001");

        ViewState.Add("EntidadesSeleccionadas", EntidadesSeleccionadas);

        CargarDesplegablesFechasPaso3Year();
        CargarDesplegablesFechasPaso3Month(null);

        Resumen();
        Reiniciar(3);
      }

      catch (Exception Exc)
      {
        string Msg = string.Empty;

        if (Resources.billing.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.billing.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);
          else if (Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
            Msg = Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);
            else if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
              Msg = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);
            else
            {
              if (Utilidad.EsEntornoDesarrollo())
                Msg = Exc.ToString();
              else
                Msg = Exc.Message.ToString();
            }

        lblResultado.Text = Msg;

        string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "MostrarErrorModif", Escript, true);
      }
    }

    protected void ddlPaso3Year_SelectedIndexChanged(object sender, EventArgs e)
    {
      CargarDesplegablesFechasPaso3Month(Convert.ToInt32(ddlPaso3Year.SelectedValue));
      ddlPaso3Month.Focus();
    }

    protected void lbPaso3Siguiente_Click(object sender, EventArgs e)
    {
      try
      {
        if (ddlPaso3Year.SelectedIndex == 0 || ddlPaso3Month.SelectedIndex == 0)
          throw new Exception("BILLERRNUM0002");

        DateTime Dia = Convert.ToDateTime(DateTime.Now.Year.ToString() + "-" + (DateTime.Now.Month).ToString() + "-01 00:00:00");
        Dia = Dia.AddMonths(1).AddDays(-1);

        if (Convert.ToInt32(ddlPaso3Year.SelectedValue) == DateTime.Now.Year && Convert.ToInt32(ddlPaso3Month.SelectedValue) == DateTime.Now.Month && DateTime.Now.Day < Dia.Day)
          throw new Exception("BILLERRNUM0004");

        Dia = Convert.ToDateTime(ddlPaso3Year.SelectedValue + "-" + ddlPaso3Month.SelectedValue + "-01 00:00:00");

        if(Dia>DateTime.Now)
          throw new Exception("BILLERRNUM0005");

        ViewState.Add("InformeYear", ddlPaso3Year.SelectedValue);
        ViewState.Add("InformeMonth", ddlPaso3Month.SelectedValue);

        Resumen();
        Reiniciar(4);
      }

      catch (Exception Exc)
      {
        string Msg = string.Empty;

        if (Resources.billing.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.billing.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);
        else if (Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);
        else if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);
        else
        {
          if (Utilidad.EsEntornoDesarrollo())
            Msg = Exc.ToString();
          else
            Msg = Exc.Message.ToString();
        }

        lblResultado.Text = Msg;

        string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "MostrarErrorModif", Escript, true);
      }
    }

    protected void lbPaso4Generar_Click(object sender, EventArgs e)
    {
      try
      {
        lbPaso4Generar.Enabled = false;

        if (txtPaso4NombreFichero.Text==string.Empty)
          throw new Exception("BILLERRNUM0003");

        ViewState.Add("NombreFichero", txtPaso4NombreFichero.Text);

        Guid G = Guid.NewGuid();

        ViewState.Add("NombreFicheroReal", G.ToString());

        Resumen();
        Reiniciar(5);

        cBilling Facturacion = new cBilling();

        ArrayList EntidadesSeleccionadas = (ArrayList)(ViewState["EntidadesSeleccionadas"]);
        DateTime Fecha = Convert.ToDateTime(ViewState["InformeYear"].ToString() + "-" + (Convert.ToInt32(ViewState["InformeMonth"])).ToString("D2") + "-01 00:00:00").AddDays(-1);
        Fecha = Fecha.AddMonths(1);
        switch (ViewState["Tipo"].ToString())
        {
          case "ISP":
            Facturacion.Facturar_ISP(EntidadesSeleccionadas, Fecha, txtPaso4NombreFichero.Text, ViewState["NombreFicheroReal"].ToString());
            break;

          case "PARTNER":
            Facturacion.Facturar_Partner(EntidadesSeleccionadas, Fecha, txtPaso4NombreFichero.Text, ViewState["NombreFicheroReal"].ToString());
            break;

          case "PROVEEDOR":
            Facturacion.Facturar_Proveedor(EntidadesSeleccionadas, Fecha, txtPaso4NombreFichero.Text, ViewState["NombreFicheroReal"].ToString());
            break;
        }

        lbPaso5Descargar.Enabled = true;
      }

      catch (Exception Exc)
      {
        lbPaso5Descargar.Enabled = false;

        string Msg = string.Empty;

        if (Resources.billing.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.billing.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);
        else if (Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);
        else if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);
        else
        {
          if (Utilidad.EsEntornoDesarrollo())
            Msg = Exc.ToString();
          else
            Msg = Exc.Message.ToString();
        }

        lblResultado.Text = Msg;

        string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "MostrarErrorModif", Escript, true);
      }
    }

    protected void lbPaso5Descargar_Click(object sender, EventArgs e)
    {
      try
      {
        cImpersonate Impersonar = new cImpersonate();

        string UsuarioImpersonalizacion = ConfigurationManager.AppSettings["usuarioPublicar"].ToString();
        string DominioImpersonalizacion = ConfigurationManager.AppSettings["usuarioDominio"].ToString();
        string PassImpersonalizacion = ConfigurationManager.AppSettings["usuarioPass"].ToString();
        Impersonar.impersonateValidUser(UsuarioImpersonalizacion, DominioImpersonalizacion, PassImpersonalizacion);

        //Añadimos en este punto una rutina que borre ficheros antiguos para que no se nos llene la carpeta de basura
        //BorrarFicherosAntiguos();

        string NombreFicheroReal = ViewState["NombreFicheroReal"].ToString();
        string NombreFichero = ViewState["NombreFichero"].ToString();
        
        string RutaReal = ConfigurationManager.AppSettings["RutaFicherosInformes"].ToString() + "\\" + NombreFicheroReal + ".xlsx";

        Response.Clear();
        Response.ClearHeaders();
        Response.Buffer = false;
        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        Response.AppendHeader("Content-Disposition", "Attachment; filename=\"" + NombreFichero + ".xlsx\"");
        Response.AppendHeader("Content-Length", File.ReadAllBytes(RutaReal).Length.ToString());
        Response.TransmitFile(RutaReal);
        Response.Flush();

        Impersonar.undoImpersonation();

        Response.End();
      }

      catch (Exception Exc)
      {
        string Msg = string.Empty;

        if (Resources.billing.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.billing.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);
        else if (Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);
        else if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);
        else
        {
          if (Utilidad.EsEntornoDesarrollo())
            Msg = Exc.ToString();
          else
            Msg = Exc.Message.ToString();
        }

        lblResultado.Text = Msg;

        string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "MostrarErrorModif", Escript, true);
      }
    }

    protected void lbPaso1_Click(object sender, EventArgs e)
    {
      if (ViewState["Tipo"] != null)
        Reiniciar(1);
    }

    protected void lbPaso2_Click(object sender, EventArgs e)
    {
      if (ViewState["EntidadesSeleccionadas"] != null)
        Reiniciar(2);
    }

    protected void lbPaso3_Click(object sender, EventArgs e)
    {
      if (ViewState["InformeYear"] != null && ViewState["InformeMonth"] != null)
        Reiniciar(3);
    }

    protected void lbPaso4_Click(object sender, EventArgs e)
    {
      Reiniciar(4);
    }



    protected void Resumen()
    {
      lblPaso1.Text = lblPaso2.Text = lblPaso3.Text = "?";

      lbPaso1.Attributes["class"] = "col-md-3 text-center capa_billingactivo";
      lbPaso2.Attributes["class"] = "col-md-3 text-center capa_billingmutedint";
      lbPaso3.Attributes["class"] = "col-md-3 text-center capa_billingmutedint";
      lbPaso4.Attributes["class"] = "col-md-3 text-center capa_billingmuted";

      //PASO 1. Tipo
      if (ViewState["Tipo"] != null)
      {
        lblPaso1.Text = ViewState["Tipo"].ToString();

        lbPaso1.Attributes["class"] = "col-md-3 text-center capa_billingactivo";
        lbPaso2.Attributes["class"] = "col-md-3 text-center capa_billingmutedint";
        lbPaso3.Attributes["class"] = "col-md-3 text-center capa_billingmutedint";
        lbPaso4.Attributes["class"] = "col-md-3 text-center capa_billingmuted";
      }

      //PASO 2. Entidades
      if (ViewState["EntidadesSeleccionadas"] != null)
      {
        lblPaso2.Text = ViewState["Tipo"].ToString().ToUpper() + "=";

        ArrayList EntidadesSeleccionadas = (ArrayList)(ViewState["EntidadesSeleccionadas"]);

        //for (int i = 0; i < EntidadesSeleccionadas.Count; i++)
        //{
        //  if (i > 0)
        //    lblPaso2.Text += ',';

        //  lblPaso2.Text += EntidadesSeleccionadas[i].ToString();
        //}
        lblPaso2.Text = EntidadesSeleccionadas.Count.ToString();

        lbPaso1.Attributes["class"] = "col-md-3 text-center capa_billingazulint";
        lbPaso2.Attributes["class"] = "col-md-3 text-center capa_billingactivo";
        lbPaso3.Attributes["class"] = "col-md-3 text-center capa_billingmutedint";
        lbPaso4.Attributes["class"] = "col-md-3 text-center capa_billingmuted";
      }

      //PASO 3. Fecha
      if (ViewState["InformeYear"] != null && ViewState["InformeMonth"] != null)
      {
        lblPaso3.Text = ViewState["InformeYear"].ToString() + "/" + Convert.ToInt32(ViewState["InformeMonth"]).ToString("D2");

        lbPaso1.Attributes["class"] = "col-md-3 text-center capa_billingazulint";
        lbPaso2.Attributes["class"] = "col-md-3 text-center capa_billingazulint";
        lbPaso3.Attributes["class"] = "col-md-3 text-center capa_billingactivo";
        lbPaso4.Attributes["class"] = "col-md-3 text-center capa_billingmuted";
      }
    }

    protected void Reiniciar(int Paso)
    {
      divPaso1.Visible = false;
      divPaso2.Visible = false;
      divPaso3.Visible = false;
      divPaso4.Visible = false;
      divPaso5.Visible = false;
      lbPaso4Generar.Enabled = true;

      //Si Paso 0 o 1 es un reinicio completo
      if (Paso == 0 || Paso == 1)
      {
        lblPaso1.Text = lblPaso2.Text = lblPaso3.Text = "?";

        ViewState["Tipo"] = null;
        ViewState["EntidadesSeleccionadas"] = null;

        for (int i = 0; i < lvEntidad.Items.Count; i++)
          ((HtmlInputCheckBox)lvEntidad.Items[i].FindControl("chkEntidad")).Checked = false;

        chkPaso2EntidadSeleccionarTodos.Checked = false;

        ddlPaso3Year.Items.Clear();
        ddlPaso3Month.Items.Clear();

        ViewState["InformeYear"] = null;
        ViewState["InformeMonth"] = null;

        txtPaso4NombreFichero.Text = string.Empty;

        divPaso1.Visible = true;

        lbPaso1.Attributes["class"] = "col-md-3 text-center capa_billingactivo";
        lbPaso2.Attributes["class"] = "col-md-3 text-center capa_billingmutedint";
        lbPaso3.Attributes["class"] = "col-md-3 text-center capa_billingmutedint";
        lbPaso4.Attributes["class"] = "col-md-3 text-center capa_billingmuted";
      }

      if (Paso == 2)
      {
        lblPaso2.Text = lblPaso3.Text = "?";

        ViewState["EntidadesSeleccionadas"] = null;

        ddlPaso3Year.Items.Clear();
        ddlPaso3Month.Items.Clear();

        ViewState["InformeYear"] = null;
        ViewState["InformeMonth"] = null;

        txtPaso4NombreFichero.Text = string.Empty;

        divPaso2.Visible = true;

        lbPaso1.Attributes["class"] = "col-md-3 text-center capa_billingazulint";
        lbPaso2.Attributes["class"] = "col-md-3 text-center capa_billingactivo";
        lbPaso3.Attributes["class"] = "col-md-3 text-center capa_billingmutedint";
        lbPaso4.Attributes["class"] = "col-md-3 text-center capa_billingmuted";
      }

      if (Paso == 3)
      {
        lblPaso3.Text = "?";

        //ddlPaso3Year.Items.Clear();
        //ddlPaso3Month.Items.Clear();

        //CargarDesplegablesFechasPaso3Year();
        //CargarDesplegablesFechasPaso3Month(null);

        ViewState["InformeYear"] = null;
        ViewState["InformeMonth"] = null;

        txtPaso4NombreFichero.Text = string.Empty;

        divPaso3.Visible = true;

        lbPaso1.Attributes["class"] = "col-md-3 text-center capa_billingazulint";
        lbPaso2.Attributes["class"] = "col-md-3 text-center capa_billingazulint";
        lbPaso3.Attributes["class"] = "col-md-3 text-center capa_billingactivo";
        lbPaso4.Attributes["class"] = "col-md-3 text-center capa_billingmuted";
      }

      if (Paso == 4)
      {
        divPaso4.Visible = true;

        lbPaso1.Attributes["class"] = "col-md-3 text-center capa_billingazulint";
        lbPaso2.Attributes["class"] = "col-md-3 text-center capa_billingazulint";
        lbPaso3.Attributes["class"] = "col-md-3 text-center capa_billingazulint";
        lbPaso4.Attributes["class"] = "col-md-3 text-center capa_billingactivo";
      }

      if (Paso == 5)
      {
        divPaso5.Visible = true;

        lbPaso1.Attributes["class"] = "col-md-3 text-center capa_billingazulint";
        lbPaso2.Attributes["class"] = "col-md-3 text-center capa_billingazulint";
        lbPaso3.Attributes["class"] = "col-md-3 text-center capa_billingazulint";
        lbPaso4.Attributes["class"] = "col-md-3 text-center capa_billingactivo";
      }
    }

    protected void CargarDesplegablesFechasPaso3Year()
    {
      ddlPaso3Year.Items.Clear();

      ListItem Elemento = new ListItem(Resources.billing.ResourceManager.GetString("Paso3FechaLiteralYearSeleccion",Thread.CurrentThread.CurrentCulture), "-1");
      ddlPaso3Year.Items.Add(Elemento);

      //La primera foto del histórico es del 28/03/2019
      for (int i=2019;i<=DateTime.Now.Year;i++)
      {
        Elemento = new ListItem(i.ToString(),i.ToString());
        ddlPaso3Year.Items.Add(Elemento);
      }
    }

    protected void CargarDesplegablesFechasPaso3Month(int? Ano)
    {
      ddlPaso3Month.Items.Clear();
      int MesInicio = 3;

      ListItem Elemento = new ListItem(Resources.billing.ResourceManager.GetString("Paso3FechaLiteralMonthSeleccion", Thread.CurrentThread.CurrentCulture), "-1");
      ddlPaso3Month.Items.Add(Elemento);

      //La primera foto del histórico es del 28/03/2019, así que si nos pasan el año y es 2019 sólo cargamos los mese de Marzo a Diciembre, para el resto de años cargamos los 12 meses.
      if (Ano == 2019)
        MesInicio = 3;
      else
        MesInicio = 1;

      for (int i = MesInicio; i <= 12; i++)
      {
        string Mes = new DateTime(2010, i, 1).ToString("MMMM", Thread.CurrentThread.CurrentCulture);
        Elemento = new ListItem(Mes, i.ToString());
        ddlPaso3Month.Items.Add(Elemento);
      }
    }

    protected void CargarCapasProgreso()
    {
      this.lbPaso1.Attributes.Add("onclick", Page.ClientScript.GetPostBackEventReference(this.lbPaso1, string.Empty));
      if (IsPostBack && Request["__EVENTTARGET"] == lbPaso1.UniqueID)
      {
        lbPaso1_Click(lbPaso1, EventArgs.Empty);
      }

      this.lbPaso2.Attributes.Add("onclick", Page.ClientScript.GetPostBackEventReference(this.lbPaso2, string.Empty));
      if (IsPostBack && Request["__EVENTTARGET"] == lbPaso2.UniqueID)
      {
        lbPaso2_Click(lbPaso2, EventArgs.Empty);
      }

      this.lbPaso3.Attributes.Add("onclick", Page.ClientScript.GetPostBackEventReference(this.lbPaso3, string.Empty));
      if (IsPostBack && Request["__EVENTTARGET"] == lbPaso3.UniqueID)
      {
        lbPaso3_Click(lbPaso3, EventArgs.Empty);
      }

      this.lbPaso4.Attributes.Add("onclick", Page.ClientScript.GetPostBackEventReference(this.lbPaso4, string.Empty));
      if (IsPostBack && Request["__EVENTTARGET"] == lbPaso4.UniqueID)
      {
        lbPaso4_Click(lbPaso4, EventArgs.Empty);
      }
    }

    protected void BorrarFicherosAntiguos()
    {
      string[] files = Directory.GetFiles(ConfigurationManager.AppSettings["RutaFicherosInformes"].ToString());

      foreach (string file in files)
      {
        FileInfo fi = new FileInfo(file);
        if (fi.CreationTime < DateTime.Now.AddDays(-1))
          fi.Delete();
      }
    }
  }
}