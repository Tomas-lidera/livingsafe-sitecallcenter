﻿<%@ Page Title="" Language="C#" MasterPageFile="~/res/LIVINGSAFE.Master" AutoEventWireup="true" CodeBehind="operators.aspx.cs" Inherits="LIVINGSAFE_SiteCallCenter.res.operators" %>
<asp:Content ID="cOperadoresHead" ContentPlaceHolderID="cpHead" runat="server"></asp:Content>
<asp:Content ID="cOperadoresBody" ContentPlaceHolderID="cpBody" runat="server">
	<asp:ScriptManager ID="smOperators" runat="server"></asp:ScriptManager>
	<div class="container-fluid section-contenido">
		<div class="txt_titulopagina_med"><asp:Literal ID="litTitulo" runat="server" EnableViewState="false" Text='<%$ Resources:operators,LiteralTitulo %>'></asp:Literal></div>
	</div>
	<div class="container">
    <div class="row espacio_inferior">
			<asp:UpdatePanel runat="server" ID="upArbol" class="col-4">
				<Triggers>
					<asp:AsyncPostBackTrigger ControlID="lbBorrar" />
				</Triggers>
	      <ContentTemplate>
				<script>
					var prm = Sys.WebForms.PageRequestManager.getInstance();
					prm.add_pageLoaded(function () {
						$(document).ready(function () {
							var SPMaskBehavior = function (val) {
								return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
							},
							spOptions = {
								onKeyPress: function (val, e, field, options) {
									field.mask(SPMaskBehavior.apply({}, arguments), options);
								}
							};

							$('#<%=txtTelefonoComunicaciones.ClientID%>').mask(SPMaskBehavior, spOptions);
						});
					});
				</script>



					<div style="display:inline-block; width:100%; height:442px; border:1px solid #AAAAAA; float:left;">
						<asp:TreeView runat="server" ID="tvOperadores" CssClass="estilotextoarbol" ShowLines="true"
						OnSelectedNodeChanged="tvOperadores_SelectedNodeChanged"
						style="width:100%; height:440px; overflow-y:scroll; float:left; margin:0px;"></asp:TreeView>
					</div>
					<div style="display:inline-block; float:left;">
						<asp:LinkButton ID="lbMover"  runat="server" Text='<%$ Resources:operators,BotonMoverPerfiles %>' OnClick="lbMover_Click" CssClass="lnk_editar_peq aire10_sup_inf" CausesValidation="false"></asp:LinkButton>	
						<asp:LinkButton ID="lbBorrar" runat="server" Text='<%$ Resources:operators,BotonBorrarPerfiles %>' OnClick="lbBorrar_Click" CssClass="lnk_editar_peq aire10_sup_inf" CausesValidation="false" style="margin-left:5px;"></asp:LinkButton>	
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>

			<asp:UpdatePanel runat="server" ID="upFicha" class="col-8">
				<Triggers>
				</Triggers>
	      <ContentTemplate>
					<div class="col-12 aire10_sup_inf text-right" runat="server" id="divBotonera">
						<asp:LinkButton ID="lbNuevoPerfil" runat="server" Text='<%$ Resources:operators,BotonNuevoPerfil %>' CssClass="lnk_passchange" OnClick="lbNuevoPerfil_Click" CausesValidation="false"></asp:LinkButton>
						<asp:LinkButton ID="lbRestablecer" runat="server" Text="X" CssClass="lnk_passchange" OnClick="lbRestablecer_Click" CausesValidation="false"></asp:LinkButton>
					</div>
					<div runat="server" id="divInstrucciones" class="row" style="background-color:#EEEEEE; width:100%; height:200px; border-radius:13px;padding-left:20px; padding-right:20px;">
						<asp:Label runat="server" ID="lblInstrucciones" Text='<%$ Resources:operators,LiteralInstrucciones %>' style="margin-top: auto;margin-bottom: auto;"></asp:Label>
					</div>
					<div runat="server" id="divFicha" class="row" visible="false">
						<div class="col-md-6">
							<div class="row">
								<div class="col-8 aire10_sup_inf">
									<asp:TextBox ID="txtLogin" runat="server" placeholder='<%$ Resources:operators,PlaceholderLogin %>' ToolTip='<%$ Resources:operators,PlaceholderLogin %>' BackColor="White" Enabled="false" CssClass="textbox_editar_perfil_peq" ValidationGroup="Login" AutoCompleteType="None" autocomplete="off" />
									<asp:RequiredFieldValidator runat="server" ID="reqLogin" Text="*" ToolTip='<%$ Resources:operators,Obligatorio %>' ControlToValidate="txtLogin" CssClass="val_error" Display="Dynamic" ValidationGroup="Login"></asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator runat="server" ID="regLogin" ControlToValidate="txtLogin" ErrorMessage='<%$ Resources:operators,RegexLogin %>' CssClass="val_error" ValidationExpression="^[a-zA-Z0-9çÇ._-]+$" Display="Dynamic" ValidationGroup="Login"></asp:RegularExpressionValidator>
								</div>
								<div class="col-4 aire10_sup_inf">
									<asp:LinkButton ID="lbGrabarLogin"     runat="server" CssClass="lnk_editar_peq" Text='<%$ Resources:operators,BotonEditar %>'  OnClick="lbGrabarLogin_Click"    Visible="true"  CausesValidation="false"></asp:LinkButton>
									<asp:LinkButton ID="lbGrabarLoginOK"   runat="server" CssClass="lnk_editar_peq" Text='<%$ Resources:operators,BotonOK %>'      OnClick="lbGrabarLoginOK_Click"  Visible="false" ValidationGroup="NombreApellidos"></asp:LinkButton>
									<asp:LinkButton ID="lbGrabarLoginNOK"  runat="server" CssClass="lnk_editar_peq" Text='<%$ Resources:operators,BotonNOK %>'     OnClick="lbGrabarLoginNOK_Click" Visible="false" CausesValidation="false"></asp:LinkButton>
								</div>
								<div class="col-8 aire10_sup_inf">
									<asp:TextBox ID="txtContactoNombre" runat="server" placeholder='<%$ Resources:operators,PlaceholderNombre %>' ToolTip='<%$ Resources:operators,PlaceholderNombre %>' BackColor="White" Enabled="false" CssClass="textbox_editar_perfil_peq" ValidationGroup="NombreApellidos" AutoCompleteType="None" autocomplete="off" />
									<asp:RequiredFieldValidator runat="server" ID="reqContactoNombre" Text="*" ToolTip='<%$ Resources:operators,Obligatorio %>' ControlToValidate="txtContactoNombre" CssClass="val_error" Display="Dynamic" ValidationGroup="NombreApellidos"></asp:RequiredFieldValidator>
								</div>
								<div class="col-8 aire10_sup_inf">
									<asp:TextBox ID="txtContactoApellidos" runat="server" placeholder='<%$ Resources:operators,PlaceholderApellidos %>'  ToolTip='<%$ Resources:operators,PlaceholderApellidos %>' BackColor="White" Enabled="false" CssClass="textbox_editar_perfil_peq" ValidationGroup="NombreApellidos" AutoCompleteType="None" autocomplete="off" />
									<asp:RequiredFieldValidator runat="server" ID="reqContactoApellidos" Text="*" ToolTip='<%$ Resources:operators,Obligatorio %>' ControlToValidate="txtContactoApellidos" CssClass="val_error" Display="Dynamic" ValidationGroup="NombreApellidos"></asp:RequiredFieldValidator>
								</div>
								<div class="col-4 aire10_sup_inf">
									<asp:LinkButton ID="lbGrabarContactoNombre"     runat="server" CssClass="lnk_editar_peq" Text='<%$ Resources:operators,BotonEditar %>'  OnClick="lbGrabarContactoNombre_Click"    Visible="true"  CausesValidation="false"></asp:LinkButton>
									<asp:LinkButton ID="lbGrabarContactoNombreOK"   runat="server" CssClass="lnk_editar_peq" Text='<%$ Resources:operators,BotonOK %>'      OnClick="lbGrabarContactoNombreOK_Click"  Visible="false" ValidationGroup="NombreApellidos"></asp:LinkButton>
									<asp:LinkButton ID="lbGrabarContactoNombreNOK"  runat="server" CssClass="lnk_editar_peq" Text='<%$ Resources:operators,BotonNOK %>'     OnClick="lbGrabarContactoNombreNOK_Click" Visible="false" CausesValidation="false"></asp:LinkButton>
								</div>
							</div>
						</div>

						<div class="col-md-6">
							<div class="row">
								<div class="col-8 aire10_sup_inf">
									<asp:TextBox ID="txtEmpresa" runat="server" placeholder='<%$ Resources:operators,PlaceholderEmpresa %>' ToolTip='<%$ Resources:operators,PlaceholderEmpresa %>' BackColor="White" Enabled="false" CssClass="textbox_editar_perfil_peq" AutoCompleteType="None" autocomplete="off" />
								</div>

								<div class="col-8 aire10_sup_inf">
									<asp:TextBox ID="txtEmailComunicaciones" runat="server" placeholder='<%$ Resources:operators,PlaceholderEmail %>' ToolTip='<%$ Resources:operators,PlaceholderEmail %>' BackColor="White" Enabled="false" CssClass="textbox_editar_perfil_peq" ValidationGroup="Email" AutoCompleteType="None" autocomplete="off" />
									<asp:RequiredFieldValidator runat="server" ID="reqEmailComunicaciones" Text="*" ToolTip='<%$ Resources:operators,Obligatorio %>' ControlToValidate="txtEmailComunicaciones" CssClass="val_error" Display="Dynamic" ValidationGroup="Email"></asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator runat="server" ID="regEmailComunicaciones" ControlToValidate="txtEmailComunicaciones" ErrorMessage='<%$ Resources:operators,RegexMail %>' CssClass="val_error" ValidationExpression="^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)+$" Display="Dynamic" ValidationGroup="Email"></asp:RegularExpressionValidator>
								</div>
								<div class="col-4 aire10_sup_inf">
									<asp:LinkButton ID="lbGrabarEmailComunicaciones"     runat="server" CssClass="lnk_editar_peq" Text='<%$ Resources:operators,BotonEditar %>'  OnClick="lbGrabarEmailComunicaciones_Click"    Visible="true"  CausesValidation="false"></asp:LinkButton>
									<asp:LinkButton ID="lbGrabarEmailComunicacionesOK"   runat="server" CssClass="lnk_editar_peq" Text='<%$ Resources:operators,BotonOK %>'      OnClick="lbGrabarEmailComunicacionesOK_Click"  Visible="false" ValidationGroup="Email"></asp:LinkButton>
									<asp:LinkButton ID="lbGrabarEmailComunicacionesNOK"  runat="server" CssClass="lnk_editar_peq" Text='<%$ Resources:operators,BotonNOK %>'     OnClick="lbGrabarEmailComunicacionesNOK_Click" Visible="false" CausesValidation="false"></asp:LinkButton>
								</div>
								<div class="col-8 aire10_sup_inf">
									<asp:TextBox ID="txtTelefonoComunicaciones" runat="server" placeholder='<%$ Resources:operators,PlaceholderTelefono %>' ToolTip='<%$ Resources:operators,PlaceholderTelefono %>' BackColor="White" Enabled="false" CssClass="textbox_editar_perfil_peq" ValidationGroup="Telefono" AutoCompleteType="None" autocomplete="off" />
									<asp:RequiredFieldValidator runat="server" ID="reqTelefonoComunicaciones" ControlToValidate="txtTelefonoComunicaciones" Text="*" ToolTip='<%$ Resources:operators,Obligatorio %>' CssClass="val_error" Display="Dynamic" ValidationGroup="Telefono"></asp:RequiredFieldValidator>
									<asp:RegularExpressionValidator runat="server" ID="regTelefonoComunicaciones" ControlToValidate="txtTelefonoComunicaciones" ErrorMessage='<%$ Resources:operators,RegexMovil %>' CssClass="val_error" ValidationExpression='<%$ Resources:operators,ValidatorTelefonoBR %>' Display="Dynamic" ValidationGroup="Telefono"></asp:RegularExpressionValidator>
								</div>
								<div class="col-4 aire10_sup_inf">
									<asp:LinkButton ID="lbGrabarTelefonoComunicaciones"     runat="server" CssClass="lnk_editar_peq" Text='<%$ Resources:operators,BotonEditar %>'  OnClick="lbGrabarTelefonoComunicaciones_Click"    Visible="true"  CausesValidation="false"></asp:LinkButton>
									<asp:LinkButton ID="lbGrabarTelefonoComunicacionesOK"   runat="server" CssClass="lnk_editar_peq" Text='<%$ Resources:operators,BotonOK %>'      OnClick="lbGrabarTelefonoComunicacionesOK_Click"  Visible="false" ValidationGroup="Telefono"></asp:LinkButton>
									<asp:LinkButton ID="lbGrabarTelefonoComunicacionesNOK"  runat="server" CssClass="lnk_editar_peq" Text='<%$ Resources:operators,BotonNOK %>'     OnClick="lbGrabarTelefonoComunicacionesNOK_Click" Visible="false" CausesValidation="false"></asp:LinkButton>
								</div>
							</div>
						</div>

						<div class="col-md-4 aire10_sup_inf" style="padding-bottom:0px;">
							<asp:DropDownList runat="server" ID="ddlTipoOperador" CssClass="textbox_editar_perfil_peq" style="height:25px;" placeholder='<%$ Resources:operators,PlaceholderTipoOperador %>' ToolTip='<%$ Resources:operators,PlaceholderTipoOperador %>'
							AutoPostBack="true" OnSelectedIndexChanged="ddlTipoOperador_SelectedIndexChanged" Enabled="false" ValidationGroup="TipoOperador" />
							<asp:RequiredFieldValidator runat="server" ID="reqddlTipoOperador" ControlToValidate="ddlTipoOperador" InitialValue="-1" ErrorMessage="*" CssClass="val_error" Display="Dynamic"></asp:RequiredFieldValidator>
						</div>
						<div class="col-md-2 aire10_sup_inf">
							<asp:LinkButton ID="lbCambiarTipoOperador"		 runat="server" CssClass="lnk_editar_peq" Text='<%$ Resources:operators,BotonEditar %>' Visible="true" CausesValidation="false" OnClick="lbCambiarTipoOperador_Click"></asp:LinkButton>
							<asp:LinkButton ID="lbCambiarTipoOperadorOK"   runat="server" CssClass="lnk_editar_peq" Text='<%$ Resources:operators,BotonOK %>'      OnClick="lbCambiarTipoOperadorOK_Click"  Visible="false" ValidationGroup="Telefono"></asp:LinkButton>
							<asp:LinkButton ID="lbCambiarTipoOperadorNOK"  runat="server" CssClass="lnk_editar_peq" Text='<%$ Resources:operators,BotonNOK %>'     OnClick="lbCambiarTipoOperadorNOK_Click" Visible="false" CausesValidation="false"></asp:LinkButton>
						</div>
						<div class="col-md-4 aire10_sup_inf">
							<asp:TextBox ID="txtPadre" runat="server" placeholder='<%$ Resources:operators,PlaceholderPadre %>' ToolTip='<%$ Resources:operators,PlaceholderPadre %>' BackColor="White" Enabled="false" CssClass="textbox_editar_perfil_peq" ValidationGroup="PerfilPadre" AutoCompleteType="None" autocomplete="off" />
							<%--<asp:RequiredFieldValidator runat="server" ID="reqPadre" ControlToValidate="txtPadre" Text="*" ToolTip='<%$ Resources:operators,Obligatorio %>' CssClass="val_error" Display="Dynamic" ValidationGroup="PerfilPadre"></asp:RequiredFieldValidator>--%>
						</div>
						<div class="col-md-2 aire10_sup_inf">
							<asp:LinkButton ID="lbCambiarPadre" runat="server" CssClass="lnk_editar_peq" Text='<%$ Resources:operators,BotonEditar %>' Visible="true" OnClick="lbCambiarPadre_Click" CausesValidation="false" ToolTip='<%$ Resources:operators,LiteralInstruccionesSuperior %>'></asp:LinkButton>
						</div>
						<div class="col-md-12 text-center aire10_sup_inf">
							<asp:LinkButton ID="lbGuardarPerfil" runat="server" Text='<%$ Resources:operators,BotonGuardarPerfil %>' CssClass="lnk_editar_peq" OnClick="lbGuardarPerfil_Click" CausesValidation="false" style="margin-top:20px;"></asp:LinkButton>
						</div>
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>

			<div id="divAsignar" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="ModalAsignar" aria-hidden="true">
				<div class="modal-dialog modal-md">
  				<div class="modal-content">
						<asp:UpdatePanel ID="upAsignar" runat="server">
							<Triggers>
								<asp:PostBackTrigger ControlID="tvOperadoresDestino" />
							</Triggers>
							<ContentTemplate>
                <div class="modal-header" style="border:none;">
                  <h5 class="modal-title txt_titulopagina_med" id="TituloModificar">
                    <asp:Literal runat="server" ID="lblTituloAsignar" Text='<%$ Resources:operators,TituloArbolAsignar %>' />
                  </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
									<div style="display:inline-block; width:100%; height:452px; border:1px solid #AAAAAA;">
										<asp:TreeView runat="server" ID="tvOperadoresDestino" CssClass="estilotextoarbol" ShowLines="true"
										OnSelectedNodeChanged="tvOperadoresDestino_SelectedNodeChanged"
										style="width:100%; height:450px; overflow-y:scroll; float:left; margin:0px;"></asp:TreeView>
									</div>
                </div>
							</ContentTemplate>
						</asp:UpdatePanel>
					</div>
				</div>
			</div>

			<div id="divBorrar" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="ModalBorrar" aria-hidden="true">
				<div class="modal-dialog modal-md">
  				<div class="modal-content">
						<asp:UpdatePanel ID="upBorrar" runat="server">
							<Triggers>

							</Triggers>
							<ContentTemplate>
                <div class="modal-header" style="border:none;">
                  <h5 class="modal-title txt_titulopagina_med" id="TituloBorrar">
                    <asp:Literal runat="server" ID="litTituloBorrar" Text='<%$ Resources:operators,TituloBorrar %>' />
                  </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
									<asp:Label runat="server" ID="lblAvisoBorrar"></asp:Label>
                </div>
								<asp:Button ID="btnConfirmar" runat="server" Text='<%$ Resources:operators,BotonConfirmar %>' OnClick="btnConfirmar_Click" class="lnk_passchange" style="margin-left:16px;margin-bottom:16px;" ValidationGroup="BorrarPerfiles" />
							</ContentTemplate>
						</asp:UpdatePanel>
					</div>
				</div>
			</div>

	  </div>
	</div>

	<asp:UpdatePanel ID="upAlerta" runat="server">
		<ContentTemplate>
			<div id="divAlerta" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="ModalAviso" aria-hidden="true">
				<div class="modal-dialog modal-md">
					<div class="modal-content">
						<div class="modal-body">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<asp:Label ID="lblResultado" runat="server" />
						</div>
					</div>
				</div>
			</div>
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Content>
