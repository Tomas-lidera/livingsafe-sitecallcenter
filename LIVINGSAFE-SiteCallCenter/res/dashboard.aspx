﻿<%@ Page Title="" Language="C#" MasterPageFile="~/res/LIVINGSAFE.Master" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeBehind="dashboard.aspx.cs" Inherits="LIVINGSAFE_SiteCallCenter.res.dashboard" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="cDashboardHead" ContentPlaceHolderID="cpHead" runat="server">
  <script type="text/javascript" src="/js/Chart/Chart.min.js"></script>
  <script type="text/javascript" src="/js/PerfectScrollbar/perfect-scrollbar.js"></script>
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" />
  <link rel="stylesheet" type="text/css" href="/js/PerfectScrollbar/perfect-scrollbar.css" />
</asp:Content>
<asp:Content ID="cDashboardBody" ContentPlaceHolderID="cpBody" runat="server">
  <asp:ScriptManager ID="smMassive" runat="server"></asp:ScriptManager>

  <div class="container-fluid section-contenido">

    <div class="row espacio_inferior">
      <div class="col-md-12">
        <div class="txt_titulopagina_med"><asp:Literal ID="litTituloDashboard" runat="server" EnableViewState="false" Text='<%$ Resources:dashboard,litTituloDashboard %>'></asp:Literal></div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-5 offset-md-1">
        <div style="display:inline-block; float:left; margin-left:10px;">
          <asp:TextBox runat="server" ID="txtFechaUso" MaxLength="10" style="text-align:left;" Width="140px" placeholder="DD/MM/YYYY" CssClass="form-control" AutoCompleteType="None" autocomplete="off" />
          <cc1:CalendarExtender runat="server" ID="ceFechaUso" TargetControlID="txtFechaUso" CssClass="Calendar" FirstDayOfWeek="Monday" DefaultView="Days" Format="dd/MM/yyyy" /> 
        </div>
        <div style="display:inline-block; float:left; margin-left:10px; margin-right:20px;">
          <asp:LinkButton runat="server" ID="lbCalendario" Text="" OnClick="lbCalendario_Click" CausesValidation="false" CssClass="fas fa-calendar-check" style="line-height:36px; font-size:30px; text-decoration:none; color:#5FA8FC;"></asp:LinkButton>
        </div>
        <div style="display:inline-block; float:left;">
          <asp:DropDownList runat="server" ID="ddlPartners" AppendDataBoundItems="true" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlPartners_SelectedIndexChanged">
            <asp:ListItem Text='<%$ Resources:dashboard,LiteralPartnersTodos %>' Value="-1"></asp:ListItem>
          </asp:DropDownList>
        </div>
        <div style="display:inline-block; float:left; margin-left:10px;">
          <asp:LinkButton runat="server" ID="lbLimpiar" Text="" OnClick="lbLimpiar_Click" CausesValidation="false" CssClass="fas fa-eraser" style="line-height:36px;font-size:30px; text-decoration:none; color:#5FA8FC;"></asp:LinkButton>
        </div>
      </div>
      <div class="col-md-5">
        <div style="display:inline-block; float:right; margin-left:30px;">
          <asp:LinkButton runat="server" ID="lbOcultar" Text="" OnClick="lbOcultar_Click" CausesValidation="false" CssClass="fas fa-eye-slash" style="line-height:36px;font-size:30px; text-decoration:none; color:#5FA8FC;"></asp:LinkButton>
        </div>
        <div style="display:inline-block; float:right;">
          <asp:LinkButton runat="server" ID="lbZoom" Text="" OnClick="lbZoom_Click" CausesValidation="false" CssClass="fas fa-search-plus" style="line-height:36px;font-size:30px; text-decoration:none; color:#5FA8FC;"></asp:LinkButton>
        </div>
      </div>
    </div>

    <div class="row espacio_inferior">
      <div class="col-md-8 offset-md-2">
        <asp:UpdatePanel runat="server" ID="upCanvas" UpdateMode="Conditional">
          <Triggers>
            <asp:PostBackTrigger ControlID="lbZoom" />
            <asp:PostBackTrigger ControlID="lbExcelTotales" />
            <asp:PostBackTrigger ControlID="gvUso" />
          </Triggers>
          <ContentTemplate>
            <div id="divCanvas" style="position:relative; overflow:hidden; width:100%; background-color:#FFFFFF;">
              <canvas id="canvas" runat="server" width="1200" height="400" style="display:block; margin-bottom:20px;" enableviewstate="true"></canvas>
            </div>
            <script>
              // Initialize the plugin
              const dCanvas = document.querySelector('#divCanvas');
              const ps = new PerfectScrollbar(dCanvas);
            </script>
          </ContentTemplate>
        </asp:UpdatePanel>
      </div>
    </div>

    <div class="row espacio_inferior">
      <div class="col-lg-2 offset-lg-1">
        <div class="card card-stats" style="background-color:#F6F6F6;" title='<%= Resources.dashboard.FichaPartnersLeyenda %>'>
          <div class="card-body">
            <div class="row">
              <div class="col-5 col-md-4">
                <div class="icon-big text-center" style="color:#FBC881;">
                  <i class="far fa-handshake"></i>
                </div>
              </div>
              <div class="col-7 col-md-8">
                <div class="numbers">
                  <p class="card-category"><asp:Literal runat="server" ID="lblTotalPartners" Text='<%$ Resources:dashboard,FichaPartnersTitulo %>' EnableViewState="false"></asp:Literal></p>
                  <p class="card-title"><asp:Label runat="server" ID="lblTPartners"></asp:Label></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-2">
        <div class="card card-stats" style="background-color:#F6F6F6;" title='<%= Resources.dashboard.FichaISPLeyenda %>'>
          <div class="card-body">
            <div class="row">
              <div class="col-5 col-md-4">
                <div class="icon-big text-center" style="color:#EF8157;">
                  <i class="fas fa-address-card"></i>
                </div>
              </div>
              <div class="col-7 col-md-8">
                <div class="numbers">
                  <p class="card-category"><asp:Literal runat="server" ID="litISPTitulo" Text='<%$ Resources:dashboard,FichaISPTitulo %>' EnableViewState="false"></asp:Literal></p>
                  <p class="card-title"><asp:Label runat="server" ID="lblTISP"></asp:Label></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-2">
        <div class="card card-stats" style="background-color:#F6F6F6;" title='<%= Resources.dashboard.FichaPedidosLeyenda %>'>
          <div class="card-body">
            <div class="row">
              <div class="col-5 col-md-4">
                <div class="icon-big text-center" style="color:#6BD098;">
                  <i class="fas fa-money-check"></i>
                </div>
              </div>
              <div class="col-7 col-md-8">
                <div class="numbers">
                  <p class="card-category"><asp:Literal runat="server" ID="Literal2" Text='<%$ Resources:dashboard,FichaPedidosTitulo %>' EnableViewState="false"></asp:Literal></p>
                  <p class="card-title"><asp:Label runat="server" ID="lblTPedidos"></asp:Label></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-2">
        <div class="card card-stats" style="background-color:#F6F6F6;"  title='<%= Resources.dashboard.FichaLicenciasLeyenda %>'>
          <div class="card-body">
            <div class="row">
              <div class="col-5 col-md-4">
                <div class="icon-big text-center" style="color:#51CBCE;">
                  <i class="fas fa-laptop"></i>
                </div>
              </div>
              <div class="col-7 col-md-8">
                <div class="numbers">
                  <p class="card-category"><asp:Literal runat="server" ID="Literal3" Text='<%$ Resources:dashboard,FichaLicenciasTitulo %>' EnableViewState="false"></asp:Literal></p>
                  <p class="card-title"><asp:Label runat="server" ID="lblTLicencias"></asp:Label></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-2">
        <a runat="server" href="~/RES/billing.aspx" class="card card-stats card-billing" title='<%= Resources.dashboard.FichaLicenciasLeyenda %>'>
          <div class="card-body">
            <div class="row">
              <div class="col-5 col-md-4">
                <div class="icon-big text-center" style="color:#FFFFFF;">
                  <i class="fas fa-money-bill-wave"></i>
                </div>
              </div>
              <div class="col-7 col-md-8">
                <div class="numbers">
                  <p class="card-billing-text"><asp:Label runat="server" ID="lbBilling" Text='<%$ Resources:dashboard,FichaBillingTitulo %>'></asp:Label></p>
                </div>
              </div>
            </div>
          </div>
        </a>
      </div>
    </div>

    <div class="row">
      <div class="col-md-10 offset-md-1">
        <asp:UpdatePanel runat="server" ID="upUso" class="col-md-12 ml-auto align-content-end espacio_inferior">
          <Triggers>
            <asp:PostBackTrigger ControlID="lbExcelTotales" />
          </Triggers>
          <ContentTemplate>
            <asp:GridView runat="server" ID="gvUso" AutoGenerateColumns="False" DataSourceID="sqlUso" DataKeyNames="IdISP" 
              EmptyDataText='<%$ Resources:dashboard,gridUsoVacio %>' AllowPaging="True" AllowSorting="True" OnPreRender="gvUso_PreRender"
              BorderWidth="0px" BorderStyle="None" GridLines="None" PagerSettings-Mode="NumericFirstLast" OnPageIndexChanging="gvUso_PageIndexChanging" OnSorting="gvUso_Sorting"
              HeaderStyle-CssClass="col-4 txt_titulo_grid cabecera_grid" RowStyle-CssClass="col-4 txt_contenido_grid franja_grid_par" AlternatingRowStyle-CssClass="col-4 txt_contenido_grid"
              PagerStyle-CssClass="paginacion" CssClass="container-fluid" EmptyDataRowStyle-CssClass="col-4 txt_contenido_grid franja_grid_par" PageSize="20">
            
              <Columns>
                <asp:TemplateField HeaderText='<%$ Resources:dashboard,gridPartner %>' SortExpression="NombrePartner">
                  <ItemTemplate>
                    <asp:Label runat="server" ID="lblNombrePartner" Text='<%# Bind("NombrePartner") %>' ToolTip='<%# Bind("NombrePartner") %>'></asp:Label>
                  </ItemTemplate>
                  <ItemStyle CssClass="col_resp_peq" HorizontalAlign="Left" />
                  <HeaderStyle CssClass="col_resp_peq" />
                </asp:TemplateField>

                <asp:TemplateField HeaderText='<%$ Resources:dashboard,gridSiglasISP %>' SortExpression="Siglas">
                  <ItemTemplate>
                    <asp:LinkButton runat="server" ID="lbSiglas"  Text='<%# Bind("SiglasActivo") %>' CommandArgument='<%# Bind("IdISP") %>' OnClick="lbVerISP_Click"></asp:LinkButton>
                  </ItemTemplate>
                  <ItemStyle CssClass="opacidad80" />
                </asp:TemplateField>

                <asp:TemplateField HeaderText='<%$ Resources:dashboard,gridNombreISP %>' SortExpression="NombreISP">
                  <ItemTemplate>
                    <asp:Label runat="server" ID="lblNombreISP" Text='<%# Bind("NombreISPActivo") %>'></asp:Label>
                  </ItemTemplate>
                  <ItemStyle CssClass="col_resp" />
                  <HeaderStyle CssClass="col_resp" />
                </asp:TemplateField>

                <asp:TemplateField HeaderText='<%$ Resources:dashboard,gridTotalPedidos %>' SortExpression="TotalPedidos">
                  <ItemTemplate>
                    <asp:Label runat="server" ID="lblTotalPedidos" Text='<%# Bind("TotalPedidos") %>'></asp:Label>
                  </ItemTemplate>
                  <ItemStyle CssClass="opacidad80" HorizontalAlign="Center" />
                </asp:TemplateField>

                <asp:TemplateField HeaderText='<%$ Resources:dashboard,gridTotalLicencias %>' SortExpression="TotalLicencias">
                  <ItemTemplate>
                    <asp:Label runat="server" ID="lblTotalLicencias" Text='<%# Bind("TotalLicencias") %>'></asp:Label>
                  </ItemTemplate>
                  <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>

                <asp:TemplateField HeaderText='<%$ Resources:dashboard,gridFechaInforme %>' SortExpression="FechaInforme">
                  <ItemTemplate>
                    <asp:Label ID="lblFechaInforme" runat="server" Text='<%# Bind("FechaInforme", "{0:dd-MM-yyyy}") %>'></asp:Label>
                  </ItemTemplate>
                  <ItemStyle CssClass="col_resp opacidad80" HorizontalAlign="Center" />
                  <HeaderStyle CssClass="col_resp" />
                </asp:TemplateField>
              </Columns>

              <EmptyDataRowStyle      CssClass="col-4 txt_dash_grid franja_grid_par" HorizontalAlign="center" />
              <HeaderStyle            CssClass="col-4 txt_titulo_grid cabecera_grid" HorizontalAlign="center" />
              <PagerSettings          Mode="NumericFirstLast" />
              <PagerStyle             BackColor="#EDEDED" HorizontalAlign="Right" BorderWidth="0px" />
              <RowStyle               CssClass="col-4 txt_dash_grid franja_grid_par" />
              <AlternatingRowStyle    CssClass="col-4 txt_dash_grid" />

            </asp:GridView>

            <asp:SqlDataSource runat="server" ID="sqlUso" CancelSelectOnNullParameter="false"
            ConnectionString="<%$ ConnectionStrings:LivingSafeCS %>" SelectCommand="Dashboard_ISPTotales" SelectCommandType="StoredProcedure">
              <SelectParameters>
                <asp:Parameter Name="IdPartner" Type="Int32" ConvertEmptyStringToNull="true" />
                <asp:Parameter Name="Fecha" Type="DateTime" ConvertEmptyStringToNull="true" />
              </SelectParameters>
            </asp:SqlDataSource>
            <div style="display:inline-block;">
              <asp:LinkButton runat="server" ID="lbExcelTotales" Text="" OnClick="lbExcelTotales_Click" CausesValidation="false" CssClass="fas fa-file-excel" style="line-height:36px;font-size:30px; text-decoration:none; color:#5FA8FC;"></asp:LinkButton>
              <span style="margin-left:20px; font-size:12px; font-family:Arial;">[D]: <asp:Literal runat="server" ID="litDesativado" Text='<%$ Resources:dashboard,LiteralDesactivado %>' EnableViewState="false"></asp:Literal></span>
            </div>
          </ContentTemplate>
        </asp:UpdatePanel>
      </div>
    </div>
  </div>

  <asp:UpdatePanel ID="upAlerta" UpdateMode="Conditional" runat="server">
    <ContentTemplate>
      <div id="divAlerta" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="ModalAviso" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-body">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body" style="padding-top:0px; padding-bottom:0px;">
              <asp:Literal ID="litCabecera" runat="server" Text='<%$ Resources:massive,litCabecera %>' EnableViewState="false" />
              <asp:Label ID="lblResultado" runat="server" EnableViewState="false" />
              <br /><br />
            </div>
          </div>
        </div>
      </div>
    </ContentTemplate>
  </asp:UpdatePanel>
</asp:Content>
