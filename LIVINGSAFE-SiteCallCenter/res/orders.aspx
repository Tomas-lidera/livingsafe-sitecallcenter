﻿<%@ Page Title="" Language="C#" MasterPageFile="~/res/LIVINGSAFE.Master" AutoEventWireup="true" CodeBehind="orders.aspx.cs" Inherits="LIVINGSAFE_SiteCallCenter.res.orders" %>
<asp:Content ID="cOrdersHead" ContentPlaceHolderID="cpHead" runat="server">
</asp:Content>
<asp:Content ID="cOrdersBody" ContentPlaceHolderID="cpBody" runat="server">
  <asp:ScriptManager ID="smOrders" runat="server"></asp:ScriptManager>
  <div class="container-fluid section-contenido">
    <div class="row espacio_inferior">
      <div class="col-md-8">
        <div class="txt_titulopagina_med"><asp:Literal ID="litTitulo" runat="server" EnableViewState="false" Text='<%$ Resources:orders,litTitulo %>'></asp:Literal></div>
        <div class="txt_titulopagina_med">
          <asp:DropDownList runat="server" ID="ddlFiltroDetalles" CssClass="form-control" Width="175px" style="float:left; margin-top:50px;" AutoPostBack="true" OnSelectedIndexChanged="ddlFiltroDetalles_SelectedIndexChanged">
            <asp:ListItem Text='<%$ Resources:orders,ddlFiltroDetallesActivos %>' Value="1"></asp:ListItem>
            <asp:ListItem Text='<%$ Resources:orders,ddlFiltroDetallesTodos %>' Value="0"></asp:ListItem>
          </asp:DropDownList>
        </div>
      </div>

      <div class="col-md-4">
        <div class="card card-stats" style="background-color:#F6F6F6;">
          <div class="card-body">
            <div class="row">
              <div class="col-3 col-md-2">
                <div class="icon-big text-center color_ficha_usuarios">
                  <i class="fas fa-user-alt"></i>
                </div>
              </div>
              <div class="col-9 col-md-10">
                <div style="line-height:14px;"><asp:Label runat="server" ID="lblUsuario" EnableViewState="false" CssClass="texto_editar_perfil"></asp:Label></div>
                <div style="line-height:14px;"><asp:Label runat="server" ID="lblComunicacion" EnableViewState="false" CssClass="texto_editar_perfil"></asp:Label></div>
                <div style="line-height:14px;"><asp:Label runat="server" ID="lblDireccion" EnableViewState="false" CssClass="texto_editar_perfil"></asp:Label></div>
                <div style="line-height:14px;"><asp:Label runat="server" ID="lblLocalidad" EnableViewState="false" CssClass="texto_editar_perfil"></asp:Label></div>
                <div style="line-height:14px; margin-top:10px;" class="text-center"><asp:Label runat="server" ID="litGUID" CssClass="GUID_editar_perfil" EnableViewState="false"></asp:Label></div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-10"></div>
      <div class="col-md-2">

      </div>
       
      <asp:UpdatePanel runat="server" ID="upLanding" ChildrenAsTriggers="true" class="col-md-12 ml-auto align-content-end">
        <Triggers>
          <asp:AsyncPostBackTrigger ControlID="btnGuardar" />
        </Triggers>
        <ContentTemplate>
          <asp:GridView ID="gvwPedidos" runat="server" AutoGenerateColumns="False" DataSourceID="sqlPedidos" DataKeyNames="IdPedidoDetalle" 
            EmptyDataText='<%$ Resources:orders,gridPedidosVacio %>' 
            AllowPaging="True" AllowSorting="True"  BorderWidth="0px" BorderStyle="None" GridLines="None" PagerSettings-Mode="NumericFirstLast" PageSize="15"
            HeaderStyle-CssClass="col-4 txt_titulo_grid cabecera_grid" RowStyle-CssClass="col-4 txt_contenido_grid franja_grid_par" AlternatingRowStyle-CssClass="col-4 txt_contenido_grid"
            PagerStyle-CssClass="paginacion" CssClass="container-fluid" EmptyDataRowStyle-CssClass="col-4 txt_contenido_grid franja_grid_par" >
            
            <Columns>
              <asp:BoundField DataField="IdUsuarioFinal" HeaderText="IdUsuarioFinal" SortExpression="IdUsuarioFinal" InsertVisible="False" ReadOnly="True" Visible="false" />
              <asp:BoundField DataField="NombreOperador" HeaderText='<%$ Resources:orders,gridPedidosOperador %>' SortExpression="NombreOperador" />

              <asp:TemplateField HeaderText='<%$ Resources:orders,gridPedidosDetalle %>' SortExpression="IdPedidoDetalle">
                <ItemTemplate>
                  <asp:Label ID="lblDetalle" runat="server" Text='<%# Bind("IdPedidoDetalle") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="75px" />
              </asp:TemplateField>

              <asp:TemplateField HeaderText='<%$ Resources:orders,gridPedidosContrato %>' SortExpression="Contrato">
                <ItemTemplate>
                  <asp:Label ID="lblContrato" runat="server" Text='<%# Bind("Contrato") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="230px" />
              </asp:TemplateField>

              <asp:TemplateField HeaderText='<%$ Resources:orders,gridPedidosSKU %>' SortExpression="SKU">
                <ItemTemplate>
                  <asp:Label ID="lblSKU" runat="server" Text='<%# Bind("SKU") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle />
              </asp:TemplateField>

              <asp:TemplateField HeaderText='<%$ Resources:orders,gridPedidosProducto %>' SortExpression="Nombre">
                <ItemTemplate>
                  <asp:Label ID="lblProducto" runat="server" Text='<%# DameNombreCortoLimpio(Eval("Nombre").ToString()) %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle  />
              </asp:TemplateField>

              <asp:TemplateField HeaderText='<%$ Resources:orders,gridPedidosCantidad %>' SortExpression="Cantidad">
                <ItemTemplate>
                  <asp:Label ID="lblCantidad" runat="server" Text='<%# Bind("Cantidad") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="100px" />
              </asp:TemplateField>

              <asp:TemplateField HeaderText='<%$ Resources:orders,gridPedidosPrecioVenta %>' SortExpression="PrecioVenta">
                <ItemTemplate>
                  <asp:Label ID="lblPrecioVenta" runat="server" Text='<%# Bind("PrecioVenta","{0:0.00}") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="100px" />
              </asp:TemplateField>

              <asp:TemplateField HeaderText='<%$ Resources:orders,gridPedidosFechaAltaDetalle %>' SortExpression="Alta">
                <ItemTemplate>
                  <asp:Label ID="lblAltaDetalle" runat="server" Text='<%# Bind("FechaAltaPedidoDetalle", "{0:dd-MM-yyyy}") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="100px" />
              </asp:TemplateField>

              <asp:TemplateField HeaderText='<%$ Resources:orders,gridPedidosFechaBajaDetalle %>' SortExpression="Baja">
                <ItemTemplate>
                  <asp:Label ID="lblBajaDetalle" runat="server" Text='<%# Bind("FechaBajaPedidoDetalle", "{0:dd-MM-yyyy}") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="100px" />
              </asp:TemplateField>

              <asp:TemplateField>
                <ItemTemplate>
                  <asp:ImageButton runat="server" ID="lnkModificar" ImageUrl="~/images/icono_editar.png" OnClick="lnkModificar_Click" ToolTip='<%$ Resources:orders,ToolTipModificar %>' 
                  CommandArgument='<%#Eval("IdPedidoDetalle")%>' CausesValidation="false" style="vertical-align:middle;" Visible='<%# Convert.ToBoolean(EsBaja(Eval("FechaBajaPedidoDetalle").ToString())) && Convert.ToBoolean(ValidarPermisos("LISTADOPEDIDOSBOTONCANTIDAD")) %>'></asp:ImageButton>
                </ItemTemplate>
                <ItemStyle Width="50px" HorizontalAlign="Center" />
              </asp:TemplateField>

							<asp:TemplateField>
                <ItemTemplate>
                  <asp:ImageButton runat="server" ID="lbCambiarProductos" ImageUrl="~/images/icono_cambiarproducto.png" OnClick="lnkCambiarProductos_Click" 
                  CommandArgument='<%#Eval("IdPedidoDetalle")%>' CausesValidation="false" style="vertical-align:middle;" Visible='<%# Convert.ToBoolean(EsBaja(Eval("FechaBajaPedidoDetalle").ToString())) && Convert.ToBoolean(ValidarPermisos("LISTADOPEDIDOSBOTONCAMBIARPRODUCTO")) %>' ToolTip='<%$ Resources:orders,ToolTipoCambiarProducto %>' />
                </ItemTemplate>
                <ItemStyle Width="50px" HorizontalAlign="Center" />
              </asp:TemplateField>

              <asp:TemplateField>
                <ItemTemplate>
                  <asp:ImageButton runat="server" ID="lnkReenviar" ImageUrl="~/images/icono_reenviar.png" OnClick="lnkReenviar_Click" ToolTip='<%$ Resources:orders,ToolTipReenviar %>' 
                  CommandArgument='<%#Eval("IdPedidoDetalle")%>' CausesValidation="false" style="vertical-align:middle;" Visible='<%# Convert.ToBoolean(EsBaja(Eval("FechaBajaPedidoDetalle").ToString())) && Convert.ToBoolean(ValidarPermisos("LISTADOPEDIDOSBOTONMAILBIENVENIDA")) %>'></asp:ImageButton>
                </ItemTemplate>
                <ItemStyle Width="50px" HorizontalAlign="Center" />
              </asp:TemplateField>

							<asp:TemplateField>
                <ItemTemplate>
                  <asp:ImageButton runat="server" ID="lbAltaDetalle" ImageUrl="~/images/icono_verpedidos.png" OnClick="lbAltaDetalle_Click" CommandArgument='<%#Eval("Contrato")%>' CausesValidation="false" style="vertical-align:middle;" Visible='<%# Convert.ToBoolean(EsBaja(Eval("FechaBajaPedidoDetalle").ToString())) && Convert.ToBoolean(ValidarPermisos("LISTADOPEDIDOSBOTONANHADIRPRODUCTOS")) %>' ToolTip='<%$ Resources:orders,TooltipAltaDetalle %>' />
                </ItemTemplate>
                <ItemStyle Width="50px" HorizontalAlign="Center" />
              </asp:TemplateField>

              <asp:TemplateField>
                <ItemTemplate>
                  <asp:ImageButton runat="server" ID="lnkBorrar" ImageUrl="~/images/icono_borrar.png" OnClick="lnkBorrar_Click" ToolTip='<%$ Resources:orders,ToolTipBorrar %>' 
                  CommandArgument='<%#Eval("GUIDPedidoDetalle")%>' CausesValidation="false" style="vertical-align:middle;" Visible='<%# Convert.ToBoolean(EsBaja(Eval("FechaBajaPedidoDetalle").ToString())) && Convert.ToBoolean(ValidarPermisos("LISTADOPEDIDOSBOTONBAJA")) %>'></asp:ImageButton>
                </ItemTemplate>
                <ItemStyle Width="50px" HorizontalAlign="Center" />
              </asp:TemplateField>
            </Columns>

            <EmptyDataRowStyle      CssClass="col-4 txt_contenido_grid franja_grid_par" HorizontalAlign="center" />
            <HeaderStyle            CssClass="col-4 txt_titulo_grid cabecera_grid" HorizontalAlign="Left" />
            <PagerSettings          Mode="NumericFirstLast" />
            <PagerStyle             BackColor="#EDEDED" HorizontalAlign="Right" BorderWidth="0px" />
            <RowStyle               CssClass="col-4 txt_contenido_grid franja_grid_par" HorizontalAlign="Left" />
            <AlternatingRowStyle    CssClass="col-4 txt_contenido_grid" HorizontalAlign="Left" />

          </asp:GridView>

          <asp:SqlDataSource ID="sqlPedidos" CancelSelectOnNullParameter="false" runat="server" ConnectionString="<%$ ConnectionStrings:LivingSafeCS %>"
          SelectCommand="Usuarios_ListadoPedidos" SelectCommandType="StoredProcedure">
            <SelectParameters>
              <asp:Parameter Name="IdOperador"      Type="Int32"    ConvertEmptyStringToNull="true" />
              <asp:Parameter Name="IdUsuarioFinal"  Type="Int32"    ConvertEmptyStringToNull="true" />
              <asp:Parameter Name="IdIdioma"        Type="Int32"    ConvertEmptyStringToNull="true" />
              <asp:Parameter Name="IdMonedaDestino" Type="Int32"    ConvertEmptyStringToNull="true" />
              <asp:Parameter Name="MostrarTodos"    Type="Boolean"  ConvertEmptyStringToNull="true" />
            </SelectParameters>
          </asp:SqlDataSource>

          <div class="container section-contenido">
            <div class="row espacio_inferior">
              <div id="divMensaje" runat="server" class="alert alert-sucess fade collapse" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <asp:Label runat="server" ID="lblMensaje" />
              </div>
            </div>
          </div>
        </ContentTemplate>
      </asp:UpdatePanel>

      <div id="divModificar" runat="server" class="modal fade bd-modificar-modal-lg" tabindex="-1" role="dialog" aria-labelledby="ModalModificar" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <asp:UpdatePanel ID="upCapaUsuarios" runat="server">
              <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnGuardar" />
              </Triggers>
              <ContentTemplate>
                <%//Este código tiene que ir dentro del UpdatePanel porque si no la máscara no se aplica %>
                <script>
                  var prm = Sys.WebForms.PageRequestManager.getInstance();
                  prm.add_pageLoaded(function () {
                    $(document).ready(function () {
                      $('#<%=txtCantidad.ClientID%>').mask('######0', { reverse: true });

                      $('#<%=txtPrecio.ClientID%>').mask('0000000,00', { reverse: true });
                    });
                  });
                </script>
                <div class="modal-header" style="border:none;">
                  <h5 class="modal-title txt_titulopagina_med" id="TituloModificar">
                    <asp:Literal runat="server" ID="lblTituloCapa" Text='<%$ Resources:orders,formTitulo2 %>' />
                  </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>

                <div class="modal-body">

                  <div runat="server" id="divProductoActual" class="form-row" visible="false" style="margin-bottom:15px;">
                    <div class="form-group col-md-12">
                      <asp:Literal ID="litProductoActual" runat="server" EnableViewState="false" Text='<%$ Resources:orders,LiteralProductoActual %>' />:
                    </div>
                    <div class="form-group col-md-12">
                      <asp:Label ID="lblProductoActual" runat="server" />
                    </div>
                  </div>

                  <div class="form-row">
                    <div class="form-group col-md-8">
                      <asp:Literal ID="litProducto" runat="server" Text='<%$ Resources:orders,formUsuariosProducto %>' />
                      <asp:DropDownList runat="server" ID="ddlProductos" AutoPostBack="True" CssClass="form-control" ValidationGroup="ModificarCantidad"></asp:DropDownList>
                      <asp:RequiredFieldValidator runat="server" ID="reqProductos" ControlToValidate="ddlProductos" InitialValue="-1" ErrorMessage='<%$ Resources:orders,formObligatorio %>' ForeColor="Red" Display="Dynamic" ValidationGroup="ModificarCantidad"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form-group col-md-2">
                      <asp:Literal ID="litCantidad" runat="server" EnableViewState="false" Text='<%$ Resources:orders,formUsuariosCantidad %>' />
                      <asp:TextBox runat="server" ID="txtCantidad" MaxLength="6" CssClass="form-control" PlaceHolder="0" ValidationGroup="ModificarCantidad"></asp:TextBox>
                      <asp:RequiredFieldValidator runat="server" ID="reqCantidad" ControlToValidate="txtCantidad" ErrorMessage='<%$ Resources:orders,formObligatorio %>' ForeColor="Red" Display="Dynamic" ValidationGroup="ModificarCantidad"></asp:RequiredFieldValidator>
                      <asp:RegularExpressionValidator runat="server" ID="regCantidad" ControlToValidate="txtCantidad" ErrorMessage='<%$ Resources:orders,formErrorCantidad %>' ValidationExpression="^[1-9]\d*$" ForeColor="Red" Display="Dynamic" ValidationGroup="ModificarCantidad"></asp:RegularExpressionValidator>
                    </div>
                    <div class="form-group col-md-2">
                      <asp:Literal ID="litPrecio" runat="server" EnableViewState="false" Text='<%$ Resources:orders,formUsuariosPrecio %>' />
                      <asp:TextBox runat="server" ID="txtPrecio" MaxLength="10" CssClass="form-control" PlaceHolder="0,00" ValidationGroup="ModificarCantidad"></asp:TextBox>
                      <asp:RequiredFieldValidator runat="server" ID="reqPrecio" ControlToValidate="txtPrecio" ErrorMessage='<%$ Resources:orders,formObligatorio %>' ForeColor="Red" Display="Dynamic" ValidationGroup="ModificarCantidad"></asp:RequiredFieldValidator>
                      <asp:RegularExpressionValidator runat="server" ID="regPrecio" ControlToValidate="txtPrecio" ErrorMessage='<%$ Resources:orders,formErrorPrecio %>' ValidationExpression="(\d+\,\d{1,2})" ForeColor="Red" Display="Dynamic" ValidationGroup="ModificarCantidad"></asp:RegularExpressionValidator>
                    </div>
                  </div>

                  <asp:Button ID="btnGuardar" runat="server" Text='<%$ Resources:orders,btnGuardar %>' OnClick="btnGuardar_Click" class="lnk_passchange" ValidationGroup="ModificarCantidad" />

                </div>
              </ContentTemplate>
            </asp:UpdatePanel>
          </div>
        </div>
      </div>

      <div id="divCancelar" class="modal fade bd-borrar-modal-lg" tabindex="-1" role="dialog" aria-labelledby="ModalBorrar" aria-hidden="true" runat="server">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <asp:UpdatePanel ID="upCapaBorrar" runat="server" ChildrenAsTriggers="true">
              <Triggers>
                <asp:AsyncPostBackTrigger ControlID="lbConfirmarBaja" />
              </Triggers>
              <ContentTemplate>
                <div class="modal-header" style="border:none;">
                  <h5 class="modal-title txt_titulopagina_med" id="TituloBorrar">
                    <asp:Literal runat="server" ID="litTituloCapaBorrar" EnableViewState="false" Text='<%$ Resources:orders,formTitulo3 %>' />
                  </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>

                <div class="modal-body">
                  <div class="form-row">
                    <div class="form-group col-md-12">
                      <asp:Literal runat="server" ID="litMensajeBorrado" Text='<%$ Resources:orders,MensajeBorrado %>' EnableViewState="false"></asp:Literal>
                    </div>
                  </div>
                  <asp:LinkButton ID="lbConfirmarBaja" runat="server" Text='<%$ Resources:orders,btnConfirmar %>' OnClick="lbConfirmarBaja_Click" class="lnk_passchange" CausesValidation="false" />
                </div>
              </ContentTemplate>
            </asp:UpdatePanel>
          </div>
        </div>
      </div>

      <div id="divReenviar" class="modal fade bd-reenviar-modal-lg" tabindex="-1" role="dialog" aria-labelledby="ModalReenviar" aria-hidden="true" runat="server">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <asp:UpdatePanel ID="upCapaReenviar" runat="server" ChildrenAsTriggers="true">
              <Triggers>
                <asp:AsyncPostBackTrigger ControlID="lbConfirmarReenvio" />
              </Triggers>
              <ContentTemplate>
                <div class="modal-header" style="border:none;">
                  <h5 class="modal-title txt_titulopagina_med" id="TituloReenviar">
                    <asp:Literal runat="server" ID="litTituloCapaReenviar" EnableViewState="false" Text='<%$ Resources:orders,TituloReenviar %>' />
                  </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>

                <div class="modal-body">
                  <div class="form-row">
                    <div class="form-group col-md-12">
                      <asp:Literal runat="server" ID="kitMensajeReenviar" Text='<%$ Resources:orders,MensajeReenviar %>' EnableViewState="false"></asp:Literal>
                    </div>
                  </div>
                  <asp:LinkButton ID="lbConfirmarReenvio" runat="server" Text='<%$ Resources:orders,btnConfirmar %>' OnClick="lbConfirmarReenvio_Click" class="lnk_passchange" CausesValidation="false" />
                </div>
              </ContentTemplate>
            </asp:UpdatePanel>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-12" style="text-align:center;">
    
  </div>

  <asp:UpdatePanel ID="upAlerta" runat="server">
    <ContentTemplate>
      <div id="divAlerta" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="ModalAviso" aria-hidden="true">
        <div class="modal-dialog modal-md">
          <div class="modal-content">
            <div class="modal-body">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <asp:Label ID="lblResultado" runat="server" />
            </div>
          </div>
        </div>
      </div>
    </ContentTemplate>
  </asp:UpdatePanel>
</asp:Content>

