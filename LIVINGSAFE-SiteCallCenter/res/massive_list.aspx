﻿<%@ Page Title="" Language="C#" MasterPageFile="~/res/LIVINGSAFE.Master" AutoEventWireup="true" CodeBehind="massive_list.aspx.cs" Inherits="LIVINGSAFE_SiteCallCenter.res.massive_list" %>
<asp:Content ID="cOrdersHead" ContentPlaceHolderID="cpHead" runat="server">
</asp:Content>
<asp:Content ID="cOrdersBody" ContentPlaceHolderID="cpBody" runat="server">
  <asp:ScriptManager ID="smMassive" runat="server"></asp:ScriptManager>
  <div class="container-fluid section-contenido">
    <div class="row espacio_inferior">
      <div class="col-md-8">
        <div class="txt_titulopagina_med"><asp:Literal ID="litTitulo" runat="server" EnableViewState="false" Text='<%$ Resources:massive_list,litTituloListado %>'></asp:Literal></div>
      </div>
      <div class="col-md-4">
        <asp:LinkButton runat="server" ID="lbNuevaAlta" Text='<%$ Resources:massive_list,lbNuevaAlta %>' CssClass="lnk_passchange aire10_sup_inf" OnClick="lbNuevaAlta_Click"></asp:LinkButton>
        <asp:LinkButton runat="server" ID="lbNuevaBaja" Text='<%$ Resources:massive_list,lbNuevaBaja %>' CssClass="lnk_passchange aire10_sup_inf" style="margin-left:20px;" OnClick="lbNuevaBaja_Click"></asp:LinkButton>
      </div>

      <div class="col-md-12" style="height:30px; padding:0px;"></div>

        <asp:UpdatePanel runat="server" ID="upGrid" class="col-md-8 offset-2" style="padding:0px;">
        <Triggers>
          <asp:AsyncPostBackTrigger ControlID="tmrTemporizador" EventName="Tick" />
        </Triggers>
        <ContentTemplate>

          <asp:GridView ID="gvwRegistrosProvision" runat="server" AutoGenerateColumns="False" DataSourceID="sqlRegistrosProvision" DataKeyNames="Id" 
          EmptyDataText='<%$ Resources:massive_list,gridPedidosVacio %>' PageSize="10"
          AllowPaging="True" AllowSorting="True"  BorderWidth="0px" BorderStyle="None" GridLines="None" PagerSettings-Mode="NumericFirstLast" 
          HeaderStyle-CssClass="txt_titulo_grid cabecera_grid" 
          RowStyle-CssClass="txt_contenido_grid franja_grid_par" 
          AlternatingRowStyle-CssClass="txt_contenido_grid"
          EmptyDataRowStyle-CssClass="txt_contenido_grid franja_grid_par"
          PagerStyle-CssClass="paginacion" Width="100%">
            
          <Columns>
            <asp:TemplateField HeaderText='<%$ Resources:massive_list,gridId %>' SortExpression="Id">
              <ItemTemplate>
                <asp:Label ID="lblIdRegistro" runat="server" Text='<%# Bind("Id") %>'></asp:Label>
              </ItemTemplate>
              <ItemStyle HorizontalAlign="Center" />

            </asp:TemplateField>

            <asp:TemplateField HeaderText='<%$ Resources:massive_list,gridFechaAlta %>' SortExpression="FechaAlta">
              <ItemTemplate>
                <asp:Label ID="lblFechaAlta" runat="server" Text='<%# Bind("FechaAlta", "{0:dd-MM-yyyy hh:mm}") %>'></asp:Label>
              </ItemTemplate>
              <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>

            <asp:TemplateField HeaderText='<%$ Resources:massive_list,gridFechaProcesado %>' SortExpression="FechaProcesado">
              <ItemTemplate>
                <asp:Label ID="lblFechaProcesado" runat="server" Text='<%# Bind("FechaProcesado", "{0:dd-MM-yyyy hh:mm}") %>'></asp:Label>
              </ItemTemplate>
              <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>

              <asp:TemplateField HeaderText='<%$ Resources:massive_list,gridTipo %>' SortExpression="Tipo">
              <ItemTemplate>
                <asp:Label ID="lblTotalTipo" runat="server" Text='<%# Bind("Tipo") %>'></asp:Label>
              </ItemTemplate>
              <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>

            <asp:TemplateField HeaderText='<%$ Resources:massive_list,gridTotal %>' SortExpression="Total">
              <ItemTemplate>
                <asp:Label ID="lblTotalOK" runat="server" Text='<%# Bind("ProvisionadosOK") %>'></asp:Label> / <asp:Label ID="lblTotalRegistros" runat="server" Text='<%# Bind("Total") %>'></asp:Label>
              </ItemTemplate>
              <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>


            <asp:TemplateField HeaderText='<%$ Resources:massive_list,gridErrores %>' SortExpression="ProvisionadosNOK">
              <ItemTemplate>
                <asp:Label ID="lblTotalErrores" runat="server" Text='<%# Bind("ProvisionadosNOK") %>'></asp:Label>
              </ItemTemplate>
              <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>

            <asp:TemplateField>
              <ItemTemplate>
                <asp:Image ID="imgFinalizado" runat="server" ImageUrl='<%# DameImagen(Eval("Finalizado").ToString(),Eval("ProvisionadosOK").ToString(),Eval("ProvisionadosPdte").ToString(),Eval("ProvisionadosNOK").ToString()) %>' />
              </ItemTemplate>
              <ItemStyle Width="65px" HorizontalAlign="Center" />
            </asp:TemplateField>

            <asp:TemplateField>
              <ItemTemplate>
                <asp:ImageButton runat="server" ID="ibDescargarXLSX" ImageUrl="~/images/icono_excel.png" ToolTip="EXCEL"
                CommandArgument='<%#Eval("Id")%>' CausesValidation="false" style="vertical-align:middle;" OnClick="ibDescargarXLSX_Click" Visible='<%# TieneEnlaces(Eval("Id").ToString()) %>'></asp:ImageButton>
              </ItemTemplate>
              <ItemStyle Width="65px" HorizontalAlign="Center" />
            </asp:TemplateField>

            <asp:TemplateField>
              <ItemTemplate>
                <asp:ImageButton runat="server" ID="ibDescargarLOG" ImageUrl="~/images/icono_log.png" ToolTip="LOG"
                CommandArgument='<%#Eval("Id")%>' CausesValidation="false" style="vertical-align:middle;" OnClick="ibDescargarLOG_Click" Visible='<%# TieneEnlaces(Eval("Id").ToString()) %>'></asp:ImageButton>
              </ItemTemplate>
              <ItemStyle Width="65px" HorizontalAlign="Center" />
            </asp:TemplateField>

  
          </Columns>

             

          <EmptyDataRowStyle      CssClass="col-4 txt_contenido_grid franja_grid_par" HorizontalAlign="center" />
          <HeaderStyle            CssClass="col-4 txt_titulo_grid cabecera_grid text-center" HorizontalAlign="Left" />
          <PagerSettings          Mode="NumericFirstLast" />
          <PagerStyle             BackColor="#EDEDED" HorizontalAlign="Right" BorderWidth="0px" />
          <RowStyle               CssClass="col-4 txt_contenido_grid franja_grid_par" HorizontalAlign="Left" />
          <AlternatingRowStyle    CssClass="col-4 txt_contenido_grid" HorizontalAlign="Left" />
          </asp:GridView>

          <asp:SqlDataSource ID="sqlRegistrosProvision" CancelSelectOnNullParameter="false" runat="server" ConnectionString="<%$ ConnectionStrings:LivingSafeCS %>"
          SelectCommand="Provision_Masiva_Registro_Listado" SelectCommandType="StoredProcedure">
            <SelectParameters>
              <asp:Parameter Name="IdISP" Type="Int32" ConvertEmptyStringToNull="true" />
            </SelectParameters>
          </asp:SqlDataSource>

        </ContentTemplate>
      </asp:UpdatePanel>

      <div class="col-md-12" style="height:30px; padding:0px;"></div>

      <asp:UpdatePanel runat="server" ID="upTimer" class="col-md-6 offset-3" style="padding:0px;">
        <ContentTemplate>
          <asp:Timer runat="server" ID="tmrTemporizador" Interval="1000" OnTick="tmrTemporizador_Tick"></asp:Timer>
        </ContentTemplate>
      </asp:UpdatePanel>
    </div>
  </div>

  <asp:UpdatePanel ID="upAlerta" UpdateMode="Conditional" runat="server">
    <ContentTemplate>
      <div id="divAlerta" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="ModalAviso" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-body">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <asp:Literal ID="litCabecera" runat="server" Text='<%$ Resources:massive,litCabecera %>' EnableViewState="false" />
              <asp:Label ID="lblResultado" runat="server" EnableViewState="false" />
            </div>
          </div>
        </div>
      </div>
    </ContentTemplate>
  </asp:UpdatePanel>
</asp:Content>

