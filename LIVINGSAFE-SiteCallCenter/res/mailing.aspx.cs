﻿using LIVINGSAFE_SiteCallCenter.App_Code;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LIVINGSAFE_SiteCallCenter.res
{
  public partial class mailing : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {


    }

    protected void btnEnviar_Click(object sender, EventArgs e)
    {
      ArrayList Emails = DameEmails();

      Response.Write("Proceso iniciado: " + DateTime.Now.ToString());

      while (Emails.Count > 0)
      {
        foreach (string Linea in Emails)
        {
          string[] Datos = Linea.Split(';');

          try
          {
            EnviarDatos(Datos[1].ToString());
            ActualizarEnvio(Convert.ToInt32(Datos[0].ToString()), 2,null);
          }

          catch(Exception Exc)
          {
            ActualizarEnvio(Convert.ToInt32(Datos[0].ToString()), 3,Exc.ToString());
          }
        }

        Emails = DameEmails();
      }

      Response.Write("<br />Proceso finalizado: " + DateTime.Now.ToString());
    }
    
    

    protected ArrayList DameEmails()
    {
      ArrayList Emails = new ArrayList();

      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = new SqlCommand();

      try
      {
        Con.Open();

        Com = Con.CreateCommand();
        Com.CommandText = "TEMP_DESPEDIDA_SELECT";
        Com.CommandTimeout = 300;
        Com.CommandType = CommandType.StoredProcedure;

        SqlDataReader Rst = Com.ExecuteReader();

        while (Rst.Read())
        {
          Emails.Add(Rst["Id"].ToString() + ";" + Rst["email"].ToString());
        }
      }

      finally
      {
        Com.Dispose();

        if (Con.State == ConnectionState.Open)
          Con.Close();

        Con.Dispose();
      }

      return Emails;
    }

    protected void EnviarDatos(string Email)
    {
      /*
      cUtil Utilidad = new cUtil();
      
      SmtpClient Correo = new SmtpClient(Utilidad.DameValorConfiguracion("ServidorSMTPCorreoSpamina"));
      NetworkCredential Credenciales = new NetworkCredential(Utilidad.DameValorConfiguracion("ServidorSMTPUser"), Utilidad.DameValorConfiguracion("ServidorSMTPPass"));
      Correo.Credentials = Credenciales;

      MailMessage Mensaje = new MailMessage();

      MailAddress MailFromSoporte = new System.Net.Mail.MailAddress("info@living-safe.com");
      Mensaje.From = MailFromSoporte;
      Mensaje.To.Add(Email);
      Mensaje.Subject = "Despedida";
      Mensaje.IsBodyHtml = true;

      Mensaje.Body = "CHAO <b>PEJKAO</b>";

      Correo.Send(Mensaje);
      */

      SmtpClientEx msmtpClient = new SmtpClientEx("email-smtp.us-east-1.amazonaws.com");
      msmtpClient.Host = "email-smtp.us-east-1.amazonaws.com";
      msmtpClient.EnableSsl = true;
      msmtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
      msmtpClient.UseDefaultCredentials = false;
      msmtpClient.Credentials = new System.Net.NetworkCredential("AKIAY2XFDHNR22YFTYW4", "BGwveGsZIgNpkYrIYAnrKb4y5ZeqYX+ypVNkI2qh/b+C");
      msmtpClient.Port = 587;

      MailMessage mmmMensaje = new MailMessage();

      MailAddress From = new MailAddress("minhavero@minhavero.com.br", "Vero Internet"); //minhavero@minhavero.com.br //Vero Internet
      mmmMensaje.From = From;
      
      mmmMensaje.To.Add(Email);
      mmmMensaje.Subject = "LivingSafe - Até logo";

      mmmMensaje.IsBodyHtml = true;
      mmmMensaje.Body = GetMessage();

      mmmMensaje.BodyEncoding = Encoding.GetEncoding("iso-8859-1");

      msmtpClient.Send(mmmMensaje);
      mmmMensaje.Dispose();
    }

    protected void ActualizarEnvio(int Id,int Estado,string Error)
    {
      ArrayList Emails = new ArrayList();

      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = new SqlCommand();

      try
      {
        Con.Open();

        Com = Con.CreateCommand();
        Com.CommandText = "TEMP_DESPEDIDA_UPDATE";
        Com.CommandTimeout = 300;
        Com.CommandType = CommandType.StoredProcedure;
        Com.Parameters.Add("@Id", SqlDbType.Int).Value = Id;
        Com.Parameters.Add("@Estado", SqlDbType.Int).Value = Estado;
        Com.Parameters.Add("@Error", SqlDbType.VarChar,-1).Value = Error;

        Com.ExecuteNonQuery();
      }

      finally
      {
        Com.Dispose();

        if (Con.State == ConnectionState.Open)
          Con.Close();

        Con.Dispose();
      }
    }

    protected string GetMessage()
    {
      string Msg = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("~/templates/mail_despedida.html"));

      return Msg;
    }
  }

  class SmtpClientEx : SmtpClient
  {
    #region Private Data

    private static readonly FieldInfo localHostName = GetLocalHostNameField();

    #endregion

    #region Constructor

    /// <summary>
    /// Initializes a new instance of the <see cref="SmtpClientEx"/> class
    /// that sends e-mail by using the specified SMTP server and port.
    /// </summary>
    /// <param name="host">
    /// A <see cref="String"/> that contains the name or 
    /// IP address of the host used for SMTP transactions.
    /// </param>
    /// <param name="port">
    /// An <see cref="Int32"/> greater than zero that 
    /// contains the port to be used on host.
    /// </param>
    /// <exception cref="ArgumentNullException">
    /// <paramref name="port"/> cannot be less than zero.
    /// </exception>
    public SmtpClientEx(string host, int port) : base(host, port)
    {
      Initialize();
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="SmtpClientEx"/> class
    /// that sends e-mail by using the specified SMTP server.
    /// </summary>
    /// <param name="host">
    /// A <see cref="String"/> that contains the name or 
    /// IP address of the host used for SMTP transactions.
    /// </param>
    public SmtpClientEx(string host) : base(host)
    {
      Initialize();
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="SmtpClientEx"/> class
    /// by using configuration file settings.
    /// </summary>
    public SmtpClientEx()
    {
      Initialize();
    }

    #endregion

    #region Properties

    /// <summary>
    /// Gets or sets the local host name used in SMTP transactions.
    /// </summary>
    /// <value>
    /// The local host name used in SMTP transactions.
    /// This should be the FQDN of the local machine.
    /// </value>
    /// <exception cref="ArgumentNullException">
    /// The property is set to a value which is
    /// <see langword="null"/> or <see cref="String.Empty"/>.
    /// </exception>
    public string LocalHostName
    {
      get
      {
        if (null == localHostName) return null;
        return (string)localHostName.GetValue(this);
      }
      set
      {
        if (string.IsNullOrEmpty(value))
        {
          throw new ArgumentNullException("value");
        }
        if (null != localHostName)
        {
          localHostName.SetValue(this, value);
        }
      }
    }

    #endregion

    #region Methods

    private static FieldInfo GetLocalHostNameField()
    {
      const BindingFlags flags = BindingFlags.Instance | BindingFlags.NonPublic;
      FieldInfo result = typeof(SmtpClient).GetField("clientDomain", flags);
      if (null == result) result = typeof(SmtpClient).GetField("localHostName", flags);
      return result;
    }

    /// <summary>
    /// Initializes the local host name to 
    /// the FQDN of the local machine.
    /// </summary>
    private void Initialize()
    {
      IPGlobalProperties ip = IPGlobalProperties.GetIPGlobalProperties();
      if (!string.IsNullOrEmpty(ip.HostName) && !string.IsNullOrEmpty(ip.DomainName))
      {
        this.LocalHostName = ip.HostName + "." + ip.DomainName;
      }
    }

    #endregion
  }
}