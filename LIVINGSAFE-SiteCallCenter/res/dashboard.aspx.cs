﻿using LIVINGSAFE_SiteCallCenter.App_Code;
using LIVINGSAFE_SiteCallCenter.Clases;
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace LIVINGSAFE_SiteCallCenter.res
{
  public partial class dashboard : System.Web.UI.Page
  {
    //cLog LOGe = new cLog();
    cUtil Utilidad = new cUtil();
    cPermisos Permisos = new cPermisos();
    cIdiomas Idioma = new cIdiomas();
    cEncriptacion Encriptacion = new cEncriptacion();
    int Entidad = Convert.ToInt32(HttpContext.Current.User.Identity.Name);
    static int CantidadISP = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
      try
      {
        int IdISPOperador = Convert.ToInt32(Utilidad.DameDatoOperador(Entidad, "IdISPOperador"));
        string Siglas = Utilidad.DameDatoISP(IdISPOperador, "Siglas");

        if (!Permisos.TienePermiso("PAGINADASHBOARD", Entidad) || Siglas.ToUpper()!="LIVINGSAFE")
          Response.Redirect("~/res/landing.aspx");

        if (!IsPostBack)
        {
          int IdPartner = -1;
          DateTime FechaTemp = Convert.ToDateTime("1900-01-01 00:00:00");
          ReiniciarDatos(FechaTemp, IdPartner,false);

          CargarDesplegablePartners();
        }
      }

      catch (Exception Exc)
      {
        string Msg = string.Empty;

        if (Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);
        else if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);
        else
        {
          if (Utilidad.EsEntornoDesarrollo())
            Msg = Exc.ToString();
          else
            Msg = Exc.Message.ToString();
        }

        lblResultado.Text = Msg;

        string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "MostrarErrorModif", Escript, true);
      }
    }

    protected void ddlPartners_SelectedIndexChanged(object sender, EventArgs e)
    {
      int IdPartner = Convert.ToInt32(((DropDownList)sender).SelectedValue);
      DateTime FechaTemp = Convert.ToDateTime("1900-01-01 00:00:00");
      string CulturaActual = string.Empty;

      try
      {
        if (txtFechaUso.Text != string.Empty)
        {
          //Para trabajar con fechas forzamos la cultura es-ES porque como puede poner idioma inglés, al cambiar la cultura cambia el tipo 
          //de fecha de dd/mm/yyyy a mm/dd/yyyy y da problemas. Al final de operar con fechas devolvermos la cultura que tuvier antes.
          if (HttpContext.Current.Request.UserLanguages != null && HttpContext.Current.Request.UserLanguages.Length > 0)
            CulturaActual = HttpContext.Current.Request.UserLanguages[0];
          else
            CulturaActual = Thread.CurrentThread.CurrentCulture.Name;

          Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("es-ES");
          Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("es-ES");

          if (!DateTime.TryParse(txtFechaUso.Text, out FechaTemp))
            throw new Exception(Resources.dashboard.ResourceManager.GetString("LiteralErrorFecha", Thread.CurrentThread.CurrentCulture));
        }

        ReiniciarDatos(FechaTemp, IdPartner,true);
      }

      finally
      {

        //Dejamos la cultura como estaba
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(CulturaActual);
        Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(CulturaActual);
      }
    }

    protected void lbCalendario_Click(object sender, EventArgs e)
    {
      string CulturaActual = string.Empty;

      try
      {
        int IdPartner = Convert.ToInt32(ddlPartners.SelectedValue);
        DateTime FechaTemp = Convert.ToDateTime("1900-01-01 00:00:00");

        if (txtFechaUso.Text != string.Empty)
        {
          //Para trabajar con fechas forzamos la cultura es-ES porque como puede poner idioma inglés, al cambiar la cultura cambia el tipo 
          //de fecha de dd/mm/yyyy a mm/dd/yyyy y da problemas. Al final de operar con fechas devolvermos la cultura que tuvier antes.
          if (HttpContext.Current.Request.UserLanguages != null && HttpContext.Current.Request.UserLanguages.Length > 0)
            CulturaActual = HttpContext.Current.Request.UserLanguages[0];
          else
            CulturaActual = Thread.CurrentThread.CurrentCulture.Name;

          Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("es-ES");
          Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("es-ES");

          if (!DateTime.TryParse(txtFechaUso.Text, out FechaTemp))
            throw new Exception(Resources.dashboard.ResourceManager.GetString("LiteralErrorFecha", Thread.CurrentThread.CurrentCulture));
        }

        ReiniciarDatos(FechaTemp,IdPartner,true);
      }

      catch (Exception Exc)
      {
        string Msg = string.Empty;

        if (Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);
        else if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);
        else
        {
          if (Utilidad.EsEntornoDesarrollo())
            Msg = Exc.ToString();
          else
            Msg = Exc.Message.ToString();
        }

        lblResultado.Text = Msg;

        string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "MostrarErrorModif", Escript, true);
      }

      finally
      {
        //Dejamos la cultura como estaba
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(CulturaActual);
        Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(CulturaActual);
      }
    }

    protected void lbLimpiar_Click(object sender, EventArgs e)
    {
      try
      {
        ddlPartners.SelectedIndex = 0;
        txtFechaUso.Text = string.Empty;

        int IdPartner = Convert.ToInt32(ddlPartners.SelectedValue);
        DateTime FechaTemp = Convert.ToDateTime("1900-01-01 00:00:00");

        if (txtFechaUso.Text != string.Empty)
        {
          if (!DateTime.TryParse(txtFechaUso.Text, out FechaTemp))
            throw new Exception(Resources.dashboard.ResourceManager.GetString("LiteralErrorFecha", Thread.CurrentThread.CurrentCulture));
        }

        ReiniciarDatos(FechaTemp, IdPartner,true);
      }

      catch (Exception Exc)
      {
        string Msg = string.Empty;

        if (Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);
        else if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);
        else
        {
          if (Utilidad.EsEntornoDesarrollo())
            Msg = Exc.ToString();
          else
            Msg = Exc.Message.ToString();
        }

        lblResultado.Text = Msg;

        string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "MostrarErrorModif", Escript, true);
      }
    }

    protected void lbZoom_Click(object sender, EventArgs e)
    {
      if (((LinkButton)sender).CssClass == "fas fa-search-plus")
      {
        ZoomCanvas(canvas, true, CantidadISP);
        lbZoom.CssClass = "fas fa-search-minus";
      }
      else
      {
        ZoomCanvas(canvas, false, CantidadISP);
        lbZoom.CssClass = "fas fa-search-plus";
      }

      int IdPartner = Convert.ToInt32(ddlPartners.SelectedValue);
      DateTime FechaTemp = Convert.ToDateTime("1900-01-01 00:00:00");

      if (txtFechaUso.Text != string.Empty)
      {
        if (!DateTime.TryParse(txtFechaUso.Text, out FechaTemp))
          throw new Exception(Resources.dashboard.ResourceManager.GetString("LiteralErrorFecha", Thread.CurrentThread.CurrentCulture));
      }

      ReiniciarDatos(FechaTemp, IdPartner,true);
    }

    protected void lbOcultar_Click(object sender, EventArgs e)
    {
      if (upCanvas.Visible == true)
      {
        upCanvas.Visible = false;
        lbZoom.Visible = false;
        lbOcultar.CssClass = "far fa-eye";
      }
      else
      {
        upCanvas.Visible = true;
        lbZoom.Visible = true;
        lbOcultar.CssClass = "fas fa-eye-slash";

        int IdPartner = Convert.ToInt32(ddlPartners.SelectedValue);
        DateTime FechaTemp = Convert.ToDateTime("1900-01-01 00:00:00");

        if (txtFechaUso.Text != string.Empty)
        {
          if (!DateTime.TryParse(txtFechaUso.Text, out FechaTemp))
            throw new Exception(Resources.dashboard.ResourceManager.GetString("LiteralErrorFecha", Thread.CurrentThread.CurrentCulture));
        }

        ReiniciarDatos(FechaTemp, IdPartner,true);
      }
    }

    protected void lbExcelTotales_Click(object sender, EventArgs e)
    {
      ExportarCSV("LivingSafe_Usage_");
    }

    protected void lbVerISP_Click(object sender, EventArgs e)
    {
      try
      {
        int IdUsuarioFinal = Convert.ToInt32(((LinkButton)sender).CommandArgument);

        string Usuario = Encriptacion.CifrarAES(Convert.ToInt32(Utilidad.DameDatoOperador(Entidad, "IdISPOperador")), IdUsuarioFinal.ToString());
        Usuario = HttpUtility.UrlEncode(Usuario);

        Response.Redirect("~/res/dashboard_isp.aspx?i=" + Usuario);
      }

      catch (Exception Exc)
      {
        if (Utilidad.EsEntornoDesarrollo())
        {
          lblResultado.Text = Exc.ToString();
        }
        else
        {
          if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
            lblResultado.Text = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
          else
            lblResultado.Text = Exc.Message.ToString() + "<br /><br />";
        }

        string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MostrarErrorAlta", Escript, true);
      }
    }

    protected void gvUso_PreRender(object sender, EventArgs e)
    {
      //Este evento fuerza que siempre salga la barra de paginación
      GridView gv = (GridView)sender;
      GridViewRow pagerRow = (GridViewRow)gv.BottomPagerRow;

      if (pagerRow != null && pagerRow.Visible == false)
        pagerRow.Visible = true;
    }

    protected void gvUso_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
      try
      {
        int IdPartner = Convert.ToInt32(ddlPartners.SelectedValue);
        DateTime FechaTemp = Convert.ToDateTime("1900-01-01 00:00:00");

        if (txtFechaUso.Text != string.Empty)
        {
          if (!DateTime.TryParse(txtFechaUso.Text, out FechaTemp))
            throw new Exception(Resources.dashboard.ResourceManager.GetString("LiteralErrorFecha", Thread.CurrentThread.CurrentCulture));
        }

        ReiniciarDatos(FechaTemp, IdPartner,true);
      }

      catch (Exception Exc)
      {
        string Msg = string.Empty;

        if (Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);
        else if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);
        else
        {
          if (Utilidad.EsEntornoDesarrollo())
            Msg = Exc.ToString();
          else
            Msg = Exc.Message.ToString();
        }

        lblResultado.Text = Msg;

        string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "MostrarErrorModif", Escript, true);
      }
    }

    protected void gvUso_Sorting(object sender, GridViewSortEventArgs e)
    {
      try
      {
        int IdPartner = Convert.ToInt32(ddlPartners.SelectedValue);
        DateTime FechaTemp = Convert.ToDateTime("1900-01-01 00:00:00");

        if (txtFechaUso.Text != string.Empty)
        {
          if (!DateTime.TryParse(txtFechaUso.Text, out FechaTemp))
            throw new Exception(Resources.dashboard.ResourceManager.GetString("LiteralErrorFecha", Thread.CurrentThread.CurrentCulture));
        }

        ReiniciarDatos(FechaTemp, IdPartner,true);
      }

      catch (Exception Exc)
      {
        string Msg = string.Empty;

        if (Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);
        else if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);
        else
        {
          if (Utilidad.EsEntornoDesarrollo())
            Msg = Exc.ToString();
          else
            Msg = Exc.Message.ToString();
        }

        lblResultado.Text = Msg;

        string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "MostrarErrorModif", Escript, true);
      }
    }



    protected ArrayList ValoresGrafica(DateTime Fecha,int IdPartner)
    {
      ArrayList ListaISP = new ArrayList(); // Cada registro contiene un ArryList DatosISP
      ArrayList DatosISP = null; // Datos del ISP: NombrePartner string, NombreISP string, SiglasISP string, FechaFechaInforme datetime, ListaValores ArrayList
      ArrayList ListaValores = null; // Cada registro contiene un ArrayList Valores
      ArrayList Valores = null; //Valores de la gráfica para ese ISP, tantos como NumCols: Valor int, Leyenda string, Color string y ColorBorde string

      ArrayList Colores = new ArrayList();
      Colores.Add("#096829;#053D18"); //Verde
      Colores.Add("#07577F;#04344C"); //Azul
      Colores.Add("#CC1460;#8E0E43"); //Rosa
      Colores.Add("#FF421C;#91250F"); //Naranja

      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = null;

      try
      {
        Con.Open();
        Com = Con.CreateCommand();
        Com.CommandType = System.Data.CommandType.StoredProcedure;
        Com.CommandText = "Dashboard_ISPTotales";
        Com.CommandTimeout = 300;

        if (Fecha != Convert.ToDateTime("1900-01-01 00:00:00"))
          Com.Parameters.Add("@Fecha", SqlDbType.DateTime).Value = Fecha.ToString("yyyy-MM-dd HH:mm:ss");

        if(IdPartner!=-1)
          Com.Parameters.Add("@IdPartner", SqlDbType.Int).Value = IdPartner;

        SqlDataReader Rst = Com.ExecuteReader();

        while(Rst.Read())
        {
          DatosISP = new ArrayList();
          
          ListaValores = new ArrayList();

          DatosISP.Add(Rst["NombrePartner"].ToString());
          DatosISP.Add(Rst["NombreISP"].ToString());
          DatosISP.Add(Rst["Siglas"].ToString());
          DatosISP.Add(Rst["FechaInforme"].ToString());

          Valores = new ArrayList();
          Valores.Add(Rst["TotalPedidos"].ToString());
          Valores.Add(Resources.dashboard.ResourceManager.GetString("GraficaUsoLeyenda1", CultureInfo.CreateSpecificCulture(Idioma.DameIdioma())));
          Valores.Add(((string[])Colores[0].ToString().Split(';'))[0]);
          Valores.Add(((string[])Colores[0].ToString().Split(';'))[1]);
          ListaValores.Add(Valores);

          Valores = new ArrayList();
          Valores.Add(Rst["TotalLicencias"].ToString());
          Valores.Add(Resources.dashboard.ResourceManager.GetString("GraficaUsoLeyenda2", CultureInfo.CreateSpecificCulture(Idioma.DameIdioma())));
          Valores.Add(((string[])Colores[1].ToString().Split(';'))[0]);
          Valores.Add(((string[])Colores[1].ToString().Split(';'))[1]);
          ListaValores.Add(Valores);

          DatosISP.Add(ListaValores);

          DatosISP.Add(Rst["NombrePnInLargo"].ToString());
          DatosISP.Add(Rst["NombrePnSiCorto"].ToString());

          ListaISP.Add(DatosISP);
        }
      }

      finally
      {
        if (Com != null)
          Com.Dispose();

        if (Con.State == ConnectionState.Open)
          Con.Close();

        Con.Dispose();
      }

      return ListaISP;
    }

    protected void CargarGrafica(string NombreGrafica, string Titulo, int NumCols, ArrayList Lista)
    {
      //Este método va a servir para cualquier gráfica. Se le pasa en un ArrayList el nombre del canvas, el total de columnas por cada ISP (min: 1, max: 4), el título y el conjunto de datos.
      //Los colores de las 4 posibles columnas están predefinidos en el ArrayList Colores y se usarán tantos como columnas se procesen.
      //El conjunto de datos será otro ArrayList donde vendrán para cada ISP tantas líneas como NumCols cada una de las cuales traerá los valores separados por coma y su leyenda.

      //1. Cargamos la fecha del informe. Para trabajar con fechas forzamos la cultura es-ES porque como puede poner idioma inglés, al cambiar la cultura cambia el tipo 
      //de fecha de dd/mm/yyyy a mm/dd/yyyy y da problemas. Al final de operar con fechas devolvermos la cultura que tuvier antes.

      string CulturaActual = string.Empty;

      if (HttpContext.Current.Request.UserLanguages != null && HttpContext.Current.Request.UserLanguages.Length > 0)
        CulturaActual = HttpContext.Current.Request.UserLanguages[0];
      else
        CulturaActual = Thread.CurrentThread.CurrentCulture.Name;

      Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("es-ES");
      Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("es-ES");

      DateTime Fecha = Convert.ToDateTime(((ArrayList)Lista[0])[3]);

      //2. Cargamos los nombres de los ISPs
      string labels = string.Empty;

      for(int i=0; i<Lista.Count;i++)
      {
        if (i > 0)
          labels += ",";

        labels += "'" + ((ArrayList)Lista[i])[6].ToString() + "'";
      }

      //3. Cargamos los valores de las columnas (máximo 4 columnas por gráfica)
      ArrayList ValoresTemp = new ArrayList();
      ArrayList Valores = new ArrayList();
      ArrayList Col = null;

      for (int i = 0; i < Lista.Count; i++)
      {
        Col = new ArrayList();

        for (int r = 0; r < ((ArrayList)((ArrayList)Lista[i])[4]).Count; r++)
        {
          Col.Add(((ArrayList)((ArrayList)((ArrayList)((ArrayList)Lista[i])[4]))[r])[0]);
        }

        ValoresTemp.Add(Col);
      }

      string valores = string.Empty;

      for (int r = 0; r < ((ArrayList)ValoresTemp[0]).Count; r++)
      {
        for (int i = 0; i < Lista.Count; i++)
        {
          if (i > 0)
            valores += ",";

          valores += ((ArrayList)ValoresTemp[i])[r].ToString();
        }

        Valores.Add(valores);
        valores = string.Empty;
      }

      //4. Cargamos las leyendas y los colores
      ArrayList Leyendas = new ArrayList();
      ArrayList Colores = new ArrayList();
      ArrayList ColoresBorde = new ArrayList();

      for (int r = 0; r < ((ArrayList)((ArrayList)Lista[0])[4]).Count; r++)
      {
        Leyendas.Add(((ArrayList)((ArrayList)((ArrayList)((ArrayList)Lista[0])[4]))[r])[1]);
        Colores.Add(((ArrayList)((ArrayList)((ArrayList)((ArrayList)Lista[0])[4]))[r])[2]);
        ColoresBorde.Add(((ArrayList)((ArrayList)((ArrayList)((ArrayList)Lista[0])[4]))[r])[3]);
      }

      string Escript2 = string.Empty;

      for (int i = 0; i < Valores.Count; i++)
      {
        Escript2 += "{ label: '" + Leyendas[i] + "',   backgroundColor: '" + Colores[i] + "', borderColor: '" + ColoresBorde[i] + "', borderWidth: 1, data: [" + Valores[i] + "] },";
      }


      string Escript = @"
        var barChartData =
        {
          labels: [" + labels + @"],
          datasets:
          [" +
          Escript2
          + @"]
	      };

        window.onload = function ()
        {
		      var ctx = document.getElementById('" + NombreGrafica + @"').getContext('2d');
		      window.myBar = new Chart(ctx,
          {
			      type: 'bar',
			      data: barChartData,

			      options: { responsive:  false,
				                legend:      { position: 'bottom', display:true },
					              title:       { display: true, text: '" + Titulo + " " + Fecha.ToShortDateString() + @"' },
                        // El bloque animation pinta los valores encima de las barras
                        animation:   {
                                        duration: 1,
                                        onComplete: function() {
                                          var chartInstance = this.chart,
                                          ctx = chartInstance.ctx;
                                          ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                                          ctx.textAlign = 'center';
                                          ctx.textBaseline = 'bottom';

                                          this.data.datasets.forEach(function(dataset, i) {
                                              var meta = chartInstance.controller.getDatasetMeta(i);
                                              meta.data.forEach(function(bar, index) {
                                                var data = dataset.data[index];
                                                ctx.fillText(data, bar._model.x, bar._model.y - 5);
                                              });
                                          });
                                        }
                                    }
				             }


		      });
	      };
      ";

      ScriptManager.RegisterStartupScript(Page, Page.GetType(), "TotalPedidosLicenciasFecha", Escript, true);

      /*
      //5. Cargamos los totales
      string PAR = ((ArrayList)Lista[0])[0].ToString();
      string ISP = ((ArrayList)Lista[0])[2].ToString();
      int ContPAR = 1;
      int ContISP = 1;
      double TotalP = 0;
      double TotalL = 0;

      for(int i=0; i<Lista.Count;i++)
      {
        if (((ArrayList)Lista[i])[0].ToString() != PAR)
        {
          ContPAR += 1;
          PAR = ((ArrayList)Lista[i])[0].ToString();
        }

        if (((ArrayList)Lista[i])[2].ToString() != ISP)
        {
          ContISP += 1;
          ISP = ((ArrayList)Lista[i])[2].ToString();
        }

        TotalP += Convert.ToDouble(((ArrayList)((ArrayList)((ArrayList)((ArrayList)Lista[i])[4]))[0])[0]);
        TotalL += Convert.ToDouble(((ArrayList)((ArrayList)((ArrayList)((ArrayList)Lista[i])[4]))[1])[0]);

        lblTPartners.Text = ContPAR.ToString();
        lblTISP.Text = ContISP.ToString();
        lblTPedidos.Text = TotalP.ToString();
        lblTLicencias.Text = TotalL.ToString();
      }
      */
      //Dejamos la cultura como estaba
      Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(CulturaActual);
      Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(CulturaActual);
    }

    protected void CargarTotales(ArrayList Lista)
    {
      string PAR = ((ArrayList)Lista[0])[0].ToString();
      string ISP = ((ArrayList)Lista[0])[2].ToString();
      int ContPAR = 1;
      int ContISP = 1;
      double TotalP = 0;
      double TotalL = 0;

      for (int i = 0; i < Lista.Count; i++)
      {
        if (((ArrayList)Lista[i])[0].ToString() != PAR)
        {
          ContPAR += 1;
          PAR = ((ArrayList)Lista[i])[0].ToString();
        }

        if (((ArrayList)Lista[i])[2].ToString() != ISP)
        {
          ContISP += 1;
          ISP = ((ArrayList)Lista[i])[2].ToString();
        }

        TotalP += Convert.ToDouble(((ArrayList)((ArrayList)((ArrayList)((ArrayList)Lista[i])[4]))[0])[0]);
        TotalL += Convert.ToDouble(((ArrayList)((ArrayList)((ArrayList)((ArrayList)Lista[i])[4]))[1])[0]);

        lblTPartners.Text = ContPAR.ToString();
        lblTISP.Text = ContISP.ToString();
        lblTPedidos.Text = TotalP.ToString();
        lblTLicencias.Text = TotalL.ToString();
      }
    }

    protected void ReiniciarDatos(DateTime F,int P,bool MostrarGrafica)
    {
      ArrayList DatosISP = new ArrayList();
      int IdPartner = -1;
      DateTime FechaTemp = Convert.ToDateTime("1900-01-01 00:00:00");
      string CulturaActual = string.Empty;

      //Para trabajar con fechas forzamos la cultura es-ES porque como puede poner idioma inglés, al cambiar la cultura cambia el tipo 
      //de fecha de dd/mm/yyyy a mm/dd/yyyy y da problemas. Al final de operar con fechas devolvermos la cultura que tuvier antes.
      if (HttpContext.Current.Request.UserLanguages != null && HttpContext.Current.Request.UserLanguages.Length > 0)
        CulturaActual = HttpContext.Current.Request.UserLanguages[0];
      else
        CulturaActual = Thread.CurrentThread.CurrentCulture.Name;

      Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("es-ES");
      Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("es-ES");

      if (F != Convert.ToDateTime("1900-01-01 00:00:00"))
        FechaTemp = F;

      if (P != -1)
        IdPartner = P;

        if(IdPartner == -1)
          sqlUso.SelectParameters["IdPartner"].DefaultValue = string.Empty;
        else
          sqlUso.SelectParameters["IdPartner"].DefaultValue = IdPartner.ToString();

      if (FechaTemp == Convert.ToDateTime("1900-01-01 00:00:00"))
        sqlUso.SelectParameters["Fecha"].DefaultValue = string.Empty;
      else
        sqlUso.SelectParameters["Fecha"].DefaultValue = FechaTemp.ToString("yyyy-MM-dd HH:mm:ss");

      gvUso.DataBind();

      DatosISP = ValoresGrafica(FechaTemp,IdPartner);
      CantidadISP = DatosISP.Count;

      //Dejamos la cultura como estaba
      Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(CulturaActual);
      Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(CulturaActual);

      if (CantidadISP > 0)
      {
        if (MostrarGrafica && lbOcultar.CssClass == "fas fa-eye-slash")
        {
          upCanvas.Visible = true;
          lbZoom.Visible = true;
          lbOcultar.CssClass = "fas fa-eye-slash";

          string Titulo = Resources.dashboard.ResourceManager.GetString("GraficaUsoTitulo", CultureInfo.CreateSpecificCulture(Idioma.DameIdioma()));

          CargarGrafica(canvas.ClientID, Titulo, 2, DatosISP);
        }
        else
        {
          upCanvas.Visible = false;
          lbZoom.Visible = false;
          lbOcultar.CssClass = "far fa-eye";
        }

        CargarTotales(DatosISP);
      }
      else
      {
        upCanvas.Visible = false;
        lbZoom.Visible = false;
        lbOcultar.CssClass = "far fa-eye";
        lblTPartners.Text = lblTISP.Text = lblTPedidos.Text = lblTLicencias.Text = Resources.dashboard.ResourceManager.GetString("LiteralSinDatos", CultureInfo.CreateSpecificCulture(Idioma.DameIdioma()));
      }
    }

    protected void CargarDesplegablePartners()
    {
      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = Con.CreateCommand();

      try
      {
        Con.Open();
        Com.CommandType = System.Data.CommandType.StoredProcedure;
        Com.CommandText = "Partners_DameDatos";
        Com.CommandTimeout = 300;

        ddlPartners.Items.Clear();

        ListItem Elemento = new ListItem();
        Elemento.Value = "-1";
        Elemento.Text = Resources.dashboard.ResourceManager.GetString("LiteralPartnersTodos", CultureInfo.CreateSpecificCulture(Idioma.DameIdioma()));
        ddlPartners.Items.Add(Elemento);

        SqlDataReader Rst = Com.ExecuteReader();

        while (Rst.Read())
        {
          Elemento = new ListItem();
          Elemento.Value = Rst["IdPartner"].ToString();
          Elemento.Text = Rst["Nombre"].ToString();
          ddlPartners.Items.Add(Elemento);
        }
      }

      finally
      {
        if (Con.State == ConnectionState.Open)
          Con.Close();

        Com.Dispose();
        Con.Dispose();
      }
    }

    protected void ExportarCSV(string Nombre)
    {
      string CulturaActual = string.Empty;

      //Para trabajar con fechas forzamos la cultura es-ES porque como puede poner idioma inglés, al cambiar la cultura cambia el tipo 
      //de fecha de dd/mm/yyyy a mm/dd/yyyy y da problemas. Al final de operar con fechas devolvermos la cultura que tuvier antes.
      if (HttpContext.Current.Request.UserLanguages != null && HttpContext.Current.Request.UserLanguages.Length > 0)
        CulturaActual = HttpContext.Current.Request.UserLanguages[0];
      else
        CulturaActual = Thread.CurrentThread.CurrentCulture.Name;

      Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("es-ES");
      Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("es-ES");

      gvUso.AllowPaging = false;

      if (txtFechaUso.Text != string.Empty)
      {
        DateTime Fecha = Convert.ToDateTime("1900-01-01 00:00:00");

        if (!DateTime.TryParse(txtFechaUso.Text, out Fecha))
          throw new Exception(Resources.dashboard.ResourceManager.GetString("LiteralErrorFecha", Thread.CurrentThread.CurrentCulture));

        sqlUso.SelectParameters["Fecha"].DefaultValue = Fecha.ToString("yyyy/MM/dd HH:mm:dd");
      }

      if (txtFechaUso.Text == string.Empty)
      {
        if (ddlPartners.SelectedValue == "-1")
          sqlUso.SelectParameters["IdPartner"].DefaultValue = string.Empty;
        else
          sqlUso.SelectParameters["IdPartner"].DefaultValue = ddlPartners.SelectedValue;
      }

      gvUso.DataBind();

      string NombreFichero = Nombre + "_";
      NombreFichero += DateTime.Now.Year.ToString().PadLeft(2, '0') + DateTime.Now.Month.ToString().PadLeft(2, '0') + DateTime.Now.Day.ToString().PadLeft(2, '0');
      NombreFichero += DateTime.Now.Hour.ToString().PadLeft(2, '0') + DateTime.Now.Minute.ToString().PadLeft(2, '0') + DateTime.Now.Second.ToString().PadLeft(2, '0');
      NombreFichero += ".csv";

      //Dejamos la cultura como estaba
      Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(CulturaActual);
      Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(CulturaActual);

      Response.Clear();
      Response.Buffer = true;
      Response.AddHeader("Content-Disposition", "attachment;filename=" + NombreFichero);
      Response.Charset = "UTF-8";
      Response.ContentEncoding = Encoding.Default;
      Response.ContentType = "application/text";

      StringBuilder sb = new StringBuilder();
      for (int k = 0; k < gvUso.Columns.Count; k++)
      {
        if (k > 0)
          sb.Append(';');

        sb.Append(gvUso.Columns[k].HeaderText);
      }
      sb.Append("\r\n");
      for (int i = 0; i < gvUso.Rows.Count; i++)
      {
        for (int k = 0; k < gvUso.Columns.Count; k++)
        {
          if (k > 0)
            sb.Append(';');

          if(gvUso.Rows[i].Cells[k].Controls[1].GetType().Name.ToString()=="LinkButton")
            sb.Append(((LinkButton)gvUso.Rows[i].Cells[k].Controls[1]).Text);
          else
            sb.Append(((Label)gvUso.Rows[i].Cells[k].Controls[1]).Text);
        }
        sb.Append("\r\n");

      }
      Response.Output.Write(sb.ToString());

      Response.Flush();
      Response.End();
    }

    protected void ZoomCanvas(HtmlGenericControl Canvas,bool Activar,int NumISP)
    {
      string AnchoDefecto = "1200";
      string Ancho = "1200";

      if ((NumISP * 80) > 1200 && Activar)
        Ancho = (NumISP * 80).ToString();
      else
        Ancho = AnchoDefecto;

      Canvas.Attributes.Add("width", Ancho);
    }
  }
}