﻿<%@ Page Title="" Language="C#" MasterPageFile="~/res/LIVINGSAFE.Master" AutoEventWireup="true" CodeBehind="massive.aspx.cs" Inherits="LIVINGSAFE_SiteCallCenter.res.massive" %>
<asp:Content ID="cOrdersHead" ContentPlaceHolderID="cpHead" runat="server">
</asp:Content>
<asp:Content ID="cOrdersBody" ContentPlaceHolderID="cpBody" runat="server">
  <asp:ScriptManager ID="smMassive" runat="server"></asp:ScriptManager>
  <script>
    var a;

    function CalcularPorcentaje() {
	    var total=parseInt(document.getElementById('<%=Total.ClientID%>').innerText);
	    var valor=parseInt(document.getElementById('<%=Valor.ClientID%>').innerText);
	    var porcentaje = (valor*100)/total;

	    document.getElementById('<%=barra.ClientID%>').style.width=porcentaje + '%';
	
      if (valor >= total) {
        clearInterval(a);
      }
    }
  </script>

  <div class="container-fluid section-contenido">
    <div class="row espacio_inferior">
      <div class="col-md-10">
        <div class="txt_titulopagina_med"><asp:Literal ID="litTitulo" runat="server" EnableViewState="false" Text='<%$ Resources:massive,litTitulo %>'></asp:Literal></div>
      </div>
      <div class="col-6 offset-3" style="height:60px; padding:0px;padding-top:18px; text-align:center; border:1px solid #5FA8FC;">
        <asp:UpdatePanel runat="server" ID="upMultiContratoGeneral" style="padding:0px;" UpdateMode="Conditional">
          <Triggers>
            <asp:AsyncPostBackTrigger ControlID="lbMultiContratoAceptar" />
            <asp:AsyncPostBackTrigger ControlID="lbMultiContratoCancelar" />
          </Triggers>
          <ContentTemplate>
            <asp:CheckBox runat="server" ID="chkMultiContratoGeneral" AutoPostBack="true" Text='<%$ Resources:massive,chkMultiContratoGeneral %>' CssClass="check_multicontrato" OnCheckedChanged="chkMultiContratoGeneral_CheckedChanged" />
          </ContentTemplate>
        </asp:UpdatePanel>
      </div>
      <div class="col-md-2">
        <asp:UpdatePanel runat="server" ID="upReiniciar" style="padding:0px;" UpdateMode="Conditional">
          <Triggers>
            <asp:AsyncPostBackTrigger ControlID="tmrTemporizador" EventName="Tick" />
          </Triggers>
          <ContentTemplate>
            <asp:LinkButton runat="server" ID="lbNuevaAlta" Visible="false" Text='<%$ Resources:massive,lbNuevaAlta %>' CssClass="lnk_passchange aire10_sup_inf" OnClick="lbNuevaAlta_Click"></asp:LinkButton>
          </ContentTemplate>
        </asp:UpdatePanel>
      </div>

      <div class="col-md-12" style="height:30px; padding:0px;"></div>

      <asp:UpdatePanel runat="server" ID="upFichero" class="col-md-7 offset-2" style="padding:0px;" UpdateMode="Conditional">
        <Triggers>
          <asp:PostBackTrigger ControlID="lbProcesar" />
        </Triggers>
        <ContentTemplate>
          <asp:FileUpload runat="server" ID="fuMasivo" Width="100%" style="border:1px solid #DEDEDE;" />
        </ContentTemplate>
      </asp:UpdatePanel>

      <asp:UpdatePanel runat="server" ID="upProcesar" class="col-md-1 text-center" style="padding:0px;" UpdateMode="Conditional">
        <Triggers>
          <asp:PostBackTrigger ControlID="lbProcesar" />
        </Triggers>
        <ContentTemplate>
          <asp:LinkButton runat="server" ID="lbProcesar" Text='<%$ Resources:massive,lbProcesar %>' CssClass="lnk_passchange" style="margin-top:-2px;" OnClick="lbProcesar_Click" CausesValidation="false" OnClientClick="this.style.visibility='hidden';document.getElementById('btnProgreso').style.visibility='visible';" />
          <button id="btnProgreso" class="CC_btnAzul32" type="button" disabled style="visibility:hidden; left:7px; position:absolute;">
            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
            <span class="sr-only">Loading...</span>
          </button>
        </ContentTemplate>
      </asp:UpdatePanel>

      <div class="col-lg-8 offset-lg-2" style="padding:0px; margin-top:30px;">
        <div class="col-12 text-left CC_tituloProgreso margenes_no"><asp:Literal runat="server" ID="litAnalisis" EnableViewState="false" Text='<%$ Resources:massive,litAnalisis %>'></asp:Literal></div>
        <div class="row" style="margin-right:0px; margin-left:0px; padding-top:25px;">
          <div class="col-6 col-md-3 text-center">
            <img src="/images/icon_registros_total.png" width="120" height="120" class="img-fluid" alt="TOTAL" />
            <div><asp:Literal runat="server" ID="litAnalizados" EnableViewState="false" Text='<%$ Resources:massive,litAnalizados %>'></asp:Literal></div>
            <div class="CC_ProvNumero"><asp:Label runat="server" ID="lblRegsTOTAL" Text="0" CssClass="CC_ProvNumero"></asp:Label></div>
          </div>
          <div class="col-6 col-md-3 text-center">
            <img src="/images/icon_registros_ok.png" width="120" height="120" class="img-fluid" alt="OK" />
            <div><asp:Literal runat="server" ID="litCorrectos" EnableViewState="false" Text='<%$ Resources:massive,litCorrectos %>'></asp:Literal></div>
            <div class="CC_ProvNumero"><asp:Label runat="server" ID="lblRegsOK" Text="0" CssClass="CC_ProvNumero"></asp:Label></div>
          </div>
          <div class="col-6 col-md-3 text-center">
            <img src="/images/icon_registros_no.png" width="120" height="120" class="img-fluid" alt="NOK" />
            <div><asp:Literal runat="server" ID="litIncorrectos" EnableViewState="false" Text='<%$ Resources:massive,litIncorrectos %>'></asp:Literal></div>
            <div class="CC_ProvNumero"><asp:Label runat="server" ID="lblRegsNOK" Text="0" CssClass="CC_ProvNumero"></asp:Label></div>
          </div>
          <div class="col-6 col-md-3 text-center">
            <img src="/images/icon_registros_vacio.png" width="120" height="120" class="img-fluid" alt="EMPTY" />
            <div><asp:Literal runat="server" ID="litVacios" EnableViewState="false" Text='<%$ Resources:massive,litVacios %>'></asp:Literal></div>
            <div class="CC_ProvNumero"><asp:Label runat="server" ID="lblRegsVacios" Text="0" CssClass="CC_ProvNumero"></asp:Label></div>
          </div>
          <div class="col-12 text-center CC_txtProcesar" style="padding-top:15px; padding-bottom:15px;"><asp:Label runat="server" ID="lblRegsProcesar" Text='<%$ Resources:massive,litResultadoDefecto %>'></asp:Label></div>
        </div>
      </div>

      <div id="divProgreso" runat="server" class=" col-lg-8 offset-lg-2" style="margin-top:40px; padding:0px 10px 0px 10px;">
  		<div class="col-12 text-left margenes_no CC_tituloProgreso" style="margin-bottom:15px;"><asp:Literal runat="server" ID="litProcesando" EnableViewState="false" Text='<%$ Resources:massive,litProcesando %>'></asp:Literal></div>
        <div class="progress">
          <div id="barra" runat="server" class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width: 0%"></div>
        </div>
        <asp:UpdatePanel runat="server" ID="upTimer" class="col-md-12 text-right margenes_no" style="padding:0px;">
          <Triggers>
            <asp:AsyncPostBackTrigger ControlID="tmrTemporizador" EventName="Tick" />
          </Triggers>
          <ContentTemplate>
            <asp:Label runat="server" ID="Valor" Text="0" />
            /
            <asp:Label runat="server" ID="Total" Text="0" />
            <asp:Literal runat="server" ID="litRegistrosProcesados" Text='<%$ Resources:massive,litRegistrosProcesados %>'></asp:Literal>
            <asp:Timer runat="server" ID="tmrTemporizador" Interval="2000" OnTick="tmrTemporizador_Tick"></asp:Timer>
          </ContentTemplate>
        </asp:UpdatePanel>
      </div>

      <asp:UpdatePanel runat="server" ID="upMensajeFinal" UpdateMode="Conditional" class="col-md-8 offset-2" style="padding:0px;">
        <Triggers>
          <asp:AsyncPostBackTrigger ControlID="tmrTemporizador" EventName="Tick" />
        </Triggers>
        <ContentTemplate>
          <div class="col-12 text-center CC_txtProcesado" style="padding-top:10px; padding-bottom:10px;"><asp:Label runat="server" ID="lblMensajeFinal"></asp:Label></div>
        </ContentTemplate>
      </asp:UpdatePanel>

      <asp:UpdatePanel runat="server" ID="upRespuesta" UpdateMode="Conditional" class="col-md-12 ml-auto align-content-end">
        <Triggers>
          <asp:PostBackTrigger ControlID="lbProcesar" />
          <asp:PostBackTrigger ControlID="lbDescargarExcel" />
          <asp:PostBackTrigger ControlID="lbDescargarLOG" />
          <asp:AsyncPostBackTrigger ControlID="tmrTemporizador" EventName="Tick" />
        </Triggers>
        <ContentTemplate>
          <div id="divInformeErrores" runat="server" visible="false" class=" col-lg-8 offset-lg-2" style="margin-top:15px; padding:0px 10px 0px 10px;">
  	        <div class="col-12 text-left margenes_no CC_tituloProgreso"><asp:Literal runat="server" ID="litInformeErrores" EnableViewState="false" Text='<%$ Resources:massive,litInformeErrores %>'></asp:Literal></div>
            <div class="col-12 text-left margenes_no"><asp:Literal runat="server" ID="litInformeErroresDescripcion" EnableViewState="false" Text='<%$ Resources:massive,litInformeErroresDescripcion %>'></asp:Literal></div>
		        <div class="col-12 text-left margenes_no">
              <asp:Label runat="server" ID="lblRespuesta" CssClass="terminal"></asp:Label>
		        </div>
            <div class="row" style="padding-top:25px; padding-bottom:25px;">
              <div class="col-lg-3 col-md-6 offset-lg-3 text-center" style="margin-bottom:10px;"><asp:LinkButton runat="server" ID="lbDescargarExcel" Text='<%$ Resources:massive,lbDescargarExcel %>'  CssClass="lnk_passchange" OnClick="lbDescargarExcel_Click"></asp:LinkButton></div>
              <div class="col-lg-3 col-md-6 text-center"><asp:LinkButton runat="server" ID="lbDescargarLOG" Text='<%$ Resources:massive,lbDescargarLOG %>'  CssClass="lnk_passchange" OnClick="lbDescargarLOG_Click"></asp:LinkButton></div>
            </div>
          </div>
        </ContentTemplate>
      </asp:UpdatePanel>
    </div>
  </div>

  <div id="divMultiContrato" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="ModalAviso" aria-hidden="true">
    <asp:UpdatePanel runat="server" ID="upMultiContrato">
      <Triggers>
        <asp:AsyncPostBackTrigger ControlID="lbMultiContratoAceptar" />
        <asp:AsyncPostBackTrigger ControlID="lbMultiContratoCancelar" />
        <asp:AsyncPostBackTrigger ControlID="chkMultiContratoGeneral" />
        <asp:AsyncPostBackTrigger ControlID="chkMultiContratoValidacion" />
      </Triggers>
      <ContentTemplate>
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-body" style="padding-top:30px; padding-bottom:30px;">
              <asp:Literal ID="litMultiContratoAviso" runat="server" Text='<%$ Resources:massive,litMultiContratoAviso %>' EnableViewState="false" />
              <br /><br />
              <div style="text-align:center; margin-bottom:15px;">
                <asp:CheckBox runat="server" ID="chkMultiContratoValidacion" Text='<%$ Resources:massive,chkMultiContratoValidacion %>' CssClass="check_multicontrato" />
              </div>
              <div style="text-align:center;">
                <asp:LinkButton runat="server" ID="lbMultiContratoAceptar" Text='<%$ Resources:massive,lbMultiContratoAceptar %>' CssClass="lnk_passchange" OnClick="lbMultiContratoAceptar_Click"></asp:LinkButton>
                <asp:LinkButton runat="server" ID="lbMultiContratoCancelar" Text='<%$ Resources:massive,lbMultiContratoCancelar %>' CssClass="lnk_passchange" style="margin-left:20px;" OnClick="lbMultiContratoCancelar_Click"></asp:LinkButton>
              </div>
            </div>
          </div>
        </div>
      </ContentTemplate>
    </asp:UpdatePanel>
  </div>

  <asp:UpdatePanel runat="server" ID="upAlerta" UpdateMode="Conditional">
      <Triggers>
        <asp:AsyncPostBackTrigger ControlID="lbMultiContratoAceptar" />
      </Triggers>
    <ContentTemplate>
      <div id="divAlerta" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="ModalAviso" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-body">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body" style="padding-top:0px; padding-bottom:0px;">
              <asp:Literal runat="server" ID="litCabecera" Text='<%$ Resources:massive,litCabecera %>' EnableViewState="false" />
              <asp:Label runat="server" ID="lblResultado" Text="w" />
              <br /><br />
            </div>
          </div>
        </div>
      </div>
    </ContentTemplate>
  </asp:UpdatePanel>
</asp:Content>

