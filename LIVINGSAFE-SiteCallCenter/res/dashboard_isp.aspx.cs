﻿using LIVINGSAFE_SiteCallCenter.App_Code;
using LIVINGSAFE_SiteCallCenter.Clases;
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.UI;

namespace LIVINGSAFE_SiteCallCenter.res
{
  public partial class dashboard_isp : System.Web.UI.Page
  {
    cUtil Utilidad = new cUtil();
    cPermisos Permisos = new cPermisos();
    cIdiomas Idioma = new cIdiomas();
    cEncriptacion Encriptacion = new cEncriptacion();
    int Entidad = Convert.ToInt32(HttpContext.Current.User.Identity.Name);
    static int CantidadISP = 0;
    int IdISP = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
      try
      {
        int IdISPOperador = Convert.ToInt32(Utilidad.DameDatoOperador(Entidad, "IdISPOperador"));
        IdISP = DameIdISP(Request.QueryString["i"]);
        string Siglas = Utilidad.DameDatoISP(IdISPOperador, "Siglas");

        if (!Permisos.TienePermiso("PAGINADASHBOARD", Entidad) || Siglas.ToUpper()!="LIVINGSAFE")
          Response.Redirect("~/res/landing.aspx");

        if (!IsPostBack)
          ReiniciarDatos();
      }

      catch (Exception Exc)
      {
        string Msg = string.Empty;

        if (Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);
        else if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);
        else
        {
          if (Utilidad.EsEntornoDesarrollo())
            Msg = Exc.ToString();
          else
            Msg = Exc.Message.ToString();
        }

        lblResultado.Text = Msg;

        string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "MostrarErrorModif", Escript, true);
      }
    }

    protected void lbCalendario_Click(object sender, EventArgs e)
    {
      string CulturaActual = string.Empty;

      try
      {
        ReiniciarDatos();
      }

      catch (Exception Exc)
      {
        string Msg = string.Empty;

        if (Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);
        else if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);
        else
        {
          if (Utilidad.EsEntornoDesarrollo())
            Msg = Exc.ToString();
          else
            Msg = Exc.Message.ToString();
        }

        lblResultado.Text = Msg;

        string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "MostrarErrorModif", Escript, true);
      }

      finally
      {
        //Dejamos la cultura como estaba
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(CulturaActual);
        Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(CulturaActual);
      }
    }

    protected void lbHistoricoLimpiar_Click(object sender, EventArgs e)
    {
      try
      {
        txtHistoricoFI.Text = txtHistoricoFF.Text = string.Empty;
        ReiniciarDatos();
      }

      catch (Exception Exc)
      {
        string Msg = string.Empty;

        if (Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);
        else if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);
        else
        {
          if (Utilidad.EsEntornoDesarrollo())
            Msg = Exc.ToString();
          else
            Msg = Exc.Message.ToString();
        }

        lblResultado.Text = Msg;

        string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "MostrarErrorModif", Escript, true);
      }
    }

    protected void lbProductosLimpiar_Click(object sender, EventArgs e)
    {
      try
      {
        txtFechaUso.Text = string.Empty;
        ReiniciarDatos();
      }

      catch (Exception Exc)
      {
        string Msg = string.Empty;

        if (Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);
        else if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);
        else
        {
          if (Utilidad.EsEntornoDesarrollo())
            Msg = Exc.ToString();
          else
            Msg = Exc.Message.ToString();
        }

        lblResultado.Text = Msg;

        string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "MostrarErrorModif", Escript, true);
      }
    }

    protected void lbVolver_Click(object sender, EventArgs e)
    {
      Response.Redirect("~/res/dashboard.aspx");
    }



    protected ArrayList DameDatosHistoricoISP(DateTime FI,DateTime FF,int IdISP)
    {
      ArrayList RegistrosISP = new ArrayList(); // Cada registro contiene día del histórico del ISP
      ArrayList FotosDias = null; // Datos del ISP, partner, y una lista con los valores de cada línea
      ArrayList Valores = new ArrayList(); // ArrayList de hasta 4 líneas con los datos de cada línea
      ArrayList DatosLinea = new ArrayList(); //Datos para pintar la línea

      ArrayList Colores = new ArrayList();
      Colores.Add("#FF421C;#91250F"); //Naranja
      Colores.Add("#CC1460;#8E0E43"); //Rosa
      Colores.Add("#096829;#053D18"); //Verde
      Colores.Add("#07577F;#04344C"); //Azul
      Colores.Add("#FFD800;#AD9000"); //Amarillo
      Colores.Add("#E00003;#A80002"); //Rojo

      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = null;

      try
      {
        Con.Open();
        Com = Con.CreateCommand();
        Com.CommandType = System.Data.CommandType.StoredProcedure;
        Com.CommandText = "Dashboard_ISPHistoricoDiario";
        Com.CommandTimeout = 300;

        if (FI != Convert.ToDateTime("1900-01-01 00:00:00"))
          Com.Parameters.Add("@FI", SqlDbType.DateTime).Value = FI.ToString("yyyy-MM-dd HH:mm:ss");

        if (FF != Convert.ToDateTime("1900-01-01 00:00:00"))
          Com.Parameters.Add("@FF", SqlDbType.DateTime).Value = FF.ToString("yyyy-MM-dd HH:mm:ss");

        Com.Parameters.Add("@IdISP", SqlDbType.Int).Value = IdISP;

        SqlDataReader Rst = Com.ExecuteReader();

        while(Rst.Read())
        {
          FotosDias = new ArrayList();

          FotosDias.Add(Rst["NombrePartner"].ToString());
          FotosDias.Add(Rst["NombreISP"].ToString());
          FotosDias.Add(Rst["Siglas"].ToString());
          FotosDias.Add(Rst["FechaInforme"].ToString());
          FotosDias.Add(Rst["FechaInformeTXT"].ToString());

          Valores = new ArrayList();

          DatosLinea = new ArrayList();
          DatosLinea.Add(Colores[0]);
          DatosLinea.Add("GraficaUsoLeyenda1");
          DatosLinea.Add(Rst["TotalLicencias"].ToString());
          Valores.Add(DatosLinea);

          DatosLinea = new ArrayList();
          DatosLinea.Add(Colores[1]);
          DatosLinea.Add("GraficaUsoLeyenda2");
          DatosLinea.Add(Rst["TotalPedidos"].ToString());
          Valores.Add(DatosLinea);

          FotosDias.Add(Valores);
          
          FotosDias.Add(Rst["NombrePnInLargo"].ToString());
          FotosDias.Add(Rst["NombrePnSiCorto"].ToString());
          FotosDias.Add(Rst["ISPActivo"].ToString());

          RegistrosISP.Add(FotosDias);
        }
      }

      finally
      {
        if (Com != null)
          Com.Dispose();

        if (Con.State == ConnectionState.Open)
          Con.Close();

        Con.Dispose();
      }

      return RegistrosISP;
    }

    protected ArrayList DameDatosProductosISP(DateTime Fecha, int IdISP)
    {
      ArrayList ListaISP = new ArrayList(); // Cada registro contiene un ArryList DatosISP
      ArrayList DatosISP = null; // Datos del ISP: NombrePartner string, NombreISP string, SiglasISP string, FechaFechaInforme datetime, ListaValores ArrayList
      ArrayList ListaValores = null; // Cada registro contiene un ArrayList Valores
      ArrayList Valores = null; //Valores de la gráfica para ese ISP, tantos como NumCols: Valor int, Leyenda string, Color string y ColorBorde string

      ArrayList Colores = new ArrayList();
      Colores.Add("#FF421C;#91250F"); //Naranja
      Colores.Add("#CC1460;#8E0E43"); //Rosa
      Colores.Add("#096829;#053D18"); //Verde
      Colores.Add("#07577F;#04344C"); //Azul
      Colores.Add("#FFD800;#AD9000"); //Amarillo
      Colores.Add("#E00003;#A80002"); //Rojo

      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = null;

      try
      {
        Con.Open();
        Com = Con.CreateCommand();
        Com.CommandType = System.Data.CommandType.StoredProcedure;
        Com.CommandText = "Dashboard_ISPTotalesProductos";
        Com.CommandTimeout = 300;

        if (Fecha != Convert.ToDateTime("1900-01-01 00:00:00"))
          Com.Parameters.Add("@Fecha", SqlDbType.DateTime).Value = Fecha.ToString("yyyy-MM-dd HH:mm:ss");

        Com.Parameters.Add("@IdISP", SqlDbType.Int).Value = IdISP;

        SqlDataReader Rst = Com.ExecuteReader();

        while (Rst.Read())
        {
          DatosISP = new ArrayList();

          ListaValores = new ArrayList();

          DatosISP.Add(Rst["SKU"].ToString());
          DatosISP.Add(Rst["NombreCorto"].ToString());
          DatosISP.Add(Rst["Siglas"].ToString());
          DatosISP.Add(Rst["FechaInforme"].ToString());

          Valores = new ArrayList();
          Valores.Add(Rst["TotalLicencias"].ToString());
          Valores.Add(Resources.dashboard.ResourceManager.GetString("GraficaDesgloseProductosISP", CultureInfo.CreateSpecificCulture(Idioma.DameIdioma())));
          Valores.Add(((string[])Colores[5].ToString().Split(';'))[0]);
          Valores.Add(((string[])Colores[5].ToString().Split(';'))[1]);
          ListaValores.Add(Valores);

          //Para la gráfica de productos no lo necesito pero eso este mismo método para los totales y ahí si lo necesiot
          Valores = new ArrayList();
          Valores.Add(Rst["TotalPedidos"].ToString());
          Valores.Add(Resources.dashboard.ResourceManager.GetString("GraficaDesgloseProductosISP2", CultureInfo.CreateSpecificCulture(Idioma.DameIdioma())));
          Valores.Add(((string[])Colores[4].ToString().Split(';'))[0]);
          Valores.Add(((string[])Colores[4].ToString().Split(';'))[1]);
          ListaValores.Add(Valores);

          DatosISP.Add(ListaValores);

          DatosISP.Add(Rst["SKUNombreCorto"].ToString());
          DatosISP.Add(Rst["NombrePartner"].ToString());
          DatosISP.Add(Rst["NombreISP"].ToString());
          DatosISP.Add(Rst["Direccion"].ToString());
          DatosISP.Add(Rst["Logo"].ToString());
          DatosISP.Add(Rst["ISPActivo"].ToString());

          ListaISP.Add(DatosISP);
        }
      }

      finally
      {
        if (Com != null)
          Com.Dispose();

        if (Con.State == ConnectionState.Open)
          Con.Close();

        Con.Dispose();
      }

      return ListaISP;
    }

    protected void CargarGraficaHistoricoISP(string NombreGrafica, string Titulo, int NumCols, ArrayList Lista)
    {
      //Este método va a servir para cualquier gráfica de líneas. Se le pasa en un ArrayList el nombre del canvas, el total de lineas por cada ISP (min: 1, max: 4), el título y el conjunto de datos.
      //Los colores de las 4 posibles columnas vienen en la lista de datos hasta un máximo de 4 líneas por gráfica.
      string CulturaActual = string.Empty;

      if (HttpContext.Current.Request.UserLanguages != null && HttpContext.Current.Request.UserLanguages.Length > 0)
        CulturaActual = HttpContext.Current.Request.UserLanguages[0];
      else
        CulturaActual = Thread.CurrentThread.CurrentCulture.Name;

      Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("es-ES");
      Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("es-ES");

      //Cargamos los valores que irán en la gráfica (máximo 4 líneas)
      ArrayList Valores = new ArrayList();
      string Datos = string.Empty;
      int ContadorValores = 0;

      //Cogemos la lista de valores de la primera fila para saber cuántas líneas vamos a tener (1-4)
      Valores = (ArrayList)((ArrayList)Lista[0])[5];

      for(int i=0; i<Valores.Count; i++)
      {
        ContadorValores += 1;
        Datos += "var datos" + i.ToString() + " = [];";
      }

      //Hemos inicializados tantos arrays de javascript de datos para gráfica como tipos de valores trae cada gráfica. Ahora rellenamos las gráficas.
      for (int i = 0; i < Lista.Count; i++)
      {
        Valores = new ArrayList();
        Valores = (ArrayList)((ArrayList)Lista[i])[5];

        for (int r = 0; r < Valores.Count; r++)
        {
          Datos += "datos" + r.ToString() + ".push({ t: '" + Convert.ToDateTime(((ArrayList)Lista[i])[4]).ToString("yyyy-MM-dd HH:mm:ss") + "', y: '" + ((ArrayList)Valores[r])[2] + "' });";
        }
      }

      string Escript = Datos + @"

		    var ctx = document.getElementById('" + NombreGrafica + @"').getContext('2d');

		    var color = Chart.helpers.color;
		    var cfg = {
			    data: {
				    datasets: 
            [
        ";

      //Para tantas líneas que como tengamos
      for (int i = 0; i < ContadorValores; i++)
      {
        if (i > 0)
          Escript += ",";

        string[] colores = (((ArrayList)Valores[i])[0]).ToString().Split(';');

        Escript += "{";
        Escript += "label: '" + Resources.dashboard.ResourceManager.GetString(((ArrayList)Valores[i])[1].ToString(), CultureInfo.CreateSpecificCulture(Idioma.DameIdioma())) + "',";
        Escript += "backgroundColor: \"" + colores[0] + "\",";
        Escript += "borderColor: \"" + colores[0] + "\",";
        Escript += "data: datos" + i.ToString() + ",";
        Escript += "type: 'line',";
        Escript += "pointRadius: 0,";
        Escript += "fill: false,";
        Escript += "lineTension: 0,";
        Escript += "borderWidth: 2";
        Escript += "}";
      }

      Escript += @"
            ]
			    },
			    options: {
				    animation:    { duration: 0 },
            legend:       { position: 'bottom', display:true },
            title:        { display: true, text: '" + Titulo + @"' },
				    scales:       {
					                  xAxes: [{
						                  type: 'time',
						                  distribution: 'series',
						                  offset: true,
						                  ticks: {
							                  major: {
								                  enabled: true,
								                  fontStyle: 'bold'
							                  },
							                  source: 'data',
							                  autoSkip: true,
							                  autoSkipPadding: 75,
							                  maxRotation: 0,
							                  sampleSize: 100,
                                time: { unit: 'day' }
						                  },
						                  afterBuildTicks: function(scale, ticks) {
							                  var majorUnit = scale._majorUnit;
							                  var firstTick = ticks[0];
							                  var i, ilen, val, tick, currMajor, lastMajor;

							                  val = moment(ticks[0].value);
							                  //if ((majorUnit === 'minute' && val.second() === 0)
									                //  || (majorUnit === 'hour' && val.minute() === 0)
									                //  || (majorUnit === 'day' && val.hour() === 9)
									                //  || (majorUnit === 'month' && val.date() <= 3 && val.isoWeekday() === 1)
									                //  || (majorUnit === 'year' && val.month() === 0)) {
								                 // firstTick.major = true;
							                  //} else {
								                 // firstTick.major = false;
							                  //}
                                firstTick.major = true;
							                  lastMajor = val.get(majorUnit);

							                  for (i = 1, ilen = ticks.length; i < ilen; i++) {
								                  tick = ticks[i];
								                  val = moment(tick.value);
								                  currMajor = val.get(majorUnit);
								                  tick.major = currMajor !== lastMajor;
								                  lastMajor = currMajor;
							                  }
							                  return ticks;
						                  }
					                  }],
					                  yAxes: [{
						                  gridLines: {
							                  drawBorder: false
						                  },
						                  scaleLabel: {
							                  display: false,
							                  labelString: 'Leyenda eje Y'
						                  }
					                  }]
				                  },
				    tooltips:     {
					                  intersect: false,
					                  mode: 'index',
					                  callbacks: {
						                  label: function(tooltipItem, myData) {
							                  var label = myData.datasets[tooltipItem.datasetIndex].label || '';
							                  if (label) {
								                    label += ': ';
							                  }
							                  label += parseFloat(tooltipItem.value).toFixed(2);
							                  return label;
						                  }
					                  }
				                  }
			    }
		    };

		    var chart = new Chart(ctx, cfg);
      ";

      ScriptManager.RegisterStartupScript(Page, Page.GetType(), "HisotricoISP", Escript, true);

      
      //Dejamos la cultura como estaba
      Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(CulturaActual);
      Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(CulturaActual);
    }

    protected void CargarGraficaProductosISP(string NombreGrafica, string Titulo, int NumCols, ArrayList Lista)
    {
      //Este método va a servir para cualquier gráfica. Se le pasa en un ArrayList el nombre del canvas, el total de columnas por cada ISP (min: 1, max: 4), el título y el conjunto de datos.
      //Los colores de las 4 posibles columnas están predefinidos en el ArrayList Colores y se usarán tantos como columnas se procesen.
      //El conjunto de datos será otro ArrayList donde vendrán para cada ISP tantas líneas como NumCols cada una de las cuales traerá los valores separados por coma y su leyenda.

      //Cargamos la fecha del informe. Para trabajar con fechas forzamos la cultura es-ES porque como puede poner idioma inglés, al cambiar la cultura cambia el tipo 
      //de fecha de dd/mm/yyyy a mm/dd/yyyy y da problemas. Al final de operar con fechas devolvermos la cultura que tuvier antes.

      string CulturaActual = string.Empty;

      if (HttpContext.Current.Request.UserLanguages != null && HttpContext.Current.Request.UserLanguages.Length > 0)
        CulturaActual = HttpContext.Current.Request.UserLanguages[0];
      else
        CulturaActual = Thread.CurrentThread.CurrentCulture.Name;

      Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("es-ES");
      Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("es-ES");

      DateTime Fecha = Convert.ToDateTime(((ArrayList)Lista[0])[3]);

      //Cargamos los nombres de los ISPs
      string labels = string.Empty;

      for (int i = 0; i < Lista.Count; i++)
      {
        if (i > 0)
          labels += ",";

        labels += "'" + ((ArrayList)Lista[i])[0].ToString() + "'"; //SKU (Leyenda)
      }

      //Cargamos los valores de las columnas (máximo 4 columnas por gráfica)
      ArrayList ValoresTemp = new ArrayList();
      ArrayList Valores = new ArrayList();
      ArrayList Col = null;

      for (int i = 0; i < Lista.Count; i++)
      {
        Col = new ArrayList();

        for (int r = 0; r < ((ArrayList)((ArrayList)Lista[i])[4]).Count; r++)
        {
          Col.Add(((ArrayList)((ArrayList)((ArrayList)((ArrayList)Lista[i])[4]))[r])[0]);
        }

        ValoresTemp.Add(Col);
      }

      string valores = string.Empty;

      for (int r = 0; r < ((ArrayList)ValoresTemp[0]).Count; r++)
      {
        for (int i = 0; i < Lista.Count; i++)
        {
          if (i > 0)
            valores += ",";

          valores += ((ArrayList)ValoresTemp[i])[r].ToString();
        }

        Valores.Add(valores);
        valores = string.Empty;
      }

      //Cargamos las leyendas y los colores
      ArrayList Leyendas = new ArrayList();
      ArrayList Colores = new ArrayList();
      ArrayList ColoresBorde = new ArrayList();

      for (int r = 0; r < ((ArrayList)((ArrayList)Lista[0])[4]).Count; r++)
      {
        Leyendas.Add(((ArrayList)((ArrayList)((ArrayList)((ArrayList)Lista[0])[4]))[r])[1]);
        Colores.Add(((ArrayList)((ArrayList)((ArrayList)((ArrayList)Lista[0])[4]))[r])[2]);
        ColoresBorde.Add(((ArrayList)((ArrayList)((ArrayList)((ArrayList)Lista[0])[4]))[r])[3]);
      }

      string Escript2 = string.Empty;

      //El conjunto de datos devuelve totales de licencias y pedidos. Como sólo queremos mostrar el total de licencias, limitamos de manera manual y forzada
      //for (int i = 0; i < Valores.Count; i++)
      int Limitacion = 1;
      for (int i = 0; i < Limitacion; i++)
      {
        Escript2 += "{ label: '" + Leyendas[i] + "',   backgroundColor: '" + Colores[i] + "', borderColor: '" + ColoresBorde[i] + "', borderWidth: 1, data: [" + Valores[i] + "] },";
      }


      string Escript = @"
        var barChartData =
        {
          labels: [" + labels + @"],
          datasets:
          [" +
          Escript2
          + @"]
	      };

        window.onload = function ()
        {
		      var ctx = document.getElementById('" + NombreGrafica + @"').getContext('2d');

		      window.myBar = new Chart(ctx,
          {
			      type: 'bar',
			      data: barChartData,

			      options: { responsive:  false,
				                legend:      { position: 'bottom', display:true },
					              title:       { display: true, text: '" + Titulo + " " + Fecha.ToShortDateString() + @"' },
                        // El bloque animation pinta los valores encima de las barras
                        animation:   {
                                        duration: 1,
                                        onComplete: function() {
                                          var chartInstance = this.chart,
                                          ctx = chartInstance.ctx;
                                          ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                                          ctx.textAlign = 'center';
                                          ctx.textBaseline = 'bottom';

                                          this.data.datasets.forEach(function(dataset, i) {
                                              var meta = chartInstance.controller.getDatasetMeta(i);
                                              meta.data.forEach(function(bar, index) {
                                                var data = dataset.data[index];
                                                ctx.fillText(data, bar._model.x, bar._model.y - 5);
                                              });
                                          });
                                        }
                                    }

				             }
		      });
	      };
      ";

      ScriptManager.RegisterStartupScript(Page, Page.GetType(), "PropductosISP", Escript, true);

      //Dejamos la cultura como estaba
      Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(CulturaActual);
      Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(CulturaActual);
    }

    protected void CargarDatosISP(ArrayList Lista)
    {
      double TotalP = 0;
      double TotalL = 0;

      if (Lista.Count > 0)
      {
        for (int i = 0; i < Lista.Count; i++)
        {
          TotalL += Convert.ToDouble(((ArrayList)((ArrayList)((ArrayList)((ArrayList)Lista[i])[4]))[0])[0]);
          TotalP += Convert.ToDouble(((ArrayList)((ArrayList)((ArrayList)((ArrayList)Lista[i])[4]))[1])[0]);
        }

        lblTLicencias.Text = TotalL.ToString();
        lblTPedidos.Text = TotalP.ToString();

        lblFichaPartner.Text = (((ArrayList)((ArrayList)Lista[0]))[6]).ToString();
        lblFichaNombre.Text = (((ArrayList)((ArrayList)Lista[0]))[7]).ToString();

        if (!Convert.ToBoolean((((ArrayList)((ArrayList)Lista[0]))[10])))
          lblFichaNombre.Text += " " + Resources.dashboard.ResourceManager.GetString("LiteralDesactivado", CultureInfo.CreateSpecificCulture(Idioma.DameIdioma()));

        lblFichaDireccion.Text = (((ArrayList)((ArrayList)Lista[0]))[8]).ToString();
        imgFichaLogo.ImageUrl = "~/images/logos/" + (((ArrayList)((ArrayList)Lista[0]))[9]).ToString();
      }
      else
      {
        lblTPedidos.Text = lblTLicencias.Text = Resources.dashboard.ResourceManager.GetString("LiteralSinDatos", CultureInfo.CreateSpecificCulture(Idioma.DameIdioma()));
      }
    }

    protected void ReiniciarDatos()
    {
      ArrayList DatosHistoricoISP = new ArrayList();
      ArrayList DatosProductosISP = new ArrayList();
      DateTime FI = Convert.ToDateTime("1900-01-01 00:00:00");
      DateTime FF = Convert.ToDateTime("1900-01-01 00:00:00");
      DateTime F =  Convert.ToDateTime("1900-01-01 00:00:00");
      string CulturaActual = string.Empty;

      //Para trabajar con fechas forzamos la cultura es-ES porque como puede poner idioma inglés, al cambiar la cultura cambia el tipo 
      //de fecha de dd/mm/yyyy a mm/dd/yyyy y da problemas. Al final de operar con fechas devolvermos la cultura que tuviera antes.
      if (HttpContext.Current.Request.UserLanguages != null && HttpContext.Current.Request.UserLanguages.Length > 0)
        CulturaActual = HttpContext.Current.Request.UserLanguages[0];
      else
        CulturaActual = Thread.CurrentThread.CurrentCulture.Name;

      Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("es-ES");
      Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("es-ES");

      if (txtHistoricoFI.Text != string.Empty)
      {
        if (!DateTime.TryParse(txtHistoricoFI.Text, out FI))
          throw new Exception(Resources.dashboard.ResourceManager.GetString("LiteralErrorFecha", Thread.CurrentThread.CurrentCulture));
      }

      if (txtHistoricoFF.Text != string.Empty)
      {
        if (!DateTime.TryParse(txtHistoricoFF.Text, out FF))
          throw new Exception(Resources.dashboard.ResourceManager.GetString("LiteralErrorFecha", Thread.CurrentThread.CurrentCulture));
      }

      if (txtFechaUso.Text != string.Empty)
      {
        if (!DateTime.TryParse(txtFechaUso.Text, out F))
          throw new Exception(Resources.dashboard.ResourceManager.GetString("LiteralErrorFecha", Thread.CurrentThread.CurrentCulture));
      }

      //Pintamos gráfica de Histórico de ISP
      DatosHistoricoISP = DameDatosHistoricoISP(FI, FF, IdISP);
      CantidadISP = DatosHistoricoISP.Count;

      if (CantidadISP > 0)
      {
        upCanvasHistorico.Visible = true;
        string Titulo = Resources.dashboard.ResourceManager.GetString("GraficaHistoricoTitulo", CultureInfo.CreateSpecificCulture(Idioma.DameIdioma()));

        CargarGraficaHistoricoISP(canvasHistorico.ClientID, Titulo, 2, DatosHistoricoISP);
      }
      else
      {
        upCanvasHistorico.Visible = false;
      }

      //Pintamos gráfica de Productos de ISP
      DatosProductosISP = DameDatosProductosISP(F, IdISP);
      CantidadISP = DatosProductosISP.Count;

      if (CantidadISP > 0)
      {
        upCanvasProductos.Visible = true;
        string Titulo = Resources.dashboard.ResourceManager.GetString("GraficaProductosTitulo", CultureInfo.CreateSpecificCulture(Idioma.DameIdioma()));

        CargarGraficaProductosISP(canvasProductos.ClientID, Titulo, 2, DatosProductosISP);
      }
      else
      {
        upCanvasProductos.Visible = false;
      }

      //Cargamos los datos del ISP y los totales
      CargarDatosISP(DatosProductosISP);

      //Dejamos la cultura como estaba
      Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(CulturaActual);
      Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(CulturaActual);
    }

    protected int DameIdISP(string ISP)
    {
      ISP = Encriptacion.DescifrarAES(Convert.ToInt32(Utilidad.DameDatoOperador(Entidad, "IdISPOperador")), ISP);
      return Convert.ToInt32(ISP);
    }
  }
}
