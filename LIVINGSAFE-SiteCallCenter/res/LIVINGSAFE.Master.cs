﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LIVINGSAFE_SiteCallCenter.App_Code;
using LIVINGSAFE_SiteCallCenter.Clases;
using System.Text;

namespace LIVINGSAFE_SiteCallCenter.res
{
  public partial class LIVINGSAFE : System.Web.UI.MasterPage
  {
    cUtil Utilidad = new cUtil();
    cPermisos Permisos = new cPermisos();
    cIdiomas Idiomas = new cIdiomas();
    int Entidad = Convert.ToInt32(HttpContext.Current.User.Identity.Name);
    int IdISPDetectado = -1;


    protected void Page_Load(object sender, EventArgs e)
    {
      try
      {
        //Siempre que se carga la master comprobamos si hay que obligar al usuario a cambiar la contraseña.
        ForzarCambioPassword();

        if (!IsPostBack)
        {
          CargarListadoIdiomas();
          liMasivo.Visible = Permisos.TienePermiso("MENUOPCIONMASIVO",Entidad);
          liMasivoResponsive.Visible = Permisos.TienePermiso("MENUOPCIONMASIVO",Entidad);
					liOperadores.Visible= Permisos.TienePermiso("PAGINAOPERADORES", Entidad);
					liOperadoresResponsive.Visible= Permisos.TienePermiso("PAGINAOPERADORES", Entidad);
          liUso.Visible = Permisos.TienePermiso("MENUOPCIONUSO", Entidad);
          liUsoResponsive.Visible = Permisos.TienePermiso("MENUOPCIONUSO", Entidad);

          int IdISP = Convert.ToInt32(Utilidad.DameDatoOperador(Entidad, "IdISPOperador"));
          string Siglas = Utilidad.DameDatoISP(IdISP, "Siglas");


          if (!Permisos.TienePermiso("PAGINADASHBOARD", Entidad) || Siglas.ToUpper() != "LIVINGSAFE")
            lbDashboard.Visible = false;
          else
            lbDashboard.Visible = true;
        }

        cSesiones Sesion = new cSesiones();

        string URL = HttpContext.Current.Request.Url.Host;
        IdISPDetectado = Utilidad.DameIdISPPorDominio(URL);
        string Nombre = Utilidad.DameDatoOperador(Entidad, "NombreOperador");
        string TipoOperador = "Operador";

        Utilidad.InsertarCabeceras();

        lblNombre.Text = Nombre;

        //Validamos la sesión en BD
        if (Sesion.ValidarSesion(Entidad, TipoOperador, IdISPDetectado) == false)
        {
          cEncriptacion AES = new cEncriptacion(Utilidad.DameClaveCifradoISP(IdISPDetectado));

          string TipoUsuario = string.Empty;

          if (Request.Cookies["SesionTipo"] != null)
            TipoUsuario = AES.DescifrarAES(IdISPDetectado, Request.Cookies["SesionTipo"].Value.ToString());
          else
            TipoUsuario = "Desconocido";

          Utilidad.EscribeLOG(Convert.ToInt32(HttpContext.Current.User.Identity.Name), TipoUsuario, "LOGOUT de Operador: " + Entidad + ", forzado por caducidad de sesión (CALLCENTER)");

          Sesion.CaducaOperador();
          Sesion.CaducarSesion(Entidad, TipoOperador, IdISPDetectado);
          Response.Redirect("~/login.aspx");
        }

        ComprobarDominio();

				if (ViewState["NombreISP"] == null || ViewState["NombreISP"].ToString() == string.Empty)
					ViewState.Add("NombreISP", Utilidad.DameDatoISP(IdISPDetectado, "Nombre"));

				Page.Title = "LivingSafe - " + ViewState["NombreISP"];

        //CargarCirculo();
      }

      catch (SqlException Exc)
      {
        string Msg = string.Empty;

        if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);
        else
          Msg = Exc.Message.ToString();


        if (Utilidad.EsEntornoDesarrollo())
          lblMensajeMaster.Text = Msg + "<br /><br />" + Exc.ToString();
        else
          lblMensajeMaster.Text = Msg;

        string Escript = "$(document).ready(function() {$('#divAlertaMaster').modal('show');});";
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "MostrarError", Escript, true);
      }

      catch (Exception Exc)
      {
        string Msg = string.Empty;
        string CodigoError = "ERRNUM0001";

        //Si nos llega una excepción con un código de error de los recursos, lo buscamos en los recursos. Si no, sacamos el error genérico ERRNUM0001 de los recursos. Si no existe el recurso sale el error a pelo.
        if (Exc.Message.StartsWith("ERRNUM"))
          CodigoError = Exc.Message.ToString();

        if (Resources.errores.ResourceManager.GetString(CodigoError, Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.errores.ResourceManager.GetString(CodigoError, Thread.CurrentThread.CurrentCulture);
        else
          Msg = Exc.Message.ToString();


        if (Utilidad.EsEntornoDesarrollo())
          lblMensajeMaster.Text = Msg + "<br /><br />" + Exc.ToString();
        else
          lblMensajeMaster.Text = Msg;

        string Escript = "$(document).ready(function() {$('#divAlertaMaster').modal('show');});";
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "MostrarError", Escript, true);
      }
    }

    protected void lnkCerrar_Click(object sender, EventArgs e)
    {
      cEncriptacion AES = new cEncriptacion();
      cUtil Utilidad = new cUtil();
      string TipoUsuario = string.Empty;
      int IdISP = Convert.ToInt32(Utilidad.DameDatoOperador(Entidad, "IdISPOperador"));

      if (Request.Cookies["SesionTipo"] != null)
        TipoUsuario = AES.DescifrarAES(IdISP, Request.Cookies["SesionTipo"].Value.ToString());
      else
        TipoUsuario = "Desconocido";

      Utilidad.EscribeLOG(Convert.ToInt32(HttpContext.Current.User.Identity.Name), TipoUsuario, "LOGOUT de usuario final: " + Entidad + ", realizado por el propio usuario final (CALLCENTER)");

      cSesiones Sesion = new cSesiones();
      Sesion.CaducarSesion(Entidad, TipoUsuario, IdISP);
      Sesion.CaducaOperador();
      Response.Redirect("~/login.aspx");
    }

    protected void ddlIdioma_SelectedIndexChanged(object sender, EventArgs e)
    {
      DropDownList ddlIdiomas = (DropDownList)sender;
      Idiomas.GrabarCookieIdioma(ddlIdiomas.SelectedItem.Value);
      Response.Redirect(Request.RawUrl);
    }

    protected void btnCambioPassword_Click(object sender, EventArgs e)
    {
      Page.Validate();

      if (!reqPassword1.IsValid || !regPassword1.IsValid || !reqPassword2.IsValid || !reqcomPasswords.IsValid)
        return;

      //Cuando se cambia la contraseña mostramos un aviso con instrucciones.
      pnlCambioPassword.Visible = false;
      lblError.Text = Resources.change_password.ResourceManager.GetString("PasswordCambiadaInterior");
      lblError.Visible = true;

      CambiarPassword();
    }

    protected void lbUso_Click(object sender, EventArgs e)
    {
      try
      {
        CargarCirculo();

        if (!Permisos.TienePermiso("MENUOPCIONUSO", Entidad))
          throw new Exception("ERRNUM0040");

        string Escript = "$(document).ready(function() {$('#divUso').modal('show');});";
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "MostrarUso", Escript, true);
      }

      catch (Exception Exc)
      {
        string Msg = string.Empty;
        string CodigoError = "ERRNUM0001";

        //Si nos llega una excepción con un código de error de los recursos, lo buscamos en los recursos. Si no, sacamos el error genérico ERRNUM0001 de los recursos. Si no existe el recurso sale el error a pelo.
        if (Exc.Message.StartsWith("ERRNUM"))
          CodigoError = Exc.Message.ToString();

        if (Resources.errores.ResourceManager.GetString(CodigoError, Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.errores.ResourceManager.GetString(CodigoError, Thread.CurrentThread.CurrentCulture);
        else
          Msg = Exc.Message.ToString();


        if (Utilidad.EsEntornoDesarrollo())
          lblMensajeMaster.Text = Msg + "<br /><br />" + Exc.ToString();
        else
          lblMensajeMaster.Text = Msg;

        string Escript = "$(document).ready(function() {$('#divAlertaMaster').modal('show');});";
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "MostrarError", Escript, true);
      }
    }

    protected void lbDescargarInforme_Click(object sender, EventArgs e)
    {
      try
      {
        ExportarCSV("LivingSafe_Orders_");
        Utilidad.EscribeLOG(Convert.ToInt32(HttpContext.Current.User.Identity.Name), "Operador", "Fichero de estadísticas de uso (pedidos/licencias) solicitado por el Operador: " + Entidad + " (CALLCENTER)");
      }

      catch (Exception Exc)
      {
        string Msg = string.Empty;
        string CodigoError = "ERRNUM0001";

        //Si nos llega una excepción con un código de error de los recursos, lo buscamos en los recursos. Si no, sacamos el error genérico ERRNUM0001 de los recursos. Si no existe el recurso sale el error a pelo.
        if (Exc.Message.StartsWith("ERRNUM"))
          CodigoError = Exc.Message.ToString();

        if (Resources.errores.ResourceManager.GetString(CodigoError, Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.errores.ResourceManager.GetString(CodigoError, Thread.CurrentThread.CurrentCulture);
        else
          Msg = Exc.Message.ToString();


        if (Utilidad.EsEntornoDesarrollo())
          lblMensajeMaster.Text = Msg + "<br /><br />" + Exc.ToString();
        else
          lblMensajeMaster.Text = Msg;

        string Escript = "$(document).ready(function() {$('#divAlertaMaster').modal('show');});";
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "MostrarError", Escript, true);
      }
    }

    protected void lbDashboard_Click(object sender, EventArgs e)
    {
      Response.Redirect("~/res/dashboard.aspx");
    }



    protected void ComprobarDominio()
    {
      cUtil Utilidad = new cUtil();
      int IdISP = -1;
      string URL = HttpContext.Current.Request.Url.Host;
      string DominioPrincipal = Utilidad.DameValorConfiguracion("DominioPrincipal");
      string Subdominio = Utilidad.DameSubdominio(URL, DominioPrincipal);
      string RutaCSS = Utilidad.CargarISP(Subdominio, ref IdISP);
      cssCustomMaster.Href = RutaCSS;
    }

    protected void CargarListadoIdiomas()
    {
      string CulturaDetectada = Idiomas.DevolverCulturaDetectada();
      string URL = HttpContext.Current.Request.Url.Host;
      IdISPDetectado = Utilidad.DameIdISPPorDominio(URL);
      ArrayList Lista = Utilidad.DameIdiomasISP(IdISPDetectado);
      ListItem Idioma = null;

      Idioma = new ListItem();
      Idioma.Text = Resources.login.ResourceManager.GetString("Seleccionar");
      Idioma.Value = string.Empty;
      Idioma.Selected = true;
      ddlIdioma.Items.Add(Idioma);

      for (int i = 0; i < Lista.Count; i++)
      {
        Idioma = new ListItem();
        Idioma.Text = HttpUtility.HtmlDecode(Utilidad.DameDato((ArrayList)Lista[i], "Idioma") + " (" + Utilidad.DameDato((ArrayList)Lista[i], "CultureCode") + ")");
        Idioma.Value = Utilidad.DameDato((ArrayList)Lista[i], "CultureCode");

        //if (Idioma.Value.ToLower() == CulturaDetectada.ToLower())
        //  Idioma.Selected = true;

        ddlIdioma.Items.Add(Idioma);
      }
    }

    protected void ForzarCambioPassword()
    {
      int PeriodoCambioPassword = Convert.ToInt32(Utilidad.DameValorConfiguracion("ForzadoCambioPasswordDias"));
      int contador = 0;
      DateTime FechaUltimaPassword=Convert.ToDateTime("1900-01-01 00:00:00");

      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = new SqlCommand();

      try
      {
        Con.Open();

        Com = Con.CreateCommand();
        Com.CommandText = "Operadores_DevolverHistoricoPasswords";
        Com.CommandTimeout = 300;
        Com.CommandType = CommandType.StoredProcedure;
        Com.Parameters.Add("@IdOperador", SqlDbType.Int).Value = Entidad;

        SqlDataReader Rst = Com.ExecuteReader();

        while(Rst.Read())
        {
          contador += 1;

          if(Convert.ToBoolean(Rst["Activo"]))
          {
            FechaUltimaPassword = Convert.ToDateTime(Rst["FechaAlta"]);
          }
        }

        if (contador == 0)
          throw new Exception("ERRNUM0006");
      }

      finally
      {
        Com.Dispose();

        if (Con.State == ConnectionState.Open)
          Con.Close();

        Con.Dispose();
      }

      if(contador==1 || (PeriodoCambioPassword>0 && contador>1 && (DateTime.Now-FechaUltimaPassword).Days>PeriodoCambioPassword))
      {
        lblMensajeMaster.Text = Resources.master.ResourceManager.GetString("DebeCambiarPass");

        //La capa de cambiar la contraseña no debe poder cerrarse
        string Escript = "$(document).ready(function() {$('#divCambioPassword').modal({backdrop:'static'});$('#divCambioPasswordBotonCierre').hide(); $('#divCambioPassword').modal('show');});";
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "MostrarCambioPassword", Escript, true);
      }
    }

    protected void CambiarPassword()
    {
      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      cEncriptacion AES = new cEncriptacion(Utilidad.DameClaveCifradoISP(IdISPDetectado));

      try
      {
        string Password = txtPassword1.Text;
        string PasswordActualCifrada = Utilidad.DameDatoOperador(Entidad, "PasswordOperador");
        string PasswordActualDescifrada = AES.DescifrarAES(IdISPDetectado, PasswordActualCifrada);

        if (PasswordActualDescifrada == Password)
        {
          divCambioPassword.Visible = false;
          throw new Exception("ERRNUM0007");
        }


        string SC = AES.CrearSecretoCompartido(Password);
        string PasswordCifrada = AES.CifrarAES(Password, SC);
        string PasswordCifradaFinal = SC + PasswordCifrada;

        Con.Open();

        SqlCommand Com = Con.CreateCommand();

        Com.Parameters.Clear();

        Com.CommandText = "Operadores_CambiarPassword";
        Com.CommandTimeout = 300;
        Com.CommandType = System.Data.CommandType.StoredProcedure;
        Com.Parameters.Add("@IdOperador", SqlDbType.Int).Value = Entidad;
        Com.Parameters.Add("@Password", SqlDbType.VarChar, -1).Value = PasswordCifradaFinal;

        Com.ExecuteNonQuery();

        txtPassword1.Text = txtPassword2.Text = string.Empty;

        //Registramos evento de cambio de contraseña
        Utilidad.EscribeLOG(Entidad, "UsuarioFinal", "Cambio de contraseña de operador forzado desde MASTER (CALLCENTER)");


        //Tras cambio de contraseña caducamos la sesion para forzar el volver a logar
        string TipoUsuario = string.Empty;

        if (Request.Cookies["SesionTipo"] != null)
          TipoUsuario = AES.DescifrarAES(IdISPDetectado, Request.Cookies["SesionTipo"].Value.ToString());
        else
          TipoUsuario = "Desconocido";

        Utilidad.EscribeLOG(Entidad, TipoUsuario, "Forzamos caducidad de sesión de operador para obligarle a volver a logar desde MASTER (Cambio de contraseña forzado) (CALLCENTER)");

        cSesiones Sesion = new cSesiones();
        Sesion.CaducarSesion(Entidad, TipoUsuario, IdISPDetectado);
        Sesion.CaducaOperador();

        //Redirigimos al login transcurridos 8.000 milisegundos
        string Escript = "setTimeout(function () {window.location.href = \"/login.aspx\";}, 8000);";
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Redireccion", Escript, true);
      }

      catch (Exception Exc)
      {
        string Msg = string.Empty;
        string CodigoError = "ERRNUM0001";

        //Si nos llega una excepción con un código de error de los recursos, lo buscamos en los recursos. Si no, sacamos el error genérico ERRNUM0001 de los recursos. Si no existe el recurso sale el error a pelo.
        if (Exc.Message.StartsWith("ERRNUM"))
          CodigoError = Exc.Message.ToString();

        if (Resources.errores.ResourceManager.GetString(CodigoError, Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.errores.ResourceManager.GetString(CodigoError, Thread.CurrentThread.CurrentCulture);
        else
          Msg = Exc.Message.ToString();


        if (Utilidad.EsEntornoDesarrollo())
          lblMensajeMaster.Text = Msg + "<br /><br />" + Exc.ToString();
        else
          lblMensajeMaster.Text = Msg;

        //Si da error porque por ejemplo ha intentado poner la misma contraseña que tenía, mostramos el error y a los 5 segundos recargamos la página para que se la vuelva a pedir
        string Escript = "$(document).ready(function() {$('#divAlertaMaster').modal('show');setTimeout(function () {window.location.href = \""+ Request.Path +"\";}, 5000)});";
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "MostrarError", Escript, true);
      }

      finally
      {
        if (Con.State == ConnectionState.Open)
          Con.Close();

        Con.Dispose();
      }
    }

    protected void CargarCirculo()
    {
      int IdISP = Convert.ToInt32(Utilidad.DameDatoOperador(Entidad, "IdISPOperador"));

      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ConnectionString);
      int Licencias = 0;
      int Pedidos = 0;
      
      try
      {
        Con.Open();

        SqlCommand Com = Con.CreateCommand();
        Com.Parameters.Clear();
        Com.CommandText = "ISP_DameUso";
        Com.CommandTimeout = 300;
        Com.CommandType = System.Data.CommandType.StoredProcedure;
        Com.Parameters.Add("@IdISP", SqlDbType.Int).Value = IdISP;

        SqlDataReader Rst = Com.ExecuteReader();

        if(Rst.Read())
        {
          Licencias = Convert.ToInt32(Rst["TotalLicencias"]);
          Pedidos = Convert.ToInt32(Rst["TotalPedidos"]);
        }
      }

      finally
      {
        if (Con.State == ConnectionState.Open)
          Con.Close();
        Con.Dispose();
      }
      
      lblLicencias.Text = Licencias.ToString();
      lblPedidos.Text = Pedidos.ToString();

      string colores = "['#414141', '#7F7F7F']";
      string simbolo = "fa-file";

      string Escript = @"
		  function shuffle(o){
			  for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
			  return o;
		  }

		  var colors = [" + colores + @"], circles = [];
      var child=document.getElementById('circles-1'),percentage = (" + Pedidos.ToString() + @" / " + Licencias.ToString() + @") * 100;
			circles.push(Circles.create({
				id:    child.id,
				value: percentage,
				radius:70,
				width: 15,
				colors:colors[0],
				text:  function() {
                  return '<i class=""fa " + simbolo + @""" aria-hidden=""true""></i>';
                }
        })
      );";

      ScriptManager.RegisterStartupScript(Page,Page.GetType(), "Circle", Escript, true);
    }

    protected void ExportarCSV(string Nombre)
    {
      int IdISP = Convert.ToInt32(Utilidad.DameDatoOperador(Entidad, "IdISPOperador"));

      sqlOrders.SelectParameters["IdISP"].DefaultValue = IdISP.ToString();

      gvOrders.AllowPaging = false;
      gvOrders.Visible = true;
      gvOrders.DataBind();

      string NombreFichero = Nombre + "_";
      NombreFichero += DateTime.Now.Year.ToString().PadLeft(2, '0') + DateTime.Now.Month.ToString().PadLeft(2, '0') + DateTime.Now.Day.ToString().PadLeft(2, '0');
      NombreFichero += DateTime.Now.Hour.ToString().PadLeft(2, '0') + DateTime.Now.Minute.ToString().PadLeft(2, '0') + DateTime.Now.Second.ToString().PadLeft(2, '0');
      NombreFichero += ".csv";

      Response.Clear();
      Response.Buffer = true;
      Response.AddHeader("Content-Disposition", "attachment;filename=" + NombreFichero);
      Response.Charset = "UTF-8";
      Response.ContentEncoding = Encoding.Default;
      Response.ContentType = "application/text";

      StringBuilder sb = new StringBuilder();
      for (int k = 0; k < gvOrders.Columns.Count; k++)
      {
        if (k > 0)
          sb.Append(';');

        sb.Append(gvOrders.Columns[k].HeaderText);
      }
      sb.Append("\r\n");
      for (int i = 0; i < gvOrders.Rows.Count; i++)
      {
        for (int k = 0; k < gvOrders.Columns.Count; k++)
        {
          if (k > 0)
            sb.Append(';');
          sb.Append(((Label)gvOrders.Rows[i].Cells[k].Controls[1]).Text);
        }
        sb.Append("\r\n");

      }
      Response.Output.Write(sb.ToString());

      gvOrders.Visible = false;

      Response.Flush();
      Response.End();
    }

  }
}
