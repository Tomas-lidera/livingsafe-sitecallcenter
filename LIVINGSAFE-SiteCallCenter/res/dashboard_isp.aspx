﻿<%@ Page Title="" Language="C#" MasterPageFile="~/res/LIVINGSAFE.Master" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeBehind="dashboard_isp.aspx.cs" Inherits="LIVINGSAFE_SiteCallCenter.res.dashboard_isp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="cDashboardHead" ContentPlaceHolderID="cpHead" runat="server">
  <script type="text/javascript" src="/js/Chart/moment.min.js"></script>
  <script type="text/javascript" src="/js/Chart/Chart.min.js"></script>
  <script type="text/javascript" src="/js/Chart/utils.js"></script>

  <script type="text/javascript" src="/js/PerfectScrollbar/perfect-scrollbar.js"></script>
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" />
  <link rel="stylesheet" type="text/css" href="/js/PerfectScrollbar/perfect-scrollbar.css" />
</asp:Content>
<asp:Content ID="cDashboardBody" ContentPlaceHolderID="cpBody" runat="server">
  <asp:ScriptManager ID="smMassive" runat="server"></asp:ScriptManager>

  <div class="container-fluid section-contenido">
    <div class="row espacio_inferior">
      <div class="col-md-9">
        <div class="txt_titulopagina_med"><asp:Literal ID="litTituloDashboardISP" runat="server" EnableViewState="false" Text='<%$ Resources:dashboard,litTituloDashboardISP %>'></asp:Literal></div>
      </div>
      <div class="col-md-3">
        <div style="float:right; margin-left:30px;">
          <asp:LinkButton runat="server" ID="lbVolver" Text="" OnClick="lbVolver_Click" CausesValidation="false" CssClass="fas fa-home" style="line-height:36px;font-size:30px; text-decoration:none; color:#5FA8FC;"></asp:LinkButton>
        </div>        
      </div>
    </div>

    <div class="row espacio_inferior">
      <div class="col-lg-6 offset-lg-1">
        <div class="card card-stats" style="background-color:#F6F6F6;" title='<%= Resources.dashboard.FichaPartnersLeyenda %>'>
          <div class="card-body">
            <div class="row">
              <div class="col-3 col-md-2">
                <div class="icon-big text-center" style="color:#FBC881;">
                  <asp:Image runat="server" ID="imgFichaLogo" Width="150px" />
                </div>
              </div>
              <div class="col-11 col-md-10">
                <div>
                  <div style="text-align:left;">
                    <div style="display:inline-block; font-size:14px; line-height:14px; width:70px; text-align:right; font-weight:bold;"><asp:Literal ID="litFichaPartner" runat="server" EnableViewState="false" Text='<%$ Resources:dashboard,litFichaPartner %>'></asp:Literal>: </div>
                    <div style="display:inline-block; font-size:14px; line-height:14px;"><asp:Label runat="server" ID="lblFichaPartner"></asp:Label></div>
                  </div>
                  <div style="text-align:left;">
                    <div style="display:inline-block; font-size:14px; line-height:14px; width:70px; text-align:right; font-weight:bold;"><asp:Literal ID="litFichaNombre" runat="server" EnableViewState="false" Text='<%$ Resources:dashboard,litFichaNombre %>'></asp:Literal>:</div>
                    <div style="display:inline-block; font-size:14px; line-height:14px;"><asp:Label runat="server" ID="lblFichaNombre"></asp:Label></div>
                  </div>
                  <div style="text-align:left;">
                    <div style="display:inline-block; font-size:14px; line-height:14px; width:70px; text-align:right; font-weight:bold;"><asp:Literal ID="litFichaDireccion" runat="server" EnableViewState="false" Text='<%$ Resources:dashboard,litFichaDireccion %>'></asp:Literal>:</div>
                    <div style="display:inline-block; font-size:14px; line-height:14px;"><asp:Label runat="server" ID="lblFichaDireccion"></asp:Label></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-2 col-6">
        <div class="card card-stats" style="background-color:#F6F6F6;" title='<%= Resources.dashboard.FichaPedidosLeyenda %>'>
          <div class="card-body">
            <div class="row">
              <div class="col-5 col-md-4">
                <div class="icon-big text-center" style="color:#6BD098;">
                  <i class="fas fa-money-check"></i>
                </div>
              </div>
              <div class="col-7 col-md-8">
                <div class="numbers">
                  <p class="card-category"><asp:Literal runat="server" ID="Literal2" Text='<%$ Resources:dashboard,FichaPedidosTitulo %>' EnableViewState="false"></asp:Literal></p>
                  <p class="card-title"><asp:Label runat="server" ID="lblTPedidos"></asp:Label></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-2 col-6">
        <div class="card card-stats" style="background-color:#F6F6F6;"  title='<%= Resources.dashboard.FichaLicenciasLeyenda %>'>
          <div class="card-body">
            <div class="row">
              <div class="col-5 col-md-4">
                <div class="icon-big text-center" style="color:#51CBCE;">
                  <i class="fas fa-laptop"></i>
                </div>
              </div>
              <div class="col-7 col-md-8">
                <div class="numbers">
                  <p class="card-category"><asp:Literal runat="server" ID="litTLicencias" Text='<%$ Resources:dashboard,FichaLicenciasTitulo %>' EnableViewState="false"></asp:Literal></p>
                  <p class="card-title"><asp:Label runat="server" ID="lblTLicencias"></asp:Label></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row espacio_inferior">
      <div class="col-md-10 offset-md-1">
        <div style="display:inline-block; float:left; margin-left:10px;">
          <asp:TextBox runat="server" ID="txtFechaUso" MaxLength="10" style="text-align:left;" Width="140px" placeholder="DD/MM/YYYY" CssClass="form-control" AutoCompleteType="None" autocomplete="off" />
          <cc1:CalendarExtender runat="server" ID="ceFechaUso" TargetControlID="txtFechaUso" CssClass="Calendar" FirstDayOfWeek="Monday" DefaultView="Days" Format="dd/MM/yyyy" /> 
        </div>
        <div style="display:inline-block; float:left; margin-left:10px; margin-right:20px;">
          <asp:LinkButton runat="server" ID="lbCalendario" Text="" OnClick="lbCalendario_Click" CausesValidation="false" CssClass="fas fa-calendar-check" style="line-height:36px; font-size:30px; text-decoration:none; color:#5FA8FC;"></asp:LinkButton>
        </div>

        <div style="display:inline-block; float:left; margin-left:10px;">
          <asp:LinkButton runat="server" ID="lbProductosLimpiar" Text="" OnClick="lbProductosLimpiar_Click" CausesValidation="false" CssClass="fas fa-eraser" style="line-height:36px;font-size:30px; text-decoration:none; color:#5FA8FC;"></asp:LinkButton>
        </div>
      </div>
    </div>

    <div class="row espacio_inferior">
      <div class="col-md-8 offset-md-2">
        <asp:UpdatePanel runat="server" ID="upCanvasProductos">
          <ContentTemplate>
            <div id="divCanvasProductos" style="position:relative; overflow:hidden; width:100%; background-color:#FFFFFF;">
              <canvas runat="server" id="canvasProductos" width="1100" height="400" style="display:block; margin-bottom:20px;" enableviewstate="true"></canvas>
            </div>
          </ContentTemplate>
        </asp:UpdatePanel>
      </div>
    </div>

    <div class="row espacio_inferior">
      <div class="col-md-10 offset-md-1">
        <div style="display:inline-block; float:left; margin-left:10px;">
          <asp:TextBox runat="server" ID="txtHistoricoFI" MaxLength="10" style="text-align:left;" Width="140px" placeholder="DD/MM/YYYY" CssClass="form-control" AutoCompleteType="None" autocomplete="off" />
          <cc1:CalendarExtender runat="server" ID="ceHistoricoFI" TargetControlID="txtHistoricoFI" CssClass="Calendar" FirstDayOfWeek="Monday" DefaultView="Days" Format="dd/MM/yyyy" /> 
        </div>
        <div style="display:inline-block; float:left; margin-left:10px;">
          <asp:TextBox runat="server" ID="txtHistoricoFF" MaxLength="10" style="text-align:left;" Width="140px" placeholder="DD/MM/YYYY" CssClass="form-control" AutoCompleteType="None" autocomplete="off" />
          <cc1:CalendarExtender runat="server" ID="ceHistoricoFF" TargetControlID="txtHistoricoFF" CssClass="Calendar" FirstDayOfWeek="Monday" DefaultView="Days" Format="dd/MM/yyyy" /> 
        </div>
        <div style="display:inline-block; float:left; margin-left:10px; margin-right:20px;">
          <asp:LinkButton runat="server" ID="lbHistoricoFF" Text="" OnClick="lbCalendario_Click" CausesValidation="false" CssClass="fas fa-calendar-check" style="line-height:36px; font-size:30px; text-decoration:none; color:#5FA8FC;"></asp:LinkButton>
        </div>
        <div style="display:inline-block; float:left; margin-left:10px;">
          <asp:LinkButton runat="server" ID="lbHistoricoLimpiar" Text="" OnClick="lbHistoricoLimpiar_Click" CausesValidation="false" CssClass="fas fa-eraser" style="line-height:36px;font-size:30px; text-decoration:none; color:#5FA8FC;"></asp:LinkButton>
        </div>
      </div>
    </div>

    <div class="row espacio_inferior">
      <div class="col-md-8 offset-md-2">
        <asp:UpdatePanel runat="server" ID="upCanvasHistorico">
          <ContentTemplate>
            <div id="divCanvasHistorico" style="position:relative; overflow:hidden; width:100%; background-color:#FFFFFF;">
              <canvas runat="server" id="canvasHistorico" width="1100" height="400" style="display:block; margin-bottom:20px;" enableviewstate="true"></canvas>
            </div>
          </ContentTemplate>
        </asp:UpdatePanel>
      </div>
    </div>
  </div>

  <asp:UpdatePanel runat="server" ID="upAlerta">
    <ContentTemplate>
      <div id="divAlerta" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="ModalAviso" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-body">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body" style="padding-top:0px; padding-bottom:0px;">
              <asp:Literal ID="litCabecera" runat="server" Text='<%$ Resources:massive,litCabecera %>' EnableViewState="false" />
              <asp:Label ID="lblResultado" runat="server" EnableViewState="false" />
              <br /><br />
            </div>
          </div>
        </div>
      </div>
    </ContentTemplate>
  </asp:UpdatePanel>
</asp:Content>
