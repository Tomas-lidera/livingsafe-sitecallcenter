﻿<%@ Page Title="" Language="C#" MasterPageFile="~/res/LIVINGSAFE.Master" AutoEventWireup="true" CodeBehind="billing.aspx.cs" Inherits="LIVINGSAFE_SiteCallCenter.res.billing" %>
<asp:Content ID="cBillingHead" ContentPlaceHolderID="cpHead" runat="server">
  <script>
    function Deshabilitar()
    {
      document.getElementById('<%=lbPaso4Generar.ClientID%>').style.visibility = 'hidden';
      document.getElementById('divLoading').style.visibility = 'visible';
    }
  </script>
</asp:Content>
<asp:Content ID="cBillingBody" ContentPlaceHolderID="cpBody" runat="server">
  <asp:ScriptManager ID="smMassive" runat="server"></asp:ScriptManager>


  <div class="container-fluid section-contenido">
    <div class="row espacio_inferior">
      <div class="col-12">
        <div class="txt_titulopagina_med">
          <asp:Literal runat="server" ID="lblTituloBilling" Text='<%$ Resources:billing,litTituloPagina %>' EnableViewState="false"></asp:Literal>
        </div>
      </div>
    </div>
    <div class="row">
      <div runat="server" id="divResumen" class="col-8 offset-2">
        <div class="row">
          <div runat="server" id="lbPaso1" class="col-md-3 text-center capa_billingactivo" style="cursor:pointer;">
     	      <span class="txt_billingtitulo"><asp:Label runat="server" ID="lblBarraPaso1" EnableViewState="false" Text='<%$ Resources:billing,BarraPaso1 %>' /></span>
            <span class="txt_billingdescripcion"><asp:Label runat="server" ID="lblPaso1">?</asp:Label></span>
          </div>
          <div runat="server" id="lbPaso2" class="col-md-3 text-center capa_billingmutedint" style="cursor:pointer;">
     	      <span class="txt_billingtitulo"><asp:Label runat="server" ID="lblBarraPaso2" EnableViewState="false" Text='<%$ Resources:billing,BarraPaso2 %>' /></span>
            <span class="txt_billingdescripcion"><asp:Label runat="server" ID="lblPaso2">?</asp:Label></span>
          </div>
          <div runat="server" id="lbPaso3" class="col-md-3 text-center capa_billingmutedint" style="cursor:pointer;">
     	      <span class="txt_billingtitulo"><asp:Label runat="server" ID="lblBarraPaso3" EnableViewState="false" Text='<%$ Resources:billing,BarraPaso3 %>' /></span>
            <span class="txt_billingdescripcion"><asp:Label runat="server" ID="lblPaso3">?</asp:Label></span>
          </div>
          <div runat="server" id="lbPaso4" class="col-md-3 text-center capa_billingmuted" style="cursor:pointer;">
     	      <span class="txt_billingtitulo span_billing_fin"><asp:Label runat="server" ID="lblBarraPaso4" EnableViewState="false" Text='<%$ Resources:billing,BarraPaso4 %>' /></span>
          </div>
        </div>
      </div>

      <div runat="server" id="divPaso1" class="col-8 offset-2" style="margin-top:20px; margin-bottom:20px;">
        <div class="row">
          <div class="col-12">
            <asp:Label runat="server" ID="lblPaso1Texto1" Text='<%$ Resources:billing,Paso1Texto1 %>' EnableViewState="false"></asp:Label>
          </div>
        </div>

        <div class="col-10 offset-1" style="margin-top:30px;">
          <div class="row">
            <div class="col-4">
              <div class="caja_producto">
                <div><img runat="server" src="~/images/icono_billing_isp.png" alt="ISP" width="250" height="130" class="img-fluid" enableviewstate="false" /></div>
                <div class="txt_productotitulo" style="min-height: 60px;"><asp:Label runat="server" ID="lblPaso1EntidadISP" Text='<%$ Resources:billing,Paso1EntidadISP %>' style="text-transform:uppercase;"></asp:Label></div>
                <div class="txt_productodescripcion" style="height:75px;"><asp:Label runat="server" id="lblPaso1DescripcionISP" Text='<%$ Resources:billing,Paso1DescripcionISP %>' EnableViewState="false" /></div>
                <div>
                  <asp:LinkButton runat="server" ID="lbPaso1SeleccionarISP" OnClick="lbPaso1Seleccionar_Click" CommandArgument="ISP"
                  Text='<%$ Resources:billing,Paso1Seleccionar %>' ToolTip='<%$ Resources:billing,Paso1Seleccionar %>' CssClass="boton_cuadro"></asp:LinkButton>
                </div>
              </div>
            </div>

            <div class="col-4">
              <div class="caja_producto">
                <div><img runat="server" src="~/images/icono_billing_partner.png" alt="PARTNER" width="250" height="130" class="img-fluid" enableviewstate="false" /></div>
                <div class="txt_productotitulo" style="min-height: 60px;"><asp:Label runat="server" ID="lblPaso1EntidadPartner" Text='<%$ Resources:billing,Paso1EntidadPartner %>' style="text-transform:uppercase;"></asp:Label></div>
                <div class="txt_productodescripcion" style="height:75px;"><asp:Label runat="server" id="lblPaso1DescripcionPartner" Text='<%$ Resources:billing,Paso1DescripcionPartner %>' EnableViewState="false" /></div>
                <div>
                  <asp:LinkButton runat="server" ID="lbPaso1SeleccionarPartner" OnClick="lbPaso1Seleccionar_Click" CommandArgument="PARTNER"
                  Text='<%$ Resources:billing,Paso1Seleccionar %>' ToolTip='<%$ Resources:billing,Paso1Seleccionar %>' CssClass="boton_cuadro"></asp:LinkButton>
                </div>
              </div>
            </div>

            <div class="col-4">
              <div class="caja_producto">
                <div><img runat="server" src="~/images/icono_billing_proveedor.png" alt="PROVEEDOR" width="250" height="130" class="img-fluid" enableviewstate="false" /></div>
                <div class="txt_productotitulo" style="min-height: 60px;"><asp:Label runat="server" ID="lblPaso1EntidadProveedor" Text='<%$ Resources:billing,Paso1EntidadProveedor %>' style="text-transform:uppercase;"></asp:Label></div>
                <div class="txt_productodescripcion" style="height:75px;"><asp:Label runat="server" id="lblPaso1DescripcionProveedor" Text='<%$ Resources:billing,Paso1DescripcionProveedor %>' EnableViewState="false" /></div>
                <div>
                  <asp:LinkButton runat="server" ID="lbPaso1SeleccionarISPProveedor" OnClick="lbPaso1Seleccionar_Click" CommandArgument="PROVEEDOR"
                  Text='<%$ Resources:billing,Paso1Seleccionar %>' ToolTip='<%$ Resources:billing,Paso1Seleccionar %>' CssClass="boton_cuadro"></asp:LinkButton>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div runat="server" id="divPaso2" class="col-8 offset-2" style="margin-top:20px; margin-bottom:20px;" visible="false">
        <div class="row">
          <div class="col-6">
            <asp:Label runat="server" ID="lblPaso2Texto1" Text='<%$ Resources:billing,Paso2Texto1 %>'></asp:Label> 
            <asp:Label runat="server" ID="lblPaso2Entidad"></asp:Label>
            <asp:Label runat="server" ID="lblPaso2Texto2" Text='<%$ Resources:billing,Paso2Texto2 %>'></asp:Label>
          </div>
          <div class="col-6" style="text-align:right;">
            <asp:CheckBox runat="server" ID="chkPaso2EntidadSeleccionarTodos" Text="Seleccionar todos&nbsp;" TextAlign="Left" CssClass="check_todos" style="float:right;" AutoPostBack="true" OnCheckedChanged="chkPaso2EntidadSeleccionarTodos_CheckedChanged"></asp:CheckBox>
          </div>
        </div>

        <div class="col-12" style="margin-top:20px;">
          <div>
            <asp:LinkButton runat="server" ID="lbPaso2SiguienteArriba" OnClick="lbPaso2Siguiente_Click"
            Text='<%$ Resources:billing,Paso2Siguiente %>' ToolTip='<%$ Resources:billing,Paso2Siguiente %>' CssClass="boton_cuadro"></asp:LinkButton>
          </div>
        </div>

        <div class="col-12" style="margin-top:20px;">
          <div class="row">
            <asp:ListView ID="lvEntidad" runat="server" DataSourceID="sqlDatosEntidad">
              <EmptyDataTemplate>
                <div class="col-12" style="display:inline-block; text-align:center;">
                  No hay información disponible en estos momentos.
                </div>
              </EmptyDataTemplate>

              <ItemTemplate>
                <div class="col-3">
                  <div runat="server" class='<%# (((Convert.ToBoolean(Eval("Activo")) == true) ? "caja_producto":"caja_producto_desactivado")) %>'>
                    <div style="display: table-cell; vertical-align:middle; text-align:center; height:125px; width:280px">
                      <asp:Image runat="server" ID="imgEntidadLogo" Visible='<%# Eval("Logo").ToString()==string.Empty ? false:true %>' AlternateText='<%# Bind("Siglas") %>'
                        ImageUrl='<%# "~/images/logos/" + Eval("Logo").ToString() %>' Width="200px" class="img-fluid text-center" ToolTip='<%# Eval("Nombre").ToString()%>'  />
                      <asp:Label runat="server" ID="lblEntidadNombre" CssClass="nombre_entidad" Visible='<%# Eval("Logo").ToString()==string.Empty ? true:false %>' Text='<%# ((Convert.ToBoolean(Eval("Activo")) == true) ? string.Empty:"[D] ") + Eval("Nombre") %>'></asp:Label>
                    </div>
                    <div style="height:80px;">
                      <asp:Label runat="server" ID="lblEntidadSiglas" Text='<%# ((Convert.ToBoolean(Eval("Activo")) == true) ? string.Empty:"[D] ") + Eval("Siglas").ToString() %>' CssClass="siglas_entidad" Visible='<%# Eval("Logo").ToString()==string.Empty ? false:true %>'></asp:Label>
                      <input runat="server" type="checkbox" id="chkEntidad" class="check_cuadro" value='<%#Bind("Id") %>' />
                    </div>
                  </div>
                </div>
              </ItemTemplate>
            </asp:ListView>

            

            <asp:SqlDataSource ID="sqlDatosEntidad" runat="server" CancelSelectOnNullParameter="False" ConnectionString="<%$ ConnectionStrings:LivingSafeCS %>" 
              SelectCommand="Facturacion40_DameEntidades" SelectCommandType="StoredProcedure">
              <SelectParameters>
                <asp:Parameter Name="Tipo" Type="String" ConvertEmptyStringToNull="true" />
              </SelectParameters>
            </asp:SqlDataSource>
          </div>

          <div style="margin-bottom:20px; font-size:12px; font-family:Arial;">[D]: <asp:Literal runat="server" ID="litDesativado" Text='<%$ Resources:dashboard,LiteralDesactivado %>' EnableViewState="false"></asp:Literal></div>
        </div>
        <div class="col-12">
          <div>
            <asp:LinkButton runat="server" ID="lbPaso2SiguienteAbajo" OnClick="lbPaso2Siguiente_Click"
            Text='<%$ Resources:billing,Paso2Siguiente %>' ToolTip='<%$ Resources:billing,Paso2Siguiente %>' CssClass="boton_cuadro"></asp:LinkButton>
          </div>
        </div>
      </div>

      <div runat="server" id="divPaso3" class="col-8 offset-2" style="margin-top:20px; margin-bottom:20px;" visible="false">
        <div class="row">
          <div class="col-12">
            <asp:Label runat="server" ID="lblPaso3Texto1" Text='<%$ Resources:billing,Paso3Texto1 %>'></asp:Label> 
          </div>
        </div>

        <div class="col-12" style="margin-top:125px; margin-bottom:125px;">
          <div class="row">
            <div class="col-2 offset-3">
              <asp:Label runat="server" ID="lblPaso3LiteralYear" Text='<%$ Resources:billing,Paso3FechaLiteralYear %>' CssClass="txt_productotitulo" EnableViewState="false"></asp:Label>
            </div>
            <div class="col-4">
              <asp:Label runat="server" ID="lblPaso3LiteralMonth" Text='<%$ Resources:billing,Paso3FechaLiteralMonth %>' CssClass="txt_productotitulo" EnableViewState="false"></asp:Label>
            </div>
            <div class="col-2 offset-3">
              <asp:DropDownList runat="server" ID="ddlPaso3Year" CssClass="ddl_Paso3_Fechas" AutoPostBack="true" OnSelectedIndexChanged="ddlPaso3Year_SelectedIndexChanged"></asp:DropDownList>
            </div>
            <div class="col-4">
              <asp:DropDownList runat="server" ID="ddlPaso3Month" CssClass="ddl_Paso3_Fechas"></asp:DropDownList>
            </div>
          </div>
        </div>

        <div class="col-12">
          <div>
            <asp:LinkButton runat="server" ID="lbPaso3Siguiente" OnClick="lbPaso3Siguiente_Click"
            Text='<%$ Resources:billing,Paso3Siguiente %>' ToolTip='<%$ Resources:billing,Paso3Siguiente %>' CssClass="boton_cuadro"></asp:LinkButton>
          </div>
        </div>
      </div>

      <div runat="server" id="divPaso4" class="col-8 offset-2" style="margin-top:20px; margin-bottom:20px;" visible="false">
        <div class="row">
          <div class="col-12">
            <asp:Label runat="server" ID="lblPaso4Texto1" Text='<%$ Resources:billing,Paso4Texto1 %>'></asp:Label> 
          </div>
        </div>

        <div class="col-12" style="margin-top:125px; margin-bottom:125px;">
          <div class="row">
            <div class="col-6 offset-3">
              <asp:Label runat="server" ID="lblPaso4LiteralNombreFichero" Text='<%$ Resources:billing,Paso4LiteralNombreFichero %>' CssClass="txt_productotitulo" EnableViewState="false"></asp:Label>
            </div>
            <div class="col-6 offset-3">
              <asp:TextBox runat="server" ID="txtPaso4NombreFichero" CssClass="ddl_Paso3_Fechas"></asp:TextBox>
              <asp:RegularExpressionValidator runat="server" ID="regPaso4NombreFichero" ControlToValidate="txtPaso4NombreFichero" ErrorMessage='<%$ Resources:billing,RegexNombreFichero %>' CssClass="val_error" ValidationExpression="^[a-zA-Z0-9çÇ_-]+$" Display="Dynamic"></asp:RegularExpressionValidator>
            </div>
            <div class="col-1">
              <asp:Label runat="server" ID="lblPaso4Extension" Text=".XLSX" CssClass="txt_productotitulo" style="line-height:40px;" EnableViewState="false"></asp:Label>
            </div>
          </div>
        </div>

        <div class="col-12" style="text-align:center;">
          <div>
            <asp:LinkButton runat="server" ID="lbPaso4Generar" OnClick="lbPaso4Generar_Click" OnClientClick="Deshabilitar()"
            Text='<%$ Resources:billing,Paso4Generar %>' ToolTip='<%$ Resources:billing,Paso4Generar %>' CssClass="boton_cuadro"></asp:LinkButton>
          </div>
          <div id="divLoading" style="visibility:hidden;margin-top:-55px;">
            <button class="btn btn-primary" type="button" disabled style="height:55px;">
              <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
              <asp:Label runat="server" ID="lblPaso4Cargando" EnableViewState="false" Text='<%$ Resources:billing,Paso4Cargando %>'></asp:Label>
            </button>
          </div>
        </div>
      </div>

      <div runat="server" id="divPaso5" class="col-8 offset-2" style="margin-top:20px; margin-bottom:20px;" visible="false">
        <div class="row">
          <div class="col-12">
            <asp:Label runat="server" ID="lblPaso5Texto1" Text='<%$ Resources:billing,Paso5Texto1 %>'></asp:Label> 
          </div>

          <div class="col-12" style="text-align:center; margin-top:75px; margin-bottom:125px;">
            <asp:LinkButton runat="server" ID="lbPaso5Descargar" OnClick="lbPaso5Descargar_Click"
            Text='<%$ Resources:billing,Paso5Descargar %>' ToolTip='<%$ Resources:billing,Paso5Descargar %>' Width="400px" CssClass="boton_cuadro"></asp:LinkButton>
          </div>
        </div>
      </div>
    </div>
  </div>


  <asp:UpdatePanel ID="upAlerta" UpdateMode="Conditional" runat="server">
    <ContentTemplate>
      <div id="divAlerta" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="ModalAviso" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header" style="border:none;">
              <h5 class="modal-title txt_titulopagina_med" id="TituloAlerta">
                <asp:Literal runat="server" ID="lblTituloAlerta" Text='<%$ Resources:billing,TituloAlerta %>' />
              </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div class="modal-body" style="padding-top: 0px; padding-bottom: 0px;">
              <asp:Label runat="server" ID="lblResultado" />
              <br />
              <br />
            </div>
          </div>
        </div>
      </div>
    </ContentTemplate>
  </asp:UpdatePanel>
</asp:Content>
