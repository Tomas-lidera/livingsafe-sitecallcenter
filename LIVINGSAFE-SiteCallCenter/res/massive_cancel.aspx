﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="massive_cancel.aspx.cs" Inherits="LIVINGSAFE_SiteCallCenter.res.massive_cancel" MasterPageFile="~/res/LIVINGSAFE.Master" %>


<asp:Content ID="cOrdersHead" ContentPlaceHolderID="cpHead" runat="server">
</asp:Content>
<asp:Content ID="cOrdersBody" ContentPlaceHolderID="cpBody" runat="server">
 <asp:ScriptManager ID="smMassive" runat="server"></asp:ScriptManager>
  <script>
    var a;

    function CalcularPorcentaje() {
	    var total=parseInt(document.getElementById('<%=Total.ClientID%>').innerText);
	    var valor=parseInt(document.getElementById('<%=Valor.ClientID%>').innerText);
	    var porcentaje = (valor*100)/total;

	    document.getElementById('<%=barra.ClientID%>').style.width=porcentaje + '%';
	
      if (valor >= total) {
        clearInterval(a);
      }
    }
  </script>

  <div class="container-fluid section-contenido">
    <div class="row espacio_inferior">
      <div class="col-md-10">
        <div class="txt_titulopagina_med"><asp:Literal ID="litTitulo" runat="server" EnableViewState="false" Text='<%$ Resources:massive_cancel,litTitulo %>'></asp:Literal></div>
      </div>
      <div class="col-md-2">
        <asp:UpdatePanel runat="server" ID="upReiniciar" style="padding:0px;" UpdateMode="Conditional">
          <Triggers>
            <asp:AsyncPostBackTrigger ControlID="tmrTemporizador" EventName="Tick" />
          </Triggers>
          <ContentTemplate>
            <asp:LinkButton runat="server" ID="lbNuevaBaja" Visible="false" Text='<%$ Resources:massive_cancel,lbNuevaBaja %>' CssClass="lnk_passchange aire10_sup_inf" OnClick="lbNuevaBaja_Click"></asp:LinkButton>
          </ContentTemplate>
        </asp:UpdatePanel>
      </div>

      <div class="col-md-12" style="height:30px; padding:0px;"></div>

      <asp:UpdatePanel runat="server" ID="upFichero" class="col-md-7 offset-2" style="padding:0px;" UpdateMode="Conditional">
        <Triggers>
          <asp:PostBackTrigger ControlID="lbProcesar" />
        </Triggers>
        <ContentTemplate>
          <asp:FileUpload runat="server" ID="fuMasivo" Width="100%" style="border:1px solid #DEDEDE;" />
        </ContentTemplate>
      </asp:UpdatePanel>

      <asp:UpdatePanel runat="server" ID="upProcesar" class="col-md-1 text-center" style="padding:0px;" UpdateMode="Conditional">
        <Triggers>
          <asp:PostBackTrigger ControlID="lbProcesar" />
        </Triggers>
        <ContentTemplate>
          <asp:LinkButton runat="server" ID="lbProcesar" Text='<%$ Resources:massive_cancel,lbProcesar %>' CssClass="lnk_passchange" style="margin-top:-2px;" OnClick="lbProcesar_Click" CausesValidation="false" OnClientClick="this.style.visibility='hidden';document.getElementById('btnProgreso').style.visibility='visible';" />
          <button id="btnProgreso" class="CC_btnAzul32" type="button" disabled style="visibility:hidden; left:7px; position:absolute;">
            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
            <span class="sr-only">Loading...</span>
          </button>
        </ContentTemplate>
      </asp:UpdatePanel>

      <div class="col-lg-8 offset-lg-2" style="padding:0px; margin-top:30px;">
        <div class="col-12 text-left CC_tituloProgreso margenes_no"><asp:Literal runat="server" ID="litAnalisis" EnableViewState="false" Text='<%$ Resources:massive_cancel,litAnalisis %>'></asp:Literal></div>
        <div class="row" style="margin-right:0px; margin-left:0px; padding-top:25px;">
          <div class="col-6 col-md-3 text-center">
            <img src="/images/icon_registros_total.png" width="120" height="120" class="img-fluid" alt="TOTAL" />
            <div><asp:Literal runat="server" ID="litAnalizados" EnableViewState="false" Text='<%$ Resources:massive_cancel,litAnalizados %>'></asp:Literal></div>
            <div class="CC_ProvNumero"><asp:Label runat="server" ID="lblRegsTOTAL" Text="0" CssClass="CC_ProvNumero"></asp:Label></div>
          </div>
          <div class="col-6 col-md-3 text-center">
            <img src="/images/icon_registros_ok.png" width="120" height="120" class="img-fluid" alt="OK" />
            <div><asp:Literal runat="server" ID="litCorrectos" EnableViewState="false" Text='<%$ Resources:massive_cancel,litCorrectos %>'></asp:Literal></div>
            <div class="CC_ProvNumero"><asp:Label runat="server" ID="lblRegsOK" Text="0" CssClass="CC_ProvNumero"></asp:Label></div>
          </div>
          <div class="col-6 col-md-3 text-center">
            <img src="/images/icon_registros_no.png" width="120" height="120" class="img-fluid" alt="NOK" />
            <div><asp:Literal runat="server" ID="litIncorrectos" EnableViewState="false" Text='<%$ Resources:massive_cancel,litIncorrectos %>'></asp:Literal></div>
            <div class="CC_ProvNumero"><asp:Label runat="server" ID="lblRegsNOK" Text="0" CssClass="CC_ProvNumero"></asp:Label></div>
          </div>
          <div class="col-6 col-md-3 text-center">
            <img src="/images/icon_registros_vacio.png" width="120" height="120" class="img-fluid" alt="EMPTY" />
            <div><asp:Literal runat="server" ID="litVacios" EnableViewState="false" Text='<%$ Resources:massive_cancel,litVacios %>'></asp:Literal></div>
            <div class="CC_ProvNumero"><asp:Label runat="server" ID="lblRegsVacios" Text="0" CssClass="CC_ProvNumero"></asp:Label></div>
          </div>
          <div class="col-12 text-center CC_txtProcesar" style="padding-top:15px; padding-bottom:15px;"><asp:Label runat="server" ID="lblRegsProcesar" Text='<%$ Resources:massive_cancel,litResultadoDefecto %>'></asp:Label></div>
        </div>
      </div>

      <div id="divProgreso" runat="server" class=" col-lg-8 offset-lg-2" style="margin-top:40px; padding:0px 10px 0px 10px;">
  		<div class="col-12 text-left margenes_no CC_tituloProgreso" style="margin-bottom:15px;"><asp:Literal runat="server" ID="litProcesando" EnableViewState="false" Text='<%$ Resources:massive_cancel,litProcesando %>'></asp:Literal></div>
        <div class="progress">
          <div id="barra" runat="server" class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width: 0%"></div>
        </div>
        <asp:UpdatePanel runat="server" ID="upTimer" class="col-md-12 text-right margenes_no" style="padding:0px;">
          <Triggers>
            <asp:AsyncPostBackTrigger ControlID="tmrTemporizador" EventName="Tick" />
          </Triggers>
          <ContentTemplate>
            <asp:Label runat="server" ID="Valor" Text="0" />
            /
            <asp:Label runat="server" ID="Total" Text="0" />
            <asp:Literal runat="server" ID="litRegistrosProcesados" Text='<%$ Resources:massive_cancel,litRegistrosProcesados %>'></asp:Literal>
            <asp:Timer runat="server" ID="tmrTemporizador" Interval="2000" OnTick="tmrTemporizador_Tick"></asp:Timer>
          </ContentTemplate>
        </asp:UpdatePanel>
      </div>

      <asp:UpdatePanel runat="server" ID="upMensajeFinal" UpdateMode="Conditional" class="col-md-8 offset-2" style="padding:0px;">
        <Triggers>
          <asp:AsyncPostBackTrigger ControlID="tmrTemporizador" EventName="Tick" />
        </Triggers>
        <ContentTemplate>
          <div class="col-12 text-center CC_txtProcesado" style="padding-top:10px; padding-bottom:10px;"><asp:Label runat="server" ID="lblMensajeFinal"></asp:Label></div>
        </ContentTemplate>
      </asp:UpdatePanel>

      <asp:UpdatePanel runat="server" ID="upRespuesta" UpdateMode="Conditional" class="col-md-12 ml-auto align-content-end">
        <Triggers>
          <asp:PostBackTrigger ControlID="lbProcesar" />
          <asp:PostBackTrigger ControlID="lbDescargarExcel" />
          <asp:PostBackTrigger ControlID="lbDescargarLOG" />
          <asp:AsyncPostBackTrigger ControlID="tmrTemporizador" EventName="Tick" />
        </Triggers>
        <ContentTemplate>
          <div id="divInformeErrores" runat="server" visible="false" class=" col-lg-8 offset-lg-2" style="margin-top:15px; padding:0px 10px 0px 10px;">
  	        <div class="col-12 text-left margenes_no CC_tituloProgreso"><asp:Literal runat="server" ID="litInformeErrores" EnableViewState="false" Text='<%$ Resources:massive_cancel,litInformeErrores %>'></asp:Literal></div>
            <div class="col-12 text-left margenes_no"><asp:Literal runat="server" ID="litInformeErroresDescripcion" EnableViewState="false" Text='<%$ Resources:massive_cancel,litInformeErroresDescripcion %>'></asp:Literal></div>
		        <div class="col-12 text-left margenes_no">
              <asp:Label runat="server" ID="lblRespuesta" CssClass="terminal"></asp:Label>
		        </div>
            <div class="row" style="padding-top:25px; padding-bottom:25px;">
              <div class="col-lg-3 col-md-6 offset-lg-3 text-center" style="margin-bottom:10px;"><asp:LinkButton runat="server" ID="lbDescargarExcel" Text='<%$ Resources:massive_cancel,lbDescargarExcel %>'  CssClass="lnk_passchange" OnClick="lbDescargarExcel_Click"></asp:LinkButton></div>
              <div class="col-lg-3 col-md-6 text-center"><asp:LinkButton runat="server" ID="lbDescargarLOG" Text='<%$ Resources:massive_cancel,lbDescargarLOG %>'  CssClass="lnk_passchange" OnClick="lbDescargarLOG_Click"></asp:LinkButton></div>
            </div>
          </div>
        </ContentTemplate>
      </asp:UpdatePanel>
    </div>
  </div>

  <asp:UpdatePanel ID="upAlerta" UpdateMode="Conditional" runat="server">
    <ContentTemplate>
      <div id="divAlerta" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="ModalAviso" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-body">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body" style="padding-top:0px; padding-bottom:0px;">
              <asp:Literal ID="litCabecera" runat="server" Text='<%$ Resources:massive_cancel,litCabecera %>' EnableViewState="false" />
              <asp:Label ID="lblResultado" runat="server" EnableViewState="false" />
              <br /><br />
            </div>
          </div>
        </div>
      </div>
    </ContentTemplate>
  </asp:UpdatePanel>
</asp:Content>