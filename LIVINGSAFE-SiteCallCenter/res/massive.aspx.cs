﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;
using APIREST;
using LIVINGSAFE_SiteCallCenter.App_Code;
using LIVINGSAFE_SiteCallCenter.Clases;
using Newtonsoft.Json;
using OfficeOpenXml;
using System.Web.UI.WebControls;

namespace LIVINGSAFE_SiteCallCenter.res
{
  public partial class massive : System.Web.UI.Page
  {
    cLog LOGe = new cLog();
    cUtil Utilidad = new cUtil();
    cPermisos Permisos = new cPermisos();
    cEncriptacion Encriptacion = new cEncriptacion();
    int Entidad = Convert.ToInt32(HttpContext.Current.User.Identity.Name);
    static string token = string.Empty;
    int IdRegistro = 0;
    string FicheroTemporal = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
      if (!Permisos.TienePermiso("PAGINAALTAMASIVA", Entidad))
        Response.Redirect("~/forbidden.aspx");

      //Si el usuario logado no tiene permisos suficientes, no puede ver los botones de descargar Excel ni LOG
      lbDescargarExcel.Visible = (new LIVINGSAFE_SiteCallCenter.Clases.cPermisos()).TienePermiso("DESCARGAREXCELERRORESALTAMASIVA", Entidad);
      lbDescargarLOG.Visible = (new LIVINGSAFE_SiteCallCenter.Clases.cPermisos()).TienePermiso("DECARGARLOGERRORESALTAMASIVA", Entidad);
    }

    protected void lbProcesar_Click(object sender, EventArgs e)
    {
      lblRespuesta.Text = string.Empty;
      lblResultado.Text = string.Empty;
      lbProcesar.Visible = false;
      cImpersonate impersonar = new cImpersonate();

      try
      {
                
        string usuario = ConfigurationManager.AppSettings["usuarioPublicar"].ToString();
        string dominio = ConfigurationManager.AppSettings["usuarioDominio"].ToString();
        string pass = ConfigurationManager.AppSettings["usuarioPass"].ToString();
        impersonar.impersonateValidUser(usuario, dominio, pass);

        int IdISP = Convert.ToInt32(Utilidad.DameDatoOperador(Entidad, "IdISPOperador"));
        LOGe.AddInformationEntry("Inicio de proceso de alta masiva para el ISP: " + IdISP.ToString());
        LOGe.AddInformationEntry("Proceso iniciado por: " + Utilidad.DameDatoOperador(Entidad, "Nombre") + " " + Utilidad.DameDatoOperador(Entidad, "Apellidos") + ", con IdOperador: " + Entidad.ToString());
        IdRegistro = InsertarRegistroProvision(IdISP, Entidad);
        LOGe.AddInformationEntry("IdRegistro generado: " + IdRegistro.ToString());
        ViewState.Add("IdRegistro", IdRegistro);

        ArrayList RegistrosOK = Validar(null);

        LOGe.AddInformationEntry("Validación finalizada. Registros OK: " + RegistrosOK.Count.ToString());

        int RegistrosCargados = Cargar(RegistrosOK,IdRegistro);

        LOGe.AddInformationEntry("Carga de datos finalizada. Registros cargados: " + RegistrosCargados.ToString());

        if (RegistrosCargados > 0)
        {
          //Lo comentamos porque ahora se va a hacer desde un servicio.
          //Procesar(IdRegistro);

          Valor.Text = DameTotalProvisionesPendientes(-1, IdRegistro, 100).ToString();
          Total.Text = RegistrosCargados.ToString();
        }

        //Una vez finalizado el proceso, validamos si el registro creado ha quedado huérfano (P.ej. Si se intentan subir registros ya provisionados). Si es huérfano, es decir, 
        //no se ha asignado el registro a ninguna provisión ni provisión masiva, lo eliminamos.
        EliminarRegistroHuerfano(IdISP,IdRegistro);

        LOGe.AddInformationEntry("Los registros se procesarán desde el servicio de provisión de LivingSafe.");
        Utilidad.EscribeLOG(Entidad, "Operador", "Finalizada solicitud de alta masiva de " + Total.Text + " registros para el ISP: " + IdISP.ToString() + " (CALLCENTER)");
        impersonar.undoImpersonation();
     }

      catch (Exception Exc)
      {
        string Msg = string.Empty;

        if (Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
            Msg = Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);
        else if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
            Msg = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);
        else
        {
          if (Utilidad.EsEntornoDesarrollo())
            Msg = Exc.ToString();
          else
            Msg = Exc.Message.ToString();
        }

        lblResultado.Text = Msg;

        string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "MostrarErrorModif", Escript, true);

        impersonar.undoImpersonation();
      }
    }

    protected void tmrTemporizador_Tick(object sender, EventArgs e)
    {
      try
      {
        if (Convert.ToInt32(Valor.Text) < Convert.ToInt32(Total.Text))
        {
          if (ViewState["IdRegistro"] != null && Convert.ToInt32(ViewState["IdRegistro"]) > 0)
            Valor.Text = DameTotalProvisionesPendientes(-1, Convert.ToInt32(ViewState["IdRegistro"]), 100).ToString();
          else
          {
            int IdISP = Convert.ToInt32(Utilidad.DameDatoOperador(Entidad, "IdISPOperador"));
            Valor.Text = DameTotalProvisionesPendientes(IdISP, -1, 100).ToString();
          }
        }

        if (Convert.ToInt32(Total.Text) > 0 && (Convert.ToInt32(Total.Text) - Convert.ToInt32(Valor.Text)) <= 0)
        {
          lblMensajeFinal.Text = Resources.massive.ResourceManager.GetString("lblFinalizado", Thread.CurrentThread.CurrentCulture);

          if (Convert.ToInt32(lblRegsNOK.Text) > 0)
          {
            if((new LIVINGSAFE_SiteCallCenter.Clases.cPermisos()).TienePermiso("DESCARGAREXCELERRORESALTAMASIVA", Entidad) || (new LIVINGSAFE_SiteCallCenter.Clases.cPermisos()).TienePermiso("DECARGARLOGERRORESALTAMASIVA", Entidad))
              divInformeErrores.Visible = true;
          }

          string Escript = "document.getElementById('" + barra.ClientID + "').className='progress-bar progress-bar-striped progress-bar';";
          ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "CambiarBarraProgreso", Escript, true);

          lbNuevaAlta.Visible = true;

          tmrTemporizador.Enabled = false;
        }
        else
          lblMensajeFinal.Text = string.Empty;
      }

      catch (Exception Exc)
      {
        string Msg = string.Empty;

        if (Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);
        else
        {
          if (Utilidad.EsEntornoDesarrollo())
            Msg = Exc.ToString();
          else
            Msg = Exc.Message.ToString();
        }

        lblResultado.Text = Msg;

        string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "MostrarErrorModif", Escript, true);
      }
    }

    protected void lbDescargarExcel_Click(object sender, EventArgs e)
    {
      cImpersonate impersonar = new cImpersonate();
      try
      {
      string usuario = ConfigurationManager.AppSettings["usuarioPublicar"].ToString();
      string dominio = ConfigurationManager.AppSettings["usuarioDominio"].ToString();
      string pass = ConfigurationManager.AppSettings["usuarioPass"].ToString();
      impersonar.impersonateValidUser(usuario, dominio, pass);

        if (ViewState["IdRegistro"] != null)
        {
          SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
          SqlCommand Com = null;
          string Nombre = string.Empty;

          try
          {
            Con.Open();
            Com = Con.CreateCommand();
            Com.CommandType = System.Data.CommandType.StoredProcedure;
            Com.CommandText = "Provision_Masiva_Registro_DameRegistro";
            Com.CommandTimeout = 300;
            Com.Parameters.Add("@IdRegistro", SqlDbType.Int).Value = Convert.ToInt32(ViewState["IdRegistro"]);

            SqlDataReader Rst = Com.ExecuteReader();

            if (Rst.Read())
            {
              Nombre = Rst["NombreFichero"].ToString();
            }
          }

          finally
          {
            if (Com != null)
              Com.Dispose();

            if (Con.State == ConnectionState.Open)
              Con.Close();

            Con.Dispose();
          }

          string Ruta = ConfigurationManager.AppSettings["RutaFicherosErrores"].ToString() + "\\TEMP\\" + Nombre + ".xlsx";

          Utilidad.EscribeLOG(Entidad, "Operador", "Descargado informe de errores (Informe: " + Nombre + ".xlsx) en nueva alta masiva (CALLCENTER)");

          Response.Clear();
          Response.ClearHeaders();
          Response.Buffer = true;
          Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
          Response.AppendHeader("Content-Disposition", "Attachment; filename=\"" + Nombre + ".xlsx\"");

          FileInfo Fich = new FileInfo(Ruta);
          Response.AppendHeader("Content-Length", Fich.Length.ToString());
          Response.TransmitFile(Ruta);
          Response.Flush();
          Response.End();

                    impersonar.undoImpersonation();
                }
      }

      catch (Exception Exc)
      {
        string Msg = string.Empty;

        if (Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);
        else
        {
          if (Utilidad.EsEntornoDesarrollo())
            Msg = Exc.ToString();
          else
            Msg = Exc.Message.ToString();
        }

        lblResultado.Text = Msg;

        string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "MostrarErrorModif", Escript, true);
                impersonar.undoImpersonation();
            }
    }

    protected void lbDescargarLOG_Click(object sender, EventArgs e)
    {
      cImpersonate impersonar = new cImpersonate();
      try
      {
                string usuario = ConfigurationManager.AppSettings["usuarioPublicar"].ToString();
                string dominio = ConfigurationManager.AppSettings["usuarioDominio"].ToString();
                string pass = ConfigurationManager.AppSettings["usuarioPass"].ToString();
                impersonar.impersonateValidUser(usuario, dominio, pass);
        if (ViewState["IdRegistro"] != null)
        {
          SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
          SqlCommand Com = null;
          string Nombre = string.Empty;

          try
          {
            Con.Open();
            Com = Con.CreateCommand();
            Com.CommandType = System.Data.CommandType.StoredProcedure;
            Com.CommandText = "Provision_Masiva_Registro_DameRegistro";
            Com.CommandTimeout = 300;
            Com.Parameters.Add("@IdRegistro", SqlDbType.Int).Value = Convert.ToInt32(ViewState["IdRegistro"]);

            SqlDataReader Rst = Com.ExecuteReader();

            if (Rst.Read())
            {
              Nombre = Rst["NombreFichero"].ToString();
            }
          }

          finally
          {
            if (Com != null)
              Com.Dispose();

            if (Con.State == ConnectionState.Open)
              Con.Close();

            Con.Dispose();
          }

          string Ruta = ConfigurationManager.AppSettings["RutaFicherosErrores"].ToString() + "\\TEMP\\" + ".log";

          Utilidad.EscribeLOG(Entidad, "Operador", "Descargado detalle de errores (Informe: " + Nombre + ".log) en nueva alta masiva (CALLCENTER)");

          Response.Clear();
          Response.ClearHeaders();
          Response.Buffer = false;
          Response.ContentType = "application/text";
          Response.AppendHeader("Content-Disposition", "Attachment; filename=\"" + Nombre + ".log\"");
          Response.AppendHeader("Content-Length", File.ReadAllBytes(Ruta).Length.ToString());
          Response.TransmitFile(Ruta);
          Response.Flush();
          Response.End();
          impersonar.undoImpersonation();
         }
      }

      catch (Exception Exc)
      {
        string Msg = string.Empty;

        if (Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);
        else
        {
          if (Utilidad.EsEntornoDesarrollo())
            Msg = Exc.ToString();
          else
            Msg = Exc.Message.ToString();
        }

        lblResultado.Text = Msg;

        string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "MostrarErrorModif", Escript, true);
        impersonar.undoImpersonation();
      }
    }

    protected void lbNuevaAlta_Click(object sender, EventArgs e)
    {
      Response.Redirect("~/res/massive.aspx");
    }

    protected void chkMultiContratoGeneral_CheckedChanged(object sender, EventArgs e)
    {
      CheckBox Caja = (CheckBox)sender;

      if(Caja.Checked)
      {
        string Escript = "$(document).ready(function() {$('#divMultiContrato').modal('show');});";
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MostrarMultiContrato", Escript, true);
      }

      chkMultiContratoValidacion.Checked = false;
      chkMultiContratoGeneral.Checked = false;
    }

    protected void lbMultiContratoAceptar_Click(object sender, EventArgs e)
    {
      if (!chkMultiContratoValidacion.Checked)
      {
        lblResultado.Text = Resources.massive.ResourceManager.GetString("ErrorValidacion", Thread.CurrentThread.CurrentCulture);

        string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "MostrarErrorModif", Escript, true);
      }
      else
      {
        chkMultiContratoGeneral.Checked = true;

        string Escript2 = "$(document).ready(function() {$('#divMultiContrato').modal('hide');});";
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "OcultarMultiContrato", Escript2, true);
      }
    }

    protected void lbMultiContratoCancelar_Click(object sender, EventArgs e)
    {
      chkMultiContratoValidacion.Checked = false;
      chkMultiContratoGeneral.Checked = false;

      string Escript2 = "$(document).ready(function() {$('#divMultiContrato').modal('hide');});";
      ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "OcultarMultiContrato", Escript2, true);
    }



    protected ArrayList Validar(string FicheroValidacion)
    {
      int ContadorFilas = 0;
      int filaAux = 3;
      int Error = 0;
      string CPF = string.Empty;
      string FirstName = string.Empty;
      string LastName = string.Empty;
      string Email = string.Empty;
      string MobilePhone = string.Empty;
      string Address = string.Empty;
      string PostalCode = string.Empty;
      string CompanyName = string.Empty;
      string Quantity = "-1";
      string NombreHoja = string.Empty;
      string Tipo = string.Empty;
      string ErrorMensaje = string.Empty;

      OleDbCommand OleCommand = null;
      OleDbConnection OleConn = null;
      OleDbDataReader OleReader = null;

      ArrayList RegistrosOK = new ArrayList();
      ArrayList RegistrosNOK = new ArrayList();
      ArrayList RegistrosVacios = new ArrayList();
      ArrayList CeldasAColorear = new ArrayList();
      ArrayList Registro = null;

      try
      {
        string NombreFichero = string.Empty;

        if (FicheroValidacion != null)
          NombreFichero = FicheroValidacion;
        else
          NombreFichero = DateTime.Today.ToString("yyyyMMdd") + "_" + System.Guid.NewGuid().ToString() + ".xlsx";

        string Extension = string.Empty;
        FicheroTemporal = ConfigurationManager.AppSettings["RutaFicherosErrores"].ToString() + "\\TEMP\\" + NombreFichero;
           

        if (FicheroValidacion == null)
        {
          if (fuMasivo.PostedFile == null || fuMasivo.PostedFile.FileName == string.Empty)
            throw new Exception("ERRNUM0012");

          if (fuMasivo.PostedFile != null)
            Extension = Path.GetExtension(fuMasivo.PostedFile.FileName);


          if (fuMasivo.PostedFile.FileName != string.Empty && Extension.ToLower() == ".xlsx")
            fuMasivo.PostedFile.SaveAs(FicheroTemporal);
          else
            throw new Exception("ERRNUM0012");
        }
      }

      catch (Exception Exc)
      {
        if (Utilidad.EsEntornoDesarrollo())
        {
          lblResultado.Text = Exc.ToString();
        }
        else
        {
          if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
            lblResultado.Text = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
          else
            lblResultado.Text = Exc.Message.ToString() + "<br /><br />";
        }

        string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MostrarErrorMassive", Escript, true);
      }

      try
      {
        string sExcelConn = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FicheroTemporal + ";Extended Properties=\"Excel 12.0;IMEX=1;HDR=YES;\"";

        if (OleConn == null)
          OleConn = new OleDbConnection(sExcelConn);

        if (OleConn.State == ConnectionState.Closed)
        {
          try
          {
            OleConn.Open();
          }

          catch(Exception EEE)
          {
            LOGe.AddInformationEntry(EEE.ToString());
          }

          NombreHoja = DameNombreHojaNumero(OleConn, 0);
        }

        if (NombreHoja == string.Empty)
          throw new Exception("ERRNUM0013");

        string sQuery = "SELECT * FROM [" + NombreHoja + "$]";

        if (OleCommand == null)
          OleCommand = new OleDbCommand(sQuery, OleConn);

        OleReader = OleCommand.ExecuteReader();

        if (OleReader != null)
        {
          if (!OleReader.HasRows)
            throw new Exception("ERRNUM0014");

          if (FicheroValidacion == null && OleReader.FieldCount != 9)
            throw new Exception("ERRNUM0015");

          if (OleReader.GetName(0).ToUpper().Trim() != "CPF")
            throw new Exception("ERRNUM0016");

          if (OleReader.GetName(1).ToUpper().Trim() != "FIRST NAME")
            throw new Exception("ERRNUM0017");

          if (OleReader.GetName(2).ToUpper().Trim() != "LAST NAME")
            throw new Exception("ERRNUM0018");

          if (OleReader.GetName(3).ToUpper().Trim() != "E-MAIL")
            throw new Exception("ERRNUM0019");

          if (OleReader.GetName(4).ToUpper().Trim() != "MOBILE PHONE")
            throw new Exception("ERRNUM0020");

          if (OleReader.GetName(5).ToUpper().Trim() != "ADDRESS")
            throw new Exception("ERRNUM0021");

          if (OleReader.GetName(6).ToUpper().Trim() != "POSTAL CODE")
            throw new Exception("ERRNUM0022");

          if (OleReader.GetName(7).ToUpper().Trim() != "COMPANY NAME")
            throw new Exception("ERRNUM0023");

          if (OleReader.GetName(8).ToUpper().Trim() != "QUANTITY")
            throw new Exception("ERRNUM0024");

          while (OleReader.Read())
          {
            ContadorFilas += 1;

            CPF = string.Empty;
            FirstName = string.Empty;
            LastName = string.Empty;
            Email = string.Empty;
            MobilePhone = string.Empty;
            Address = string.Empty;
            PostalCode = string.Empty;
            CompanyName = string.Empty;
            Quantity = "-1";
            Tipo = string.Empty;
            Error = 0;
            lblRespuesta.Text = string.Empty;

            if (OleReader[0] != null && OleReader[0].ToString().Trim() != string.Empty)
            {
              CPF = OleReader[0].ToString().Trim();
              Tipo = Utilidad.validarCPF(CPF);

              if (Tipo == string.Empty)
              {
                Error = 1;
                ErrorMensaje += Resources.errores.ResourceManager.GetString("ERRNUM0026", Thread.CurrentThread.CurrentCulture) + " " + filaAux.ToString() + ". " + Resources.errores.ResourceManager.GetString("ERRNUM0027", Thread.CurrentThread.CurrentCulture) + "<br />";

                if (FicheroValidacion != null)
                  CeldasAColorear.Add("B" + filaAux.ToString());
              }
            }

            if (OleReader[1] != null && OleReader[1].ToString().Trim() != string.Empty)
              FirstName = OleReader[1].ToString().Trim();

            if (OleReader[2] != null && OleReader[2].ToString().Trim() != string.Empty)
              LastName = OleReader[2].ToString().Trim();

            if (OleReader[3] != null && OleReader[3].ToString().Trim() != string.Empty)
            {
              Email = OleReader[3].ToString().Trim();
              string EmailOK = Utilidad.validarEmail(Email);

              if (EmailOK == string.Empty)
              {
                Error = 1;
                ErrorMensaje += Resources.errores.ResourceManager.GetString("ERRNUM0026", Thread.CurrentThread.CurrentCulture) + " " + filaAux.ToString() + ". " + Resources.errores.ResourceManager.GetString("ERRNUM0028", Thread.CurrentThread.CurrentCulture) + "<br />";

                if (FicheroValidacion != null)
                  CeldasAColorear.Add("E" + filaAux.ToString());
              }
            }

            if (OleReader[4] != null && OleReader[4].ToString().Trim() != string.Empty)
              MobilePhone = OleReader[4].ToString().Trim();

            if (OleReader[5] != null && OleReader[5].ToString().Trim() != string.Empty)
              Address = OleReader[5].ToString().Trim();

            if (OleReader[6] != null && OleReader[6].ToString().Trim() != string.Empty)
            {
              PostalCode = OleReader[6].ToString().Trim();

              string CEPOK = Utilidad.validarCEP(PostalCode);

              if (CEPOK == string.Empty)
              {
                Error = 1;
                ErrorMensaje += Resources.errores.ResourceManager.GetString("ERRNUM0026", Thread.CurrentThread.CurrentCulture) + " " + filaAux.ToString() + ". " + Resources.errores.ResourceManager.GetString("ERRNUM0029", Thread.CurrentThread.CurrentCulture) + "<br />";

                if (FicheroValidacion != null)
                  CeldasAColorear.Add("H" + filaAux.ToString());
              }
            }

            if (OleReader[7] != null && OleReader[7].ToString().Trim() != string.Empty)
              CompanyName = OleReader[7].ToString().Trim();

            if (Tipo == "CPF" && CompanyName != string.Empty)
            {
              Error = 1;
              ErrorMensaje += Resources.errores.ResourceManager.GetString("ERRNUM0026", Thread.CurrentThread.CurrentCulture) + " " + filaAux.ToString() + ". " + Resources.errores.ResourceManager.GetString("ERRNUM0030", Thread.CurrentThread.CurrentCulture) + "<br />";

              if (FicheroValidacion != null)
                CeldasAColorear.Add("I" + filaAux.ToString());
            }

            if (OleReader[8] != null && OleReader[8].ToString().Trim() != string.Empty)
              Quantity = OleReader[8].ToString().Trim();

            Registro = new ArrayList();
            Registro.Add(CPF);
            Registro.Add(FirstName);
            Registro.Add(LastName);
            Registro.Add(Email);
            Registro.Add(MobilePhone);
            Registro.Add(Address);
            Registro.Add(PostalCode);
            Registro.Add(CompanyName);
            Registro.Add(Quantity);
            Registro.Add(CeldasAColorear);

            if (CPF == string.Empty && FirstName == string.Empty && LastName == string.Empty && Email == string.Empty && MobilePhone == string.Empty && Address == string.Empty && PostalCode == string.Empty && (Tipo == string.Empty || (Tipo == "CNPJ" && CompanyName == string.Empty)) && Quantity == "-1")
            {
              Error = 2;
            }
            else
            {
              if (CPF == string.Empty || FirstName == string.Empty || LastName == string.Empty || Email == string.Empty || MobilePhone == string.Empty || Address == string.Empty || PostalCode == string.Empty || (Tipo == "CNPJ" && CompanyName == string.Empty) || Quantity == "-1")
              {
                string DatosFaltan = string.Empty;

                if (CPF == string.Empty)
                {
                  DatosFaltan += "CPF";
                  if (FicheroValidacion != null)
                    CeldasAColorear.Add("B" + filaAux.ToString());
                }

                if (FirstName == string.Empty)
                {
                  DatosFaltan += DatosFaltan == string.Empty ? "First name" : ", First name";
                  if (FicheroValidacion != null)
                    CeldasAColorear.Add("C" + filaAux.ToString());
                }

                if (LastName == string.Empty)
                {
                  DatosFaltan += DatosFaltan == string.Empty ? "Last name" : ", Last name";
                  if (FicheroValidacion != null)
                    CeldasAColorear.Add("D" + filaAux.ToString());
                }

                if (Email == string.Empty)
                {
                  DatosFaltan += DatosFaltan == string.Empty ? "E-mail" : ", E-mail";
                  if (FicheroValidacion != null)
                    CeldasAColorear.Add("E" + filaAux.ToString());
                }

                if (MobilePhone == string.Empty)
                {
                  DatosFaltan += DatosFaltan == string.Empty ? "Mobile phone" : ", Mobile phone";
                  if (FicheroValidacion != null)
                    CeldasAColorear.Add("F" + filaAux.ToString());
                }

                if (Address == string.Empty)
                {
                  DatosFaltan += DatosFaltan == string.Empty ? "Address" : ", Address";
                  if (FicheroValidacion != null)
                    CeldasAColorear.Add("G" + filaAux.ToString());
                }

                if (PostalCode == string.Empty)
                {
                  DatosFaltan += DatosFaltan == string.Empty ? "Postal code" : ", Postal code";
                  if (FicheroValidacion != null)
                    CeldasAColorear.Add("H" + filaAux.ToString());
                }

                if (Tipo == "CNPJ" && CompanyName == string.Empty)
                { 
                  DatosFaltan += DatosFaltan == string.Empty ? "Company name" : ", Company name";
                  if (FicheroValidacion != null)
                    CeldasAColorear.Add("I" + filaAux.ToString());
                }

                if (Quantity == "-1")
                {
                  DatosFaltan += DatosFaltan == string.Empty ? "Quantity" : ", Quantity";
                  if (FicheroValidacion != null)
                    CeldasAColorear.Add("J" + filaAux.ToString());
                }

                Error = 1;
                ErrorMensaje += Resources.errores.ResourceManager.GetString("ERRNUM0026", Thread.CurrentThread.CurrentCulture) + " " + filaAux.ToString() + ". " + Resources.errores.ResourceManager.GetString("ERRNUM0031", Thread.CurrentThread.CurrentCulture) + ": " + DatosFaltan + "<br />";
              }

              if (Quantity!="-1" && Convert.ToInt32(Quantity) <= 0)
              {
                Error = 1;
                ErrorMensaje += Resources.errores.ResourceManager.GetString("ERRNUM0026", Thread.CurrentThread.CurrentCulture) + " " + filaAux.ToString() + ". " + Resources.errores.ResourceManager.GetString("ERRNUM0032", Thread.CurrentThread.CurrentCulture) + "." + "<br />";

                if (FicheroValidacion != null)
                  CeldasAColorear.Add("J" + filaAux.ToString());
              }

              if (Quantity != "-1" && Convert.ToInt32(Quantity) > Convert.ToInt32(((ArrayList)DameProductoTumbamento(Utilidad.DameValorConfiguracion("FabricanteTumbamento")))[3]))
              {
                Error = 1;
                ErrorMensaje += Resources.errores.ResourceManager.GetString("ERRNUM0026", Thread.CurrentThread.CurrentCulture) + " " + filaAux.ToString() + ". " + Resources.errores.ResourceManager.GetString("ERRNUM0033", Thread.CurrentThread.CurrentCulture) + " " + ((ArrayList)DameProductoTumbamento(Utilidad.DameValorConfiguracion("FabricanteTumbamento")))[3].ToString() + "." + "<br />";

                if (FicheroValidacion != null)
                  CeldasAColorear.Add("J" + filaAux.ToString());
              }
            }

            //Si el registro tiene un error de formato lo guardamos para corregir, si no, lo guardamos para procesar
            if (FicheroValidacion == null)
            {
              switch (Error)
              {
                case 0:
                  RegistrosOK.Add(Registro);
                  break;
                case 1:
                  RegistrosNOK.Add(Registro);
                  break;
                case 2:
                  RegistrosVacios.Add(Registro);
                  break;

                default:
                  throw new Exception("Unexpected error");
              }
            }

            filaAux++;
          }

          if (FicheroValidacion == null)
          {
            lblRegsTOTAL.Text = ContadorFilas.ToString();
            lblRegsOK.Text = RegistrosOK.Count.ToString();
            lblRegsNOK.Text = RegistrosNOK.Count.ToString();
            lblRegsVacios.Text = RegistrosVacios.Count.ToString();

            string Escript = @"document.getElementById('" + Total.ClientID + @"').value=" + RegistrosOK.Count.ToString() + @";
            a=setInterval(CalcularPorcentaje,300)";

            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "BarraProgreso", Escript, true);
          }
        }
      }

      catch (Exception ex)
      {
        if (OleReader != null)
          lblRespuesta.Text += "<br />" + Resources.errores.ResourceManager.GetString("ERRNUM0026", Thread.CurrentThread.CurrentCulture) + " " + filaAux.ToString() + ". " + ex.Message;
        else
          lblRespuesta.Text = ex.Message;
      }

      finally
      {
        if (OleConn != null)
        {
          OleConn.Close();
          OleConn.Dispose();

          if (OleCommand != null)
            OleCommand.Dispose();
        }

        if (OleReader != null)
        {
          OleReader.Close();
          OleReader.Dispose();
        }

        if (!String.IsNullOrEmpty(FicheroTemporal) && FicheroValidacion==null)
          File.Delete(FicheroTemporal);

        if (lblResultado.Text != string.Empty)
        {
          string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
          Page.ClientScript.RegisterStartupScript(Page.GetType(), "MostrarErrorMassive", Escript, true);
        }
      }

      //Si es la primera pasada (es decir, se está validando el fichero subido por el usuario) y se han generado errores, guardamos el excel de Errores y lo validamos.
      if (FicheroValidacion == null)
      {
        if (RegistrosNOK.Count > 0)
        {
          string NombreFicheroErrores = GenerarExcel(RegistrosNOK);
          //Una vez generado, volvemos a validarlo para generar el informe de errores.
          ArrayList Temp = Validar(NombreFicheroErrores);
        }
      }
      else
      {
        ColorearCelda(FicheroValidacion, CeldasAColorear);

        cLog LOG = new cLog("\\TEMP\\" + FicheroValidacion.Replace(".xlsx",".log").Replace("temp_",""));
        LOG.AddLiteralEntry(ErrorMensaje.Replace("<br />", Environment.NewLine), "\\TEMP\\" + FicheroValidacion.Replace(".xlsx", ".log").Replace("temp_", ""));

        //Guardamos en BD el nombre del fichero de errores/logs para futuras referencias.
        ActualizarRegistroProvision(Convert.ToInt32(ViewState["IdRegistro"]), null, null, FicheroValidacion.Replace(".xlsx","").Replace("temp_", ""));
      }

      return RegistrosOK;
    }

    protected int Cargar(ArrayList RegistrosOK,int IdRegistro)
    {
      SqlTransaction Trans = null;
      SqlConnection Con = null;
      int IdISP = -1;
      int IdPartner = -1;
      int ContadorUsuariosCargadosOK = 0;
      int ContadorUsuariosCargadosNOK = 0;

      if (RegistrosOK.Count == 0 && Convert.ToInt32(lblRegsNOK.Text)>0)
      {
        if((new LIVINGSAFE_SiteCallCenter.Clases.cPermisos()).TienePermiso("DESCARGAREXCELERRORESALTAMASIVA", Entidad) || (new LIVINGSAFE_SiteCallCenter.Clases.cPermisos()).TienePermiso("DECARGARLOGERRORESALTAMASIVA", Entidad))
          divInformeErrores.Visible = true;

        throw new Exception("ERRNUM0025");
      }

      Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ConnectionString);
      Con.Open();
      Trans = Con.BeginTransaction();

      SqlCommand Com = Con.CreateCommand();

      try
      {
        IdISP = Convert.ToInt32(Utilidad.DameDatoOperador(Entidad, "IdISPOperador"));
        IdPartner = Convert.ToInt32(Utilidad.DameDatoOperador(Entidad, "IdPartner"));

        Com.CommandType = System.Data.CommandType.StoredProcedure;
        Com.CommandText = "Provision_Masiva_Guardar";
        Com.CommandTimeout = 300;
        Com.Transaction = Trans;

        for (int i = 0; i < RegistrosOK.Count; i++)
        {
          Com.Parameters.Clear();

          Com.Parameters.Add("@IdPartner", SqlDbType.Int).Value = IdPartner;
          Com.Parameters.Add("@IdISP", SqlDbType.Int).Value = IdISP;
          Com.Parameters.Add("@IdProducto", SqlDbType.Int).Value = Convert.ToInt32(((ArrayList)DameProductoTumbamento(Utilidad.DameValorConfiguracion("FabricanteTumbamento")))[0]);
          Com.Parameters.Add("@NIF", SqlDbType.VarChar, 25).Value = ((ArrayList)RegistrosOK[i])[0];
          Com.Parameters.Add("@Nombre", SqlDbType.VarChar, 500).Value = ((ArrayList)RegistrosOK[i])[1];
          Com.Parameters.Add("@Apellidos", SqlDbType.VarChar, 500).Value = ((ArrayList)RegistrosOK[i])[2];
          Com.Parameters.Add("@Email", SqlDbType.VarChar, 500).Value = ((ArrayList)RegistrosOK[i])[3];
          Com.Parameters.Add("@Movil", SqlDbType.VarChar, 50).Value = ((ArrayList)RegistrosOK[i])[4];
          Com.Parameters.Add("@Direccion", SqlDbType.VarChar, 500).Value = ((ArrayList)RegistrosOK[i])[5];
          Com.Parameters.Add("@CPostalTexto", SqlDbType.VarChar, 9).Value = ((ArrayList)RegistrosOK[i])[6];

          if(((ArrayList)RegistrosOK[i])[7]!=null && ((ArrayList)RegistrosOK[i])[7].ToString()!=string.Empty)
            Com.Parameters.Add("@Empresa", SqlDbType.VarChar, 500).Value = ((ArrayList)RegistrosOK[i])[7];

          Com.Parameters.Add("@Cantidad", SqlDbType.Int).Value = ((ArrayList)RegistrosOK[i])[8];

          if(chkMultiContratoGeneral.Checked && chkMultiContratoValidacion.Checked)
            Com.Parameters.Add("@MultiContrato", SqlDbType.Int).Value = 1;
          else
            Com.Parameters.Add("@MultiContrato", SqlDbType.Int).Value = 0;

          Com.Parameters.Add("PrecioVenta", SqlDbType.Float).Value = "0,01";
          Com.Parameters.Add("@Estado", SqlDbType.Int).Value = 0;

          if(IdRegistro>0)
            Com.Parameters.Add("@IdRegistro", SqlDbType.Int).Value = IdRegistro;

          try
          {
            Com.ExecuteNonQuery();
            ContadorUsuariosCargadosOK += 1;
          }

          catch(Exception Exc)
          {
            if (Exc.Message != "PMG001")
              throw;
            else
              ContadorUsuariosCargadosNOK += 1;
          }
        }

        Trans.Commit();
      }

      catch(Exception)
      {
        if (Trans != null)
          Trans.Rollback();

        throw;
      }

      finally
      {
        if (Con != null && Con.State==ConnectionState.Open)
          Con.Close();

        Con.Dispose();

        if (ContadorUsuariosCargadosOK == 0)
        {
          lblRegsProcesar.Text = Resources.massive.ResourceManager.GetString("litProcesarNinguno", Thread.CurrentThread.CurrentCulture);
          lbNuevaAlta.Visible = true;
        }
        else
        {
          if (ContadorUsuariosCargadosNOK > 0)
          {
            lblRegsProcesar.Text = ContadorUsuariosCargadosOK.ToString() + " " + Resources.massive.ResourceManager.GetString("litProcesarParcial1", Thread.CurrentThread.CurrentCulture) + " " + RegistrosOK.Count.ToString() + " " + Resources.massive.ResourceManager.GetString("litProcesarParcial2", Thread.CurrentThread.CurrentCulture);
            lblRegsProcesar.Text += " " + Resources.massive.ResourceManager.GetString("litProcesarParcial3", Thread.CurrentThread.CurrentCulture) + ContadorUsuariosCargadosNOK.ToString() + Resources.massive.ResourceManager.GetString("litProcesarParcial4", Thread.CurrentThread.CurrentCulture);
          }
          else
          {
            lblRegsProcesar.Text = ContadorUsuariosCargadosOK.ToString() + " " + Resources.massive.ResourceManager.GetString("litProcesarTodos1", Thread.CurrentThread.CurrentCulture) + " " + RegistrosOK.Count.ToString() + " " + Resources.massive.ResourceManager.GetString("litProcesarTodos2", Thread.CurrentThread.CurrentCulture);
          }
        }
      }

      return ContadorUsuariosCargadosOK;
    }

    protected void Procesar(int IdRegistro)
    {
      string GUIDISP = String.Empty;
      string SKU = String.Empty;
      int cantidad = -1;
      string partner = String.Empty;
      string email = String.Empty;
      string nombre = String.Empty;
      string apellido = String.Empty;
      string CP = String.Empty;
      string CPF_CNPJ = String.Empty;
      string movil = String.Empty;
      string direccion = String.Empty;
      string empresa = String.Empty;
      double pvp = 0.01;
      int id = -1;
      int IdISP = -1;
      int IdPartner = -1;
      int ContadorOK = 0;
      int ContadorNOK = 0;

      //Inicializar las variables con datos de  P.A de BBDD
      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = null;
      SqlDataReader Rst = null;

      try
      {
        IdISP = Convert.ToInt32(Utilidad.DameDatoOperador(Entidad, "IdISPOperador"));
        IdPartner = Convert.ToInt32(Utilidad.DameDatoOperador(Entidad, "IdPartner"));

        Com = Con.CreateCommand();
        Com.CommandType = System.Data.CommandType.StoredProcedure;
        Com.CommandText = "Provison_DameDatosProvisionMasiva";
        Com.CommandTimeout = 300;
        Com.Parameters.Add("@estado", SqlDbType.Int).Value = 0;
        Com.Parameters.Add("@idIsp", SqlDbType.Int).Value = IdISP;

        Con.Open();
        Rst = Com.ExecuteReader();

        while (Rst.Read())
        {
          Login(IdISP);

          GUIDISP = string.Empty;
          SKU = string.Empty;
          cantidad = 0;
          partner = string.Empty;
          email = string.Empty;
          nombre = string.Empty;
          apellido = string.Empty;
          CP = string.Empty;
          CPF_CNPJ = string.Empty;
          movil = string.Empty;
          direccion = string.Empty;
          id = 0;
          empresa = string.Empty;
          pvp = 0.0;

          GUIDISP = Rst["UsernameAPI"].ToString();
          SKU = Rst["SKU"].ToString();
          cantidad = Convert.ToInt32(Rst["Cantidad"].ToString());
          partner = Rst["GUID"].ToString();
          email = Rst["Email"].ToString();
          nombre = Rst["Nombre"].ToString();
          apellido = Rst["Apellidos"].ToString();
          CP = Rst["CPostalTexto"].ToString();
          CPF_CNPJ = Rst["NIF"].ToString();
          movil = Rst["Movil"].ToString();
          direccion = Rst["Direccion"].ToString();
          id = Convert.ToInt32(Rst["id"].ToString());

          if (Rst["Empresa"] != null && Rst["Empresa"].ToString() != String.Empty)
            empresa = Rst["Empresa"].ToString();

          pvp = Convert.ToDouble(Rst["PrecioVenta"].ToString());

          string aux = Provisionar(GUIDISP, SKU, cantidad, partner, email, nombre, apellido, CP, CPF_CNPJ, movil, direccion, empresa, pvp, IdRegistro);

          OrderResponse res = JsonConvert.DeserializeObject<OrderResponse>(aux);

          if (res.error != String.Empty)
          {
            ActualizarEstadoProvison(id, 66);
            ContadorNOK += 1;
          }
          else
          {
            ActualizarEstadoProvison(id, 100);
            ContadorOK += 1;
          }

          System.Threading.Thread.Sleep(500);
        }
      }

      catch
      {
        ActualizarEstadoProvison(id, 66);
        throw;
      }

      finally
      {
        if (Rst != null && !Rst.IsClosed)
          Rst.Close();

        if (Com != null)
          Com.Dispose();

        if (Con.State == ConnectionState.Open)
          Con.Close();

        Con.Dispose();
      }
    }

    protected string DameNombreHojaNumero(OleDbConnection objConn, int pos)
    {
      int ContadorFilas;
      string[] Hojas = null;

      DataTable dt = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
      
      if (dt != null)
      {
        ContadorFilas = dt.Rows.Count;
        Hojas = new String[ContadorFilas];
        int i = 0;

        foreach (DataRow row in dt.Rows)
        {
          Hojas[i] = row["TABLE_NAME"].ToString().Trim().Replace("'", "").Replace("$", "");
          i++;
        }
      }

      if (Hojas[pos] != null && Hojas[pos] != string.Empty)
        return Hojas[pos];
      else
        return string.Empty;
    }

    protected ArrayList DameProductoTumbamento(string Fabricante)
    {
      ArrayList ProductoTumbamento = new ArrayList();

      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = Con.CreateCommand();

      try
      {
        Com.CommandType = System.Data.CommandType.StoredProcedure;
        Com.CommandText = "Productos_DameProductoTumbamentoPorMarca";
        Com.CommandTimeout = 300;
        Com.Parameters.Add("@Fabricante", SqlDbType.VarChar,500).Value = Fabricante;
        Con.Open();

        SqlDataReader Rst = Com.ExecuteReader();

        if(Rst.Read())
        {
          ProductoTumbamento.Add(Convert.ToInt32(Rst["IdProducto"]));
          ProductoTumbamento.Add(Convert.ToDouble(Rst["PrecioEuros"]));
          ProductoTumbamento.Add(Convert.ToInt32(Rst["RangoI"]));
          ProductoTumbamento.Add(Convert.ToInt32(Rst["RangoS"]));
        }

        Rst.Close();
        Rst.Dispose();
      }

      finally
      {
        if (Con.State == ConnectionState.Open)
          Con.Close();

        Com.Dispose();
        Con.Dispose();
      }

      return ProductoTumbamento;
    }

    protected void Login(int IdISP)
    {
      cLivingSafeAPI util = new cLivingSafeAPI();
      LoginRequest login = new LoginRequest();
      login.UsernameAPI = Utilidad.DameDatoISP(IdISP, "UsernameAPI");
      login.PasswordAPI = Encriptacion.DescifrarAES(IdISP, Utilidad.DameDatoISP(IdISP, "PasswordAPI"));
      token = string.Empty;

      JsonSerializerSettings ConfigJson = new JsonSerializerSettings(); //Evitar que se serialicen los nodos cuyo valor sea nulo (Los ignora).
      ConfigJson.NullValueHandling = NullValueHandling.Ignore;
      byte[] Datos = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(login, Formatting.None, ConfigJson));
      token = util.RealizarSolicitudPOST(Utilidad.DameRutaAPI("RutaAPI_Login"), Datos, token);
    }

    protected string Provisionar(string GUIDISP, string SKU, int cantidad, string partner, string email, string nombre, string apellido,string CP, string CPF_CNPJ, string movil, string direccion, string empresa, double pvp, int? IdRegistro)
    {
      OrderRequest Orden = new OrderRequest();
      Orden.IspGuid = GUIDISP;
      Orden.SKU = SKU;
      Orden.Quantity = cantidad;
      Orden.PartnerGuid = partner;

      if(IdRegistro!=null && IdRegistro>0)
        Orden.IdRegistro = Convert.ToInt32(IdRegistro);

      Orden.customer = new CustomerRequest();
      Orden.customer.email = email;
      Orden.customer.FirstName = nombre;
      Orden.customer.LastName = apellido;
      Orden.customer.CP = CP;
      Orden.customer.CPF_CNPJ = CPF_CNPJ;
      Orden.customer.mobile = movil;
      Orden.customer.Address = direccion;
      Orden.PVP = pvp;

      if (empresa != string.Empty)
        Orden.customer.company = empresa;

      JsonSerializerSettings ConfigJson = new JsonSerializerSettings();

      byte[] Datos = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(Orden, Formatting.None, ConfigJson));
      string resultado = RealizarSolicitudPOST(Utilidad.DameRutaAPI("RutaAPI_NewOrder"), Datos, token.Replace("\"", ""));

      return resultado;
    }

    protected string RealizarSolicitudPOST(string ruta, byte[] Datos, string token)
    {
      string ResultadoCadena = String.Empty;

      if (ruta == String.Empty)
        throw new Exception("Source not valid.");

      if (Datos == null && token == String.Empty)
        throw new Exception("Data not valid.");

      HttpWebRequest WReq = (HttpWebRequest)HttpWebRequest.Create(ruta);
      if (token != String.Empty)
        WReq.Headers["Authorization"] = "Bearer " + token;

      WReq.ContentType = "application/json; charset=UTF-8";

      WReq.ContentLength = Datos.Length;

      WReq.Method = "POST";
      WReq.GetRequestStream().Write(Datos, 0, Datos.Length);

      HttpWebResponse res = (HttpWebResponse)WReq.GetResponse();
      Encoding Codificacion = ASCIIEncoding.UTF8;

      StreamReader SReader = new StreamReader(res.GetResponseStream(), Codificacion);
      ResultadoCadena = SReader.ReadToEnd();

      return ResultadoCadena;
    }

    protected void ActualizarEstadoProvison(int Id, int Estado)
    {
      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = null;

      try
      {
        Con.Open();
        Com = Con.CreateCommand();
        Com.CommandType = System.Data.CommandType.StoredProcedure;
        Com.CommandText = "dbo.Provison_AcualizarProvisionMasiva";
        Com.CommandTimeout = 300;
        Com.Parameters.Add("@id", SqlDbType.Int).Value = Id;
        Com.Parameters.Add("@estado", SqlDbType.Int).Value = Estado;
        Com.ExecuteNonQuery();
      }

      finally
      {
        if (Com != null)
          Com.Dispose();

        if (Con.State == ConnectionState.Open)
          Con.Close();
        
        Con.Dispose();
      }
    }

    protected int InsertarRegistroProvision(int IdISP, int IdOperador)
    {
      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = null;
      int IdRegistro = 0;

      try
      {
        Con.Open();
        Com = Con.CreateCommand();
        Com.CommandType = System.Data.CommandType.StoredProcedure;
        Com.CommandText = "Provision_Masiva_Registro_Insertar";
        Com.CommandTimeout = 300;
        Com.Parameters.Add("@IdISP", SqlDbType.Int).Value = IdISP;
        Com.Parameters.Add("@IdOperador", SqlDbType.Int).Value = IdOperador;
        Com.Parameters.Add("Tipo", SqlDbType.Char, 1).Value = 'A';

        SqlParameter parIdRegistro = Com.Parameters.Add("@RETURN_VALUE", SqlDbType.Int);
        parIdRegistro.Direction = ParameterDirection.ReturnValue;

        Com.ExecuteNonQuery();

        IdRegistro = Convert.ToInt32(parIdRegistro.Value);
      }

      finally
      {
        if (Com != null)
          Com.Dispose();

        if (Con.State == ConnectionState.Open)
          Con.Close();

        Con.Dispose();
      }

      return IdRegistro;
    }

    protected void ActualizarRegistroProvision(int IdRegistro,bool? Finalizado,DateTime? FechaProcesado,string NombreFichero)
    {
      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = null;

      try
      {
        Con.Open();
        Com = Con.CreateCommand();
        Com.CommandType = System.Data.CommandType.StoredProcedure;
        Com.CommandText = "Provision_Masiva_Registro_Actualizar";
        Com.CommandTimeout = 300;
        Com.Parameters.Add("@IdRegistro", SqlDbType.Int).Value = IdRegistro;

        if(Finalizado!=null)
          Com.Parameters.Add("@Finalizado", SqlDbType.Bit).Value = Finalizado;

        if(FechaProcesado!=null)
          Com.Parameters.Add("@FechaProcesado", SqlDbType.DateTime).Value = FechaProcesado;

        if (NombreFichero!=null)
          Com.Parameters.Add("@NombreFichero", SqlDbType.VarChar,-1).Value = NombreFichero;

        Com.ExecuteNonQuery();
      }

      finally
      {
        if (Com != null)
          Com.Dispose();

        if (Con.State == ConnectionState.Open)
          Con.Close();

        Con.Dispose();
      }
    }

    protected int DameTotalProvisionesPendientes(int IdISP,int IdRegistro,int Estado)
    {
      int Total = 0;
      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = null;

      try
      {
        Con.Open();
        Com = Con.CreateCommand();
        Com.CommandType = System.Data.CommandType.StoredProcedure;
        Com.CommandText = "Provision_Masiva_Pendientes";
        Com.CommandTimeout = 300;

        if (IdISP>0)
          Com.Parameters.Add("@IdISP", SqlDbType.Int).Value = IdISP;

        if(IdRegistro>0)
          Com.Parameters.Add("@IdRegistro", SqlDbType.Int).Value = IdRegistro;

        Com.Parameters.Add("@Estado", SqlDbType.Int).Value = Estado;


        SqlParameter parTotal = Com.Parameters.Add("@RETURN_VALUE", SqlDbType.Int);
        parTotal.Direction = ParameterDirection.ReturnValue;

        Com.ExecuteNonQuery();

        Total = Convert.ToInt32(parTotal.Value);
      }

      finally
      {
        if (Com != null)
          Com.Dispose();

        if (Con.State == ConnectionState.Open)
          Con.Close();

        Con.Dispose();
      }

      return Total;
    }

    protected string GenerarExcel(ArrayList Errores)
    {
      string Nombre = string.Empty;
      string Sufijo = DateTime.Now.Year.ToString("0000") + DateTime.Now.Month.ToString("00") + DateTime.Now.Day.ToString("00") + DateTime.Now.Hour.ToString("00") + DateTime.Now.Minute.ToString("00") + DateTime.Now.Second.ToString("00");

      using (ExcelPackage Excel = new ExcelPackage())
      {
        ExcelWorksheet Hoja = Excel.Workbook.Worksheets.Add("ERRORS");

        Hoja.Cells["B2"].LoadFromText("CPF");
        Hoja.Cells["C2"].LoadFromText("FIRST NAME");
        Hoja.Cells["D2"].LoadFromText("LAST NAME");
        Hoja.Cells["E2"].LoadFromText("E-MAIL");
        Hoja.Cells["F2"].LoadFromText("MOBILE PHONE");
        Hoja.Cells["G2"].LoadFromText("ADDRESS");
        Hoja.Cells["H2"].LoadFromText("POSTAL CODE");
        Hoja.Cells["I2"].LoadFromText("COMPANY NAME");
        Hoja.Cells["J2"].LoadFromText("QUANTITY");

        for (int i = 0; i < Errores.Count; i++)
        {
          ArrayList linea = (ArrayList)Errores[i];

          for (int r = 0; r < linea.Count - 1; r++)
          {
            if (r == 0)
              Hoja.Cells["B" + (i + 3).ToString()].LoadFromText(linea[r].ToString());
            if (r == 1)
              Hoja.Cells["C" + (i + 3).ToString()].LoadFromText(linea[r].ToString());
            if (r == 2)
              Hoja.Cells["D" + (i + 3).ToString()].LoadFromText(linea[r].ToString());
            if (r == 3)
              Hoja.Cells["E" + (i + 3).ToString()].LoadFromText(linea[r].ToString());
            if (r == 4)
              Hoja.Cells["F" + (i + 3).ToString()].LoadFromText(linea[r].ToString());
            if (r == 5)
              Hoja.Cells["G" + (i + 3).ToString()].LoadFromText(linea[r].ToString());
            if (r == 6)
              Hoja.Cells["H" + (i + 3).ToString()].LoadFromText(linea[r].ToString());
            if (r == 7)
              Hoja.Cells["I" + (i + 3).ToString()].LoadFromText(linea[r].ToString());
            if (r == 8)
              Hoja.Cells["J" + (i + 3).ToString()].LoadFromText(linea[r].ToString());
          }
        }

        Nombre = "temp_ERRORS_" + Sufijo + ".xlsx";
        string filePath = ConfigurationManager.AppSettings["RutaFicherosErrores"].ToString() + "\\TEMP\\" + Nombre;

        using (FileStream Fichero = new FileStream(filePath, FileMode.Create))
        {
          Excel.SaveAs(Fichero);
        }
      }
      
      return Nombre;
    }

    protected void ColorearCelda(string NombreFichero,ArrayList CeldasError)
    {
      using (FileStream Fichero = new FileStream(ConfigurationManager.AppSettings["RutaFicherosErrores"].ToString() + "\\TEMP\\" + NombreFichero, FileMode.Open, FileAccess.Read, FileShare.Read))
      {
        using (FileStream FicheroFinal = new FileStream(ConfigurationManager.AppSettings["RutaFicherosErrores"].ToString() + "\\TEMP\\"+ NombreFichero.Replace("temp_", ""), FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite))
        {
          using (ExcelPackage Excel = new ExcelPackage(Fichero))
          {
            ExcelWorksheet Hoja = Excel.Workbook.Worksheets[1];

            for (int i = 0; i < CeldasError.Count; i++)
            {
              Hoja.Cells[CeldasError[i].ToString()].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
              Hoja.Cells[CeldasError[i].ToString()].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightSteelBlue);
            }

            Excel.SaveAs(FicheroFinal);
          }
        }
      }

            if (System.IO.File.Exists(ConfigurationManager.AppSettings["RutaFicherosErrores"].ToString() + "\\TEMP\\" + NombreFichero))
                System.IO.File.Delete(ConfigurationManager.AppSettings["RutaFicherosErrores"].ToString() + "\\TEMP\\" + NombreFichero);

    }

    protected void EliminarRegistroHuerfano(int IdISP,int IdRegistro)
    {
      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = null;

      try
      {
        Con.Open();
        Com = Con.CreateCommand();
        Com.CommandType = System.Data.CommandType.StoredProcedure;
        Com.CommandText = "Provision_Masiva_Registro_LimpiarProvisionesHuerfanas";
        Com.CommandTimeout = 300;
        Com.Parameters.Add("@IdRegistro", SqlDbType.Int).Value = IdRegistro;
        Com.Parameters.Add("@IdISP", SqlDbType.Int).Value = IdISP;
        Com.Parameters.Add("@Dias", SqlDbType.Int).Value = 365;
        Com.ExecuteNonQuery();
      }

      finally
      {
        if (Com != null)
          Com.Dispose();

        if (Con.State == ConnectionState.Open)
          Con.Close();

        Con.Dispose();
      }
    }
  }
}