﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using APIREST;
using LIVINGSAFE_SiteCallCenter.App_Code;
using Newtonsoft.Json;
using LIVINGSAFE_SiteCallCenter.Clases;

namespace LIVINGSAFE_SiteCallCenter.res
{
  class ElementoProvison
  {
    public string parametro { get; set; }

    public string valor { get; set; }
  }

  public partial class orders : System.Web.UI.Page
  {
    cUtil Utilidad = new cUtil();
    cIdiomas Idiomas = new cIdiomas();
    cEncriptacion Encriptacion = new cEncriptacion();
    cPermisos Permisos = new cPermisos();
    string token = string.Empty;
    int Entidad = Convert.ToInt32(HttpContext.Current.User.Identity.Name);
    int IdUsuarioFinal = 0;

    protected void Page_Init(object sender, EventArgs e)
    {
      IdUsuarioFinal = DameIdUsuarioFinal(Request.QueryString["i"]);
      int IdIdioma = Idiomas.DameIdIdioma(Idiomas.DameIdioma());
      sqlPedidos.SelectParameters["IdOperador"].DefaultValue = HttpContext.Current.User.Identity.Name;
      sqlPedidos.SelectParameters["IdUsuarioFinal"].DefaultValue = IdUsuarioFinal.ToString();
      sqlPedidos.SelectParameters["IdIdioma"].DefaultValue = IdIdioma.ToString();
      sqlPedidos.SelectParameters["IdMonedaDestino"].DefaultValue = ConfigurationManager.AppSettings["IdMonedaDefecto"];
      sqlPedidos.SelectParameters["MostrarTodos"].DefaultValue = "false";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
      try
      {
        //Si el usuario logado no tiene permisos suficientes, no puede ver la columna de Operador
        gvwPedidos.Columns[1].Visible = (new LIVINGSAFE_SiteCallCenter.Clases.cPermisos()).TienePermiso("COLGRIDORDERSOPERADORES", Entidad);

        int IdUsuario = IdUsuarioFinal;

        if (IdUsuario==-1)
          Response.Redirect("~/forbidden.aspx");

        CargarUsuario(IdUsuario);
        RegisterPostBackControl();
        MostrarColumaOperador();
        

        //if(!IsPostBack)
        //  CargarProductos(Convert.ToInt32(Utilidad.DameDatoOperador(Entidad, "IdISPOperador")), Idiomas.DameIdIdioma(Idiomas.DameIdioma()));
      }

      catch (Exception Exc)
      {
        if (Utilidad.EsEntornoDesarrollo())
        {
          lblResultado.Text = Exc.ToString();
        }
        else
        {
          if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
            lblResultado.Text = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
          else
            lblResultado.Text = Exc.Message.ToString() + "<br /><br />";
        }

        string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MostrarErrorPedidos", Escript, true);
      }
    }

    protected void lnkModificar_Click(object sender, EventArgs e)
    {
			//Cargamos el desplegable de productos. Si es para modificar la cantidad, sólo pueden mostrarse los productos que lo tengan permitido. Creamos nueva ZONA.
			CargarProductos(Convert.ToInt32(Utilidad.DameDatoOperador(Entidad, "IdISPOperador")), Idiomas.DameIdIdioma(Idiomas.DameIdioma()),"DESPLEGABLEPRODUCTOSCAMBIOCANTIDAD");


      int IdPedidoDetalle = Convert.ToInt32(((ImageButton)sender).CommandArgument);
      

      lblResultado.Text = String.Empty;
      lblTituloCapa.Text = Resources.orders.ResourceManager.GetString("formTitulo2", Thread.CurrentThread.CurrentCulture);

      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = Con.CreateCommand();

      btnGuardar.Visible = true;

      try
      {
        Com.CommandType = System.Data.CommandType.StoredProcedure;
        Com.CommandText = "Pedidos_DamePedidoDetalle";
        Com.CommandTimeout = 300;
        Com.Parameters.Add("@IdPedidoDetalle", SqlDbType.Int).Value = IdPedidoDetalle;

        Con.Open();

        SqlDataReader Rst = Com.ExecuteReader();

        lblProductoActual.Text = string.Empty;
        divProductoActual.Visible = false;
        litProducto.Text = Resources.orders.ResourceManager.GetString("formUsuariosProducto", Thread.CurrentThread.CurrentCulture);

        bool Entra = false;

        if (Rst.Read())
        {
          ListItem Elemento = ddlProductos.Items.FindByValue(Rst["SKU"].ToString());
          ddlProductos.ClearSelection();

          if (Elemento != null)
          {
            Entra = true;
            Elemento.Selected = true;
          }

          txtCantidad.Text = Rst["Cantidad"].ToString();
          txtPrecio.Text = String.Format("{0:0.00}", Convert.ToDouble(Rst["PrecioVenta"]));

					btnGuardar.CommandName = "MODIFICAR";
          btnGuardar.CommandArgument = Rst["GUID_PD"].ToString();

          ddlProductos.Enabled = false;
          txtPrecio.Enabled = false;

          if (!Entra)
            throw new Exception(Resources.errores.ResourceManager.GetString("ERRNUM0041", Thread.CurrentThread.CurrentCulture));

          //Mostramos al final porque si hay error antes no quiero que la capa salga.
          Page.ClientScript.RegisterStartupScript(this.GetType(), "Show", "$(document).ready(function() {$('.bd-modificar-modal-lg').modal('show');});", true);
        }
      }

      catch (Exception Exc)
      {
        if (Utilidad.EsEntornoDesarrollo())
        {
          lblResultado.Text = Exc.ToString();
        }
        else
        {
          if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
            lblResultado.Text = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
          else
            lblResultado.Text = Exc.Message.ToString() + "<br /><br />";
        }

        string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MostrarErrorAlta", Escript, true);
      }

      finally
      {
        Com.Dispose();
        Con.Close();
        Con.Dispose();
      }
    }

    protected void lnkBorrar_Click(object sender, EventArgs e)
    {
      ScriptManager.RegisterStartupScript(this.Page,Page.GetType(), "ShowDel", "$(document).ready(function() {$('.bd-borrar-modal-lg').modal('show');});", true);
      string GUID = ((ImageButton)sender).CommandArgument;

      if (GUID == null || GUID == string.Empty)
        throw new Exception("ERRNUM0008");

      lbConfirmarBaja.CommandArgument = GUID;
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = Con.CreateCommand();
      string Identificador = string.Empty;
			string Tipo = string.Empty;

      try
      {
				Identificador = ((Button)sender).CommandArgument;
				Tipo = ((Button)sender).CommandName;

				//Si se quiere cambiar la cantidad (Tipo=MODIFICAR)
				if (Tipo == "MODIFICAR")
				{
					if (Identificador == null || Identificador == string.Empty)
						throw new Exception("ERRNUM0008");

					cLivingSafeAPI util = new cLivingSafeAPI();
					int IdISP = Convert.ToInt32(Utilidad.DameDatoOperador(Entidad, "IdISPOperador"));

					LoginRequest login = new LoginRequest();
					login.UsernameAPI = Utilidad.DameDatoISP(IdISP, "UsernameAPI");
					login.PasswordAPI = Encriptacion.DescifrarAES(IdISP, Utilidad.DameDatoISP(IdISP, "PasswordAPI"));

					JsonSerializerSettings ConfigJson = new JsonSerializerSettings(); //Evitar que se serialicen los nodos cuyo valor sea nulo (Los ignora).
					ConfigJson.NullValueHandling = NullValueHandling.Ignore;
					byte[] Datos = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(login, Formatting.None, ConfigJson));
					token = util.RealizarSolicitudPOST(Utilidad.DameRutaAPI("RutaAPI_Login"), Datos, token);


					OrderDetailsUpdateRequest o = new OrderDetailsUpdateRequest();
					o.PartnerGuid = Utilidad.DameDatoISP(IdISP, "PartnerGUID");
					o.IspGuid = Utilidad.DameDatoISP(IdISP, "UsernameAPI");
					o.OrderDetailsGuid = Identificador;
					o.Quantity = Convert.ToInt32(txtCantidad.Text);
					o.PVP = Convert.ToDouble(txtPrecio.Text);
					o.OperatorLogin = Entidad.ToString();

					ConfigJson = new JsonSerializerSettings();
					Datos = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(o, Formatting.None, ConfigJson));
					string x = util.RealizarSolicitudPOST(Utilidad.DameRutaAPI("RutaAPI_ModifyQuantity"), Datos, token.Replace("\"", ""));

					OrderDetailsUpdateResponse res = JsonConvert.DeserializeObject<OrderDetailsUpdateResponse>(x);

					if (res.error != String.Empty)
						throw new Exception(res.error);
					else
					{
						lblResultado.Text = Resources.orders.ResourceManager.GetString("LiteralModificacionOK", Thread.CurrentThread.CurrentCulture);

						CerrarFormulario();

						string Escript = "$(document).ready(function() {$('.bd-modificar-modal-lg').modal('hide');});$(document).ready(function() {$('#divAlerta').modal('show');});";
						ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MostrarOKModif", Escript, true);

						Utilidad.EscribeLOG(Entidad, "Operador", "Modificada cantidad de pedido (Pedido: " + Identificador + ", Cantidad: " + txtCantidad.Text + ", PVP: " + txtPrecio.Text + ") (CALLCENTER)");
					}
				}

				//Si se quiere añadir un nuevo detalle al contrato (Tipo=ANHADIR)
				if (Tipo == "ANHADIR")
				{
					//Datos que necesito: Contrato, IdISP, SKU, Cantidad, PVP, CustomerGUID (GUID de USUARIOS_FINALES)
					cLivingSafeAPI util = new cLivingSafeAPI();
					int IdISP = Convert.ToInt32(Utilidad.DameDatoOperador(Entidad, "IdISPOperador"));

					LoginRequest login = new LoginRequest();
					login.UsernameAPI = Utilidad.DameDatoISP(IdISP, "UsernameAPI");
					login.PasswordAPI = Encriptacion.DescifrarAES(IdISP, Utilidad.DameDatoISP(IdISP, "PasswordAPI"));

					JsonSerializerSettings ConfigJson = new JsonSerializerSettings(); //Evitar que se serialicen los nodos cuyo valor sea nulo (Los ignora).
					ConfigJson.NullValueHandling = NullValueHandling.Ignore;
					byte[] Datos = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(login, Formatting.None, ConfigJson));
					token = util.RealizarSolicitudPOST(Utilidad.DameRutaAPI("RutaAPI_Login"), Datos, token);

					CustomerRequest c = new CustomerRequest();
					c.CustomerGuid = Utilidad.DameDatoUsuarioFinal(IdUsuarioFinal, "GUID", null);
					c.IspGuid= Utilidad.DameDatoISP(IdISP, "UsernameAPI");

					OrderAddDetailsRequest o = new OrderAddDetailsRequest();
					o.IspGuid = Utilidad.DameDatoISP(IdISP, "UsernameAPI");
					o.customer = c;
					o.OperatorLogin = Entidad.ToString();
					o.orderGuid = Identificador;
					o.SKU = ddlProductos.SelectedValue;
					o.Quantity = Convert.ToInt32(txtCantidad.Text);
					o.PVP = Convert.ToDouble(txtPrecio.Text);

					ConfigJson = new JsonSerializerSettings();
					Datos = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(o, Formatting.None, ConfigJson));
					string x = util.RealizarSolicitudPOST(Utilidad.DameRutaAPI("RutaAPI_AddDetails"), Datos, token.Replace("\"", ""));

					OrderAddDetailsResponse res = JsonConvert.DeserializeObject<OrderAddDetailsResponse>(x);

					if (res.error != String.Empty)
						throw new Exception(res.error);
					else
					{
						lblResultado.Text = Resources.orders.ResourceManager.GetString("LiteralAnhadirOK", Thread.CurrentThread.CurrentCulture);

						CerrarFormulario();

						string Escript = "$(document).ready(function() {$('.bd-modificar-modal-lg').modal('hide');});$(document).ready(function() {$('#divAlerta').modal('show');});";
						ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MostrarOKAnhadir", Escript, true);

						Utilidad.EscribeLOG(Entidad, "Operador", "Añadir producto al pedido (Pedido: " + Identificador + ", cantidad: " + txtCantidad.Text + ", PVP: " + txtPrecio.Text + ") (CALLCENTER)");
					}
				}
        //Si se quiere cambiar un producto de un detalle de pedido por otro producto (Tipo=CAMBIAR)
        if (Tipo == "CAMBIAR")
        {

          /*
          public string IspGuid { get; set; }//GUID ISP
          public string PartnerGuid { get; set; }//GUID Partner
          public string OrderDetailsGuid { get; set; }
          public string NewSku { get; set; }
          public string OperatorLogin { get; set; }
          public double PVP { get; set; }
          */

          //Datos que necesito: IspGuid, PartnerGuid, OrderDetailsGuid, NewSku, OperatorLogin, PVP
          cLivingSafeAPI util = new cLivingSafeAPI();
          int IdISP = Convert.ToInt32(Utilidad.DameDatoOperador(Entidad, "IdISPOperador"));

          LoginRequest login = new LoginRequest();
          login.UsernameAPI = Utilidad.DameDatoISP(IdISP, "UsernameAPI");
          login.PasswordAPI = Encriptacion.DescifrarAES(IdISP, Utilidad.DameDatoISP(IdISP, "PasswordAPI"));

          JsonSerializerSettings ConfigJson = new JsonSerializerSettings(); //Evitar que se serialicen los nodos cuyo valor sea nulo (Los ignora).
          ConfigJson.NullValueHandling = NullValueHandling.Ignore;
          byte[] Datos = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(login, Formatting.None, ConfigJson));
          token = util.RealizarSolicitudPOST(Utilidad.DameRutaAPI("RutaAPI_Login"), Datos, token);

          OrderChangeProductRequest o = new OrderChangeProductRequest();
          o.IspGuid = Utilidad.DameDatoISP(IdISP, "UsernameAPI");
          o.PartnerGuid = Utilidad.DameDatoISP(IdISP, "PartnerGUID");
          o.OrderDetailsGuid = Identificador;
          o.NewSku = ddlProductos.SelectedValue;
          o.OperatorLogin = Entidad.ToString();
          o.PVP = Convert.ToDouble(txtPrecio.Text);

          ConfigJson = new JsonSerializerSettings();
          Datos = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(o, Formatting.None, ConfigJson));
          string x = util.RealizarSolicitudPOST(Utilidad.DameRutaAPI("RutaAPI_ChangeProduct"), Datos, token.Replace("\"", ""));

          OrderChangeProductResponse res = JsonConvert.DeserializeObject<OrderChangeProductResponse>(x);

          if (res.error != String.Empty)
            throw new Exception(res.error);
          else
          {
            lblResultado.Text = Resources.orders.ResourceManager.GetString("LiteralCambioProductoOK", Thread.CurrentThread.CurrentCulture);

            CerrarFormulario();

            string Escript = "$(document).ready(function() {$('.bd-modificar-modal-lg').modal('hide');});$(document).ready(function() {$('#divAlerta').modal('show');});";
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MostrarOKAnhadir", Escript, true);

            Utilidad.EscribeLOG(Entidad, "Operador", "Cambio de producto del pedido detalle (Pedido detalle: " + Identificador + ", cantidad: " + txtCantidad.Text + ", PVP: " + txtPrecio.Text + ") (CALLCENTER)");
          }
        }


        gvwPedidos.DataBind();
      }

      catch (Exception Exc)
      {
        string Msg = string.Empty;

        if (Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);
        else
        {
          if (Utilidad.EsEntornoDesarrollo())
            Msg = Exc.ToString();
          else
            Msg = Exc.Message.ToString();
        }

        lblResultado.Text = Msg;

        string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        ScriptManager.RegisterClientScriptBlock(this.Page,Page.GetType(), "MostrarErrorModif", Escript, true);
      }

      finally
      {
        if (Con.State == ConnectionState.Open)
          Con.Close();

        Con.Dispose();
      }
    }

    protected void lbConfirmarBaja_Click(object sender, EventArgs e)
    {
      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = Con.CreateCommand();
      string GUID = string.Empty;

      try
      {
        GUID = ((LinkButton)sender).CommandArgument;
        if (GUID == null || GUID == string.Empty)
          throw new Exception("ERRNUM0008");

        cLivingSafeAPI util = new cLivingSafeAPI();
        int IdISP = Convert.ToInt32(Utilidad.DameDatoOperador(Entidad, "IdISPOperador"));

        LoginRequest login = new LoginRequest();
        login.UsernameAPI = Utilidad.DameDatoISP(IdISP, "UsernameAPI");
        login.PasswordAPI = Encriptacion.DescifrarAES(IdISP, Utilidad.DameDatoISP(IdISP, "PasswordAPI"));

        JsonSerializerSettings ConfigJson = new JsonSerializerSettings(); //Evitar que se serialicen los nodos cuyo valor sea nulo (Los ignora).
        ConfigJson.NullValueHandling = NullValueHandling.Ignore;
        byte[] Datos = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(login, Formatting.None, ConfigJson));
        token = util.RealizarSolicitudPOST(Utilidad.DameRutaAPI("RutaAPI_Login"), Datos, token);


        OrderDetailsUpdateRequest o = new OrderDetailsUpdateRequest();
        o.PartnerGuid = Utilidad.DameDatoISP(IdISP, "PartnerGUID");
        o.IspGuid = Utilidad.DameDatoISP(IdISP, "UsernameAPI");
        o.OrderDetailsGuid = GUID;

        ConfigJson = new JsonSerializerSettings();
        Datos = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(o, Formatting.None, ConfigJson));
        string x = util.RealizarSolicitudPOST(Utilidad.DameRutaAPI("RutaAPI_Cancel"), Datos, token.Replace("\"", ""));

        OrderDetailsUpdateResponse res = JsonConvert.DeserializeObject<OrderDetailsUpdateResponse>(x);

        if (res.error != String.Empty)
          throw new Exception(res.error);
        else
        {
          lblResultado.Text = Resources.orders.ResourceManager.GetString("LiteralBorradoOK", Thread.CurrentThread.CurrentCulture);

          string Escript = "$(document).ready(function() {$('.bd-borrar-modal-lg').modal('hide');});$(document).ready(function() {$('#divAlerta').modal('show');});";
          ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MostrarOKBorrar", Escript, true);

          Utilidad.EscribeLOG(Entidad, "Operador", "Pedido dado de baja (GUID: " + GUID + ") (CALLCENTER)");
        }

        gvwPedidos.DataBind();
      }

      catch (Exception Exc)
      {
        string Msg = string.Empty;

        if (Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);
        else
        {
          if (Utilidad.EsEntornoDesarrollo())
            Msg = Exc.ToString();
          else
            Msg = Exc.Message.ToString();
        }

        lblResultado.Text = Msg;

        string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "MostrarErrorModif", Escript, true);
      }

      finally
      {
        if (Con.State == ConnectionState.Open)
          Con.Close();

        Con.Dispose();
      }
    }

    protected void lnkReenviar_Click(object sender, EventArgs e)
    {
      int IdPedidoDetalle = Convert.ToInt32(((ImageButton)sender).CommandArgument);
      ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "ShowRee", "$(document).ready(function() {$('.bd-reenviar-modal-lg').modal('show');});", true);

      lbConfirmarReenvio.CommandArgument = IdPedidoDetalle.ToString();
    }

    protected void lbConfirmarReenvio_Click(object sender, EventArgs e)
    {
      string IdPedidoDetalle = string.Empty;

      try
      {
        IdPedidoDetalle = ((LinkButton)sender).CommandArgument;

        if (IdPedidoDetalle == null || IdPedidoDetalle == string.Empty)
          throw new Exception("ERRNUM0008");

        EnviaMailBienvenida(Convert.ToInt32(IdPedidoDetalle));

        lblResultado.Text = Resources.orders.ResourceManager.GetString("LiteralReenviadoOK", Thread.CurrentThread.CurrentCulture);

        string Escript = @"$(document).ready(function() {$('.bd-reenviar-modal-lg').modal('hide');});$(document).ready(function() {$('#divAlerta').modal('show');});";
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MostrarOKBorrar", Escript, true);
      }

      catch (Exception Exc)
      {
        string Msg = string.Empty;

        if (Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);
        else
        {
          if (Utilidad.EsEntornoDesarrollo())
            Msg = Exc.ToString();
          else
            Msg = Exc.Message.ToString();
        }

        lblResultado.Text = Msg;

        string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "MostrarErrorModif", Escript, true);
      }
    }

		protected void lbAltaDetalle_Click(object sender, EventArgs e)
		{
			try
			{
				//Cargamos el desplegable de productos. Si es para añadir una línea deben mostrarse sólo los productos de la zona.
				string Zona = "DESPLEGABLEPRODUCTOSNUEVALINEAPEDIDO";
				CargarProductos(Convert.ToInt32(Utilidad.DameDatoOperador(Entidad, "IdISPOperador")), Idiomas.DameIdIdioma(Idiomas.DameIdioma()),Zona);

				ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Show", "$(document).ready(function() {$('.bd-modificar-modal-lg').modal('show');});", true);

        lblProductoActual.Text = string.Empty;
        divProductoActual.Visible = false;
        litProducto.Text = Resources.orders.ResourceManager.GetString("formUsuariosProducto", Thread.CurrentThread.CurrentCulture);

        lblResultado.Text = String.Empty;
				lblTituloCapa.Text = Resources.orders.ResourceManager.GetString("formTitulo4", Thread.CurrentThread.CurrentCulture);

				SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
				SqlCommand Com = Con.CreateCommand();

				txtCantidad.Text = string.Empty;
				txtPrecio.Text = string.Empty;
				btnGuardar.Visible = true;
				ddlProductos.Enabled = true;
				txtPrecio.Enabled = true;

				btnGuardar.CommandName = "ANHADIR";
				btnGuardar.CommandArgument = ((ImageButton)sender).CommandArgument;
			}

			catch (Exception Exc)
			{
				if (Utilidad.EsEntornoDesarrollo())
				{
					lblResultado.Text = Exc.ToString();
				}
				else
				{
					if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
						lblResultado.Text = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
					else
						lblResultado.Text = Exc.Message.ToString() + "<br /><br />";
				}

				string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
				ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MostrarErrorAlta", Escript, true);
			}
		}

    protected void ddlFiltroDetalles_SelectedIndexChanged(object sender, EventArgs e)
    {
      DropDownList Combo = (DropDownList)sender;

      bool MostrarTodos = true;

      if(Combo.SelectedValue=="1")
        MostrarTodos = false;

      IdUsuarioFinal = DameIdUsuarioFinal(Request.QueryString["i"]);
      int IdIdioma = Idiomas.DameIdIdioma(Idiomas.DameIdioma());
      sqlPedidos.SelectParameters["IdOperador"].DefaultValue = HttpContext.Current.User.Identity.Name;
      sqlPedidos.SelectParameters["IdUsuarioFinal"].DefaultValue = IdUsuarioFinal.ToString();
      sqlPedidos.SelectParameters["IdIdioma"].DefaultValue = IdIdioma.ToString();
      sqlPedidos.SelectParameters["IdMonedaDestino"].DefaultValue = ConfigurationManager.AppSettings["IdMonedaDefecto"];
      sqlPedidos.SelectParameters["MostrarTodos"].DefaultValue = MostrarTodos.ToString();

      gvwPedidos.DataBind();
    }

    protected void lnkCambiarProductos_Click(object sender, EventArgs e)
    {
      //Cargamos el desplegable de productos. Si es para cambio de productos, sólo pueden mostrarse los productos que se pueden cambiar que son los mismo que los de añadir. Creamos nueva ZONA.
      CargarProductos(Convert.ToInt32(Utilidad.DameDatoOperador(Entidad, "IdISPOperador")), Idiomas.DameIdIdioma(Idiomas.DameIdioma()), "DESPLEGABLEPRODUCTOSCAMBIOPRODUCTO");


      int IdPedidoDetalle = Convert.ToInt32(((ImageButton)sender).CommandArgument);

      string Escript = "$(document).ready(function() {$('#" + divModificar.ClientID + "').modal('show');});";
      ScriptManager.RegisterStartupScript(this.Page,Page.GetType(), "MostrarCambioProducto", Escript, true);

      lblResultado.Text = String.Empty;
      lblTituloCapa.Text = Resources.orders.ResourceManager.GetString("formTitulo5", Thread.CurrentThread.CurrentCulture);

      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = Con.CreateCommand();

      btnGuardar.Visible = true;

      try
      {
        Com.CommandType = System.Data.CommandType.StoredProcedure;
        Com.CommandText = "Pedidos_DamePedidoDetalle";
        Com.CommandTimeout = 300;
        Com.Parameters.Add("@IdPedidoDetalle", SqlDbType.Int).Value = IdPedidoDetalle;
        Com.Parameters.Add("@CultureCode", SqlDbType.VarChar, 50).Value = Idiomas.DameIdioma();
        
        Con.Open();

        SqlDataReader Rst = Com.ExecuteReader();

        if (Rst.Read())
        {
          txtCantidad.Text = string.Empty;
          txtPrecio.Text = string.Empty;

          lblProductoActual.Text = "[" + Rst["SKU"].ToString() + "] " + Regex.Replace(Rst["NombreCorto"].ToString().Replace("<br />", " "), "<.*?>", String.Empty) + " - " + Rst["Cantidad"].ToString() + " " + Rst["Unidades"].ToString();
          divProductoActual.Visible = true;

          litProducto.Text = Resources.orders.ResourceManager.GetString("formUsuariosProductoNuevo", Thread.CurrentThread.CurrentCulture);

          btnGuardar.CommandName = "CAMBIAR";
          btnGuardar.CommandArgument = Rst["GUID_PD"].ToString();

          ddlProductos.Enabled = true;
          txtPrecio.Enabled = true;
        }
      }

      catch (Exception Exc)
      {
        if (Utilidad.EsEntornoDesarrollo())
        {
          lblResultado.Text = Exc.ToString();
        }
        else
        {
          if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
            lblResultado.Text = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
          else
            lblResultado.Text = Exc.Message.ToString() + "<br /><br />";
        }

        Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MostrarErrorAlta", Escript, true);
      }

      finally
      {
        Com.Dispose();
        Con.Close();
        Con.Dispose();
      }
    }



    private void RegisterPostBackControl()
    {
      foreach (GridViewRow row in gvwPedidos.Rows)
      {
        ImageButton lnkFull = row.FindControl("lnkModificar") as ImageButton;
        ScriptManager.GetCurrent(this).RegisterPostBackControl(lnkFull);

        ImageButton lnkBorrarF = row.FindControl("lnkBorrar") as ImageButton;
        ScriptManager.GetCurrent(this).RegisterPostBackControl(lnkBorrarF);
      }

    }

    public void MostrarColumaOperador()
    {
      string esAdministrador = Utilidad.DameDatoOperador(Convert.ToInt32(HttpContext.Current.User.Identity.Name), "PuedeAdministrarOperador");
      gvwPedidos.Columns[1].Visible = Convert.ToBoolean(esAdministrador);
    }

    protected void CargarProductos(int IdISP,int IdIdioma,string Zona)
    {
			ddlProductos.Items.Clear();
			ArrayList Linea = null;
      ListItem Elemento = new ListItem();
      Elemento.Text = Resources.landing.ResourceManager.GetString("formUsuariosComboProductos", Thread.CurrentThread.CurrentCulture);
      Elemento.Value = "-1";

      ddlProductos.Items.Add(Elemento);

      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = Con.CreateCommand();

      try
      {
				ArrayList ZonasProductos = Utilidad.DameZonasProductosISP(IdISP,Zona);

				Com.CommandType = System.Data.CommandType.StoredProcedure;
        Com.CommandText = "Productos_DameProductosISP";
        Com.CommandTimeout = 300;
        Com.Parameters.Add("@IdISP", SqlDbType.Int).Value = IdISP;
        Com.Parameters.Add("@IdIdioma", SqlDbType.Int).Value = IdIdioma;
        Con.Open();

        SqlDataReader Rst = Com.ExecuteReader();

        while (Rst.Read())
        {
					for(int i=0; i< ZonasProductos.Count;i++)
					{
						Linea = new ArrayList();
						Linea = (ArrayList)ZonasProductos[i];

						if (Convert.ToInt32(Utilidad.DameDato(Linea, "IdProducto")) == Convert.ToInt32(Rst["IdProducto"]) && Convert.ToBoolean(Utilidad.DameDato(Linea, "Visible")) == true)
						{
							Elemento = new ListItem();
							Elemento.Text = "[" + Rst["SKU"].ToString() + "] " + Regex.Replace(Rst["NombreCorto"].ToString().Replace("<br />", " "), "<.*?>", String.Empty);
							Elemento.Value = Rst["SKU"].ToString();
							ddlProductos.Items.Add(Elemento);
						}
					}
				}

        Rst.Close();
        Rst.Dispose();
      }

      finally
      {
        if (Con.State == ConnectionState.Open)
          Con.Close();

        Com.Dispose();
        Con.Dispose();
      }

    }

    protected void CerrarFormulario()
    {
      lblTituloCapa.Text = Resources.orders.ResourceManager.GetString("formTitulo2", Thread.CurrentThread.CurrentCulture);
      btnGuardar.Visible = false;
      btnGuardar.CommandArgument = string.Empty;
      ddlProductos.SelectedIndex = 0;
      txtCantidad.Text = string.Empty;
      txtPrecio.Text = string.Empty;
    }

    protected int DameIdUsuarioFinal(string Usuario)
    {
      Usuario = Encriptacion.DescifrarAES(Convert.ToInt32(Utilidad.DameDatoOperador(Entidad, "IdISPOperador")), Usuario);

      int IdUsuario = Convert.ToInt32(Usuario);

      string IdISPUsuario = Utilidad.DameDatoUsuarioFinal(IdUsuario, "IdISP",true);

      if (Convert.ToInt32(Utilidad.DameDatoOperador(Entidad, "IdISPOperador")) != Convert.ToInt32(IdISPUsuario))
        IdUsuario = -1;

      return IdUsuario;
    }

    protected string DameNombreCortoLimpio(string Nombre)
    {
      string NombreCorto = string.Empty;
      NombreCorto = Regex.Replace(HttpUtility.HtmlDecode(Nombre).Replace("<br />", " "), "<.*?>", string.Empty);
      return NombreCorto;
    }

    protected void CargarUsuario(int IdUsuario)
    {
      if(Utilidad.DameDatoUsuarioFinal(IdUsuario, "Empresa",true)!=null && Utilidad.DameDatoUsuarioFinal(IdUsuario, "Empresa",true)!=string.Empty)
        lblUsuario.Text = Utilidad.DameDatoUsuarioFinal(IdUsuario, "NombreCompletoEmpresa", true);
      else
        lblUsuario.Text = Utilidad.DameDatoUsuarioFinal(IdUsuario, "NombreCompleto", true);

      lblUsuario.Text += " - ID: " + Utilidad.DameDatoUsuarioFinal(IdUsuario, "NIF", true);
      lblComunicacion.Text = Utilidad.DameDatoUsuarioFinal(IdUsuario, "Email", true) + " - " + Utilidad.DameDatoUsuarioFinal(IdUsuario, "Movil", true);
      lblDireccion.Text = Utilidad.DameDatoUsuarioFinal(IdUsuario, "Direccion", true);
      lblLocalidad.Text = Utilidad.DameDatoUsuarioFinal(IdUsuario, "LocalidadNombreBRA", true) + " (" + Utilidad.DameDatoUsuarioFinal(IdUsuario, "CPostalTexto", true) + ")" + " - " + Utilidad.DameDatoUsuarioFinal(IdUsuario, "EstadoNombreBRA", true);

      litGUID.Text = "GUID: " + Utilidad.DameDatoUsuarioFinal(IdUsuario, "GUID", true);
    }

    protected bool EsBaja(string FechaBaja)
    {
      if (FechaBaja == null || FechaBaja == string.Empty)
        return true;
      else
        return false;
    }

    protected void EnviaMailBienvenida(int IdPedidoDetalle)
    {
      SqlConnection ConPedido = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ConnectionString);
      int IdProvision = -1;
      int IdUsuario = -1;
      int IdISP = -1;
      string TipoPlantilla = string.Empty;

      try
      {
        ConPedido.Open();
        SqlCommand ComPedido = ConPedido.CreateCommand();
        ComPedido.CommandType = System.Data.CommandType.StoredProcedure;
        ComPedido.CommandText = "Pedidos_DamePedidoDetalle";
        ComPedido.CommandTimeout = 300;
        ComPedido.Parameters.Add("@IdPedidoDetalle", SqlDbType.Int).Value = IdPedidoDetalle;

        SqlDataReader Rst = ComPedido.ExecuteReader();

        if(Rst.Read())
        {
          IdUsuario = Convert.ToInt32(Rst["IdUsuarioFinal"]);
          IdISP = Convert.ToInt32(Rst["IdISP"]);
          IdProvision = Convert.ToInt32(Rst["IdProvision"].ToString());
        }
      }

      finally
      {
        if (ConPedido.State == ConnectionState.Open)
          ConPedido.Close();

        ConPedido.Dispose();
      }


      if (IdProvision == -1)
        throw new Exception("Provision Id not found.");

      ArrayList Lista = DameValoresProvison(IdProvision);

      foreach (ElementoProvison e in Lista)
      {
        if (e.parametro == "TIPO-PLANTILLA" && TipoPlantilla==string.Empty)
          TipoPlantilla = e.valor;
      }

      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = Con.CreateCommand();

      try
      {
        Com.CommandType = System.Data.CommandType.StoredProcedure;
        Com.CommandText = "Provison_EnvioMail";
        Com.CommandTimeout = 300;
        Com.Parameters.Add("@tipoPlantilla", SqlDbType.VarChar, 200).Value = TipoPlantilla;
        Com.Parameters.Add("@idPedidoDetalle", SqlDbType.Int).Value = IdPedidoDetalle;
        Com.Parameters.Add("@idusuario", SqlDbType.Int).Value = IdUsuario;
        Com.Parameters.Add("@idisP", SqlDbType.Int).Value = IdISP;

        Con.Open();

        Com.ExecuteNonQuery();

        Utilidad.EscribeLOG(Entidad, "Operador", "Reenviado el e-mail de bienvenida al usuario (IdUsuario: " + IdUsuario.ToString() + " del IdPedidoDetalle: " + IdPedidoDetalle.ToString() + ") (CALLCENTER)");
      }

      finally
      {
        Com.Dispose();

        if(Con.State==ConnectionState.Open)
          Con.Close();

        Con.Dispose();
      }
    }

    protected ArrayList DameValoresProvison(int IdProvison)
    {
      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      ArrayList lista = new ArrayList();
      SqlCommand Com = null;
      string aux = string.Empty;

      try
      {
        Com = Con.CreateCommand();
        Com.CommandType = System.Data.CommandType.StoredProcedure;
        Com.CommandText = "Provison_DameDatosProvision";
        Com.CommandTimeout = 300;
        Com.Parameters.Add("@idProvison", SqlDbType.Int).Value = IdProvison;

        Con.Open();

        SqlDataReader Rst = Com.ExecuteReader();

        lista.Clear();

        while (Rst.Read())
        {
          ElementoProvison e = new ElementoProvison();
          e.parametro = Rst["Parametro"].ToString();
          e.valor = Rst["Valor"].ToString();
          lista.Add(e);
        }
      }

      finally
      {
        if (Com != null)
          Com.Dispose();

        if (Con.State == ConnectionState.Open)
          Con.Close();

        Con.Dispose();
      }

      return lista;
    }

    protected bool ValidarPermisos(string Zona)
    {
      bool vuelta = false;

      if (Permisos.TienePermiso(Zona, Entidad))
        vuelta = true;

      return vuelta;
    }
  }
}