﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using APIREST;
using LIVINGSAFE_SiteCallCenter.App_Code;
using Newtonsoft.Json;
using System.Collections;
using LIVINGSAFE_SiteCallCenter.Clases;

namespace LIVINGSAFE_SiteCallCenter.res
{
  public partial class landing : System.Web.UI.Page
  {
    cUtil Utilidad = new cUtil();
    cPermisos Permisos = new cPermisos();
    cIdiomas Idiomas = new cIdiomas();
    cEncriptacion Encriptacion = new cEncriptacion();
    string token = string.Empty;
    int Entidad = Convert.ToInt32(HttpContext.Current.User.Identity.Name);

    protected void Page_Init(object sender, EventArgs e)
    {
      sqlClientes.SelectParameters["idOperador"].DefaultValue = HttpContext.Current.User.Identity.Name;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
      try
      {
        RegisterPostBackControl();
        //MostrarColumaOperador();

        ////Si el usuario logado no tiene permisos suficientes, no puede ver la columna de Operador
        //gvwClientes.Columns[1].Visible = (new LIVINGSAFE_SiteCallCenter.Clases.cPermisos()).TienePermiso("COLGRIDUSERSOPERADORES", Entidad);

        CompruebaPermisos();

        if (!IsPostBack)
        {
          CargarProductos(Convert.ToInt32(Utilidad.DameDatoOperador(Entidad, "IdISPOperador")), Idiomas.DameIdIdioma(Idiomas.DameIdioma()));
        }
      }

      catch (Exception Exc)
      {
        if (Utilidad.EsEntornoDesarrollo())
        {
          lblResultado.Text = Exc.ToString();
        }
        else
        {
          if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
            lblResultado.Text = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
          else
            lblResultado.Text = Exc.Message.ToString() + "<br /><br />";
        }

        string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MostrarErrorAlta", Escript, true);
      }
    }




    protected void ddlEstados_SelectedIndexChanged(object sender, EventArgs e)
    {
      try
      {
        if (ddlEstados.SelectedValue != "-1")
        {
          SqlDataSourceLocalidades.SelectParameters["idEstado"].DefaultValue = ddlEstados.SelectedValue;
          ddlLocalidades.Items.Clear();
          ddlLocalidades.Items.Add(new ListItem(Resources.landing.ResourceManager.GetString("formUsuariosComboLocalidad", Thread.CurrentThread.CurrentCulture), "-1"));
          ddlLocalidades.DataBind();
        }
      }

      catch (Exception Exc)
      {
        if (Utilidad.EsEntornoDesarrollo())
        {
          lblResultado.Text = Exc.ToString();
        }
        else
        {
          if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
            lblResultado.Text = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
          else
            lblResultado.Text = Exc.Message.ToString() + "<br /><br />";
        }

        string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MostrarErrorAlta", Escript, true);
      }
    }

    protected void lbAltaUsuario_Click(object sender, EventArgs e)
    {
      try
      {
        MostrarActivarFranjaProducto(true);

        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Show", "$(document).ready(function() {$('.bd-example-modal-lg').modal('show');});", true);

        lblTituloCapa.Text = Resources.landing.ResourceManager.GetString("formTitulo", Thread.CurrentThread.CurrentCulture);
        lblResultado.Text = string.Empty;
        lbGrabar.Visible = true;
        lbGrabar.CommandArgument = "NUEVO";
        lbGrabar.CommandName = string.Empty;
        txtNombre.Text = string.Empty;
        txtApellidos.Text = string.Empty;
        txtNIF.Text = string.Empty;
        txtEmpresa.Text = string.Empty;
        txtEmail.Text = string.Empty;
        txtTelefono.Text = string.Empty;
        txtDireccion.Text = string.Empty;
        ddlEstados.Items.Clear();
        ddlEstados.Items.Add(new ListItem(Resources.landing.ResourceManager.GetString("formUsuariosComboEstado", Thread.CurrentThread.CurrentCulture), "-1"));
        ddlEstados.DataBind();
        ddlLocalidades.Items.Clear();
        ddlLocalidades.Items.Add(new ListItem(Resources.landing.ResourceManager.GetString("formUsuariosComboLocalidad", Thread.CurrentThread.CurrentCulture), "-1"));
        ddlLocalidades.DataBind();
        txtCPtexto.Text = string.Empty;
        ddlProductos.SelectedIndex = 0;
        txtCantidad.Text = string.Empty;
        txtPrecio.Text = string.Empty;
        txtNIF.Enabled = true;
        txtNIF.ReadOnly = false;
        ddlDocumento.Enabled = true;
      }

      catch (Exception Exc)
      {
        if (Utilidad.EsEntornoDesarrollo())
        {
          lblResultado.Text = Exc.ToString();
        }
        else
        {
          if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
            lblResultado.Text = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
          else
            lblResultado.Text = Exc.Message.ToString() + "<br /><br />";
        }

        string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MostrarErrorAlta", Escript, true);
      }
    }

    protected void lbModificar_Click(object sender, EventArgs e)
    {
      try
      {
        ImageButton Boton = (ImageButton)sender;
        int IdUsuarioFinal = Convert.ToInt32(Boton.CommandArgument);
        int IdISP = Convert.ToInt32(Utilidad.DameDatoOperador(Entidad, "IdISPOperador"));

        string GUID = Boton.CommandName;

        MostrarActivarFranjaProducto(false);

        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Show", "$(document).ready(function() {$('.bd-example-modal-lg').modal('show');});", true);

        lblTituloCapa.Text = Resources.landing.ResourceManager.GetString("formTitulo", Thread.CurrentThread.CurrentCulture);
        lblResultado.Text = string.Empty;
        lbGrabar.Visible = true;
        lbGrabar.CommandArgument = "MODIF";
        lbGrabar.CommandName = GUID;
        txtNombre.Text = string.Empty;
        txtApellidos.Text = string.Empty;
        txtNIF.Text = string.Empty;
        txtEmpresa.Text = string.Empty;
        txtEmail.Text = string.Empty;
        txtTelefono.Text = string.Empty;
        txtDireccion.Text = string.Empty;
        ddlEstados.Items.Clear();
        ddlEstados.Items.Add(new ListItem(Resources.landing.ResourceManager.GetString("formUsuariosComboEstado", Thread.CurrentThread.CurrentCulture), "-1"));
        ddlEstados.DataBind();
        ddlLocalidades.Items.Clear();
        ddlLocalidades.Items.Add(new ListItem(Resources.landing.ResourceManager.GetString("formUsuariosComboLocalidad", Thread.CurrentThread.CurrentCulture), "-1"));
        ddlLocalidades.DataBind();
        txtCPtexto.Text = string.Empty;
        txtNIF.Enabled = false;
        ddlDocumento.Enabled = false;

        CargarDatosUsuarioFinal(IdUsuarioFinal, IdISP,null);
      }

      catch (Exception Exc)
      {
        if (Utilidad.EsEntornoDesarrollo())
        {
          lblResultado.Text = Exc.ToString();
        }
        else
        {
          if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
            lblResultado.Text = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
          else
            lblResultado.Text = Exc.Message.ToString() + "<br /><br />";
        }

        string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MostrarErrorAlta", Escript, true);
      }
    }

    protected void lbVerPedidos_Click(object sender, EventArgs e)
    {
      try
      {
        ImageButton Boton = (ImageButton)sender;

        int IdUsuarioFinal = Convert.ToInt32(Boton.CommandArgument);

        string Usuario = Encriptacion.CifrarAES(Convert.ToInt32(Utilidad.DameDatoOperador(Entidad, "IdISPOperador")), IdUsuarioFinal.ToString());
        Usuario = HttpUtility.UrlEncode(Usuario);

        Response.Redirect("~/res/orders.aspx?i=" + Usuario);
      }

      catch (Exception Exc)
      {
        if (Utilidad.EsEntornoDesarrollo())
        {
          lblResultado.Text = Exc.ToString();
        }
        else
        {
          if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
            lblResultado.Text = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
          else
            lblResultado.Text = Exc.Message.ToString() + "<br /><br />";
        }

        string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MostrarErrorAlta", Escript, true);
      }
    }

    protected void lbMailPassword_Click(object sender, EventArgs e)
    {
      int IdUsuarioFinal = Convert.ToInt32(((ImageButton)sender).CommandArgument);
      ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "ShowRee", "$(document).ready(function() {$('.bd-reenviar-modal-lg').modal('show');});", true);

      lbConfirmarReenvio.CommandArgument = IdUsuarioFinal.ToString();
    }

    protected void lbConfirmarReenvio_Click(object sender, EventArgs e)
    {
      try
      {
        LinkButton Boton = (LinkButton)sender;
        int IdUsuarioFinal = Convert.ToInt32(Boton.CommandArgument);

        SolicitarCambioPassword(IdUsuarioFinal);

        lblResultado.Text = Resources.landing.ResourceManager.GetString("litReenvioMailPassEnviado");

        string Escript = @"$(document).ready(function() {$('.bd-reenviar-modal-lg').modal('hide'); });$(document).ready(function() {$('#divAlerta').modal('show');});";
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "ReenvioMailPassOK", Escript, true);
      }

      catch(Exception Exc)
      {
        if (Utilidad.EsEntornoDesarrollo())
        {
          lblResultado.Text = Exc.ToString();
        }
        else
        {
          if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
            lblResultado.Text = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
          else
            lblResultado.Text = Exc.Message.ToString() + "<br /><br />";
        }

        string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MostrarErrorRee", Escript, true);
      }
    }
      
    protected void lbGrabar_Click(object sender, EventArgs e)
    {
      try
      {
        lblResultado.Text = String.Empty;
        cEncriptacion c = new cEncriptacion();
        LinkButton Boton = (LinkButton)sender;
        string Tipo = Boton.CommandArgument.ToUpper();

        if (Tipo == "NUEVO")
        {
          NuevoUsuario(Convert.ToInt32(Utilidad.DameDatoOperador(Entidad, "IdISPOperador")));
          lblResultado.Text = Resources.landing.ResourceManager.GetString("LiteralAltaOK", Thread.CurrentThread.CurrentCulture);
        }

        if (Tipo=="MODIF")
        {
          ModificarUsuario(Convert.ToInt32(Utilidad.DameDatoOperador(Entidad, "IdISPOperador")),Boton.CommandName);
          lblResultado.Text = Resources.landing.ResourceManager.GetString("LiteralModificacionOK", Thread.CurrentThread.CurrentCulture);
        }

        if (Tipo == "REACTIVAR")
        {
          ReactivarUsuario(Convert.ToInt32(Utilidad.DameDatoOperador(Entidad, "IdISPOperador")));
          lblResultado.Text = Resources.landing.ResourceManager.GetString("LiteralReactivarOK", Thread.CurrentThread.CurrentCulture);
        }

        gvwClientes.DataBind();

        string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MuestraResultado", Escript, true);

        //Vaciamos formulario
        txtNombre.Text = txtApellidos.Text = txtNIF.Text = txtEmpresa.Text = txtEmail.Text = txtTelefono.Text = txtDireccion.Text = txtCantidad.Text = txtPrecio.Text = string.Empty;
        ddlDocumento.ClearSelection();
        ddlEstados.ClearSelection();
        ddlLocalidades.ClearSelection();
        ddlProductos.ClearSelection();

        CerrarFormulario();
      }

      catch (Exception Exc)
      {
        if (Utilidad.EsEntornoDesarrollo())
        {
          lblResultado.Text = Exc.ToString();
        }
        else
        {
          if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
            lblResultado.Text = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
          else
            lblResultado.Text = Exc.Message.ToString() + "<br /><br />";
        }

        string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MostrarErrorAlta", Escript, true);
      }
    }

    protected void ddlDocumento_SelectedIndexChanged(object sender, EventArgs e)
    {
      try
      {
        string Valor = ((DropDownList)sender).SelectedValue.ToUpper();
        string Escript = string.Empty;
        txtNIF.Text = string.Empty;

        if (Valor == "CPF")
        {
          txtNIF.Attributes.Add("placeholder", Resources.landing.ResourceManager.GetString("PlaceHolderLoginCPF", Thread.CurrentThread.CurrentCulture));
          txtEmpresa.ReadOnly = true;
          reqEmpresa.Enabled = false;
        }
        else
        {
          txtNIF.Attributes.Add("placeholder", Resources.landing.ResourceManager.GetString("PlaceHolderLoginCNPJ", Thread.CurrentThread.CurrentCulture));
          txtEmpresa.ReadOnly = false;
          reqEmpresa.Enabled = true;
        }
      }

      catch(Exception Exc)
      {
        if (Utilidad.EsEntornoDesarrollo())
        {
          lblResultado.Text = Exc.ToString();
        }
        else
        {
          if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
            lblResultado.Text = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
          else
            lblResultado.Text = Exc.Message.ToString() + "<br /><br />";
        }

        string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MostrarErrorAlta", Escript, true);
      }
    }

    protected void lbBuscar_Click(object sender, EventArgs e)
    {
      try
      {
        gvwClientes.DataSourceID = "sqlClientes";
        sqlClientes.SelectParameters["idOperador"].DefaultValue = HttpContext.Current.User.Identity.Name;

        if (txtBuscar.Text != string.Empty)
          sqlClientes.SelectParameters["Cadena"].DefaultValue = txtBuscar.Text;
        else
          sqlClientes.SelectParameters["Cadena"].DefaultValue = null;

        gvwClientes.DataBind();
      }

      catch(Exception Exc)
      {
        if (Utilidad.EsEntornoDesarrollo())
        {
          lblResultado.Text = Exc.ToString();
        }
        else
        {
          if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
            lblResultado.Text = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
          else
            lblResultado.Text = Exc.Message.ToString() + "<br /><br />";
        }

        string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MostrarErrorAlta", Escript, true);
      }
    }

    protected void lbActDesact_Click(object sender, EventArgs e)
    {
      try
      {
        ImageButton Boton = (ImageButton)sender;
        int IdUsuarioFinal = Convert.ToInt32(Boton.CommandName);
        bool Activo = Convert.ToBoolean(Boton.CommandArgument);
        int IdISP = Convert.ToInt32(Utilidad.DameDatoOperador(Entidad, "IdISPOperador"));

        ActivarDesactivarUsuario(IdUsuarioFinal,IdISP,Activo);

        if(Activo)
          lblResultado.Text = Resources.landing.ResourceManager.GetString("LiteralDesactivacion", Thread.CurrentThread.CurrentCulture);
        else
          lblResultado.Text = Resources.landing.ResourceManager.GetString("LiteralActivacion", Thread.CurrentThread.CurrentCulture);

        gvwClientes.DataBind();

        string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MuestraResultadoActDesact", Escript, true);
      }

      catch (Exception Exc)
      {
        if (Utilidad.EsEntornoDesarrollo())
        {
          lblResultado.Text = Exc.ToString();
        }
        else
        {
          if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
            lblResultado.Text = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
          else
            lblResultado.Text = Exc.Message.ToString() + "<br /><br />";
        }

        string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MostrarErrorAlta", Escript, true);
      }
    }

    protected void lbReactivar_Click(object sender, EventArgs e)
    {
      try
      {
        ImageButton Boton = (ImageButton)sender;
        int IdUsuarioFinal = Convert.ToInt32(Boton.CommandArgument);
        int IdISP = Convert.ToInt32(Utilidad.DameDatoOperador(Entidad, "IdISPOperador"));

        MostrarActivarFranjaProducto(true);

        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Show", "$(document).ready(function() {$('.bd-example-modal-lg').modal('show');});", true);

        lblTituloCapa.Text = Resources.landing.ResourceManager.GetString("formTitulo", Thread.CurrentThread.CurrentCulture);
        lblResultado.Text = string.Empty;
        lbGrabar.Visible = true;
        lbGrabar.CommandArgument = "REACTIVAR";
        lbGrabar.CommandName = IdUsuarioFinal.ToString();
        txtNombre.Text = string.Empty;
        txtApellidos.Text = string.Empty;
        txtNIF.Text = string.Empty;
        txtEmpresa.Text = string.Empty;
        txtEmail.Text = string.Empty;
        txtTelefono.Text = string.Empty;
        txtDireccion.Text = string.Empty;
        ddlEstados.Items.Clear();
        ddlEstados.Items.Add(new ListItem(Resources.landing.ResourceManager.GetString("formUsuariosComboEstado", Thread.CurrentThread.CurrentCulture), "-1"));
        ddlEstados.DataBind();
        ddlLocalidades.Items.Clear();
        ddlLocalidades.Items.Add(new ListItem(Resources.landing.ResourceManager.GetString("formUsuariosComboLocalidad", Thread.CurrentThread.CurrentCulture), "-1"));
        ddlLocalidades.DataBind();
        txtCPtexto.Text = string.Empty;

        CargarDatosUsuarioFinal(IdUsuarioFinal, IdISP,false);
      }

      catch (Exception Exc)
      {
        if (Utilidad.EsEntornoDesarrollo())
        {
          lblResultado.Text = Exc.ToString();
        }
        else
        {
          if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
            lblResultado.Text = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
          else
            lblResultado.Text = Exc.Message.ToString() + "<br /><br />";
        }

        string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MostrarErrorAlta", Escript, true);
      }
    }



    protected void NuevoUsuario(int IdISP)
    {
      cLivingSafeAPI util = new cLivingSafeAPI();
      LoginRequest login = new LoginRequest();
      login.UsernameAPI = Utilidad.DameDatoISP(IdISP, "UsernameAPI");
      login.PasswordAPI = Encriptacion.DescifrarAES(IdISP, Utilidad.DameDatoISP(IdISP, "PasswordAPI"));

      JsonSerializerSettings ConfigJson = new JsonSerializerSettings(); //Evitar que se serialicen los nodos cuyo valor sea nulo (Los ignora).
      ConfigJson.NullValueHandling = NullValueHandling.Ignore;
      byte[] Datos = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(login, Formatting.None, ConfigJson));
      token = util.RealizarSolicitudPOST(Utilidad.DameRutaAPI("RutaAPI_Login"), Datos, token);

      OrderRequest o = new OrderRequest();
      o.PartnerGuid = Utilidad.DameDatoISP(IdISP, "PartnerGUID");
      o.IspGuid = Utilidad.DameDatoISP(IdISP, "UsernameAPI");
      o.OperatorLogin = Entidad.ToString();
      o.SKU = ddlProductos.SelectedValue;
      o.Quantity = Convert.ToInt32(txtCantidad.Text);
      o.PVP = Convert.ToDouble(txtPrecio.Text);

      o.customer = new CustomerRequest();
      o.customer.email = txtEmail.Text;
      o.customer.FirstName = txtNombre.Text;
      o.customer.LastName = txtApellidos.Text;
      o.customer.CP = txtCPtexto.Text;
      o.customer.CPF_CNPJ = txtNIF.Text;
      o.customer.mobile = txtTelefono.Text;
      o.customer.Address = txtDireccion.Text;
      o.customer.IdCPostal = Convert.ToInt32(ddlLocalidades.SelectedValue);

      if (txtEmpresa.ReadOnly == false && txtEmpresa.Text != string.Empty)
        o.customer.company = txtEmpresa.Text;


      ConfigJson = new JsonSerializerSettings();
      Datos = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(o, Formatting.None, ConfigJson));
      string x = util.RealizarSolicitudPOST(Utilidad.DameRutaAPI("RutaAPI_NewOrder"), Datos, token.Replace("\"", ""));
      OrderResponse res = JsonConvert.DeserializeObject<OrderResponse>(x);

      if (res.error != String.Empty)
        throw new Exception(res.error);

      Utilidad.EscribeLOG(Entidad, "Operador", "Alta de usuario nuevo (CPF/CNPJ: " + txtNIF.Text + ") (CALLCENTER)");
    }

    protected void ModificarUsuario(int IdISP,string GUID)
    {
      cLivingSafeAPI util = new cLivingSafeAPI();
      LoginRequest login = new LoginRequest();
      login.UsernameAPI = Utilidad.DameDatoISP(IdISP, "UsernameAPI");
      login.PasswordAPI = Encriptacion.DescifrarAES(IdISP, Utilidad.DameDatoISP(IdISP, "PasswordAPI"));

      JsonSerializerSettings ConfigJson = new JsonSerializerSettings(); //Evitar que se serialicen los nodos cuyo valor sea nulo (Los ignora).
      ConfigJson.NullValueHandling = NullValueHandling.Ignore;
      byte[] Datos = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(login, Formatting.None, ConfigJson));
      token = util.RealizarSolicitudPOST(Utilidad.DameRutaAPI("RutaAPI_Login"), Datos, token);

      CustomerRequest o = new CustomerRequest();
      o.CustomerGuid = GUID;
      o.IspGuid = Utilidad.DameDatoISP(IdISP, "UsernameAPI");
      o.email = txtEmail.Text;
      o.FirstName = txtNombre.Text;
      o.LastName = txtApellidos.Text;
      o.CP = txtCPtexto.Text;
      o.CPF_CNPJ = txtNIF.Text; //No se puede pasar modificado porque como valida si existe el usuario por este campo, si se le pasa modificado no lo va a encontrar en la vida.
      o.mobile = txtTelefono.Text;
      o.Address = txtDireccion.Text;
      o.IdCPostal = Convert.ToInt32(ddlLocalidades.SelectedValue);

      if (txtEmpresa.ReadOnly == false && txtEmpresa.Text != string.Empty)
        o.company = txtEmpresa.Text;


      ConfigJson = new JsonSerializerSettings();
      Datos = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(o, Formatting.None, ConfigJson));
      string x = util.RealizarSolicitudPOST(Utilidad.DameRutaAPI("RutaAPI_ModifyUser"), Datos, token.Replace("\"", ""));
      OrderResponse res = JsonConvert.DeserializeObject<OrderResponse>(x);

      if (res.error != String.Empty)
        throw new Exception(res.error);

      Utilidad.EscribeLOG(Entidad, "Operador", "Modificación de datos de de usuario (CPF/CNPJ: " + txtNIF.Text + ") (CALLCENTER)");
    }

    private void RegisterPostBackControl()
    {
      foreach (GridViewRow row in gvwClientes.Rows)
      {
        ImageButton lbModificarTemp = (ImageButton)row.FindControl("lbModificar");
        ScriptManager.GetCurrent(this).RegisterPostBackControl(lbModificarTemp);

        ImageButton lbMailPasswordTemp = (ImageButton)row.FindControl("lbMailPassword");
        ScriptManager.GetCurrent(this).RegisterPostBackControl(lbMailPasswordTemp);
      }
    }

    //public void MostrarColumaOperador()
    //{
    //  string esAdministrador = Utilidad.DameDatoOperador(Convert.ToInt32(HttpContext.Current.User.Identity.Name), "PuedeAdministrarOperador");
    //  gvwClientes.Columns[1].Visible = Convert.ToBoolean(esAdministrador);
    //}

    protected void CargarProductos(int IdISP,int IdIdioma)
    {
			string Zona = "DESPLEGABLEPRODUCTOSNUEVOUSUARIO";
			ArrayList Linea = null;
			ListItem Elemento = new ListItem();
      Elemento.Text = Resources.landing.ResourceManager.GetString("formUsuariosComboProductos", Thread.CurrentThread.CurrentCulture);
      Elemento.Value = "-1";

      ddlProductos.Items.Add(Elemento);

      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = Con.CreateCommand();

      try
      {
				ArrayList ZonasProductos = Utilidad.DameZonasProductosISP(IdISP, Zona);

				Com.CommandType = System.Data.CommandType.StoredProcedure;
        Com.CommandText = "Productos_DameProductosISP";
        Com.CommandTimeout = 300;
        Com.Parameters.Add("@IdISP", SqlDbType.Int).Value = IdISP;
        Com.Parameters.Add("@IdIdioma", SqlDbType.Int).Value = IdIdioma;
        Con.Open();

        SqlDataReader Rst = Com.ExecuteReader();

        while (Rst.Read())
        {
					for (int i = 0; i < ZonasProductos.Count; i++)
					{
						Linea = new ArrayList();
						Linea = (ArrayList)ZonasProductos[i];

						if (Convert.ToInt32(Utilidad.DameDato(Linea, "IdProducto")) == Convert.ToInt32(Rst["IdProducto"]) && Convert.ToBoolean(Utilidad.DameDato(Linea, "Visible")) == true)
						{
							Elemento = new ListItem();
							Elemento.Text = "[" + Rst["SKU"].ToString() + "] " + Regex.Replace(Rst["NombreCorto"].ToString().Replace("<br />"," "), "<.*?>", String.Empty);
							Elemento.Value = Rst["SKU"].ToString();
							ddlProductos.Items.Add(Elemento);
						}
					}
				}

        Rst.Close();
        Rst.Dispose();
      }

      finally
      {
        if (Con.State == ConnectionState.Open)
          Con.Close();

        Com.Dispose();
        Con.Dispose();
      }
    }

    protected void CerrarFormulario()
    {
      ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Show", "$(document).ready(function() {$('.bd-example-modal-lg').modal('hide');});", true);

      lblTituloCapa.Text = Resources.landing.ResourceManager.GetString("formTitulo", Thread.CurrentThread.CurrentCulture);
      lbGrabar.Visible = true;
      txtNombre.Text = string.Empty;
      txtApellidos.Text = string.Empty;
      txtNIF.Text = string.Empty;
      txtEmpresa.Text = string.Empty;
      txtEmail.Text = string.Empty;
      txtTelefono.Text = string.Empty;
      txtDireccion.Text = string.Empty;
      ddlEstados.Items.Clear();
      ddlEstados.Items.Add(new ListItem(Resources.landing.ResourceManager.GetString("formUsuariosComboEstado", Thread.CurrentThread.CurrentCulture), "-1"));
      ddlEstados.DataBind();
      ddlLocalidades.Items.Clear();
      ddlLocalidades.Items.Add(new ListItem(Resources.landing.ResourceManager.GetString("formUsuariosComboLocalidad", Thread.CurrentThread.CurrentCulture), "-1"));
      ddlLocalidades.DataBind();
      txtCPtexto.Text = string.Empty;
      ddlProductos.SelectedIndex = 0;
      txtCantidad.Text = string.Empty;
      txtPrecio.Text = string.Empty;
    }

    protected void MostrarActivarFranjaProducto(bool Mostrar)
    {
      divFormProducto.Visible = Mostrar;
      litProducto.Visible = Mostrar;
      ddlProductos.Enabled = ddlProductos.Visible = Mostrar;
      reqProductos.Enabled = reqProductos.Visible = Mostrar;
      litCantidad.Visible = Mostrar;
      txtCantidad.Enabled = txtCantidad.Visible = Mostrar;
      reqCantidad.Enabled = reqCantidad.Visible = Mostrar;
      regCantidad.Enabled = regCantidad.Visible = Mostrar;
      litPrecio.Visible = Mostrar;
      txtPrecio.Enabled = txtPrecio.Visible = Mostrar;
      reqPrecio.Enabled = reqPrecio.Visible = Mostrar;
      regPrecio.Enabled = regPrecio.Visible = Mostrar;
    }

    protected void CargarDatosUsuarioFinal(int IdUsuarioFinal, int IdISP, bool? MostrarTodos)
    {
      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = Con.CreateCommand();
      ListItem Elemento = new ListItem();

      try
      {
        Com.CommandType = System.Data.CommandType.StoredProcedure;
        Com.CommandText = "Usuarios_DevolverDatos";
        Com.CommandTimeout = 300;
        Com.Parameters.Add("@IdUsuarioFinal", SqlDbType.Int).Value = IdUsuarioFinal;
        Com.Parameters.Add("@IdISP", SqlDbType.Int).Value = IdISP;

        if(MostrarTodos!=null)
          Com.Parameters.Add("@Activo", SqlDbType.Bit).Value = MostrarTodos;

        Con.Open();

        SqlDataReader Rst = Com.ExecuteReader();

        if (Rst.Read())
        {
          //Si el detalle del usuario está activo precargamos esos datos. Si no, los tienen que volver a dar de alta.
          if(Convert.ToBoolean(Rst["ActivoUFD"]))
          {
            txtNombre.Text = Rst["Nombre"].ToString();
            txtApellidos.Text = Rst["Apellidos"].ToString();
            txtTelefono.Text = Rst["Movil"].ToString();
            txtDireccion.Text = Rst["Direccion"].ToString();
            txtEmail.Text = Rst["Email"].ToString();

            ddlEstados.ClearSelection();
            ddlEstados.Items.Clear();
            ddlEstados.Items.Add(new ListItem(Resources.landing.ResourceManager.GetString("formUsuariosComboEstado", Thread.CurrentThread.CurrentCulture), "-1"));
            ddlEstados.DataBind();

            Elemento = ddlEstados.Items.FindByValue(Rst["EstadoBRA"].ToString());
            if (Elemento != null)
              Elemento.Selected = true;

            SqlDataSourceLocalidades.SelectParameters["idEstado"].DefaultValue = ddlEstados.SelectedValue;

            ddlLocalidades.ClearSelection();
            ddlLocalidades.Items.Clear();
            ddlLocalidades.Items.Add(new ListItem(Resources.landing.ResourceManager.GetString("formUsuariosComboLocalidad", Thread.CurrentThread.CurrentCulture), "-1"));
            ddlLocalidades.DataBind();

            Elemento = ddlLocalidades.Items.FindByValue(Rst["IdCPostal"].ToString());
            if (Elemento != null)
              Elemento.Selected = true;

            txtCPtexto.Text = Rst["CPostalTexto"].ToString();
          }
          
          ddlDocumento.ClearSelection();

          if (Utilidad.validarCPF(Rst["NIF"].ToString()) == "CPF")
          {
            Elemento = ddlDocumento.Items.FindByValue("CPF");
            if (Elemento != null)
              Elemento.Selected = true;

            txtNIF.Attributes.Add("placeholder", Resources.landing.ResourceManager.GetString("PlaceHolderLoginCPF", Thread.CurrentThread.CurrentCulture));
            txtEmpresa.ReadOnly = true;
            reqEmpresa.Enabled = false;
          }
          else
          {
            Elemento = ddlDocumento.Items.FindByValue("CNPJ");
            if (Elemento != null)
              Elemento.Selected = true;

            txtNIF.Attributes.Add("placeholder", Resources.landing.ResourceManager.GetString("PlaceHolderLoginCNPJ", Thread.CurrentThread.CurrentCulture));
            txtEmpresa.ReadOnly = false;
            reqEmpresa.Enabled = true;
            txtEmpresa.Text = Rst["Empresa"].ToString();
          }


          txtNIF.Text = Rst["NIF"].ToString();

          //En una reactivación no permitimos cambiar el CPF/CNPJ
          txtNIF.ReadOnly = true;
          ddlDocumento.Enabled = false;
        }

        Rst.Close();
        Rst.Dispose();
      }

      finally
      {
        if (Con.State == ConnectionState.Open)
          Con.Close();

        Com.Dispose();
        Con.Dispose();
      }
    }

    protected bool EsBaja(string FechaBaja)
    {
      if (FechaBaja == null || FechaBaja == string.Empty)
        return true;
      else
        return false;
    }

    protected string SolicitarCambioPassword(int IdUsuarioFinal)
    {
      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = Con.CreateCommand();

      string EMail = string.Empty;
      int IdISP = Convert.ToInt32(Utilidad.DameDatoOperador(Entidad, "IdISPOperador"));
      string NombreUsuario = string.Empty;
      string RutaLogo = string.Empty;

      try
      {
        Con.Open();

        Com.CommandType = CommandType.StoredProcedure;
        Com.CommandText = "Usuarios_DevolverDatos";
        Com.CommandTimeout = 300;
        Com.Parameters.Add("@IdUsuarioFinal", SqlDbType.Int).Value = IdUsuarioFinal;
        Com.Parameters.Add("@IdISP", SqlDbType.Int).Value = IdISP;

        SqlDataReader Rst = Com.ExecuteReader();

        if (Rst.Read())
        {
          EMail = Rst["Email"].ToString();
          NombreUsuario = Rst["Nombre"].ToString();
          RutaLogo = Rst["Logo"].ToString();

          string[] CadenaLogo = RutaLogo.Split('.');

          string url = ConfigurationManager.AppSettings["HOST"].ToString();

					string Subdominio = Utilidad.DameSubdominio(HttpContext.Current.Request.Url.Host, "living-safe");
					if(Subdominio.StartsWith("local"))
						Subdominio = "local-" + Subdominio.Substring(Subdominio.IndexOf("-") + 1, Subdominio.Length - (Subdominio.IndexOf("-") + 1));
					else
						if (Subdominio.StartsWith("test"))
							Subdominio = "test-" + Subdominio.Substring(Subdominio.IndexOf("-") + 1, Subdominio.Length - (Subdominio.IndexOf("-") + 1));
						else
							if (Subdominio.StartsWith("pre"))
								Subdominio = "pre-" + Subdominio.Substring(Subdominio.IndexOf("-") + 1, Subdominio.Length - (Subdominio.IndexOf("-") + 1));
							else
								Subdominio = Subdominio.Substring(Subdominio.IndexOf("-") + 1, Subdominio.Length - (Subdominio.IndexOf("-") + 1));


					if (HttpContext.Current.Request.Url.Host.ToLower() == "localhost")
            RutaLogo = url + "://" + HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port + "/images/logos/" + CadenaLogo[0] + "_email." + CadenaLogo[1];
          else
            RutaLogo = url + "://" + Subdominio + ".living-safe.com" + "/images/logos/" + CadenaLogo[0] + "_email." + CadenaLogo[1];

          EnviarDatos(IdISP, IdUsuarioFinal, EMail, NombreUsuario, RutaLogo);

          Utilidad.EscribeLOG(Entidad, "Operador", "Solicitud de reenvío de e-mail de cambio de contraseña de usuario (IdUsuario: " + IdUsuarioFinal.ToString() + ") (CALLCENTER)");
        }
        else
        {
          throw new Exception(Resources.login.ResourceManager.GetString("ERRNUM0002"));
        }

        Rst.Dispose();
      }

      finally
      {
        if (Con.State == ConnectionState.Open)
          Con.Close();

        Com.Dispose();
        Con.Dispose();
      }

      return EMail;
    }

    protected void ActivarDesactivarUsuario(int IdUsuarioFinal,int IdISP,bool Activo)
    {
      cLivingSafeAPI util = new cLivingSafeAPI();
      LoginRequest login = new LoginRequest();
      login.UsernameAPI = Utilidad.DameDatoISP(IdISP, "UsernameAPI");
      login.PasswordAPI = Encriptacion.DescifrarAES(IdISP, Utilidad.DameDatoISP(IdISP, "PasswordAPI"));

      JsonSerializerSettings ConfigJson = new JsonSerializerSettings(); //Evitar que se serialicen los nodos cuyo valor sea nulo (Los ignora).
      ConfigJson.NullValueHandling = NullValueHandling.Ignore;
      byte[] Datos = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(login, Formatting.None, ConfigJson));
      token = util.RealizarSolicitudPOST(Utilidad.DameRutaAPI("RutaAPI_Login"), Datos, token);

      CustomerRequest c = new CustomerRequest();
      c.IspGuid = Utilidad.DameDatoISP(IdISP, "UsernameAPI");
      c.CPF_CNPJ = Utilidad.DameDatoUsuarioFinal(IdUsuarioFinal,"NIF",false);

      ConfigJson = new JsonSerializerSettings();
      Datos = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(c, Formatting.None, ConfigJson));
      string Ruta = string.Empty;

      if (Activo)
        Ruta = Utilidad.DameRutaAPI("RutaAPI_Deactivate");
      else
        Ruta = Utilidad.DameRutaAPI("RutaAPI_Activate");


      string x = util.RealizarSolicitudPOST(Ruta, Datos, token.Replace("\"", ""));
      CustomerResponse res = JsonConvert.DeserializeObject<CustomerResponse>(x);

      if (res.error != String.Empty)
        throw new Exception(res.error);

      if(Activo)
        Utilidad.EscribeLOG(Entidad, "Operador", "Desactivación de usuario (CPF/CNPJ: " + Utilidad.DameDatoUsuarioFinal(IdUsuarioFinal, "NIF",false) + ") (CALLCENTER)");
      else
        Utilidad.EscribeLOG(Entidad, "Operador", "Activación del usuario (CPF/CNPJ: " + Utilidad.DameDatoUsuarioFinal(IdUsuarioFinal, "NIF",false) + ") (CALLCENTER)");
    }

    protected void ReactivarUsuario(int IdISP)
    {
      cLivingSafeAPI util = new cLivingSafeAPI();
      LoginRequest login = new LoginRequest();
      login.UsernameAPI = Utilidad.DameDatoISP(IdISP, "UsernameAPI");
      login.PasswordAPI = Encriptacion.DescifrarAES(IdISP, Utilidad.DameDatoISP(IdISP, "PasswordAPI"));

      JsonSerializerSettings ConfigJson = new JsonSerializerSettings(); //Evitar que se serialicen los nodos cuyo valor sea nulo (Los ignora).
      ConfigJson.NullValueHandling = NullValueHandling.Ignore;
      byte[] Datos = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(login, Formatting.None, ConfigJson));
      token = util.RealizarSolicitudPOST(Utilidad.DameRutaAPI("RutaAPI_Login"), Datos, token);

      OrderRequest o = new OrderRequest();
      o.PartnerGuid = Utilidad.DameDatoISP(IdISP, "PartnerGUID");
      o.IspGuid = Utilidad.DameDatoISP(IdISP, "UsernameAPI");
      o.OperatorLogin = Entidad.ToString();
      o.SKU = ddlProductos.SelectedValue;
      o.Quantity = Convert.ToInt32(txtCantidad.Text);
      o.PVP = Convert.ToDouble(txtPrecio.Text);

      o.customer = new CustomerRequest();
      o.customer.email = txtEmail.Text;
      o.customer.FirstName = txtNombre.Text;
      o.customer.LastName = txtApellidos.Text;
      o.customer.CP = txtCPtexto.Text;
      o.customer.CPF_CNPJ = txtNIF.Text;
      o.customer.mobile = txtTelefono.Text;
      o.customer.Address = txtDireccion.Text;
      o.customer.IdCPostal = Convert.ToInt32(ddlLocalidades.SelectedValue);

      if (txtEmpresa.ReadOnly == false && txtEmpresa.Text != string.Empty)
        o.customer.company = txtEmpresa.Text;


      ConfigJson = new JsonSerializerSettings();
      Datos = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(o, Formatting.None, ConfigJson));
      string x = util.RealizarSolicitudPOST(Utilidad.DameRutaAPI("RutaAPI_Reactivate"), Datos, token.Replace("\"", ""));
      OrderResponse res = JsonConvert.DeserializeObject<OrderResponse>(x);

      if (res.error != String.Empty)
        throw new Exception(res.error);

      Utilidad.EscribeLOG(Entidad, "Operador", "Reactivación de usuario de baja (CPF/CNPJ: " + txtNIF.Text + ") (CALLCENTER)");
    }

    protected void EnviarDatos(int IdISP, int IdUsuarioFinal, string Email, string NombreUsuario, string RutaLogo)
    {
      DateTime fecha = DateTime.Now;
      cEncriptacion AES = new cEncriptacion(Utilidad.DameClaveCifradoISP(IdISP));
      string host = String.Empty;
      string URL = HttpContext.Current.Request.Url.Host;
      string DominioPrincipal = Utilidad.DameValorConfiguracion("DominioPrincipal");

      string url = ConfigurationManager.AppSettings["HOST"].ToString();

      if (Email == null || Email == string.Empty)
        throw new Exception(Resources.login.ResourceManager.GetString("ERRNUM0004"));

      string SC = AES.CrearSecretoCompartido(IdUsuarioFinal.ToString());
      string IdOperadorCifrado = AES.CifrarAES(IdUsuarioFinal.ToString(), SC);
      string IdOperadorCifradoFinal = SC + IdOperadorCifrado;

      SC = AES.CrearSecretoCompartido(IdISP.ToString());
      string IdISPCifrado = AES.CifrarAES(IdISP.ToString(), SC);
      string IdISPCifradoFinal = SC + IdISPCifrado;

      SmtpClient Correo = new SmtpClient(Utilidad.DameValorConfiguracion("ServidorSMTPCorreoSpamina"));
      NetworkCredential Credenciales = new NetworkCredential(Utilidad.DameValorConfiguracion("ServidorSMTPUser"), Utilidad.DameValorConfiguracion("ServidorSMTPPass"));
      Correo.Credentials = Credenciales;

      Correo.Port = Convert.ToInt32(Utilidad.DameValorConfiguracion("ServidorSMTPPuerto"));
      Correo.EnableSsl = true;

      MailMessage Mensaje = new MailMessage();

			MailAddress MailFromSoporte = new System.Net.Mail.MailAddress(Utilidad.DameValorConfiguracion("MailFromCambioPassword"));

			if (Utilidad.DameDatoISP(IdISP, "EmailSoporte") != null && Utilidad.DameDatoISP(IdISP, "EmailSoporte") != string.Empty)
				MailFromSoporte = new System.Net.Mail.MailAddress(Utilidad.DameDatoISP(IdISP, "EmailSoporte"));

			Mensaje.From = MailFromSoporte; // new System.Net.Mail.MailAddress(Utilidad.DameValorConfiguracion("MailFromCambioPassword"));
      Mensaje.To.Add(Email);
      Mensaje.Subject = Resources.login.ResourceManager.GetString("AsuntoMailCambioPass");
      Mensaje.IsBodyHtml = true;

			if (HttpContext.Current.Request.Url.Port != 80)
				host = HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port;
			else
			{
				string Subdominio = Utilidad.DameSubdominio(HttpContext.Current.Request.Url.Host, "living-safe");

				if (Subdominio.StartsWith("local"))
					Subdominio = "local-" + Subdominio.Substring(Subdominio.IndexOf("-") + 1, Subdominio.Length - (Subdominio.IndexOf("-") + 1));
				else
					if (Subdominio.StartsWith("test"))
						Subdominio = "test-" + Subdominio.Substring(Subdominio.IndexOf("-") + 1, Subdominio.Length - (Subdominio.IndexOf("-") + 1));
					else
						if (Subdominio.StartsWith("pre"))
							Subdominio = "pre-" + Subdominio.Substring(Subdominio.IndexOf("-") + 1, Subdominio.Length - (Subdominio.IndexOf("-") + 1));
						else
							Subdominio = Subdominio.Substring(Subdominio.IndexOf("-") + 1, Subdominio.Length - (Subdominio.IndexOf("-") + 1));

				host = Subdominio + ".living-safe.com";
			}

      string Enlace = url + "://" + host + "/change-password.aspx?i=" + HttpUtility.UrlEncode(IdOperadorCifradoFinal) + "&p=" + HttpUtility.UrlEncode(IdISPCifradoFinal) + "&pd=" + IdISP.ToString() + "&f=" + fecha.Ticks.ToString();
      string Duracion = Utilidad.DameValorConfiguracion("CaducidadEnlaceCambioPassword");

      Mensaje.Body = GetMessage(NombreUsuario, Enlace, Duracion, RutaLogo);

      Correo.Send(Mensaje);
    }

    protected string GetMessage(string Usuario, string Enlace, string Duracion, string RutaLogo)
    {
      string Msg = String.Empty;
      string ruta = ConfigurationManager.AppSettings["HOST"].ToString() + "://";

      if (HttpContext.Current.Request.Url.Port != 80)
        ruta += HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port;
      else
        ruta += HttpContext.Current.Request.Url.Host;

      string LogoLivingSafe = ruta + "/images/logos/logo_livingsafe_email.png";
      string LogoLivingSafeEmail = ruta + "/images/logos/logo_livingsafe_simbolo.png";
      string FondoCelda = ruta + "/images/email_fondo.jpg";

      Msg = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath(Resources.login.ResourceManager.GetString("RutaPlantillaMailCambioPass")));
      Msg = Msg.Replace("[LOGOLIVINGSAFE]", LogoLivingSafe);
      Msg = Msg.Replace("[LOGOLIVINGSAFEEMAIL]", LogoLivingSafeEmail);
      Msg = Msg.Replace("[LOGOPARTNER]", RutaLogo);
      Msg = Msg.Replace("[USUARIO]", Usuario);
      Msg = Msg.Replace("[ENLACE]", Enlace);
      Msg = Msg.Replace("[DURACION]", Duracion);

      return Msg;
    }

    protected string DameImagen(bool Activo)
    {
      string Ruta = string.Empty;

      if (Activo)
        Ruta = "~/images/icono_activado.png";
      else
        Ruta = "~/images/icono_desactivado.png";


      return Ruta;
    }

    protected string DameTextoActDesact(bool Activo)
    {
      string Tooltip = string.Empty;

      if (Activo)
        Tooltip = Resources.landing.ResourceManager.GetString("ToolTipAct", Thread.CurrentThread.CurrentCulture);
      else
        Tooltip = Resources.landing.ResourceManager.GetString("ToolTipDesact", Thread.CurrentThread.CurrentCulture);

      return Tooltip;
    }

    protected void CompruebaPermisos()
    {
      if (Permisos.TienePermiso("BOTONNUEVOPEDIDO", Entidad))
        upBotonAlta.Visible = true;
      else
        upBotonAlta.Visible = false;

      if (Permisos.TienePermiso("BOTONNUEVOPEDIDO", Entidad))
        upBotonAlta.Visible = true;
      else
        upBotonAlta.Visible = false;
    }

    protected bool ValidarPermisos(string Zona)
    {
      bool vuelta = false;

      if (Permisos.TienePermiso(Zona, Entidad))
        vuelta = true;

      return vuelta;
    }
  }
}