﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LIVINGSAFE_SiteCallCenter.App_Code;
using LIVINGSAFE_SiteCallCenter.Clases;

namespace LIVINGSAFE_SiteCallCenter.res
{
  public partial class massive_list : System.Web.UI.Page
  {
    cUtil Utilidad = new cUtil();
    cPermisos Permisos = new cPermisos();
    cEncriptacion Encriptacion = new cEncriptacion();
    int Entidad = Convert.ToInt32(HttpContext.Current.User.Identity.Name);
    static string token = string.Empty;
    string FicheroTemporal = string.Empty;

    protected void Page_Init(object sender, EventArgs e)
    {
      sqlRegistrosProvision.SelectParameters["IdISP"].DefaultValue = Utilidad.DameDatoOperador(Entidad, "IdISPOperador");
    }

    protected void Page_Load(object sender, EventArgs e)
    {
      RegistrarPostBack();

      if (!Permisos.TienePermiso("PAGINALISTADOALTASMASIVAS", Entidad))
        Response.Redirect("~/forbidden.aspx");

      //Si el usuario logado no tiene permisos suficientes, no puede ver la columna de descargar Excel ni LOG
      gvwRegistrosProvision.Columns[6].Visible = (new LIVINGSAFE_SiteCallCenter.Clases.cPermisos()).TienePermiso("DESCARGAREXCELERRORESALTAMASIVA", Entidad);
      gvwRegistrosProvision.Columns[7].Visible = (new LIVINGSAFE_SiteCallCenter.Clases.cPermisos()).TienePermiso("DECARGARLOGERRORESALTAMASIVA", Entidad);
    }

    protected void RegistrarPostBack()
    {
      foreach (GridViewRow gr in gvwRegistrosProvision.Rows)
      {
        ImageButton ibExcel = (ImageButton)gr.FindControl("ibDescargarXLSX");
        ImageButton ibLOG = (ImageButton)gr.FindControl("ibDescargarLOG");
        smMassive.RegisterPostBackControl(ibExcel);
        smMassive.RegisterPostBackControl(ibLOG);
      }
    }

    protected void tmrTemporizador_Tick(object sender, EventArgs e)
    {
      try
      {
        gvwRegistrosProvision.DataBind();
      }

      catch (Exception Exc)
      {
        string Msg = string.Empty;

        if (Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
          Msg = Resources.errores.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture);
        else
        {
          if (Utilidad.EsEntornoDesarrollo())
            Msg = Exc.ToString();
          else
            Msg = Exc.Message.ToString();
        }

        lblResultado.Text = Msg;

        string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        ScriptManager.RegisterClientScriptBlock(this.Page, Page.GetType(), "MostrarErrorModif", Escript, true);
      }
    }

    protected void ibDescargarXLSX_Click(object sender, ImageClickEventArgs e)
    {
            cImpersonate impersonar = new cImpersonate();

            try
      {
                string usuario = ConfigurationManager.AppSettings["usuarioPublicar"].ToString();
                string dominio = ConfigurationManager.AppSettings["usuarioDominio"].ToString();
                string pass = ConfigurationManager.AppSettings["usuarioPass"].ToString();
                impersonar.impersonateValidUser(usuario, dominio, pass);

                SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
        SqlCommand Com = null;
        string Nombre = string.Empty;

        int IdRegistro = Convert.ToInt32(((ImageButton)sender).CommandArgument);

        try
        {
          Con.Open();
          Com = Con.CreateCommand();
          Com.CommandType = System.Data.CommandType.StoredProcedure;
          Com.CommandText = "Provision_Masiva_Registro_DameRegistro";
          Com.CommandTimeout = 300;
          Com.Parameters.Add("@IdRegistro", SqlDbType.Int).Value = IdRegistro;

          SqlDataReader Rst = Com.ExecuteReader();

          if (Rst.Read())
          {
            Nombre = Rst["NombreFichero"].ToString();
          }
        }

        finally
        {
          if (Com != null)
            Com.Dispose();

          if (Con.State == ConnectionState.Open)
            Con.Close();

          Con.Dispose();
        }

        string Ruta = ConfigurationManager.AppSettings["RutaFicherosErrores"].ToString() + "\\TEMP\\" + Nombre + ".xlsx";
        Utilidad.EscribeLOG(Entidad, "Operador", "Descargado informe de errores (Informe: " + Nombre + ".xlsx) en listado de altas masivas (CALLCENTER)");

        Response.Clear();
        Response.ClearHeaders();
        Response.Buffer = false;
        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        Response.AppendHeader("Content-Disposition", "Attachment; filename=\"" + Nombre + ".xlsx\"");
        Response.AppendHeader("Content-Length", File.ReadAllBytes(Ruta).Length.ToString());
        Response.TransmitFile(Ruta);
        Response.Flush();
        Response.End();
                impersonar.undoImpersonation();
            }

      catch (Exception Exc)
      {
        if (Utilidad.EsEntornoDesarrollo())
        {
          lblResultado.Text = Exc.ToString();
        }
        else
        {
          if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
            lblResultado.Text = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
          else
            lblResultado.Text = Exc.Message.ToString() + "<br /><br />";
        }

        string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MostrarErrorAlta", Escript, true);
        impersonar.undoImpersonation();
       }
    }

    protected void ibDescargarLOG_Click(object sender, ImageClickEventArgs e)
    {
      cImpersonate impersonar = new cImpersonate();
      try
      {
                string usuario = ConfigurationManager.AppSettings["usuarioPublicar"].ToString();
                string dominio = ConfigurationManager.AppSettings["usuarioDominio"].ToString();
                string pass = ConfigurationManager.AppSettings["usuarioPass"].ToString();
                impersonar.impersonateValidUser(usuario, dominio, pass);

                SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
        SqlCommand Com = null;
        string Nombre = string.Empty;

        int IdRegistro = Convert.ToInt32(((ImageButton)sender).CommandArgument);

        try
        {
          Con.Open();
          Com = Con.CreateCommand();
          Com.CommandType = System.Data.CommandType.StoredProcedure;
          Com.CommandText = "Provision_Masiva_Registro_DameRegistro";
          Com.CommandTimeout = 300;
          Com.Parameters.Add("@IdRegistro", SqlDbType.Int).Value = IdRegistro;

          SqlDataReader Rst = Com.ExecuteReader();

          if (Rst.Read())
          {
            Nombre = Rst["NombreFichero"].ToString();
          }
        }

        finally
        {
          if (Com != null)
            Com.Dispose();

          if (Con.State == ConnectionState.Open)
            Con.Close();

          Con.Dispose();
        }

        string Ruta = ConfigurationManager.AppSettings["RutaFicherosErrores"].ToString() + "\\TEMP\\" + Nombre + ".log";

        Utilidad.EscribeLOG(Entidad, "Operador", "Descargado detalle de errores (Informe: " + Nombre + ".log) en listado de altas masivas (CALLCENTER)");

        Response.Clear();
        Response.ClearHeaders();
        Response.Buffer = false;
        Response.ContentType = "application/text";
        Response.AppendHeader("Content-Disposition", "Attachment; filename=\"" + Nombre + ".log\"");
        Response.AppendHeader("Content-Length", File.ReadAllBytes(Ruta).Length.ToString());
        Response.TransmitFile(Ruta);
        Response.Flush();
        Response.End();
                impersonar.undoImpersonation();
            }

      catch (Exception Exc)
      {
        if (Utilidad.EsEntornoDesarrollo())
        {
          lblResultado.Text = Exc.ToString();
        }
        else
        {
          if (Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) != null)
            lblResultado.Text = Resources.pas.ResourceManager.GetString(Exc.Message.ToString(), Thread.CurrentThread.CurrentCulture) + "<br /><br />";
          else
            lblResultado.Text = Exc.Message.ToString() + "<br /><br />";
        }

        string Escript = "$(document).ready(function() {$('#divAlerta').modal('show');});";
        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "MostrarErrorAlta", Escript, true);
                impersonar.undoImpersonation();
            }
    }

    protected void lbNuevaAlta_Click(object sender, EventArgs e)
    {
      Response.Redirect("~/res/massive.aspx");
    }

    protected void lbNuevaBaja_Click(object sender, EventArgs e)
    {
     Response.Redirect("~/res/massive_cancel.aspx");
    }


    protected string DameImagen(string Finalizado,string OK, string Pdte, string NOK)
    {
      string Imagen = string.Empty;

      if (Convert.ToInt32(Pdte) > 0)
        Imagen = "~/images/icon_pendiente.gif";
      else
        if (Convert.ToInt32(NOK) > 0)
          Imagen = "~/images/icon_errores.png";
        else
          Imagen = "~/images/icon_finalizado.png";
      /*
      if (Convert.ToBoolean(Finalizado)==false)
        Imagen = "~/images/icon_pendiente.gif";
      else
        if(Convert.ToInt32(NOK)>0)
          Imagen = "~/images/icon_errores.png";
        else
          Imagen = "~/images/icon_finalizado.png";
      */

      return Imagen;
    }

    protected bool TieneEnlaces(string Id)
    {
      bool tiene = false;
      string NombreFichero = string.Empty;

      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = null;

      try
      {
        Con.Open();
        Com = Con.CreateCommand();
        Com.CommandType = System.Data.CommandType.StoredProcedure;
        Com.CommandText = "Provision_Masiva_Registro_DameRegistro";
        Com.CommandTimeout = 300;
        Com.Parameters.Add("@IdRegistro", SqlDbType.Int).Value = Convert.ToInt32(Id);

        SqlDataReader Rst = Com.ExecuteReader();

        if (Rst.Read())
          NombreFichero = Rst["NombreFichero"].ToString();
      }

      finally
      {
        if (Com != null)
          Com.Dispose();

        if (Con.State == ConnectionState.Open)
          Con.Close();

        Con.Dispose();
      }

      if (NombreFichero != string.Empty)
      {
        cImpersonate impersonar = new cImpersonate();
        string usuario = ConfigurationManager.AppSettings["usuarioPublicar"].ToString();
        string dominio = ConfigurationManager.AppSettings["usuarioDominio"].ToString();
        string pass = ConfigurationManager.AppSettings["usuarioPass"].ToString();
        impersonar.impersonateValidUser(usuario, dominio, pass);

        string FicheroExcel = ConfigurationManager.AppSettings["RutaFicherosErrores"].ToString() + "\\TEMP\\" + NombreFichero + ".xlsx";
        string FicheroLOG = ConfigurationManager.AppSettings["RutaFicherosErrores"].ToString() + "\\TEMP\\" + NombreFichero + ".log";

        if (File.Exists(FicheroExcel) && File.Exists(FicheroLOG))
          tiene = true;
        else
          tiene = false;

        impersonar.undoImpersonation();
       }

      return tiene;
    }
  }
}