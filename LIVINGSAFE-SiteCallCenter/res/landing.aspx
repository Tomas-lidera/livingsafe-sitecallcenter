﻿<%@ Page Title="" Language="C#" MasterPageFile="~/res/LIVINGSAFE.Master" AutoEventWireup="true" CodeBehind="landing.aspx.cs" Inherits="LIVINGSAFE_SiteCallCenter.res.landing" %>
<asp:Content ID="cLandingHead" ContentPlaceHolderID="cpHead" runat="server"></asp:Content>
<asp:Content ID="cLandingBody" ContentPlaceHolderID="cpBody" runat="server">
  <asp:ScriptManager ID="smLanding" runat="server"></asp:ScriptManager>
  <div class="container-fluid section-contenido">
    <div class="row espacio_inferior">
      <div class="col-md-9">
        <div class="txt_titulopagina_med"><asp:Literal ID="litTitulo" runat="server" EnableViewState="false" Text='<%$ Resources:landing,litTitulo %>'></asp:Literal></div>
        <asp:Literal ID="litSubtitulo" runat="server" EnableViewState="false" Text='<%$ Resources:landing,litSubtitulo %>'></asp:Literal>
      </div>

      <br /><br />

      <div class="col-md-3" style="text-align:right; padding:0px; padding-right:20px;">
        <asp:UpdatePanel runat="server" ID="upBotonAlta" UpdateMode="Conditional">
          <ContentTemplate>
            <asp:LinkButton ID="lbAltaUsuario" runat="server" CausesValidation="false" data-target=".bd-example-modal-lg" class="lnk_newuser" OnClick="lbAltaUsuario_Click" Text='<%$ Resources:landing,btnNuevo %>'></asp:LinkButton>
          </ContentTemplate>
        </asp:UpdatePanel>
      </div>

      <div class="col-md-12" style="height:10px; padding:0px;"></div>

      <div class="col-md-12 CC_buscador" style="padding:0px;">
        <asp:Panel runat="server" ID="pnlBuscador" DefaultButton="lbBuscar">
          <asp:UpdatePanel runat="server" ID="upBuscador" class="col-md-12 ml-auto align-content-end CC_buscador">
            <Triggers>
              <asp:AsyncPostBackTrigger ControlID="lbBuscar" />
            </Triggers>
            <ContentTemplate>
              <div class="col-xl-10 col-lg-9 col-md-8 col-sm-7 col-6">
                <div class="input-group mb-3">
                  <asp:TextBox runat="server" ID="txtBuscar" CssClass="form-control CC_inputbuscar" placeholder='<%$ Resources:landing,phBuscar %>' aria-label="Buscar"></asp:TextBox>
                  <div class="input-group-append">
                    <asp:LinkButton runat="server" ID="lbBuscar" CausesValidation="false" CssClass="btn btn-outline-secondary CC_buscarinput" OnClick="lbBuscar_Click"></asp:LinkButton>
                  </div>
                </div>
              </div>
            </ContentTemplate>
          </asp:UpdatePanel>
        </asp:Panel>
      </div>

      <div class="col-md-12" style="height:10px; padding:0px;"></div>
       
      <asp:UpdatePanel runat="server" ID="upLanding" ChildrenAsTriggers="true" class="col-md-12 ml-auto align-content-end">
        <Triggers>
          <asp:AsyncPostBackTrigger ControlID="lbGrabar" />
        </Triggers>
        <ContentTemplate>
          <asp:GridView ID="gvwClientes" runat="server" AutoGenerateColumns="False" DataKeyNames="IdUsuarioFinal" 
            EmptyDataText='<%$ Resources:landing,gridUsuariosVacio %>' DataSourceID="sqlClientes"
            AllowPaging="True" AllowSorting="True"  BorderWidth="0px" BorderStyle="None" GridLines="None" PagerSettings-Mode="NumericFirstLast"
            HeaderStyle-CssClass="col-4 txt_titulo_grid cabecera_grid" RowStyle-CssClass="col-4 txt_contenido_grid franja_grid_par" AlternatingRowStyle-CssClass="col-4 txt_contenido_grid"
            PagerStyle-CssClass="paginacion" CssClass="container-fluid" EmptyDataRowStyle-CssClass="col-4 txt_contenido_grid franja_grid_par" PageSize="10">
            
            <Columns>
              <asp:BoundField DataField="IdUsuarioFinal" HeaderText="IdUsuarioFinal" SortExpression="IdUsuarioFinal" InsertVisible="False" ReadOnly="True" Visible="false" />

<%--              <asp:TemplateField HeaderText='<%$ Resources:landing,gridUsuariosOperador %>' SortExpression="NombreOperador">
                <ItemTemplate>
                  <asp:Label ID="lblOperador" runat="server" Text='<%# Bind("NombreOperador") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle  HorizontalAlign="Center" />
              </asp:TemplateField>--%>

              <asp:TemplateField HeaderText='<%$ Resources:landing,gridUsuariosNIF %>' SortExpression="NIF">
                <ItemTemplate>
                  <asp:Label ID="lblNIF" runat="server" Text='<%# Bind("NIF") %>' ToolTip='<%# Bind("GUID") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle  HorizontalAlign="Center" />
              </asp:TemplateField>

              <asp:TemplateField HeaderText='<%$ Resources:landing,gridUsuariosNombre %>' SortExpression="NombreCompleto">
                <ItemTemplate>
                  <asp:Label ID="lblNome" runat="server" Text='<%# Bind("NombreCompleto") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle CssClass="col_resp" />
                <HeaderStyle CssClass="col_resp" />
              </asp:TemplateField>

              <asp:TemplateField HeaderText='<%$ Resources:landing,gridUsuariosEmail %>' SortExpression="Email">
                <ItemTemplate>
                  <asp:Label ID="lblEmail" runat="server" Text='<%# Bind("Email") %>'></asp:Label>
                </ItemTemplate>
              </asp:TemplateField>

              <asp:TemplateField HeaderText='<%$ Resources:landing,gridUsuariosTelefono %>' SortExpression="movil">
                <ItemTemplate>
                  <asp:Label ID="lblMovil" runat="server" Text='<%# Bind("movil") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle CssClass="col_resp_peq" HorizontalAlign="Center" />
                <HeaderStyle CssClass="col_resp_peq" />
              </asp:TemplateField>

              <asp:TemplateField HeaderText='<%$ Resources:landing,gridUsuariosFecha %>' SortExpression="FechaAlta">
                <ItemTemplate>
                  <asp:Label ID="lblData" runat="server" Text='<%# Bind("FechaAlta", "{0:dd-MM-yyyy}") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="95px" CssClass="col_resp" HorizontalAlign="Center" />
                <HeaderStyle CssClass="col_resp" />
              </asp:TemplateField>

              <asp:TemplateField HeaderText='<%$ Resources:landing,gridUsuariosFechaFin %>' SortExpression="FechaBaja">
                <ItemTemplate>
                  <asp:Label ID="lblDataFin" runat="server" Text='<%# Bind("FechaBaja", "{0:dd-MM-yyyy}") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="95px" CssClass="col_resp" HorizontalAlign="Center" />
                <HeaderStyle CssClass="col_resp" />
              </asp:TemplateField>

              <asp:TemplateField>
                <ItemTemplate>
                  <asp:ImageButton runat="server" ID="lbActDesact" ImageUrl='<%# DameImagen(Convert.ToBoolean(Eval("Activo")))%>' OnClick="lbActDesact_Click" CommandName='<%#Eval("IdUsuarioFinal")%>' CommandArgument='<%#Eval("Activo")%>' CausesValidation="false" style="vertical-align:middle;" Visible='<%# Convert.ToBoolean(EsBaja(Eval("FechaBaja").ToString())) && Convert.ToBoolean(ValidarPermisos("LISTADOUSUARIOSBOTONACTIVAR")) %>' ToolTip='<%# DameTextoActDesact(Convert.ToBoolean(Eval("Activo"))) %>' />
                </ItemTemplate>
                <ItemStyle Width="50px" HorizontalAlign="Center" />
              </asp:TemplateField>

              <asp:TemplateField>
                <ItemTemplate>
                  <asp:ImageButton runat="server" ID="lbModificar" ImageUrl="~/images/icono_editar.png" OnClick="lbModificar_Click" CommandName='<%#Eval("GUID")%>' CommandArgument='<%#Eval("IdUsuarioFinal")%>' CausesValidation="false" style="vertical-align:middle;" Visible='<%# Convert.ToBoolean(EsBaja(Eval("FechaBaja").ToString())) && Convert.ToBoolean(Eval("Activo")) && Convert.ToBoolean(ValidarPermisos("LISTADOUSUARIOSBOTONEDITAR")) %>' ToolTip='<%$ Resources:landing,TooltipModificar %>' />
                </ItemTemplate>
                <ItemStyle Width="50px" HorizontalAlign="Center" />
              </asp:TemplateField>

              <asp:TemplateField>
                <ItemTemplate>
                  <asp:ImageButton runat="server" ID="lbMailPassword" ImageUrl="~/images/icono_cambiopass.png" OnClick="lbMailPassword_Click" CommandArgument='<%#Eval("IdUsuarioFinal")%>' CausesValidation="false" style="vertical-align:middle;" Visible='<%# Convert.ToBoolean(EsBaja(Eval("FechaBaja").ToString())) && Convert.ToBoolean(Eval("Activo")) && Convert.ToBoolean(ValidarPermisos("LISTADOUSUARIOSBOTONMAILPASS")) %>' ToolTip='<%$ Resources:landing,TooltipMailPassword %>' />
                </ItemTemplate>
                <ItemStyle Width="50px" HorizontalAlign="Center" />
              </asp:TemplateField>

              <asp:TemplateField>
                <ItemTemplate>
                  <asp:ImageButton runat="server" ID="lbVerPedidos" ImageUrl="~/images/icono_verpedidos.png" OnClick="lbVerPedidos_Click"  CommandArgument='<%#Eval("IdUsuarioFinal")%>' CausesValidation="false" style="vertical-align:middle;" Visible='<%# Convert.ToBoolean(EsBaja(Eval("FechaBaja").ToString())) %>' ToolTip='<%$ Resources:landing,TooltipVerPedidos %>' />
                  <asp:ImageButton runat="server" ID="ibReactivate" ImageUrl="~/images/icono_reactivar.png"  OnClick="lbReactivar_Click"   CommandArgument='<%#Eval("IdUsuarioFinal")%>' CausesValidation="false" style="vertical-align:middle;" Visible='<%# Convert.ToBoolean(!EsBaja(Eval("FechaBaja").ToString())) && Convert.ToBoolean(ValidarPermisos("LISTADOUSUARIOSBOTONREACTIVAR")) %>' ToolTip='<%$ Resources:landing,TooltipReactivar %>' />
                </ItemTemplate>
                <ItemStyle Width="50px" HorizontalAlign="Center" />
              </asp:TemplateField>
            </Columns>

            <EmptyDataRowStyle      CssClass="col-4 txt_contenido_grid franja_grid_par" Height="80px" HorizontalAlign="center" />
            <HeaderStyle            CssClass="col-4 txt_titulo_grid cabecera_grid" HorizontalAlign="Center" />
            <PagerSettings          Mode="NumericFirstLast" />
            <PagerStyle             BackColor="#EDEDED" HorizontalAlign="Right" BorderWidth="0px" />
            <RowStyle               CssClass="col-4 txt_contenido_grid franja_grid_par" />
            <AlternatingRowStyle    CssClass="col-4 txt_contenido_grid" />

          </asp:GridView>
          <div class="col-md-12 ml-auto align-content-end" style="font-size:12px;">
            <asp:Label runat="server" ID="lblAvisoGrid" Text='<%$ Resources:landing,AvisoGrid %>'></asp:Label>
          </div>

          <asp:SqlDataSource ID="sqlClientes" CancelSelectOnNullParameter="false" runat="server" ConnectionString="<%$ ConnectionStrings:LivingSafeCS %>"
          SelectCommand="Usuarios_ListadoUsuarios" SelectCommandType="StoredProcedure">
            <SelectParameters>
              <asp:Parameter Name="IdOperador" Type="Int32" />
              <%--<asp:Parameter Name="IdUsuarioFinal" Type="Int32" />--%>
              <asp:Parameter Name="Cadena" Type="String" ConvertEmptyStringToNull="true" />
            </SelectParameters>
          </asp:SqlDataSource>

          <div class="container section-contenido">
            <div class="row espacio_inferior">
              <div id="divMensaje" runat="server" class="alert alert-sucess fade collapse" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <asp:Label runat="server" ID="lblMensaje" />
              </div>
            </div>
          </div>
        </ContentTemplate>
      </asp:UpdatePanel>

      <br /><br />

      <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" runat="server">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <asp:UpdatePanel ID="upCapaUsuarios" runat="server">
              <Triggers>
                <asp:AsyncPostBackTrigger ControlID="lbGrabar" />
                <asp:AsyncPostBackTrigger ControlID="ddlDocumento" />
              </Triggers>
              <ContentTemplate>
                <%//Este código tiene que ir dentro del UpdatePanel porque si no la máscara no se aplica %>
                <script>
                  var prm = Sys.WebForms.PageRequestManager.getInstance();
                  prm.add_pageLoaded(function () {
                    $(document).ready(function () {
                      var e = document.getElementById("<%=ddlDocumento.ClientID%>");
      
                      if(e.options[e.selectedIndex].value=='CPF')
                      {
                        $('#<%=txtNIF.ClientID%>').mask('000.000.000-00', { reverse: false });
                      }
                      else
                      {
                        $('#<%=txtNIF.ClientID%>').mask('00.000.000/0000-00', { reverse: false });
                      }

                      $('#<%=txtCPtexto.ClientID%>').mask('00000-000', { reverse: false });

                      var SPMaskBehavior = function (val) {
                        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
                      },
                      spOptions = {
                        onKeyPress: function (val, e, field, options) {
                          field.mask(SPMaskBehavior.apply({}, arguments), options);
                        }
                      };

                      $('#<%=txtTelefono.ClientID%>').mask(SPMaskBehavior, spOptions);

                      $('#<%=txtCantidad.ClientID%>').mask('######0', { reverse: true });

                      $('#<%=txtPrecio.ClientID%>').mask('0000000,00', { reverse: true });
                    });
                  });
                </script>
                <div class="modal-header" style="border:none;">
                  <h5 class="modal-title txt_titulopagina_med" id="exampleModalLabel">
                    <asp:Literal runat="server" ID="lblTituloCapa" EnableViewState="false" Text='<%$ Resources:landing,formTitulo %>' />
                  </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>

                <div class="modal-body">
                  <div class="form-row">
                    <div class="form-group col-md-5">
                      <asp:Literal ID="litNombre" runat="server" EnableViewState="false" Text='<%$ Resources:landing,formUsuariosNombre %>' />
                      <asp:TextBox runat="server" ID="txtNombre" class="form-control"></asp:TextBox>
                      <asp:RequiredFieldValidator runat="server" ID="reqNombre" Text='<%$ Resources:landing,formObligatorio %>' ControlToValidate="txtNombre" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form-group col-md-7">
                      <asp:Literal ID="litApellidos" runat="server" EnableViewState="false" Text='<%$ Resources:landing,formUsuariosApellidos %>' />
                      <asp:TextBox runat="server" ID="txtApellidos" class="form-control"></asp:TextBox>
                      <asp:RequiredFieldValidator runat="server" ID="reqApellidos" Text='<%$ Resources:landing,formObligatorio %>' ControlToValidate="txtApellidos" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                  </div>

                  <div class="form-row">
                    <div class="form-group col-md-5">
                      <div class="form-row">
                        <div class="form-group col-md-4">
                          <asp:Literal ID="litNIF" runat="server" EnableViewState="false" Text='<%$ Resources:landing,formUsuariosNIF %>' />
                          <asp:DropDownList ID="ddlDocumento" runat="server" CssClass="form-control" ValidationGroup="usuario" ToolTip='<%$ Resources:landing,TipoDocumento %>' AutoPostBack="True" OnSelectedIndexChanged="ddlDocumento_SelectedIndexChanged">
                            <asp:ListItem Text="CPF"  Value="CPF"></asp:ListItem>
                            <asp:ListItem Text="CNPJ" Value="CNPJ"></asp:ListItem>
                          </asp:DropDownList>
                        </div>
                        <div class="form-group col-md-8">
                          &nbsp;
                          <asp:TextBox runat="server" ID="txtNIF" CssClass="form-control" placeholder='<%$ Resources:landing,PlaceHolderLoginCPF %>'></asp:TextBox>
                          <asp:RequiredFieldValidator runat="server" ID="reqNIF" Text='<%$ Resources:landing,formObligatorio %>' ControlToValidate="txtNIF" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                          <asp:RegularExpressionValidator runat="server" ID="regNIF" ControlToValidate="txtNIF" ForeColor="Red" ValidationExpression="(^\d{3}\.\d{3}\.\d{3}\-\d{2}$)|(^\d{2}\.\d{3}\.\d{3}\/\d{4}\-\d{2}$)" ErrorMessage='<%$ Resources:landing,formErrorNIF %>' Display="Dynamic"></asp:RegularExpressionValidator>
                        </div>
                      </div>
                    </div>
                    <div class="form-group col-md-7">
                      <asp:Literal ID="litEmpresa" runat="server" EnableViewState="false" Text='<%$ Resources:landing,formUsuariosEmpresa %>' />
                      <asp:TextBox runat="server" ID="txtEmpresa" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                      <asp:RequiredFieldValidator runat="server" ID="reqEmpresa" Text='<%$ Resources:landing,formObligatorio %>' ControlToValidate="txtEmpresa" Enabled="false" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                  </div>

                  <div class="form-row">
                    <div class="form-group col-md-8">
                      <asp:Literal ID="litEmail" runat="server" EnableViewState="false" Text='<%$ Resources:landing,formUsuariosEmail %>' />
                      <asp:TextBox runat="server" ID="txtEmail" class="form-control"></asp:TextBox>
                      <asp:RequiredFieldValidator runat="server" ID="reqEmail" Text='<%$ Resources:landing,formObligatorio %>' ControlToValidate="txtEmail" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                      <asp:RegularExpressionValidator ID="regEmail" runat="server" ControlToValidate="txtEmail" ErrorMessage='<%$ Resources:landing,formErrorEmail %>' ForeColor="Red" ValidationExpression="^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)+$" Display="Dynamic"></asp:RegularExpressionValidator>
                    </div>
                    <div class="form-group col-md-4">
                      <asp:Literal ID="litTelefono" runat="server" EnableViewState="false" Text='<%$ Resources:landing,formUsuariosTelefono %>' />
                      <asp:TextBox runat="server" ID="txtTelefono" PlaceHolder='<%$ Resources:landing,phTelefono %>' class="form-control" MaxLength="15"></asp:TextBox>
                      <asp:RequiredFieldValidator runat="server" ID="reqMovil" Text='<%$ Resources:landing,formObligatorio %>' ControlToValidate="txtTelefono" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                      <asp:RegularExpressionValidator ID="regMovil" runat="server" ControlToValidate="txtTelefono" ErrorMessage='<%$ Resources:landing,formErrorTelefono %>' ForeColor="Red" ValidationExpression="(^|\()?\s*(\d{2})\s*(\s|\))*(9?\d{4,})(\s|-)?(\d{4})($|\n)" Display="Dynamic"></asp:RegularExpressionValidator>
                    </div>
                  </div>

                  <div class="form-row">
                    <div class="form-group col-md-12">
                      <asp:Literal ID="litDireccion" runat="server" EnableViewState="false" Text='<%$ Resources:landing,formUsuariosDireccion %>' />
                      <asp:TextBox runat="server" ID="txtDireccion" class="form-control"></asp:TextBox>
                      <asp:RequiredFieldValidator runat="server" ID="reqDireccion" Text='<%$ Resources:landing,formObligatorio %>' ControlToValidate="txtDireccion" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                  </div>

                  <div class="form-row">
                    <div class="form-group col-md-4">
                      <asp:Literal ID="litProvincia" runat="server" EnableViewState="false" Text='<%$ Resources:landing,formUsuariosProvincia %>' />
                      <asp:DropDownList runat="server" ID="ddlEstados" AutoPostBack="True" class="form-control" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlEstados_SelectedIndexChanged" DataSourceID="SqlDataSourceEstados" DataTextField="NombreBRA" DataValueField="IdEstado">
                        <asp:ListItem Value="-1" Text='<%$ Resources:landing,formUsuariosComboEstado %>'></asp:ListItem>
                      </asp:DropDownList>
                      <asp:RequiredFieldValidator runat="server" ID="reqddlEstados" ControlToValidate="ddlEstados" InitialValue="-1" ErrorMessage='<%$ Resources:landing,formObligatorio %>' ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                      <asp:SqlDataSource ID="SqlDataSourceEstados" runat="server" ConnectionString="<%$ ConnectionStrings:LivingSafeCS %>" SelectCommand="Localizacion_ListadoEstados" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                          <asp:Parameter Name="codPaisAlfa3" Type="String" DefaultValue="BRA" />
                        </SelectParameters>
                      </asp:SqlDataSource>
                    </div>
                    <div class="form-group col-md-5">
                      <asp:Literal ID="litLocalidad" runat="server" EnableViewState="false" Text='<%$ Resources:landing,formUsuariosLocalidad %>' />
                      <asp:DropDownList ID="ddlLocalidades" class="form-control" runat="server" DataSourceID="SqlDataSourceLocalidades" DataTextField="LocalidadBRA" DataValueField="IdCPostal" AutoPostBack="true" AppendDataBoundItems="true">
                        <asp:ListItem Value="-1" Text='<%$ Resources:landing,formUsuariosComboLocalidad %>'></asp:ListItem>
                      </asp:DropDownList>
                      <asp:RequiredFieldValidator runat="server" ID="reqddlLocalidades" ControlToValidate="ddlLocalidades" InitialValue="-1" ErrorMessage='<%$ Resources:landing,formObligatorio %>' ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                      <asp:SqlDataSource ID="SqlDataSourceLocalidades" runat="server" ConnectionString="<%$ ConnectionStrings:LivingSafeCS %>" SelectCommand="Localizacion_ListadoLocalidades" SelectCommandType="StoredProcedure" CancelSelectOnNullParameter="false">
                        <SelectParameters>
                          <asp:Parameter Name="Localidad" Type="String" />
                          <asp:Parameter Name="CPostal" Type="String" />
                          <asp:Parameter Name="idEstado" Type="Int32" DefaultValue="" />
                        </SelectParameters>
                      </asp:SqlDataSource>
                    </div>
                    <div class="form-group col-md-3">
                      <asp:Literal ID="litCPostal" runat="server" EnableViewState="false" Text='<%$ Resources:landing,formUsuariosCPostal %>' />
                      <asp:TextBox runat="server" ID="txtCPtexto" MaxLength="9" CssClass="form-control" PlaceHolder='<%$ Resources:landing,phCPostal %>'></asp:TextBox>
                      <asp:RequiredFieldValidator runat="server" ID="reqCPtexto" Text="* Obrigatório" ControlToValidate="txtCPtexto" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                      <asp:RegularExpressionValidator ID="regCPTexto" runat="server" ControlToValidate="txtCPtexto" ErrorMessage='<%$ Resources:landing,formErrorCPostal %>' ValidationExpression="^\d{5}\-\d{3}$" ForeColor="Red" Display="Dynamic"></asp:RegularExpressionValidator>
                    </div>
                  </div>

                  <div class="form-row" runat="server" id="divFormProducto">
                    <div class="form-group col-md-8">
                      <asp:Literal ID="litProducto" runat="server" EnableViewState="false" Text='<%$ Resources:landing,formUsuariosProducto %>' />
                      <asp:DropDownList runat="server" ID="ddlProductos" AutoPostBack="True" class="form-control"></asp:DropDownList>
                      <asp:RequiredFieldValidator runat="server" ID="reqProductos" ControlToValidate="ddlProductos" InitialValue="-1" ErrorMessage='<%$ Resources:landing,formObligatorio %>' ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form-group col-md-2">
                      <asp:Literal ID="litCantidad" runat="server" EnableViewState="false" Text='<%$ Resources:landing,formUsuariosCantidad %>' />
                      <asp:TextBox runat="server" ID="txtCantidad" MaxLength="6" CssClass="form-control" PlaceHolder="0"></asp:TextBox>
                      <asp:RequiredFieldValidator runat="server" ID="reqCantidad" ControlToValidate="txtCantidad" ErrorMessage='<%$ Resources:landing,formObligatorio %>' ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                      <asp:RegularExpressionValidator runat="server" ID="regCantidad" ControlToValidate="txtCantidad" ErrorMessage='<%$ Resources:landing,formErrorCantidad %>' ValidationExpression="^[1-9]\d*$" ForeColor="Red" Display="Dynamic"></asp:RegularExpressionValidator>
                    </div>
                    <div class="form-group col-md-2">
                      <asp:Literal ID="litPrecio" runat="server" EnableViewState="false" Text='<%$ Resources:landing,formUsuariosPrecio %>' />
                      <asp:TextBox runat="server" ID="txtPrecio" MaxLength="10" CssClass="form-control" PlaceHolder="0,00"></asp:TextBox>
                      <asp:RequiredFieldValidator runat="server" ID="reqPrecio" ControlToValidate="txtPrecio" ErrorMessage='<%$ Resources:landing,formObligatorio %>' ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                      <asp:RegularExpressionValidator runat="server" ID="regPrecio" ControlToValidate="txtPrecio" ErrorMessage='<%$ Resources:landing,formErrorCantidad %>' ValidationExpression="(\d+\,\d{1,2})" ForeColor="Red" Display="Dynamic"></asp:RegularExpressionValidator>
                    </div>
                  </div>

                  <asp:LinkButton ID="lbGrabar" runat="server" Text='<%$ Resources:landing,btnGrabar %>' CommandName="0" CommandArgument="NUEVO" OnClick="lbGrabar_Click" class="lnk_passchange" />
                </div>
              </ContentTemplate>
            </asp:UpdatePanel>
          </div>
        </div>
      </div>
    </div>
  </div>


  <div id="divReenviar" class="modal fade bd-reenviar-modal-lg" tabindex="-1" role="dialog" aria-labelledby="ModalReenviar" aria-hidden="true" runat="server">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <asp:UpdatePanel ID="upReenvioPassword" runat="server" ChildrenAsTriggers="true">
          <Triggers>
            <asp:AsyncPostBackTrigger ControlID="lbConfirmarReenvio" />
          </Triggers>
          <ContentTemplate>
            <div class="modal-header" style="border:none;">
              <h5 class="modal-title txt_titulopagina_med" id="TituloReenviar">
                <asp:Literal runat="server" ID="litTituloCapaReenviar" EnableViewState="false" Text='<%$ Resources:landing,TituloReenviar %>' />
              </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div class="modal-body">
              <div class="form-row">
                <div class="form-group col-md-12">
                  <asp:Literal runat="server" ID="kitMensajeReenviar" Text='<%$ Resources:landing,litReenvioMailPass %>' EnableViewState="false"></asp:Literal>
                </div>
              </div>
              <asp:LinkButton ID="lbConfirmarReenvio" runat="server" Text='<%$ Resources:orders,btnConfirmar %>' OnClick="lbConfirmarReenvio_Click" class="lnk_passchange" CausesValidation="false" />
            </div>
          </ContentTemplate>
        </asp:UpdatePanel>
      </div>
    </div>
  </div>


  <asp:UpdatePanel ID="upAlerta" runat="server">
    <ContentTemplate>
      <div id="divAlerta" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="ModalAviso" aria-hidden="true">
        <div class="modal-dialog modal-md">
          <div class="modal-content">
            <div class="modal-body">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <asp:Label ID="lblResultado" runat="server" />
            </div>
          </div>
        </div>
      </div>
    </ContentTemplate>
  </asp:UpdatePanel>
</asp:Content>

