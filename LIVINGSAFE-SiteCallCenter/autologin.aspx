﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="autologin.aspx.cs" Inherits="LIVINGSAFE_SiteCallCenter.autologin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title></title>
</head>
<body style="font-family:'Courier New',Arial; font-size:12px; color:#303030;">
    <form id="form1" runat="server">
      <div style="width:100%;">
        <div style=" width:800px; text-align:center; margin:0px auto; margin-top:100px;">
          <asp:HyperLink ID="hlLogo" runat="server" EnableViewState="false">
            <img src="~/images/logos/logo_livingsafe_simbolo.png" runat="server" enableviewstate="false" alt="LivingSafe"/>
          </asp:HyperLink>
          <br /><br />
          <asp:Label ID="lblMensaje" runat="server" EnableViewState="false" />
        </div>
      </div>
    </form>
</body>
</html>
