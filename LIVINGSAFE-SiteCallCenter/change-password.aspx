﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="change_password.aspx.cs" Inherits="LIVINGSAFE_SiteCallCenter.change_password" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <title>LivingSafe</title>
  <link runat="server" rel="stylesheet" type="text/css" href="~/style/bootstrap.css" />
  <link runat="server" rel="stylesheet" type="text/css" href="~/style/generalstyle.css" />
  <link runat="server" rel="stylesheet" type="text/css" href="~/style/custom/personalstyle.css" id="cssCustom" />
  <script type="text/javascript" src="/js/jquery-3.3.1.slim.min.js"></script>
  <script type="text/javascript" src="/js/bootstrap.bundle.js"></script>
  <script type="text/javascript" src="/js/bootstrap.js"></script>
  <link rel="apple-touch-icon" sizes="180x180" href="~/apple-touch-icon.png" runat="server" />
  <link rel="icon" type="image/png" sizes="32x32" href="~/favicon-32x32.png" runat="server" />
  <link rel="icon" type="image/png" sizes="16x16" href="~/favicon-16x16.png" runat="server" />
  <link rel="mask-icon" href="~/safari-pinned-tab.svg" color="#5bbad5" runat="server" />
</head>
<body class="body_login">
  <form id="form1" runat="server">
    <asp:ScriptManager ID="smCambioPassword" runat="server"></asp:ScriptManager>
    <div>
      <div class="container">
        <asp:UpdatePanel runat="server" ID="upCambioPasswordFormulario">
          <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnCambioPassword" />
          </Triggers>
          <ContentTemplate>
            <asp:Panel ID="pnlCambioPassword" runat="server" DefaultButton="btnCambioPassword" Width="100%">
              <div class="cuadrologin" style="margin: 0px auto;">
                <div class="logo_login" onclick="location.href='/login.aspx';" onmouseover="this.style.cursor='pointer';">&nbsp;</div>
                <div class="login-form">
                  <div class="div_selector_idioma">
                    <a class="login_selector_idiomas" data-target="#CapaIdiomas" onclick="$(document).ready(function() {$('#CapaIdiomas').modal('show');});" onmouseover="this.style.cursor='pointer';">&nbsp;</a>
                  </div>
                  <span class="span_login"><asp:Literal id="litPassword1" Text='<%$ Resources:change_password,Password1 %>' runat="server" EnableViewState="false" /></span>
                  <span class="span_login"><asp:TextBox runat="server" ID="txtPassword1" placeholder='<%$ Resources:change_password,Password1 %>' ValidationGroup="CambioPassword" TextMode="Password" AutoCompleteType="None" autocomplete="off" CssClass="input_login" ToolTip='<%$ Resources:change_password,TooltipPassword %>' /></span>
                  <span class="span_login txt_error">
                    <asp:RequiredFieldValidator runat="server" ID="reqPassword1" ControlToValidate="txtPassword1" ValidationGroup="CambioPassword" Display="Dynamic" ErrorMessage='<%$ Resources:change_password,Obligatorio %>' />
                    <asp:RegularExpressionValidator ID="regPassword1" runat="server" ControlToValidate="txtPassword1" ValidationGroup="CambioPassword" Display="Dynamic" ErrorMessage='<%$ Resources:change_password,ErrorPassword %>' ValidationExpression="[a-zA-Z0-9çÇ\@\/+!#$)(_-]{8,20}" />
                  </span>
                  <span class="span_login"><asp:Literal id="litPassword2" Text='<%$ Resources:change_password,Password2 %>' runat="server" EnableViewState="false" /></span>
                  <span class="span_login"><asp:TextBox runat="server" ID="txtPassword2" placeholder='<%$ Resources:change_password,Password2 %>' ValidationGroup="CambioPassword" TextMode="Password" AutoCompleteType="None" autocomplete="off" CssClass="input_login" /></span>
                  <span class="span_login txt_error">
                    <asp:RequiredFieldValidator ID="reqPassword2" runat="server" ControlToValidate="txtPassword2" ValidationGroup="CambioPassword" Display="Dynamic" ErrorMessage='<%$ Resources:change_password,Obligatorio %>' />
                    <asp:CompareValidator ID="reqcomPasswords" runat="server" ControlToCompare="txtPassword1" ControlToValidate="txtPassword2" ValidationGroup="CambioPassword" Display="Dynamic" Text='<%$ Resources:change_password,ErrorCompararPasswords %>' />
                  </span>
                </div>
                <div class="login-btn">
                  <asp:LinkButton ID="btnCambioPassword" runat="server" CssClass="btn_acceder" OnClick="btnCambioPassword_Click" Text='<%$ Resources:change_password,BotonCambiarPasswords %>' ValidationGroup="CambioPassword" />
                </div>
              </div>
            </asp:Panel>
          </ContentTemplate>
        </asp:UpdatePanel>

        <asp:UpdatePanel runat="server" ID="upCambioPasswordAlerta">
          <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnCambioPassword" />
          </Triggers>
          <ContentTemplate>
            <div class="alert alert-success fade collapse" style="margin-top:20px;" role="alert" runat="server" id="divAlerta">
              <asp:Label ID="lblError" runat="server" />
            </div>
          </ContentTemplate>
        </asp:UpdatePanel>
      </div>

      <div id="CapaIdiomas" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" runat="server">
        <div class="modal-dialog modal-sm">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title txt_titulopagina_peq" id="exampleModalLabel">
                <asp:Literal id="litSeleccionarIdioma" Text='<%$ Resources:login,TitSeleccionarIdioma %>' runat="server" EnableViewState="false" />
              </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div class="modal-body">
              <asp:DropDownList ID="ddlIdioma" runat="server" AutoPostBack="True" class="form-control" OnSelectedIndexChanged="ddlIdioma_SelectedIndexChanged"></asp:DropDownList>
            </div>
          </div>
        </div>
      </div>
    </div>
  </form>
</body>
</html>
