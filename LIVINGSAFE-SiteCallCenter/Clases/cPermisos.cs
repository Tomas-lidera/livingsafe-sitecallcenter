﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;

namespace LIVINGSAFE_SiteCallCenter.Clases
{
  public class cPermisos
  {
    public bool TienePermiso(string Zona,int IdOperador)
    {
      bool Tiene = false;
      ArrayList PermisosZona = new ArrayList();
      int IdTipoOperador = -1;

      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = Con.CreateCommand();

      try
      {
        Com.CommandType = System.Data.CommandType.StoredProcedure;
        Com.CommandText = "Acceso_DamePermisosZonas";
        Com.CommandTimeout = 300;
        Com.Parameters.Add("@IdOperador", SqlDbType.Int).Value = IdOperador;
        Com.Parameters.Add("@Zona", SqlDbType.VarChar,500).Value = Zona;
        Con.Open();

        SqlDataReader Rst = Com.ExecuteReader();

        while (Rst.Read())
        {
          PermisosZona.Add(Convert.ToInt32(Rst["IdTipoOperador"]));
        }

        Rst.Close();
        Rst.Dispose();

        if (Con.State == ConnectionState.Open)
          Con.Close();

        Com = new SqlCommand();
        Com = Con.CreateCommand();

        Com.CommandType = System.Data.CommandType.StoredProcedure;
        Com.CommandText = "Operadores_DevolverDatos";
        Com.CommandTimeout = 300;
        Com.Parameters.Add("@IdOperador", SqlDbType.Int).Value = IdOperador;
        Con.Open();

        SqlDataReader Rst2 = Com.ExecuteReader();

        if (Rst2.Read())
        {
          IdTipoOperador = Convert.ToInt32(Rst2["IdTipoOperador"]);
        }

        Rst2.Close();
        Rst2.Dispose();
      }

      finally
      {
        if (Con.State == ConnectionState.Open)
          Con.Close();

        Com.Dispose();
        Con.Dispose();
      }

      //Si el operador no existe o no tiene definidos permisos, no le damos acceso.
      if (IdTipoOperador == -1)
        Tiene = false;

      //Por defecto permisos pesimistas. Es decir, si la zona no está definida para ese ISP o el operador no existe o hay cualquier otra situación, no tiene permiso.
      if (PermisosZona.Count == 0)
        Tiene = false;

      if(IdTipoOperador>0 && PermisosZona.Count>0)
      {
        //Si el perfil del operador está entre los perfiles permitidos, le damos acceso.
        for(int i=0; i<PermisosZona.Count; i++)
        {
          if (Convert.ToInt32(PermisosZona[i]) == IdTipoOperador && Tiene == false)
            Tiene = true;
        }
      }

      return Tiene;
    }

    public bool TienePermiso(string Zona)
    {
      bool Tiene = false;
      ArrayList PermisosZona = new ArrayList();
      int IdTipoOperador = -1;

      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = Con.CreateCommand();

      try
      {
        Com.CommandType = System.Data.CommandType.StoredProcedure;
        Com.CommandText = "Acceso_DamePermisosZonas";
        Com.CommandTimeout = 300;
        Com.Parameters.Add("@IdOperador", SqlDbType.Int).Value = Convert.ToInt32(HttpContext.Current.User.Identity.Name);
        Com.Parameters.Add("@Zona", SqlDbType.VarChar, 500).Value = Zona;
        Con.Open();

        SqlDataReader Rst = Com.ExecuteReader();

        while (Rst.Read())
        {
          PermisosZona.Add(Convert.ToInt32(Rst["IdTipoOperador"]));
        }

        Rst.Close();
        Rst.Dispose();

        if (Con.State == ConnectionState.Open)
          Con.Close();

        Com = new SqlCommand();
        Com = Con.CreateCommand();

        Com.CommandType = System.Data.CommandType.StoredProcedure;
        Com.CommandText = "Operadores_DevolverDatos";
        Com.CommandTimeout = 300;
        Com.Parameters.Add("@IdOperador", SqlDbType.Int).Value = Convert.ToInt32(HttpContext.Current.User.Identity.Name);
        Con.Open();

        SqlDataReader Rst2 = Com.ExecuteReader();

        if (Rst2.Read())
        {
          IdTipoOperador = Convert.ToInt32(Rst2["IdTipoOperador"]);
        }

        Rst2.Close();
        Rst2.Dispose();
      }

      finally
      {
        if (Con.State == ConnectionState.Open)
          Con.Close();

        Com.Dispose();
        Con.Dispose();
      }

      //Si el operador no existe o no tiene definidos permisos, no le damos acceso.
      if (IdTipoOperador == -1)
        Tiene = false;

      //Por defecto permisos pesimistas. Es decir, si la zona no está definida para ese ISP o el operador no existe o hay cualquier otra situación, no tiene permiso.
      if (PermisosZona.Count == 0)
        Tiene = false;

      if (IdTipoOperador > 0 && PermisosZona.Count > 0)
      {
        //Si el perfil del operador está entre los perfiles permitidos, le damos acceso.
        for (int i = 0; i < PermisosZona.Count; i++)
        {
          if (Convert.ToInt32(PermisosZona[i]) == IdTipoOperador && Tiene == false)
            Tiene = true;
        }
      }

      return Tiene;
    }
  }
}