﻿using LIVINGSAFE_SiteCallCenter.App_Code;
using OfficeOpenXml;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading;
using System.Web;

namespace LIVINGSAFE_Billing
{
  public class cBilling
  {
    cUtil Utilidad = new cUtil();

    public cBilling()
    {
    }

    public void Facturar_ISP(ArrayList ListaISP,DateTime Fecha,string NombreFichero,string NombreFicheroReal)
    {
      cBilling Facturacion = new cBilling();
      cExcel ObjetoExcel = new cExcel();

      ExcelPackage Excel = ObjetoExcel.GenerarExcel();

      //Para trabajar con fechas forzamos la cultura es-ES porque como puede poner idioma inglés, al cambiar la cultura cambia el tipo 
      //de fecha de dd/mm/yyyy a mm/dd/yyyy y da problemas. Al final de operar con fechas devolvermos la cultura que tuvier antes.
      string CulturaActual = string.Empty;

      if (HttpContext.Current.Request.UserLanguages != null && HttpContext.Current.Request.UserLanguages.Length > 0)
        CulturaActual = HttpContext.Current.Request.UserLanguages[0];
      else
        CulturaActual = Thread.CurrentThread.CurrentCulture.Name;

      Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("es-ES");
      Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("es-ES");

      for (int i = 0; i < ListaISP.Count; i++)
      {
        ArrayList Datos = Facturacion.ISP_DameDatosFecha(Fecha, Convert.ToInt32(ListaISP[i]));

        if (Datos.Count > 0)
        {
          string NombreHoja = string.Empty;

          if (!Convert.ToBoolean(Utilidad.DameDatoISP(Convert.ToInt32(ListaISP[i]), "Activo", true)))
            NombreHoja = "(D) ";

          NombreHoja += Utilidad.DameDatoISP(Convert.ToInt32(ListaISP[i]), "Siglas", true);

          ObjetoExcel.GenerarHoja(ref Excel, NombreHoja, Datos);
        }
      }

      //Dejamos la cultura como estaba
      Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(CulturaActual);
      Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(CulturaActual);

      string Nombre = ObjetoExcel.GrabarExcel(NombreFichero,NombreFicheroReal);
    }

    public void Facturar_Partner(ArrayList ListaPartner, DateTime Fecha, string NombreFichero, string NombreFicheroReal)
    {
      cBilling Facturacion = new cBilling();
      cExcel ObjetoExcel = new cExcel();

      ExcelPackage Excel = ObjetoExcel.GenerarExcel();

      //Para trabajar con fechas forzamos la cultura es-ES porque como puede poner idioma inglés, al cambiar la cultura cambia el tipo 
      //de fecha de dd/mm/yyyy a mm/dd/yyyy y da problemas. Al final de operar con fechas devolvermos la cultura que tuvier antes.
      string CulturaActual = string.Empty;

      if (HttpContext.Current.Request.UserLanguages != null && HttpContext.Current.Request.UserLanguages.Length > 0)
        CulturaActual = HttpContext.Current.Request.UserLanguages[0];
      else
        CulturaActual = Thread.CurrentThread.CurrentCulture.Name;

      Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("es-ES");
      Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("es-ES");

      for (int i = 0; i < ListaPartner.Count; i++)
      {
        ArrayList Datos = Facturacion.Partner_DameDatosFecha(Fecha, Convert.ToInt32(ListaPartner[i]));

        if (Datos.Count > 0)
        {
          string NombreHoja = string.Empty;

          if (!Convert.ToBoolean(((ArrayList)(Datos[1]))[7]))
            NombreHoja = "(D) ";

          NombreHoja += ((ArrayList)(Datos[1]))[6].ToString().ToUpper().Trim();

          ObjetoExcel.GenerarHoja(ref Excel, NombreHoja, Datos);
        }
      }

      //Dejamos la cultura como estaba
      Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(CulturaActual);
      Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(CulturaActual);

      string Nombre = ObjetoExcel.GrabarExcel(NombreFichero, NombreFicheroReal);
    }

    public void Facturar_Proveedor(ArrayList ListaProveedor, DateTime Fecha, string NombreFichero, string NombreFicheroReal)
    {
      cBilling Facturacion = new cBilling();
      cExcel ObjetoExcel = new cExcel();

      ExcelPackage Excel = ObjetoExcel.GenerarExcel();

      //Para trabajar con fechas forzamos la cultura es-ES porque como puede poner idioma inglés, al cambiar la cultura cambia el tipo 
      //de fecha de dd/mm/yyyy a mm/dd/yyyy y da problemas. Al final de operar con fechas devolvermos la cultura que tuvier antes.
      string CulturaActual = string.Empty;

      if (HttpContext.Current.Request.UserLanguages != null && HttpContext.Current.Request.UserLanguages.Length > 0)
        CulturaActual = HttpContext.Current.Request.UserLanguages[0];
      else
        CulturaActual = Thread.CurrentThread.CurrentCulture.Name;

      Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("es-ES");
      Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("es-ES");

      for (int i = 0; i < ListaProveedor.Count; i++)
      {
        ArrayList Datos = Facturacion.Proveedor_DameDatosFecha(Fecha, Convert.ToInt32(ListaProveedor[i]));

        ArrayList DatosOrdenado = new ArrayList();

        DatosOrdenado = Facturacion.OrdenarArrayList(Datos, 9, true);

        if (DatosOrdenado.Count > 0)
        {
          string NombreHoja = string.Empty;

          if (!Convert.ToBoolean(((ArrayList)(DatosOrdenado[1]))[8]))
            NombreHoja = "(D) ";

          NombreHoja += ((ArrayList)(DatosOrdenado[1]))[7].ToString().ToUpper().Trim();

          ObjetoExcel.GenerarHoja(ref Excel, NombreHoja, DatosOrdenado);
        }
      }

      //Dejamos la cultura como estaba
      Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(CulturaActual);
      Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(CulturaActual);

      string Nombre = ObjetoExcel.GrabarExcel(NombreFichero, NombreFicheroReal);
    }

    public ArrayList ISP_DameDatosFecha(DateTime Fecha, int IdISP)
    {
      ArrayList Datos = new ArrayList();
      ArrayList Linea = new ArrayList();
      int i = 0;

      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = Con.CreateCommand();

      try
      {
        Com.CommandType = System.Data.CommandType.StoredProcedure;
        Com.CommandText = "Facturacion40_DevolverDatosISP";
        Com.CommandTimeout = 600;
        Com.Parameters.Add("@Fecha", SqlDbType.DateTime).Value = Fecha;
        Com.Parameters.Add("@IdISP", SqlDbType.Int).Value = IdISP;

        Con.Open();

        SqlDataReader Rst = Com.ExecuteReader();

        while (Rst.Read())
        {
          //Cabecera
          if (i == 0)
          {
            Linea.Add("IDCARD");
            Linea.Add("FIRST_NAME");
            Linea.Add("LAST_NAME");
            Linea.Add("EMAIL");
            Linea.Add("PHONE");
            Linea.Add("COMPANY");
            Linea.Add("CONTRACT");
            Linea.Add("QUANTITY_SUPPLIED");
            Linea.Add("SKU_SUPPLIED");
            Linea.Add("QUANTITY_INVOICE");
            Linea.Add("SKU_INVOICE");
            Linea.Add("PRODUCT_INVOICE");
            Linea.Add("ORDER_DATE");
            Linea.Add("EXTRACTION_DATE");
            Linea.Add("XXX-IDPRODUCT");

            if (Linea.Count > 0)
              Datos.Add(Linea);
          }

          Linea = new ArrayList();

          Linea.Add(Rst["NIF"].ToString());                       // IDCARD
          Linea.Add(Rst["Nombre"].ToString());                    // FIRST_NAME
          Linea.Add(Rst["Apellidos"].ToString());                 // LAST_NAME
          Linea.Add(Rst["Email"].ToString());                     // EMAIL
          Linea.Add(Rst["Movil"].ToString());                     // PHONE
          Linea.Add(Rst["Empresa"].ToString());                   // COMPANY
          Linea.Add(Rst["Contrato"].ToString());                  // CONTRACT
          Linea.Add(Convert.ToInt32(Rst["Cantidad"]));            // QUANTITY_SUPPLIED
          Linea.Add(Rst["SKU"].ToString());                       // SKU_SUPPLIED
          Linea.Add(string.Empty);                                // QUANTITY_INVOICE
          Linea.Add(string.Empty);                                // SKU_INVOICE
          Linea.Add(string.Empty);                                // PRODUCT_INVOICE
          Linea.Add(Convert.ToDateTime(Rst["FechaPedido"]));      // ORDER_DATE
          Linea.Add(Convert.ToDateTime(Rst["FechaExtraccion"]));  // EXTRACTION_DATE
          Linea.Add(Convert.ToInt32(Rst["IdProducto"]));          // IDPRODUCT

          if (Linea.Count > 0)
            Desglose_ISP(ref Datos, Linea); //En Desglose se hace la separación por tramos y se añade a Datos.

          i = i + 1;
        }

        Rst.Close();
        Rst.Dispose();
      }

      finally
      {
        if (Con.State == ConnectionState.Open)
          Con.Close();

        Com.Dispose();
        Con.Dispose();
      }

      return Datos;
    }

    public ArrayList Partner_DameDatosFecha(DateTime Fecha, int IdPartner)
    {
      ArrayList Datos = new ArrayList();
      ArrayList Linea = new ArrayList();
      int i = 0;

      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = Con.CreateCommand();

      try
      {
        Com.CommandType = System.Data.CommandType.StoredProcedure;
        Com.CommandText = "Facturacion40_DevolverDatosPartner";
        Com.CommandTimeout = 600;
        Com.Parameters.Add("@Fecha", SqlDbType.DateTime).Value = Fecha;
        Com.Parameters.Add("@IdPartner", SqlDbType.Int).Value = IdPartner;

        Con.Open();

        SqlDataReader Rst = Com.ExecuteReader();

        while (Rst.Read())
        {
          //Cabecera
          if (i == 0)
          {
            Linea.Add("ISP");
            Linea.Add("SKU_INVOICE");
            Linea.Add("PRODUCT_INVOICE");
            Linea.Add("QUANTITY_SUPPLIED");
            Linea.Add("RANGE");
            Linea.Add("TOTAL");
            Linea.Add("XXX-PARTNERNAME");
            Linea.Add("XXX-ACTIVO");

            if (Linea.Count > 0)
              Datos.Add(Linea);
          }

          Linea = new ArrayList();

          string NombreISP = string.Empty;

          if (!Convert.ToBoolean(Rst["ISPActivo"]))
            NombreISP = "(D) " + Rst["ISPNombre"].ToString();
          else
            NombreISP = Rst["ISPNombre"].ToString();

          Linea.Add(NombreISP);                                   // ISP
          Linea.Add(Rst["ProductoSKU"].ToString());               // SKU_INVOICE
          Linea.Add(Rst["ProductoNombre"].ToString());            // PRODUCT_INVOICE
          Linea.Add(Convert.ToInt32(Rst["SumCantidad"]));         // QUANTITY_SUPPLIED
          Linea.Add(Convert.ToInt32(Rst["RangoI"]));              // RANGE
          Linea.Add(Convert.ToInt32(Rst["Subtotal"]));            // TOTAL
          Linea.Add(Rst["NombrePartnerLimpio"].ToString());       // XXX-PARTNERNAME
          Linea.Add(Convert.ToBoolean(Rst["Activo"]));            // XXX-ACTIVO
          Linea.Add(Convert.ToBoolean(Rst["ISPActivo"]));         // XXX-ACTIVOISP

          if (Linea.Count > 0)
            Desglose_Partner(ref Datos, Linea);

          i = i + 1;
        }

        Rst.Close();
        Rst.Dispose();
      }

      finally
      {
        if (Con.State == ConnectionState.Open)
          Con.Close();

        Com.Dispose();
        Con.Dispose();
      }

      return Datos;
    }

    public ArrayList Proveedor_DameDatosFecha(DateTime Fecha, int IdProveedor)
    {
      ArrayList Datos = new ArrayList();
      ArrayList Linea = new ArrayList();
      ArrayList Acumulado = new ArrayList();
      int i = 0;

      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = Con.CreateCommand();

      try
      {
        Com.CommandType = System.Data.CommandType.StoredProcedure;
        Com.CommandText = "Facturacion40_DevolverDatosISPResumen";
        Com.CommandTimeout = 600;
        Com.Parameters.Add("@IdProveedor", SqlDbType.Int).Value = IdProveedor;
        Com.Parameters.Add("@Fecha", SqlDbType.DateTime).Value = Fecha;
        //Com.Parameters.Add("@IdPartner", SqlDbType.Int).Value = IdPartner;

        Con.Open();

        SqlDataReader Rst = Com.ExecuteReader();

        while (Rst.Read())
        {
          //Cabecera
          if (i == 0)
          {
            Linea.Add("PRODUCT");
            Linea.Add("QUANTITY");
            Linea.Add("PERIOD");
            Linea.Add("UNIT_PRICE");
            Linea.Add("SUBTOTAL");
            Linea.Add("XXX-IDPRODUCTO");
            Linea.Add("XXX-ISPNOMBRE");
            Linea.Add("XXX-NOMBREPROVEEDOR");
            Linea.Add("XXX-ACTIVOPROVEEDOR");
            Linea.Add("XXX-ORDEN");

            if (Linea.Count > 0)
              Datos.Add(Linea);
          }

          Linea = new ArrayList();

          //Lo añadimos a la lista de reporte si ese ISP reporta a Proveedor (por defecto todos reportan menos los marcados específicamente)
          if (Convert.ToBoolean(Rst["ReportaFacturacionProveedor"]))
            {
            Linea.Add(Rst["ProductoNombre"].ToString().Trim());     // PRODUCT
            Linea.Add(Rst["Cantidad"].ToString());                  // QUANTITY
            Linea.Add(Rst["PeriodoDescripcion"].ToString());        // PERIOD
            Linea.Add(Convert.ToDouble(Rst["PrecioUnitario"]));     // UNIT_PRICE
            Linea.Add(Convert.ToDouble(0.0));                       // SUBTOTAL
            Linea.Add(Convert.ToInt32(Rst["IdProducto"]));          // XXX-IDPRODUCTO
            Linea.Add(Rst["ISPNombre"].ToString());                 // XXX-ISPNOMBRE
            Linea.Add(Rst["NombreProveedor"].ToString());           // XXX-NOMBREPROVEEDOR
            Linea.Add(Rst["ActivoProveedor"].ToString());           // XXX-ACTIVOPROVEEDOR
            Linea.Add(Rst["Orden"].ToString());                     // XXX-ORDEN

            if (Linea.Count > 0)
              Desglose_Proveedor(ref Datos, Linea, ref Acumulado);
          }

          i = i + 1;
        }

        Rst.Close();
        Rst.Dispose();

        ArrayList Desglose = null;
        Datos = new ArrayList();

        //Volvemos a añadir la cabecera
        Desglose = new ArrayList();

        Desglose.Add("PRODUCT");
        Desglose.Add("QUANTITY");
        Desglose.Add("PERIOD");
        Desglose.Add("UNIT_PRICE");
        Desglose.Add("SUBTOTAL");
        Desglose.Add("XXX-IDPRODUCTO");
        Desglose.Add("XXX-ISPNOMBRE");
        Desglose.Add("XXX-NOMBREPROVEEDOR");
        Desglose.Add("XXX-ACTIVOPROVEEDOR");
        Desglose.Add("XXX-ORDEN");

        Datos.Add(Desglose);

        for (int r = 0; r < Acumulado.Count; r++)
        {
          Desglose = new ArrayList();

          Desglose.Add(((ArrayList)Acumulado[r])[3].ToString());        //PRODUCT
          Desglose.Add(Convert.ToInt32(((ArrayList)Acumulado[r])[1]));  //QUANTITY
          Desglose.Add(((ArrayList)Acumulado[r])[6].ToString());        //PERIOD
          Desglose.Add(Convert.ToDouble(((ArrayList)Acumulado[r])[7])); //UNIT_PRICE
          Desglose.Add(Convert.ToDouble(((ArrayList)Acumulado[r])[5])); //SUBTOTAL
          Desglose.Add(0);                                              //XXX-IDPRODUCTO
          Desglose.Add(((ArrayList)Acumulado[r])[2]);                   //XXX-ISPNOMBRE
          Desglose.Add(((ArrayList)Acumulado[r])[8]);                   //XXX-NOMBREPROVEEDOR
          Desglose.Add(((ArrayList)Acumulado[r])[9]);                   //XXX-ACTIVOPROVEEDOR
          Desglose.Add(((ArrayList)Acumulado[r])[10]);                   //XXX-ORDEN

          Datos.Add(Desglose);
        }
      }

      finally
      {
        if (Con.State == ConnectionState.Open)
          Con.Close();

        Com.Dispose();
        Con.Dispose();
      }

      return Datos;
    }

    protected void Desglose_ISP(ref ArrayList Datos,ArrayList Linea)
    {
      //LINEA CONTIENE: 0-IDCARD, 1-FIRST_NAME, 2-LAST_NAME, 3-EMAIL, 4-PHONE, 5-COMPANY, 6-CONTRACT, 7-QUANTITY_SUPPLIED, 8-SKU_SUIPPLIED, 
      //                9 -QUANTITY_INVOICE, 10-SKU_INVOICE, 11-PRODUCT_INVOICE, 12-ORDER_DATE, 13-EXTRACTION_DATE, 14-XXX-IDPRODUCT
      ArrayList Desglose = null;
      int CANT = Convert.ToInt32(Linea[7]);
      int IdProducto = Convert.ToInt32(Linea[14]);
      int DIV = 0;
      int COC = 0;
      int RES = 0;

      //Inicializo los productos.
      ArrayList Productos = DameProductosFacturacion(IdProducto);

      for (int I = 0; I < Productos.Count; I++)
      {
        ArrayList Producto = DameProductoFacturacion(I, Productos);

        DIV = Convert.ToInt32(Producto[3]);

        if (CANT != 0)
        {
          COC = CANT / DIV;
          RES = CANT % DIV;

          if (COC > 0)
          {
            Desglose = new ArrayList();

            Desglose.Add(Linea[0]);               // IDCARD
            Desglose.Add(Linea[1]);               // FIRST_NAME
            Desglose.Add(Linea[2]);               // LAST_NAME
            Desglose.Add(Linea[3]);               // EMAIL
            Desglose.Add(Linea[4]);               // PHONE
            Desglose.Add(Linea[5]);               // COMPANY
            Desglose.Add(Linea[6]);               // CONTRACT
            Desglose.Add(Linea[7]);               // QUANTITY_SUPPLIED
            Desglose.Add(Linea[8]);               // SKU_SUPPLIED
            Desglose.Add(COC);                    // QUANTITY_INVOICE
            Desglose.Add(Producto[2].ToString()); // SKU_INVOICE
            Desglose.Add(Producto[1].ToString()); // PRODUCT_INVOICE
            Desglose.Add(Linea[12]);              // ORDER_DATE
            Desglose.Add(Linea[13]);              // EXTRACTION_DATE
            Desglose.Add(Linea[14]);              // XXX-IDPRODUCT

            Datos.Add(Desglose);

            CANT = RES;
          }
        }
      }
    }

    protected void Desglose_Partner(ref ArrayList Datos,ArrayList Linea)
    {
      ArrayList Desglose = null;

      Desglose = new ArrayList();

      Desglose.Add(Linea[0]);               // ISP
      Desglose.Add(Linea[1]);               // SKU_INVOICE
      Desglose.Add(Linea[2]);               // PRODUCT_INVOICE
      Desglose.Add(Linea[3]);               // QUANTITY_SUPPLIED
      Desglose.Add(Linea[4]);               // RANGE
      Desglose.Add(Linea[5]);               // TOTAL
      Desglose.Add(Linea[6]);               // XXX-PARTNERNAME
      Desglose.Add(Linea[7]);               // XXX-ACTIVO

      Datos.Add(Desglose);
    }

    protected void Desglose_Proveedor(ref ArrayList Datos, ArrayList Linea, ref ArrayList Acumulado)
    {
      //LINEA CONTIENE: 0-PRODUCT, 1-QUANTITY, 2-PERIOD, 3-UNIT_PRICE, 4-SUBTOTAL, 5-XXX-IDPRODUCTO, 6-XXX-ISPNOMBRE, 7-XXXNOMBREPROVEEDOR, 8-XXX-ACTIVOPROVEEDOR, 9-XXX-ORDEN

      ArrayList Desglose = new ArrayList();
      ArrayList Detalles = new ArrayList();

      int CANT = Convert.ToInt32(Linea[1]);
      int IdProducto = 0;
      int DIV = 0;
      int COC = 0;
      int RES = 0;

      IdProducto = Convert.ToInt32(Linea[5]);

      ArrayList Productos = DameProductosFacturacion(IdProducto);

      for (int I = 0; I < Productos.Count; I++)
      {
        ArrayList Producto = DameProductoFacturacion(I, Productos);
        DIV = Convert.ToInt32(Producto[3]);

        if (CANT != 0)
        {
          COC = CANT / DIV;
          RES = CANT % DIV;

          if (COC > 0)
          {
            if (Acumulado.Count == 0)
            {
              Detalles = new ArrayList();
              Detalles.Add(Producto[2]);                                        // SKU Producto
              Detalles.Add(COC);                                                // Cantidad
              Detalles.Add(Linea[6].ToString().Trim());                         // Nombre ISP
              Detalles.Add(Producto[1].ToString().Trim());                      // Nombre Producto
              Detalles.Add(Producto[3].ToString());                             // Rango
              Detalles.Add(Convert.ToDouble(COC) * Convert.ToDouble(Linea[3])); // Subtotal
              Detalles.Add(Linea[2].ToString());                                // Periodo
              Detalles.Add(Convert.ToDouble(Linea[3]));                         // Unit_price
              Detalles.Add(Linea[7].ToString().Trim());                         // Nombre proveedor
              Detalles.Add(Convert.ToBoolean(Linea[8]));                        // Activo proveedor
              Detalles.Add(Convert.ToInt32(Producto[4]));                       // Orden
              Acumulado.Add(Detalles);
            }
            else
            {
              bool Encontrado = false;

              for (int r = 0; r < Acumulado.Count; r++)
              {
                Detalles = new ArrayList();
                Detalles = (ArrayList)Acumulado[r];

                if (Detalles[0].ToString() == Producto[2].ToString())
                {
                  Detalles[1] = Convert.ToInt32(Detalles[1]) + COC;
                  Detalles[5] = Convert.ToDouble(Detalles[1]) * Convert.ToDouble(Detalles[7]);
                  Acumulado[r] = Detalles;
                  Encontrado = true;
                }
              }

              if (Encontrado == false)
              {
                Detalles = new ArrayList();
                Detalles.Add(Producto[2]);
                Detalles.Add(COC);
                Detalles.Add(Linea[6].ToString().Trim());
                Detalles.Add(Producto[1].ToString().Trim());
                Detalles.Add(Producto[3].ToString());
                Detalles.Add(Convert.ToDouble(COC) * Convert.ToDouble(Linea[3]));
                Detalles.Add(Linea[2].ToString());
                Detalles.Add(Convert.ToDouble(Linea[3]));
                Detalles.Add(Linea[7].ToString().Trim());
                Detalles.Add(Convert.ToBoolean(Linea[8]));
                Detalles.Add(Convert.ToInt32(Producto[4]));
                Acumulado.Add(Detalles);
              }
            }

            CANT = RES;
          }
        }
      }
    }

    protected ArrayList DameProductosFacturacion(int IdProducto)
    {
      ArrayList Productos = new ArrayList();
      ArrayList Producto = new ArrayList();
      int i = 0;

      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = Con.CreateCommand();

      try
      {
        
        Com.CommandType = System.Data.CommandType.StoredProcedure;
        Com.CommandText = "Facturacion40_DameProductosFacturacion";
        Com.CommandTimeout = 600;
        Com.Parameters.Add("@IdProducto", SqlDbType.Int).Value = IdProducto;

        Con.Open();

        SqlDataReader Rst = Com.ExecuteReader();

        while (Rst.Read())
        {
          Producto = new ArrayList();
          Producto.Add(i);
          Producto.Add(Rst["NombreFacturacion"]);
          Producto.Add(Rst["SKUFacturacion"]);
          Producto.Add(Convert.ToDouble(Rst["RangoI"]));
          Producto.Add(Convert.ToInt32(Rst["Orden"]));

          Productos.Add(Producto);

          i = i + 1;
        }

        Rst.Close();
        Rst.Dispose();
      }

      finally
      {
        if (Con.State == ConnectionState.Open)
          Con.Close();

        Com.Dispose();
        Con.Dispose();
      }

      return Productos;
    }

    protected ArrayList DameProductoFacturacion(int pos, ArrayList Productos)
    {
      ArrayList Producto = new ArrayList();

      for (int i = 0; i < Productos.Count; i++)
      {
        if (Convert.ToInt32(((ArrayList)Productos[i])[0]) == pos)
          Producto = (ArrayList)Productos[i];
      }

      return Producto;
    }

    protected ArrayList OrdenarArrayList(ArrayList Lista,int Posicion,bool TieneCabecera)
    {
      //Posicion indica que posición del ArrayList anidado dentro de Lista está el valor por el que ordenar. Por ejemplo, si llega un 9, significa que para cada posición
      //de Lista, en la posición 9 del ArrayList que contiene está el dato por el que se ha de ordenarnarna
      ArrayList ListaOrdenada = new ArrayList();
      int Inicio = 0;

      if (TieneCabecera)
      {
        Inicio = 1;
        ListaOrdenada.Add(Lista[0]);
      }

      List<int> Posiciones = new List<int>();

      for(int i=Inicio; i<Lista.Count;i++)
      {
        Posiciones.Add(Convert.ToInt32(((ArrayList)Lista[i])[Posicion]));
      }

      Posiciones.Sort();

      for(int i=0; i<Posiciones.Count;i++)
      {
        for(int r=Inicio; r<Lista.Count;r++)
        {
          if (Convert.ToInt32(((ArrayList)Lista[r])[Posicion]) == Posiciones[i])
            ListaOrdenada.Add(Lista[r]);
        }
      }

      return ListaOrdenada;
    }
  }
}
