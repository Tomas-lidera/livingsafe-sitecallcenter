﻿using System;
using System.IO;
using System.Configuration;
using System.Text;
using System.Collections;
using OfficeOpenXml;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Threading;
using LIVINGSAFE_SiteCallCenter.App_Code;

namespace LIVINGSAFE_Billing
{
  /*
   * 1. Se instancia la clase.
   * 2. Se llama a GenerarExcel que devuelve el objeto Excel vacío.
   * 2. Se llama a GenerarHoja tantas veces como se necesite pasándole el objeto Excel.
   * 3. Se llama a GrabarExcely esto guarda el fichero.
   */
  class cExcel
  {
    private string Nombre = string.Empty;
    private ExcelPackage Excel = null;

    public cExcel()
    {
      string Sufijo = DateTime.Now.Year.ToString("0000") + DateTime.Now.Month.ToString("00") + DateTime.Now.Day.ToString("00") + "_" + DateTime.Now.Hour.ToString("00") + DateTime.Now.Minute.ToString("00") + DateTime.Now.Second.ToString("00");

      Nombre = "LS_Billing_" + "_" + Sufijo;
    }


    //public string GenerarExcel()
    //{
    //  using (ExcelPackage Excel = new ExcelPackage())
    //  {
    //    //Nombre de la hoja
    //    ExcelWorksheet Hoja = Excel.Workbook.Worksheets.Add("ERRORS");

    //    //Nombre	Apellidos	CPF/CNPJ	E-mail	Contrato	Unidades provisionadas	SKU	Producto	FechaAlta


    //    Hoja.Cells["A1"].LoadFromText("NAME");
    //    Hoja.Cells["B1"].LoadFromText("SURNAME");
    //    Hoja.Cells["C1"].LoadFromText("IDCARD");
    //    Hoja.Cells["D1"].LoadFromText("EMAIL");
    //    Hoja.Cells["E1"].LoadFromText("CONTRACT");
    //    Hoja.Cells["F1"].LoadFromText("UNITS");
    //    Hoja.Cells["G1"].LoadFromText("SKU");
    //    Hoja.Cells["H1"].LoadFromText("PRODUCT");
    //    Hoja.Cells["I1"].LoadFromText("CREATED");


    //    for (int i = 1; i < 5; i++)
    //    {
    //      for (int r = 0; r < 9; r++)
    //      {
    //        if (r == 0)
    //          Hoja.Cells["A" + (i + 1).ToString()].LoadFromText((i+r).ToString());
    //        if (r == 1)
    //          Hoja.Cells["B" + (i + 1).ToString()].LoadFromText((i + r).ToString());
    //        if (r == 2)
    //          Hoja.Cells["C" + (i + 1).ToString()].LoadFromText((i + r).ToString());
    //        if (r == 3)
    //          Hoja.Cells["D" + (i + 1).ToString()].LoadFromText((i + r).ToString());
    //        if (r == 4)
    //          Hoja.Cells["E" + (i + 1).ToString()].LoadFromText((i + r).ToString());
    //        if (r == 5)
    //          Hoja.Cells["F" + (i + 1).ToString()].LoadFromText((i + r).ToString());
    //        if (r == 6)
    //          Hoja.Cells["G" + (i + 1).ToString()].LoadFromText((i + r).ToString());
    //        if (r == 7)
    //          Hoja.Cells["H" + (i + 1).ToString()].LoadFromText((i + r).ToString());
    //        if (r == 8)
    //          Hoja.Cells["I" + (i + 1).ToString()].LoadFromText((i + r).ToString());
    //      }
    //    }

    //    string filePath = ConfigurationManager.AppSettings["RutaFicherosInformes"].ToString() + "\\" + Nombre + ".xlsx";

    //    using (FileStream Fichero = new FileStream(filePath, FileMode.Create))
    //    {
    //      Excel.SaveAs(Fichero);
    //    }
    //  }

    //  return Nombre;
    //}

    public ExcelPackage GenerarExcel()
    {
      Excel = new ExcelPackage();

      return Excel;
    }

    public void GenerarHoja(ref ExcelPackage Excel, string NombreHoja, ArrayList Datos)
    {
      if (Datos.Count == 0)
        throw new Exception("ERRNUM0042");

      ArrayList Cabecera = null;
      ArrayList Linea = null;

      //Nombre de la hoja
      ExcelWorksheet Hoja = Excel.Workbook.Worksheets.Add(NombreHoja);

      //Cabecera. Las columnas que empiezan por "XXX-" no se añaden al Excel porque son de apoyo y deberían ir todas seguidas al final
      char[] Alfabeto = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();

      Cabecera = (ArrayList)Datos[0];

      for(int i=0; i<Cabecera.Count; i++)
      {
        if (!Cabecera[i].ToString().ToUpper().Trim().StartsWith("XXX-"))
        {
          string Celda = Alfabeto[i] + "1";
          Hoja.Cells[Celda].LoadFromText(Cabecera[i].ToString().ToUpper().Trim());
        }
      }

      //Contenido. Por cada dato que añadimos comprobamos en la cabecera si el campo empieza por "XXX-", en cuyo caso no se añade al Excel porque son de apoyo
      for (int i = 1; i < Datos.Count; i++)
      {
        Linea = (ArrayList)Datos[i];

        for (int r = 0; r < Linea.Count; r++)
        {
          if (!Cabecera[r].ToString().ToUpper().Trim().StartsWith("XXX-"))
          {
            string Celda = Alfabeto[r] + (i + 1).ToString();


            if (Hoja.Cells[Alfabeto[r] + "1"].Value.ToString().ToUpper().Contains("DATE"))
            {
              Hoja.Cells[Celda].Style.Numberformat.Format = "yyyy-MM-dd";
              Hoja.Cells[Celda].LoadFromText(Convert.ToDateTime(Linea[r]).ToString("yyyy-MM-dd"));
            }
            else
            {
              if(Hoja.Cells[Alfabeto[r] + "1"].Value.ToString().ToUpper()=="UNIT_PRICE" || Hoja.Cells[Alfabeto[r] + "1"].Value.ToString().ToUpper() == "SUBTOTAL")
              {
                Hoja.Cells[Celda].Style.Numberformat.Format = "0.00";
                Hoja.Cells[Celda].LoadFromText(Linea[r].ToString().Replace(",","."));
              }
              else{
                Hoja.Cells[Celda].Style.Numberformat.Format = "@";
                Hoja.Cells[Celda].LoadFromText(Linea[r].ToString());
              }
            }

            Hoja.Cells[Celda].AutoFitColumns();
          }
        }
      }
    }

    public string GrabarExcel(string NombrePersonalizado,string NombreReal)
    {
      if (NombrePersonalizado != string.Empty)
        Nombre = NombrePersonalizado.Trim().Replace(" ", "_");

      string Patron = @"^[a-zA-Z0-9çÇ_-]+$";

      Match Validador = Regex.Match(Nombre, Patron,RegexOptions.None);

      if (Validador.Success)
      {
        cImpersonate Impersonar = new cImpersonate();

        string UsuarioImpersonalizacion = ConfigurationManager.AppSettings["usuarioPublicar"].ToString();
        string DominioImpersonalizacion = ConfigurationManager.AppSettings["usuarioDominio"].ToString();
        string PassImpersonalizacion = ConfigurationManager.AppSettings["usuarioPass"].ToString();
        Impersonar.impersonateValidUser(UsuarioImpersonalizacion, DominioImpersonalizacion, PassImpersonalizacion);

        //string filePath = ConfigurationManager.AppSettings["RutaFicherosInformes"].ToString() + "\\" + Nombre + ".xlsx";
        string filePath = ConfigurationManager.AppSettings["RutaFicherosInformes"].ToString() + "\\" + NombreReal + ".xlsx";

        using (FileStream Fichero = new FileStream(filePath, FileMode.Create))
        {
          Excel.SaveAs(Fichero);
        }

        Impersonar.undoImpersonation();
      }
      else
      {
        throw new Exception("ERRNUM0043");
      }

      return Nombre;
    }
  }
}
