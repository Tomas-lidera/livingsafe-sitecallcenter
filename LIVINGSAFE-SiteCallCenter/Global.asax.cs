﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using LIVINGSAFE_SiteCallCenter.App_Code;

namespace LIVINGSAFE_SiteCallCenter
{
  public class Global : System.Web.HttpApplication
  {
    // Hacer un override de InitializeCulture en cada página sólo funciona con páginas de tipo "Page". En las de tipo "MasterPage" no funciona porque no tienen
    // ese método base que sobreescribir. Así que el cambio de cultura lo vamos a hacer en otro de los eventos previos a la carga de la página. Usamos el 
    //Application_BeginRequest para esas páginas lo cual me sirve tanto para las "Page" como para las "MasterPage".
    protected void Application_BeginRequest(object sender, EventArgs e)
    {
      cIdiomas Idiomas = new cIdiomas();
      Idiomas.InicializarCultura();
    }
  }
}