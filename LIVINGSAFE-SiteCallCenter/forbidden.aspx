﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="forbidden.aspx.cs" Inherits="LIVINGSAFE_SiteCallCenter.forbidden" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>LivingSafe - Forbidden</title>
  <link rel="stylesheet" type="text/css" href="~/style/bootstrap.css" runat="server" />
  <link rel="stylesheet" type="text/css" href="~/style/generalstyle.css" runat="server" />
  <link rel="stylesheet" type="text/css" href="~/style/custom/personalstyle.css" runat="server" id="cssCustomMaster" />

  <script type="text/javascript" src="/js/jquery-3.3.1.slim.min.js"></script>
  <script type="text/javascript" src="/js/bootstrap.bundle.js"></script>
  <script type="text/javascript" src="/js/jquery.mask.js"></script>
  <script type="text/javascript" src="/js/jquery.mask.min.js"></script>

  <link rel="apple-touch-icon" sizes="180x180" href="~/apple-touch-icon.png" runat="server" />
  <link rel="icon" type="image/png" sizes="32x32" href="~/favicon-32x32.png" runat="server" />
  <link rel="icon" type="image/png" sizes="16x16" href="~/favicon-16x16.png" runat="server" />
  <link rel="mask-icon" href="~/safari-pinned-tab.svg" color="#5bbad5" runat="server" />
</head>
<body style="background-color:#e5e9f0;">
<form id="form1" runat="server">
  <div class="container">
      <div class="row align-items-center" style="height:100vh; background-image:url(images/logo_blanco_peq.png); background-repeat:no-repeat; background-position:center bottom;">
          <div class="col-lg-5 offset-lg-1 text-center"><img src="images/balrog.jpg" class="img-fluid"></div>
            <div class="col-lg-5 text-center">
                  <span class="CC_mensajepag text-center"><asp:Literal runat="server" ID="litFrase1" Text='<%$ Resources:forbidden,litFrase1 %>' /></span>
                  <span class="CC_mensajepagpeq text-center"><asp:Literal runat="server" ID="litFrase2" Text='<%$ Resources:forbidden,litFrase2 %>' /></span>
                  <asp:LinkButton runat="server" ID="lbVolver" Text='<%$ Resources:forbidden,btnVolver %>' ToolTip='<%$ Resources:forbidden,btnVolver %>' CssClass="btn btn-primary CC_noradius" OnClick="lbVolver_Click"></asp:LinkButton>
            </div>
      </div>
  </div>
</form>
</body>
</html>
