//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   Clase de recurso fuertemente tipado, para buscar cadenas traducidas, etc.
    /// </summary>
    // StronglyTypedResourceBuilder generó automáticamente esta clase
    // a través de una herramienta como ResGen o Visual Studio.
    // Para agregar o quitar un miembro, edite el archivo .ResX y, a continuación, vuelva a ejecutar ResGen
    // con la opción /str o recompile el proyecto de Visual Studio.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "14.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class errores {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal errores() {
        }
        
        /// <summary>
        ///   Devuelve la instancia de ResourceManager almacenada en caché utilizada por esta clase.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.errores", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Invalida la propiedad CurrentUICulture del subproceso actual para todas las
        ///   búsquedas de recursos mediante esta clase de recurso fuertemente tipado.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Ocorreu um erro inesperado ao carregar seu perfil de usu&amp;aacute;rio. Por favor, tente novamente e se o erro persistir, entre em contato conosco. Desculpe pelo inconveniente..
        /// </summary>
        internal static string ERRNUM0001 {
            get {
                return ResourceManager.GetString("ERRNUM0001", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Ocorreu um erro ao carregar suas informa&amp;ccedil;&amp;otilde;es do ISP. Por favor, tente novamente e se o erro persistir, entre em contato conosco. Desculpe pelo inconveniente..
        /// </summary>
        internal static string ERRNUM0002 {
            get {
                return ResourceManager.GetString("ERRNUM0002", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Ocorreu um erro ao enviar a consulta. Por favor, tente novamente e se o erro persistir, entre em contato conosco. Desculpe pelo inconveniente..
        /// </summary>
        internal static string ERRNUM0003 {
            get {
                return ResourceManager.GetString("ERRNUM0003", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Ocorreu um erro inesperado ao redirecion&amp;aacute;-lo para o painel de gerenciamento do provedor..
        /// </summary>
        internal static string ERRNUM0004 {
            get {
                return ResourceManager.GetString("ERRNUM0004", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Ocorreu um erro inesperado ao redirecion&amp;aacute;-lo para o link de download do provedor..
        /// </summary>
        internal static string ERRNUM0005 {
            get {
                return ResourceManager.GetString("ERRNUM0005", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Ocorreu um erro inesperado ao validar as credenciais..
        /// </summary>
        internal static string ERRNUM0006 {
            get {
                return ResourceManager.GetString("ERRNUM0006", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a A senha indicada n&amp;atilde;o &amp;eacute; v&amp;aacute;lida. Lembre-se de que o sistema n&amp;atilde;o permite que voc&amp;ecirc; repita a mesma senha que voc&amp;ecirc; possui atualmente..
        /// </summary>
        internal static string ERRNUM0007 {
            get {
                return ResourceManager.GetString("ERRNUM0007", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a &amp;Eacute; necess&amp;aacute;rio indicar o identificador de pedido..
        /// </summary>
        internal static string ERRNUM0008 {
            get {
                return ResourceManager.GetString("ERRNUM0008", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a O formato do CPF / CNPJ não está correto..
        /// </summary>
        internal static string ERRNUM0009 {
            get {
                return ResourceManager.GetString("ERRNUM0009", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a O formato do e-mail não está correto..
        /// </summary>
        internal static string ERRNUM0010 {
            get {
                return ResourceManager.GetString("ERRNUM0010", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a O formato CEP não está correto..
        /// </summary>
        internal static string ERRNUM0011 {
            get {
                return ResourceManager.GetString("ERRNUM0011", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Você deve especificar um arquivo do Excel (.xlsx)..
        /// </summary>
        internal static string ERRNUM0012 {
            get {
                return ResourceManager.GetString("ERRNUM0012", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a O arquivo do Excel não contém nenhuma planilha..
        /// </summary>
        internal static string ERRNUM0013 {
            get {
                return ResourceManager.GetString("ERRNUM0013", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a O arquivo do Excel não contém dados..
        /// </summary>
        internal static string ERRNUM0014 {
            get {
                return ResourceManager.GetString("ERRNUM0014", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a O arquivo deve conter 9 colunas..
        /// </summary>
        internal static string ERRNUM0015 {
            get {
                return ResourceManager.GetString("ERRNUM0015", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a A primeira coluna deve ser chamada de &quot;CPF&quot;..
        /// </summary>
        internal static string ERRNUM0016 {
            get {
                return ResourceManager.GetString("ERRNUM0016", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a A segunda coluna deve ser chamada de &quot;First name&quot;..
        /// </summary>
        internal static string ERRNUM0017 {
            get {
                return ResourceManager.GetString("ERRNUM0017", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a A terceira coluna deve ser chamada &quot;Last name&quot;..
        /// </summary>
        internal static string ERRNUM0018 {
            get {
                return ResourceManager.GetString("ERRNUM0018", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a A quarta coluna deve ser chamada &quot;E-mail&quot;..
        /// </summary>
        internal static string ERRNUM0019 {
            get {
                return ResourceManager.GetString("ERRNUM0019", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a A quinta coluna deve ser chamada &quot;Mobile phone&quot;..
        /// </summary>
        internal static string ERRNUM0020 {
            get {
                return ResourceManager.GetString("ERRNUM0020", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a A sexta coluna deve ser chamada &quot;Address&quot;..
        /// </summary>
        internal static string ERRNUM0021 {
            get {
                return ResourceManager.GetString("ERRNUM0021", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a A sétima coluna deve ser chamada &quot;Postal Code&quot;..
        /// </summary>
        internal static string ERRNUM0022 {
            get {
                return ResourceManager.GetString("ERRNUM0022", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a A oitava coluna deve ser chamada &quot;Company name&quot;..
        /// </summary>
        internal static string ERRNUM0023 {
            get {
                return ResourceManager.GetString("ERRNUM0023", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a A nona coluna deve ser chamada &quot;Quantity&quot;..
        /// </summary>
        internal static string ERRNUM0024 {
            get {
                return ResourceManager.GetString("ERRNUM0024", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Nenhum dado válido encontrado para processar. Verifique se o arquivo selecionado contém dados.&lt;br /&gt;
        ///Se o arquivo não estiver vazio, todos os registros podem conter erros. Para resolver este problema, baixe e valide o relatório de erros e o registro de detalhes, onde você pode encontrar informações sobre os erros detectados durante a análise..
        /// </summary>
        internal static string ERRNUM0025 {
            get {
                return ResourceManager.GetString("ERRNUM0025", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Erro na fileira.
        /// </summary>
        internal static string ERRNUM0026 {
            get {
                return ResourceManager.GetString("ERRNUM0026", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a O formato do CPF / CNPJ não é válido..
        /// </summary>
        internal static string ERRNUM0027 {
            get {
                return ResourceManager.GetString("ERRNUM0027", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a O formato do e-mail não é válido..
        /// </summary>
        internal static string ERRNUM0028 {
            get {
                return ResourceManager.GetString("ERRNUM0028", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a O formato do CEP não é válido..
        /// </summary>
        internal static string ERRNUM0029 {
            get {
                return ResourceManager.GetString("ERRNUM0029", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Um usuário que não é uma empresa não deve especificar o campo &apos;Company&apos;..
        /// </summary>
        internal static string ERRNUM0030 {
            get {
                return ResourceManager.GetString("ERRNUM0030", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a As seguintes informações estão faltando.
        /// </summary>
        internal static string ERRNUM0031 {
            get {
                return ResourceManager.GetString("ERRNUM0031", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a A quantidade deve ser maior que 0.
        /// </summary>
        internal static string ERRNUM0032 {
            get {
                return ResourceManager.GetString("ERRNUM0032", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a A quantidade não pode ser maior que.
        /// </summary>
        internal static string ERRNUM0033 {
            get {
                return ResourceManager.GetString("ERRNUM0033", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Você não pode mover operadores de perfis diferentes ao mesmo tempo.
        /// </summary>
        internal static string ERRNUM0034 {
            get {
                return ResourceManager.GetString("ERRNUM0034", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Você não pode mover operadores para serem gerenciados por outro operador com perfil igual ou inferior.
        /// </summary>
        internal static string ERRNUM0035 {
            get {
                return ResourceManager.GetString("ERRNUM0035", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Você deve preencher todos os campos para garantir que eles não contenham erros..
        /// </summary>
        internal static string ERRNUM0036 {
            get {
                return ResourceManager.GetString("ERRNUM0036", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Você não pode classificar o novo nó em outro de categoria menor ou igual..
        /// </summary>
        internal static string ERRNUM0037 {
            get {
                return ResourceManager.GetString("ERRNUM0037", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Voc&amp;ecirc; n&amp;atilde;o pode selecionar o mesmo tipo de perfil ou mais alto que o do n&amp;oacute; pai..
        /// </summary>
        internal static string ERRNUM0038 {
            get {
                return ResourceManager.GetString("ERRNUM0038", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Para selecionar o perfil superior, voc&amp;ecirc; deve primeiro selecionar um tipo de perfil..
        /// </summary>
        internal static string ERRNUM0039 {
            get {
                return ResourceManager.GetString("ERRNUM0039", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Seu perfil n&amp;atilde;o tem permiss&amp;otilde;es para executar esta opera&amp;ccedil;&amp;atilde;o..
        /// </summary>
        internal static string ERRNUM0040 {
            get {
                return ResourceManager.GetString("ERRNUM0040", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a O produto selecionado n&amp;atilde;o permite altera&amp;ccedil;&amp;atilde;o de quantidade..
        /// </summary>
        internal static string ERRNUM0041 {
            get {
                return ResourceManager.GetString("ERRNUM0041", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Nenhum dado foi encontrado para gerar o arquivo de faturamento..
        /// </summary>
        internal static string ERRNUM0042 {
            get {
                return ResourceManager.GetString("ERRNUM0042", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a O arquivo cont&amp;eacute;m caracteres ilegais..
        /// </summary>
        internal static string ERRNUM0043 {
            get {
                return ResourceManager.GetString("ERRNUM0043", resourceCulture);
            }
        }
    }
}
