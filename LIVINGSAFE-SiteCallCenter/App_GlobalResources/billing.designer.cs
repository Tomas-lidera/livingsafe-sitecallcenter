//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   Clase de recurso fuertemente tipado, para buscar cadenas traducidas, etc.
    /// </summary>
    // StronglyTypedResourceBuilder generó automáticamente esta clase
    // a través de una herramienta como ResGen o Visual Studio.
    // Para agregar o quitar un miembro, edite el archivo .ResX y, a continuación, vuelva a ejecutar ResGen
    // con la opción /str o recompile el proyecto de Visual Studio.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "14.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class billing {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal billing() {
        }
        
        /// <summary>
        ///   Devuelve la instancia de ResourceManager almacenada en caché utilizada por esta clase.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.billing", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Invalida la propiedad CurrentUICulture del subproceso actual para todas las
        ///   búsquedas de recursos mediante esta clase de recurso fuertemente tipado.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a TIPO.
        /// </summary>
        internal static string BarraPaso1 {
            get {
                return ResourceManager.GetString("BarraPaso1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a ENTIDADES.
        /// </summary>
        internal static string BarraPaso2 {
            get {
                return ResourceManager.GetString("BarraPaso2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a DATA.
        /// </summary>
        internal static string BarraPaso3 {
            get {
                return ResourceManager.GetString("BarraPaso3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a NOME DO ARQUIVO.
        /// </summary>
        internal static string BarraPaso4 {
            get {
                return ResourceManager.GetString("BarraPaso4", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Voc&amp;ecirc; deve selecionar pelo menos uma entidade..
        /// </summary>
        internal static string BILLERRNUM0001 {
            get {
                return ResourceManager.GetString("BILLERRNUM0001", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Voc&amp;ecirc; deve selecionar um ano e um m&amp;ecirc;s..
        /// </summary>
        internal static string BILLERRNUM0002 {
            get {
                return ResourceManager.GetString("BILLERRNUM0002", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Voc&amp;ecirc; deve indicar um nome de arquivo..
        /// </summary>
        internal static string BILLERRNUM0003 {
            get {
                return ResourceManager.GetString("BILLERRNUM0003", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Voc&amp;ecirc; n&amp;atilde;o pode gerar relat&amp;oacute;rios de cobran&amp;ccedil;a para o m&amp;ecirc;s atual at&amp;eacute; o fim do m&amp;ecirc;s..
        /// </summary>
        internal static string BILLERRNUM0004 {
            get {
                return ResourceManager.GetString("BILLERRNUM0004", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Voc&amp;ecirc; n&amp;atilde;o pode gerar relat&amp;oacute;rios de cobran&amp;ccedil;a por meses ap&amp;oacute;s o m&amp;ecirc;s do curso..
        /// </summary>
        internal static string BILLERRNUM0005 {
            get {
                return ResourceManager.GetString("BILLERRNUM0005", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Aten&amp;ccedil;&amp;atilde;o.
        /// </summary>
        internal static string litAvisoCabecera {
            get {
                return ResourceManager.GetString("litAvisoCabecera", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Billing.
        /// </summary>
        internal static string litTituloPagina {
            get {
                return ResourceManager.GetString("litTituloPagina", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a &amp;Eacute; quem presta servi&amp;ccedil;o direto ao usu&amp;aacute;rio final. Depende sempre de um partner..
        /// </summary>
        internal static string Paso1DescripcionISP {
            get {
                return ResourceManager.GetString("Paso1DescripcionISP", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a &amp;Eacute; quem inclui um conjunto de ISPs para quem fatura..
        /// </summary>
        internal static string Paso1DescripcionPartner {
            get {
                return ResourceManager.GetString("Paso1DescripcionPartner", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a &amp;Eacite; quem fornece o servi&amp;ccedil;o ou a tecnologia oferecida ao usu&amp;aacute;rio final..
        /// </summary>
        internal static string Paso1DescripcionProveedor {
            get {
                return ResourceManager.GetString("Paso1DescripcionProveedor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a ISP.
        /// </summary>
        internal static string Paso1EntidadISP {
            get {
                return ResourceManager.GetString("Paso1EntidadISP", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Partner.
        /// </summary>
        internal static string Paso1EntidadPartner {
            get {
                return ResourceManager.GetString("Paso1EntidadPartner", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Provider.
        /// </summary>
        internal static string Paso1EntidadProveedor {
            get {
                return ResourceManager.GetString("Paso1EntidadProveedor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a SELECIONAR.
        /// </summary>
        internal static string Paso1Seleccionar {
            get {
                return ResourceManager.GetString("Paso1Seleccionar", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Selecione a entidade na qual voc&amp;ecirc; deseja obter o relat&amp;oacute;rio:.
        /// </summary>
        internal static string Paso1Texto1 {
            get {
                return ResourceManager.GetString("Paso1Texto1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a SEGUINTE.
        /// </summary>
        internal static string Paso2Siguiente {
            get {
                return ResourceManager.GetString("Paso2Siguiente", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Selecione cada .
        /// </summary>
        internal static string Paso2Texto1 {
            get {
                return ResourceManager.GetString("Paso2Texto1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a  que deseja que apare&amp;ccedil;a no relat&amp;oacute;rio:.
        /// </summary>
        internal static string Paso2Texto2 {
            get {
                return ResourceManager.GetString("Paso2Texto2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a M&amp;ecirc;s.
        /// </summary>
        internal static string Paso3FechaLiteralMonth {
            get {
                return ResourceManager.GetString("Paso3FechaLiteralMonth", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Selecionar.
        /// </summary>
        internal static string Paso3FechaLiteralMonthSeleccion {
            get {
                return ResourceManager.GetString("Paso3FechaLiteralMonthSeleccion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Ano.
        /// </summary>
        internal static string Paso3FechaLiteralYear {
            get {
                return ResourceManager.GetString("Paso3FechaLiteralYear", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Selecionar.
        /// </summary>
        internal static string Paso3FechaLiteralYearSeleccion {
            get {
                return ResourceManager.GetString("Paso3FechaLiteralYearSeleccion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a SEGUINTE.
        /// </summary>
        internal static string Paso3Siguiente {
            get {
                return ResourceManager.GetString("Paso3Siguiente", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Selecione o per&amp;iacute;odo de faturamento que deseja obter. Lembre-se que as informa&amp;ccedil;&amp;otilde;es apresentadas correspondem ao &amp;uacute;ltimo dia do m&amp;ecirc;s &amp;agrave;s 0,00 horas. Ou seja, n&amp;atilde;o ser&amp;atilde;o considerados pedidos novos ou cancelados no &amp;uacute;ltimo dia do m&amp;ecirc;s..
        /// </summary>
        internal static string Paso3Texto1 {
            get {
                return ResourceManager.GetString("Paso3Texto1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Gerando arquivo. Por favor espere. Este processo pode demorar v&amp;aacute;rios minutos....
        /// </summary>
        internal static string Paso4Cargando {
            get {
                return ResourceManager.GetString("Paso4Cargando", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a GERAR RELAT&amp;Oacute;RIO.
        /// </summary>
        internal static string Paso4Generar {
            get {
                return ResourceManager.GetString("Paso4Generar", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Nome do arquivo (sem extens&amp;atilde;o).
        /// </summary>
        internal static string Paso4LiteralNombreFichero {
            get {
                return ResourceManager.GetString("Paso4LiteralNombreFichero", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Selecione o nome do arquivo onde o resultado do relat&amp;oacute;rio ser&amp;aacute; salvo:.
        /// </summary>
        internal static string Paso4Texto1 {
            get {
                return ResourceManager.GetString("Paso4Texto1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a DOWNLOAD ARQUIVO.
        /// </summary>
        internal static string Paso5Descargar {
            get {
                return ResourceManager.GetString("Paso5Descargar", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a Pressione o bot&amp;atilde;o que aparece abaixo para baixar o arquivo gerado:.
        /// </summary>
        internal static string Paso5Texto1 {
            get {
                return ResourceManager.GetString("Paso5Texto1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a O nome do arquivo pode conter apenas letras sem acento, n&amp;uacute;meros e s&amp;iacute;mbolos : - _.
        /// </summary>
        internal static string RegexNombreFichero {
            get {
                return ResourceManager.GetString("RegexNombreFichero", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a ATEN&amp;Ccedil;&amp;Atilde;O.
        /// </summary>
        internal static string TituloAlerta {
            get {
                return ResourceManager.GetString("TituloAlerta", resourceCulture);
            }
        }
    }
}
