﻿using System;
using System.Globalization;
using LIVINGSAFE_SiteCallCenter.App_Code;

namespace LIVINGSAFE_SiteCallCenter
{
  public partial class autologin : System.Web.UI.Page
  {
    cUtil Utilidad = new cUtil();
    protected void Page_Load(object sender, EventArgs e)
    {
      //Enlazamos el logo a la página del site del que venga (Sacado de la URL)
      //hlLogo.NavigateUrl = String.Format("{0}{1}{2}{3}", Request.Url.Scheme, Uri.SchemeDelimiter, Request.Url.Authority, Request.Url.AbsolutePath);
      hlLogo.NavigateUrl = String.Format("{0}{1}{2}{3}", Request.Url.Scheme, Uri.SchemeDelimiter, Request.Url.Authority, "/");

      string TSCif = string.Empty;
      string USCif = string.Empty;
      string PWCif = string.Empty;
      string IICif = string.Empty;
      string TSDes = string.Empty;
      string USDes = string.Empty;
      string PWDes = string.Empty;
      string IIDes = string.Empty;
      int IIPar=-1;

      try
      {
        if (Request.QueryString["D0"] == null || Request.QueryString["D1"] == null || Request.QueryString["D2"] == null || Request.QueryString["D3"] == null || Request.QueryString["D4"] == null)
        {
          throw new Exception("You do not have permission to access this site. Parameters missing.");
        }
        else
        {
          TSCif = Request.QueryString["D0"].ToString();
          USCif = Request.QueryString["D1"].ToString();
          PWCif = Request.QueryString["D2"].ToString();
          IICif = Request.QueryString["D3"].ToString();
          IIPar = Convert.ToInt32(Request.QueryString["D4"].ToString());

          cEncriptacion AES = new cEncriptacion(Utilidad.DameClaveCifradoISP(IIPar));

          TSDes = AES.DescifrarAES(IIPar, TSCif);
          USDes = AES.DescifrarAES(IIPar, USCif);
          PWDes = AES.DescifrarAES(IIPar, PWCif);
          IIDes = AES.DescifrarAES(IIPar, IICif);

          if (Convert.ToInt32(IIDes) != IIPar)
            throw new Exception("You do not have permission to access this site. Bad ISP code.");

          DateTime Ahora = DateTime.Now.ToUniversalTime();
          DateTime TStamp = new DateTime(long.Parse(TSDes));

          if (Ahora > TStamp)
          {
            throw new Exception("You do not have permission to access this site. The link has expired.");
          }
          else
          {
            //Como el TS es válido. Le logamos y le redirigimos a la landing page.
            //Autenticar devuelve true o false en función de si hay que llamar a activar o desactivar captcha.
            cSesiones Sesion = new cSesiones();
            bool ActivarDesactivarCAPTCHA = Sesion.Autenticar(USDes,PWDes,false,"(autologin)");

          }
        }
      }

      catch (Exception Exc)
      {
        if (Utilidad.EsEntornoDesarrollo())
          lblMensaje.Text = Exc.ToString();
        else
          lblMensaje.Text = Exc.Message.ToString();
      }
    }
  }
}