﻿using System;

namespace APIREST
{
  public class CustomerResponse
  {

    private Boolean active = true;
    public string CustomerGuid { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Address { get; set; }
    public string CP { get; set; }
    public string email { get; set; }
    public string mobile { get; set; }
    public string CPF_CNPJ { get; set; }
    public string company { get; set; }
    public string result { get; set; }
    public string error { get; set; }
    public bool Active
    {
      get { return active; }
      set { active = value; }
    }
  }
}