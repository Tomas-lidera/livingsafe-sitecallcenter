﻿namespace APIREST
{
  public class OrderRequest
  {
    public string IspGuid { get; set; }
    public string PartnerGuid { get; set; }
    public string OperatorLogin { get; set; }
    public CustomerRequest customer { get; set; }
    public string SKU { get; set; }
    public int Quantity { get; set; }
    public double PVP { get; set; }
    public int IdRegistro { get; set; }
    public string MultiContrato { get; set; }
  }
}