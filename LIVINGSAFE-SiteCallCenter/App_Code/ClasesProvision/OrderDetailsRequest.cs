﻿namespace APIREST
{
  public class OrderDetailsRequest
  {
    public string IspGuid { get; set; }
    public string PartnerGuid { get; set; }
    public string orderGuid { get; set; }
    public string OperatorLogin { get; set; }
  }
}