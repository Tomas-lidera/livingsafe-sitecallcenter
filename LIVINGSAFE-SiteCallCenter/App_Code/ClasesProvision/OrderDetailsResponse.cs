﻿namespace APIREST
{
  public class OrderDetailsResponse
  {
    public string CustomerGuid { get; set; }
    public string OrderDetailsGuid { get; set; }
    public string OrderGuid { get; set; }
    public double Price { get; set; }
    public string Sku { get; set; }
    public string Product { get; set; }
    public int Quantity { get; set; }
    public string Date { get; set; }
    public string DateCancel { get; set; }
  }
}