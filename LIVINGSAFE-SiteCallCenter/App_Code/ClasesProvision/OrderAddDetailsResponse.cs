﻿namespace APIREST
{
  public class OrderAddDetailsResponse
  {
    public string OrderDetailsGuid { get; set; }
    public string result { get; set; }
    public string error { get; set; }
  }
}