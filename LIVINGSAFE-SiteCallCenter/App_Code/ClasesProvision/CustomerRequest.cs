﻿namespace APIREST
{
  public class CustomerRequest
  {
    public string IspGuid { get; set; }
    public string CustomerGuid { get; set; } //SOLO NECESARIO PARA IDENTIFAR CLIENTES YA EXISTENTES
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Address { get; set; }
    public string CP { get; set; }
    public string OperatorLogin { get; set; }
    public int IdCPostal { get; set; } //AUNQUE ESTÉ DEFINIDO EN ESTA CLASE, EN PRINCIPIO SÓLO VAMOS A UTILIZARLO INTERNAMENTE
    public string email { get; set; }
    public string mobile { get; set; }
    public string CPF_CNPJ { get; set; }
    public string company { get; set; }
    public int IdRegistro { get; set; }
  }
}