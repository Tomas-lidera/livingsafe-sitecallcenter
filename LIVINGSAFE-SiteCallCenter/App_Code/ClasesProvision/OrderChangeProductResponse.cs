﻿namespace APIREST
{
  public class OrderChangeProductResponse
  {
    public string OrderDetailsGuid { get; set; }
    public string result { get; set; }
    public string error { get; set; }
  }
}