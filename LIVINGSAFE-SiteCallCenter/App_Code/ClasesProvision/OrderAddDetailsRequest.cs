﻿namespace APIREST
{
  public class OrderAddDetailsRequest
  {
    public string IspGuid { get; set; }
    public CustomerRequest customer { get; set; }
    public string orderGuid { get; set; }
    public string SKU { get; set; }
    public int Quantity { get; set; }
    public double PVP { get; set; }
    public string OperatorLogin { get; set; }
  }
}