﻿namespace APIREST
{
  public class OrderDetailsUpdateResponse
  {
    public string OrderDetailsGuid { get; set; }
    public string result { get; set; }
    public string error { get; set; }
  }
}