﻿using System.Collections.Generic;

namespace APIREST
{
  public class ListOrderDetailsResponse
  {
    public List<OrderDetailsResponse> orderDetails { get; set; }
    public string result { get; set; }
    public string error { get; set; }
  }
}