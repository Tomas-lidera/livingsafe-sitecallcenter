﻿namespace APIREST
{
  public class Order
  {
    public int idUsuarioFinal { get; set; }
    public int idIsp { get; set; }
    public int idPartner { get; set; }
    public int idPedidoDetalle { get; set; }
    public int idProvison { get; set; }
    public string pass { get; set; }
    public string requestType { get; set; }
    public string ItemsAction { get; set; }
    public string tipoPlantilla { get; set; }
    public int cantidad { get; set; }
    public int IdRegistro { get; set; }
    public int cantidadAux { get; set; }
    public string skuAux { get; set; }
  }
}