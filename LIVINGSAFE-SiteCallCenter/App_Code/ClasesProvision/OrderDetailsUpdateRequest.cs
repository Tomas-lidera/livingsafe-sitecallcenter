﻿namespace APIREST
{
  public class OrderDetailsUpdateRequest
  {
    public string IspGuid { get; set; }
    public string PartnerGuid { get; set; }
    public string OrderDetailsGuid { get; set; }
    public string OperatorLogin { get; set; }
    public int Quantity { get; set; }
    public double PVP { get; set; }
  }
}