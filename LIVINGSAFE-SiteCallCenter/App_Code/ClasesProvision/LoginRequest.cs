﻿namespace APIREST
{
  public class LoginRequest
  {
    public string UsernameAPI { get; set; }
    public string PasswordAPI { get; set; }
    public string OperatorLogin { get; set; }
  }
}