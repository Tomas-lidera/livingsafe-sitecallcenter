﻿namespace APIREST
{
  public class OrderResponse
  {
    public CustomerResponse customer { get; set; }
    public string orderGuid { get; set; }
    public string result { get; set; }
    public string error { get; set; }
  }
}