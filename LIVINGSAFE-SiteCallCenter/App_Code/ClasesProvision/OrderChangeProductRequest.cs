﻿namespace APIREST
{
  public class OrderChangeProductRequest
  {
    public string IspGuid { get; set; }
    public string PartnerGuid { get; set; }
    public string OrderDetailsGuid { get; set; }
    public string NewSku { get; set; }
    public string OperatorLogin { get; set; }
    public double PVP { get; set; }
  }
}