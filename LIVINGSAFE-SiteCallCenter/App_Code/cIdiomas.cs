﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading;
using System.Web;

namespace LIVINGSAFE_SiteCallCenter.App_Code
{
  public class cIdiomas
  {
    cUtil Utilidad = new cUtil();

    public cIdiomas()
    {

    }

    public string DevolverCulturaDetectada()
    {
      string URL = HttpContext.Current.Request.Url.Host;
      int IdISPDetectado = Utilidad.DameIdISPPorDominio(URL);

      ArrayList Lista = Utilidad.DameIdiomasISP(IdISPDetectado);

      string CulturaActual = string.Empty;

      //Analizamos el idioma para mostrar que tiene especificado el sistema. Si no tuviera ninguno, cogemos el idioma del sistema.
      if(HttpContext.Current.Request.UserLanguages!=null && HttpContext.Current.Request.UserLanguages.Length>0)
        CulturaActual = HttpContext.Current.Request.UserLanguages[0];
      else
        CulturaActual = Thread.CurrentThread.CurrentCulture.Name;

      string CulturaISP = Utilidad.DameCulturaPrincipal(Lista);
      string CulturaSistema = string.Empty;

      CulturaSistema = (CulturaActual == string.Empty) ? CulturaISP : CulturaActual;

      return CulturaSistema;
    }

    public string DameIdioma()
    {
      string Idioma = "";

      if (HttpContext.Current.Request.Cookies["Idioma"] == null)
        CargarIdiomaPorDefecto();

      Idioma = HttpContext.Current.Request.Cookies["Idioma"].Value.ToString();

      return Idioma;
    }

    public int DameIdIdioma(string Cultura)
    {
      int IdIdioma = 0;

      ArrayList Linea = DameIdiomaPorCultura(Cultura);

      IdIdioma = Convert.ToInt32(Utilidad.DameDato((ArrayList)Linea[0], "IdIdioma"));

      return IdIdioma;
    }

    public void CargarIdiomaPorDefecto()
    {
      string Idioma = string.Empty;
      Idioma = DevolverCulturaDetectada();

      Thread.CurrentThread.CurrentCulture   = CultureInfo.CreateSpecificCulture(Idioma);
      Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(Idioma);
      GrabarCookieIdioma(Thread.CurrentThread.CurrentCulture.ToString());
    }

    public void GrabarCookieIdioma(string Idioma)
    {
      HttpCookie CookieIdioma = new HttpCookie("Idioma");
      CookieIdioma.Value = Idioma;
      HttpContext.Current.Response.Cookies.Add(CookieIdioma);
    }

    public void InicializarCultura()
    {
      Thread.CurrentThread.CurrentCulture   = CultureInfo.GetCultureInfo(DameIdioma());
      Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(DameIdioma());
    }

    protected ArrayList DameIdiomaPorCultura(string Cultura)
    {
      ArrayList Lista = new ArrayList();
      ArrayList Registro = new ArrayList();
      ArrayList Dupla = new ArrayList();

      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = Con.CreateCommand();

      try
      {
        Com.CommandType = System.Data.CommandType.StoredProcedure;
        Com.CommandText = "Idiomas_DameIdIdiomaPorCultura";
        Com.CommandTimeout = 300;
        Com.Parameters.Add("@CultureCode", SqlDbType.VarChar,50).Value = Cultura;
        Con.Open();

        SqlDataReader Rst = Com.ExecuteReader();

        while (Rst.Read())
        {
          Registro = new ArrayList();

          for (int i = 0; i < Rst.FieldCount; i++)
          {
            Dupla = new ArrayList();

            Dupla.Add(Rst.GetName(i));
            Dupla.Add(Rst.GetValue(i));

            if (Dupla.Count > 0)
              Registro.Add(Dupla);
          }

          if (Registro.Count > 0)
            Lista.Add(Registro);
        }

        Rst.Close();
        Rst.Dispose();
      }

      finally
      {
        if (Con.State == ConnectionState.Open)
          Con.Close();

        Com.Dispose();
        Con.Dispose();
      }

      return Lista;
    }
  }
}