﻿using System;
using System.Configuration;
using System.IO;
using System.Web;

namespace LIVINGSAFE_SiteCallCenter.App_Code
{
  class cLog
  {
    private static StreamWriter mswSw = null;
    private static int Instancias;

    public cLog()
    {
      lock (this)
      {
        if (Instancias == 0)
          AbrirFichero(null);

        Instancias++;
      }
    }

    public cLog(string Nombre)
    {
      lock (this)
      {
        if (Instancias == 0)
          AbrirFichero(Nombre);

        Instancias++;
      }
    }

    protected void AbrirFichero(string Nombre)
    {
      string NombreFichero = GetNombreFichero(Nombre);

      if (!(new FileInfo(NombreFichero).Exists))
      {
        if (mswSw != null)
        {
          mswSw.Close();
          mswSw.Dispose();
        }

        mswSw = new StreamWriter(NombreFichero, true);
      }
      else
      {
        if (mswSw == null)
        {
          mswSw = new StreamWriter(NombreFichero, true);
        }
      }
    }

    protected string GetNombreFichero(string Nombre)
    {
      DateTime Fecha = DateTime.Now;
      string Fichero = String.Empty;
     

      if (Nombre == null || Nombre == string.Empty)
      {
       Fichero = System.IO.Path.GetDirectoryName(HttpContext.Current.Server.MapPath("/"));
       Fichero += "\\LOGS";
        if (!Directory.Exists(Fichero)) Directory.CreateDirectory(Fichero);

        Fichero += "\\LOGERROR_";
        Fichero += Fecha.Year.ToString();
        Fichero += Fecha.Month.ToString().PadLeft(2, '0');
        Fichero += Fecha.Day.ToString().PadLeft(2, '0');
        Fichero += ".LOG";
      }
      else
      {
              
        Fichero = ConfigurationManager.AppSettings["RutaFicherosErrores"].ToString();
        if (!Directory.Exists(Fichero+"\\TEMP")) Directory.CreateDirectory(Fichero + "\\TEMP");

        Fichero += Nombre;
      }

      return Fichero;
    }

    public void AddErrorEntry(string Mensaje)
    {
      Escribir("[   ERROR   ]", Mensaje);
    }

    public void AddWarningEntry(string Mensaje)
    {
      Escribir("[  WARNING  ]", Mensaje);
    }

    public void AddInformationEntry(string Mensaje)
    {
      Escribir("[INFORMATION]", Mensaje);
    }

    public void AddLiteralEntry(string Mensaje,string Nombre)
    {
      //EscribirFichero(Nombre,Mensaje);

      Nombre = ConfigurationManager.AppSettings["RutaFicherosErrores"].ToString() + Nombre;

      if (!(new FileInfo(Nombre).Exists))
      {
        using (StreamWriter SWFichero = new StreamWriter(Nombre, true))
        {
          SWFichero.WriteLine(Mensaje);
        }
      }
    }

    protected void Escribir(string Tipo, string Mensaje)
    {
      lock (this)
      {
        AbrirFichero(null);
        mswSw.WriteLine(DateTime.Now.ToString("HH:mm:ss") + "##" + Tipo + "##" + Mensaje);
        mswSw.Flush();
      }
    }

    //protected void EscribirFichero(string Nombre,string Mensaje)
    //{
    //  lock (this)
    //  {
    //    AbrirFichero(Nombre);
    //    mswSw.WriteLine(Mensaje);
    //    mswSw.Flush();
    //    Dispose();
    //  }
    //}

    public void Dispose()
    {
      lock (this)
      {
        if (--Instancias == 0)
        {
          mswSw.Close();
          mswSw.Dispose();
        }
      }
    }
  }
}
