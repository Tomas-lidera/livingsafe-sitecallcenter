﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace LIVINGSAFE_SiteCallCenter.App_Code
{
  class cEncriptacion
  {
    private byte[] pClave;
    cUtil Utilidad = new cUtil();

    #region Propiedades
    public string Clave
    {
      set
      {
        this.pClave = Encoding.ASCII.GetBytes(value);
      }
    }

    #endregion

    #region Constructores

    /// <summary>
    /// Constructor vacío de la clase cEncriptacion.
    /// </summary>
    public cEncriptacion()
    {

    }

    /// <summary>
    /// Constructor de la clase cEncriptación que inicializa la clave de encriptación.
    /// </summary>
    /// <param name="clave"></param>
    public cEncriptacion(string clave)
    {
      pClave = System.Text.ASCIIEncoding.ASCII.GetBytes(clave);
    }

    #endregion

    #region Métodos

    /// <summary>
    /// Método que realiza un cifrado AES usando un secreto compartido y una clave.
    /// </summary>
    /// <param name="Cadena">(string) Cadena a cifrar.</param>
    /// <param name="SecretoCompartido">(string) Secreto compartido para el cifrado.</param>
    /// <returns>(string) Cadena cifrada con el método AES .</returns>
    public string CifrarAES(string Cadena, string SecretoCompartido)
    {
      if (Cadena == null || Cadena == String.Empty)
        throw new Exception("A string  a ser criptografada não é válida.");

      if (SecretoCompartido == null || SecretoCompartido == String.Empty)
        throw new Exception("O segredo compartilhado não é válido.");

      string CadenaEncriptada = null;
      RijndaelManaged pRijndael = null;

      try
      {
        // Generamos la clave a usar con la Clave general y el secreto compartido
        Rfc2898DeriveBytes key = new Rfc2898DeriveBytes(SecretoCompartido, pClave);

        pRijndael = new RijndaelManaged();
        pRijndael.Key = key.GetBytes(pRijndael.KeySize / 8);

        // El Encriptador se usa para transformar el stream
        ICryptoTransform Encriptador = pRijndael.CreateEncryptor(pRijndael.Key, pRijndael.IV);

        using (MemoryStream msEncriptador = new MemoryStream())
        {
          msEncriptador.Write(BitConverter.GetBytes(pRijndael.IV.Length), 0, sizeof(int));
          msEncriptador.Write(pRijndael.IV, 0, pRijndael.IV.Length);
          using (CryptoStream csEncriptador = new CryptoStream(msEncriptador, Encriptador, CryptoStreamMode.Write))
          {
            using (StreamWriter swEncriptador = new StreamWriter(csEncriptador))
            {
              swEncriptador.Write(Cadena);
            }
          }
          CadenaEncriptada = Convert.ToBase64String(msEncriptador.ToArray());
        }
      }

      finally
      {
        // Pase lo que pase limpiamos el objeto RijndaelManaged.
        if (pRijndael != null)
          pRijndael.Clear();
      }

      return CadenaEncriptada;
    }

    /// <summary>
    /// Método que realiza un descifrado AES usando un secreto compartido y una clave.
    /// </summary>
    /// <param name="CadenaCifrada">(string) Cadena a descifrar.</param>
    /// <param name="SecretoCompartido">(string) Secreto compartido para el cifrado.</param>
    /// <returns>(string) Cadena descifrada con el método AES.</returns>
    public string DescifrarAES(string CadenaCifrada, string SecretoCompartido)
    {
      if (CadenaCifrada == null || CadenaCifrada == String.Empty)
        throw new Exception("A string a ser criptografada não é válida.");

      if (SecretoCompartido == null || SecretoCompartido == String.Empty)
        throw new Exception("O segredo compartilhado não é válido.");

      RijndaelManaged pAES = null;

      string Cadena = null;

      try
      {
        // Generamos la clave a usar con la Clave general y el secreto compartido
        Rfc2898DeriveBytes key = new Rfc2898DeriveBytes(SecretoCompartido, pClave);

        // Create the streams used for decryption.                
        byte[] bytes = Convert.FromBase64String(CadenaCifrada);
        using (MemoryStream msDecrypt = new MemoryStream(bytes))
        {
          // Inicializamos el objeto RijndaelManaged con los datos especificados
          pAES = new RijndaelManaged();
          pAES.Key = key.GetBytes(pAES.KeySize / 8);
          //El vector de inicialización se obtiene del stream encriptado
          pAES.IV = LeerByteArray(msDecrypt);

          // El Desencriptador se usa para transformar el stream
          ICryptoTransform decryptor = pAES.CreateDecryptor(pAES.Key, pAES.IV);
          using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
          {
            using (StreamReader srDecrypt = new StreamReader(csDecrypt))
              Cadena = srDecrypt.ReadToEnd();
          }
        }
      }
      finally
      {
        // Pase lo que pase limpiamos el objeto RijndaelManaged.
        if (pAES != null)
          pAES.Clear();
      }

      return Cadena;
    }

    /// <summary>
    /// Método que genera un secreto compartido a partir de una cadena. Siempre tiene longitud 44.
    /// </summary>
    /// <param name="cadena">(string) Cadena fuente para la generación del secreto compartido.</param>
    /// <returns>(string) Secreto compatido generado.</returns>
    public string CrearSecretoCompartido(string cadena)
    {
      string SC;
      var SCBytes = new byte[32];

      using (var proveedor = new RNGCryptoServiceProvider())
        proveedor.GetNonZeroBytes(SCBytes);
      SC = Convert.ToBase64String(SCBytes);

      return SC;
    }

    /// <summary>
    /// Método que convierte un Stream a Bytes.
    /// </summary>
    /// <param name="s">(Stream) Stream a convertir.</param>
    /// <returns>(byte[]) Array de bytes convertidos.</returns>
    private byte[] LeerByteArray(Stream s)
    {
      byte[] Longitud = new byte[sizeof(int)];
      if (s.Read(Longitud, 0, Longitud.Length) != Longitud.Length)
        throw new Exception("A string  a ser descriptografada não é válida.");

      byte[] Buffer = new byte[BitConverter.ToInt32(Longitud, 0)];
      if (s.Read(Buffer, 0, Buffer.Length) != Buffer.Length)
        throw new SystemException("O fluxo não pôde ser carregado corretamente");

      return Buffer;
    }


    /// <summary>
    /// Se le pasa el IdISP para obtener su clave de cifrado de la BD y la cadena a cifrar y la devuelve cifrada.
    /// </summary>
    /// <param name="IdISP">(int) Identificador del ISP para coger su clave de cifrado de la BD</param>
    /// <param name="CadenaCifrar">(string) Cadena que se desea cifrar</param>
    /// <returns>(string) Cadena cifrada</returns>
    public string CifrarAES(int IdISP, string CadenaCifrar)
    {


      string ClaveCifradoPartner = Utilidad.DameClaveCifradoISP(IdISP);
      cEncriptacion AES = new cEncriptacion(ClaveCifradoPartner);

      string secretoCompartido = AES.CrearSecretoCompartido(CadenaCifrar);
      string clavecifrada = AES.CifrarAES(CadenaCifrar, secretoCompartido);
      string CadenaCifrada = secretoCompartido + clavecifrada;

      return CadenaCifrada;
    }

    /// <summary>
    /// Se le pasa el IdISP para obtener su clave de cifrado de la BD y la cadena a descifrar y la devuelve descifrada.
    /// </summary>
    /// <param name="IdISP">(int) Identificador del ISP para coger su clave de cifrado de la BD</param>
    /// <param name="CadenaDescifrar">(string) Cadena que se desea descifrar</param>
    /// <returns>(string) Cadena descifrada</returns>
    public string DescifrarAES(int IdISP, string CadenaDescifrar)
    {
      string ClaveCifradoPartner = Utilidad.DameClaveCifradoISP(IdISP);
      cEncriptacion AES = new cEncriptacion(ClaveCifradoPartner);

      string SecretoCompartido = CadenaDescifrar.Substring(0, 44);
      string clavedescifrada = CadenaDescifrar.Substring(SecretoCompartido.Length, CadenaDescifrar.Length - SecretoCompartido.Length);
      string CadenaDescifrada = AES.DescifrarAES(clavedescifrada.Replace("\0", ""), SecretoCompartido);

      return CadenaDescifrada;
    }

    #endregion
  }
}
