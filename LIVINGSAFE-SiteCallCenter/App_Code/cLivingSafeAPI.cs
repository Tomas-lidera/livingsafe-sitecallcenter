﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Text;

namespace LIVINGSAFE_SiteCallCenter.App_Code
{
  public class cLivingSafeAPI
  {
    public string RealizarSolicitudGET(string ruta, string token)
    {
      string ResultadoCadena = String.Empty;
      HttpWebResponse res = null;
      StreamReader SReader = null;

      try
      {
        HttpWebRequest WReq = (HttpWebRequest)HttpWebRequest.Create(ruta);
        WReq.ContentType = "application/json; charset=UTF-8";
        WReq.Method = "GET";

        if (token != String.Empty)
        {
          WReq.Headers["Authorization"] = "Bearer " + token;
        }

        WReq.Accept = "application/json";
        res = (HttpWebResponse)WReq.GetResponse();
        Encoding Codificacion = ASCIIEncoding.UTF8;
        SReader = new StreamReader(res.GetResponseStream(), Codificacion);
        ResultadoCadena = SReader.ReadToEnd();
      }

      catch (WebException Exc)
      {
        var webResponse = (HttpWebResponse)Exc.Response;
        StreamReader Lector = new StreamReader(webResponse.GetResponseStream());
        ResultadoCadena = Lector.ReadToEnd();
      }

      finally
      {
        if (SReader != null)
          SReader.Dispose();
        if (res != null)
          res.Close();
      }
      return ResultadoCadena;
    }

    public string RealizarSolicitudPOST(string ruta, byte[] Datos, string token)
    {
      string ResultadoCadena = String.Empty;

      if (ruta == String.Empty)
        throw new Exception("Ruta no válida.");

      if (Datos == null && token == String.Empty)
        throw new Exception("Datos no válidos.");

      //Variable creada sólo para depuración. Poner punto de ruptura. Aquí puedes ver el mensaje que se envía. (BORRABLE)
      //string EnvioCadena = System.Text.Encoding.UTF8.GetString(Datos);

      //Esto es para evitar error de certificado en la comunicación al ser una llamada a HTTPS.
      //ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(AcceptAllCertifications);
      //ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
      HttpWebRequest WReq = (HttpWebRequest)HttpWebRequest.Create(ruta);
      if (token != String.Empty)
        WReq.Headers["Authorization"] = "Bearer " + token;

      WReq.ContentType = "application/json; charset=UTF-8";

      WReq.ContentLength = Datos.Length;

      WReq.Method = "POST";
      WReq.GetRequestStream().Write(Datos, 0, Datos.Length);

      HttpWebResponse res = (HttpWebResponse)WReq.GetResponse();
      Encoding Codificacion = ASCIIEncoding.UTF8;

      StreamReader SReader = new StreamReader(res.GetResponseStream(), Codificacion);
      ResultadoCadena = SReader.ReadToEnd();

      return ResultadoCadena;
    }
  }
}
