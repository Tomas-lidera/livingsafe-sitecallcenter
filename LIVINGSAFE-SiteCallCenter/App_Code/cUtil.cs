﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.UI.WebControls;

namespace LIVINGSAFE_SiteCallCenter.App_Code
{
  public class cUtil
  {
    /// <summary>
    /// Método que nos devuelve la pass cifrada
    /// </summary>
    /// <param name="IdISP">int Identificador del partenr</param>
    /// <param name="password">string pass en texto plano</param>
    /// <returns>Devolvemos un string con las password cifrada</returns>
    public string CifrarPass(int IdISP, string password)
    {
      string passCifrada = string.Empty;

      string claveCifradoPartner = DameClaveCifradoISP(IdISP);
      cEncriptacion AES = new cEncriptacion(claveCifradoPartner);
      string secretoCompartido = AES.CrearSecretoCompartido(password);
      string cifrado = AES.CifrarAES(password, secretoCompartido);
      passCifrada = secretoCompartido + cifrado;
      return passCifrada;
    }

    /// <summary>
    /// Método que nos devuelve la clave de cifrado de cada ISP
    /// </summary>
    /// <param name="parametro">(int) Identificador del ISP</param>
    /// <returns>(string) Valor encontrado en la tabla</returns>
    public string DameClaveCifradoISP(int IdISP)
    {
      string valor = string.Empty;
      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = Con.CreateCommand();
      Com.CommandType = System.Data.CommandType.StoredProcedure;
      Com.CommandText = "ISP_DameDatos";
      Com.CommandTimeout = 300;
      Com.Parameters.AddWithValue("@IdISP", IdISP);

      Con.Open();
      SqlDataReader Rst = Com.ExecuteReader();

      if (Rst.Read())
        valor = Rst["ClaveAES"].ToString();

      Com.Dispose();
      Con.Close();
      Con.Dispose();

      return valor;
    }

    /// <summary>
    /// Método que nos devuelve el valor de un parametro especificado en la tabla CONFIGURACION
    /// </summary>
    /// <param name="parametro">String valor a buscar en la tabla</param>
    /// <returns>String valor encontrado en la tabla</returns>
    public string DameValorConfiguracion(string Parametro)
    {
      string valor = string.Empty;

      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = Con.CreateCommand();

      try
      {
        Com.CommandType = System.Data.CommandType.StoredProcedure;
        Com.CommandText = "Configuracion_DameDatos";
        Com.CommandTimeout = 300;
        Com.Parameters.Add("@Parametro", SqlDbType.VarChar, 250).Value = Parametro;

        Con.Open();

        SqlDataReader Rst = Com.ExecuteReader();

        if (Rst.Read())
          valor = Rst["Valor"].ToString();
      }

      finally
      {
        Com.Dispose();

        if (Con.State == ConnectionState.Open)
          Con.Close();

        Con.Dispose();
      }

      return valor;
    }

    /// <summary>
    /// Se le pasa la URL raíz de un site y el nombre del dominio principal (sin el dominio de primer nivel) y devuelve los subdominios de dicha URL.
    /// P. ej. Si se le pasa http://www.pruebas.test.staging.livingsafe.com.br, devolverá "pruebas.test.staging".
    /// </summary>
    /// <param name="URL">(string) URL raíz con o sin http y con o sin www. P. ej. "http://www.pruebas.test.staging.livingsafe.com.br" </param>
    /// <param name="Dominio">(string) Nombre del dominio. P. ej. En "http://www.pruebas.test.staging.livingsafe.com.br" habría que indicar "livingsafe"</param>
    /// <returns>(string) Devuelve el subdominio. P. ej. En "http://www.pruebas.test.staging.livingsafe.com.br" devuelve "pruebas.test.satging"</returns>
    public string DameSubdominio(string URL, string dominioprincipal)
    {
      string Dominio = DameDominio(URL, dominioprincipal);
      string Subdominio = URL.Replace("https://", "").Replace("http://", "").Replace("www.", "");
      Subdominio = Subdominio.Replace(Dominio, "");
      Subdominio = Subdominio.Replace(DameValorConfiguracion("PrefijoCallCenter"), "");
      //Subdominio = Subdominio.Replace("local", "");
      //Subdominio = Subdominio.Replace("test", "");
      Subdominio = Subdominio.TrimEnd(new[] { '.' }).TrimStart(new[] { '.' });

      return Subdominio;
    }

    /// <summary>
    /// Se le pasa una URL y devuelve el dominio principal y los de primer nivel. P. ej. si se le pasa "http://www.pruebas.test.staging.livingsafe.com.br" devolverá "livingsafe.com.br".
    /// </summary>
    /// <param name="URL">(string) URL de la raíz del site. P. ej. "http://www.pruebas.test.staging.livingsafe.com.br".</param>
    /// <returns>(string) Devuelve el dominio principal y los de primer nivel P. ej. "livingsafe.com.br"</returns>
    public string DameDominio(string URL, string dominioprincipal)
    {
      return Regex.Replace(URL, ".*" + dominioprincipal, dominioprincipal, RegexOptions.IgnoreCase);
    }

    /// <summary>
    /// Método que genera aleatoriamente una password con caracteres alfanuméricos de una longitud dada.
    /// </summary>
    /// <param name="longitud">(int) Longitud que se desea que tenga la password</param>
    /// <returns>(String) Password generada</returns>
    public string GenerarPassword(int longitud)
    {
      const string chars = "abcdefghijklmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ0123456789#!$-_@";
      var random = new Random();
      return new string(Enumerable.Repeat(chars, longitud)
        .Select(s => s[random.Next(s.Length)]).ToArray());
    }

    /// <summary>
    /// Identifica el tipo de usuario por el patrón del login. Si tiene forma de CPF o CNPJ es un UF. Si no, es un operador.
    /// </summary>
    /// <param name="Identificador">(string) Login del usuario</param>
    /// <returns>(string) Tipo de usuario: UsuarioFinal,Operador</returns>
    public string DameTipoDeEntidad(string Identificador)
    {
      string Tipo = string.Empty;

      //Expresión regular para CPF  y CNPJ
      string Expresion = "(^\\d{3}\\.\\d{3}\\.\\d{3}\\-\\d{2}$)|(^\\d{2}\\.\\d{3}\\.\\d{3}\\/\\d{4}\\-\\d{2}$)";

      Regex rgx = new Regex(@Expresion);
      if (rgx.IsMatch(Identificador))
        Tipo = "UsuarioFinal";
      else
        Tipo = "Operador";


      return Tipo;
    }

    /// <summary>
    /// Indica si la página pertenece a uno de los sitios de desarrollo (local / test / localhost), en cuyo caso pueden mostrarse
    /// mensajes descriptivos de error.
    /// </summary>
    /// <returns>(bool) Indica si se pueden mostrar errores descriptivos o no.</returns>
    public bool EsEntornoDesarrollo()
    {
      bool Mostrar = false;

      string URL = HttpContext.Current.Request.Url.Host;

      string DominioPrincipal = DameValorConfiguracion("DominioPrincipal");

      if (URL.ToLower().Contains("localhost") ||
          DameSubdominio(URL, DominioPrincipal).ToLower().Contains("local") ||
          DameSubdominio(URL, DominioPrincipal).ToLower().Contains("test"))
        Mostrar = true;

      return Mostrar;
    }

    /// <summary>
    /// Se le pasa un Id de un usuario final y el nombre de una columna y devuelve el dato en formato string. Si en el parámetro MostrarTodos
    /// no se le pasa nada, devuelve sólo si está activo (este comportamiento era el que tenía por defecto antes de incorporar este tercer parámetro).
    /// Si en el parámetro MostrarTodos se le pasa algo (true o false) lo devuelve tanto si está activo como si no.
    /// </summary>
    /// <param name="Identificador">(int) Identificador del usuario final que se busca</param>
    /// <param name="Columna">(string) Nombre de la columna a devolver</param>
    /// <returns>(string) Valor del dato de la columna especificada</returns>
    public string DameDatoUsuarioFinal(int Identificador, string Columna, bool? MostrarTodos)
    {
      ArrayList Lista = DameUsuarioFinal(Identificador,MostrarTodos);

      string Dato = string.Empty;

      if (Lista.Count>0)
        Dato = DameDato((ArrayList)Lista[0], Columna);

      return Dato;
    }

		/// <summary>
		/// Se le pasa un Id de un operador y el nombre de una columna y devuelve el dato en formato string.
		/// </summary>
		/// <param name="IdOperador">(int) Identificador del operador que se busca</param>
		/// <param name="Campo">(string) Nombre de la columna a devolver</param>
		/// <returns>(string) Valor del dato de la columna especificada</returns>
		public string DameDatoOperador(int IdOperador, string Campo)
    {
      ArrayList Lista = DameOperador(IdOperador);

			string Nombre = string.Empty;
			if(Lista.Count>0)
				Nombre = DameDato((ArrayList)Lista[0], Campo);

      return Nombre;
    }

    /// <summary>
    /// Se le pasa un Id de un ISP y el nombre de una columna y devuelve el dato en formato string.
    /// </summary>
    /// <param name="Identificador">(int) Identificador del usuario final que se busca</param>
    /// <param name="Columna">(string) Nombre de la columna a devolver</param>
    /// <returns>(string) Valor del dato de la columna especificada</returns>
    public string DameDatoISP(int Identificador, string Columna)
    {
      ArrayList Lista = DameISP(Identificador,false);

      string Dato = DameDato((ArrayList)Lista[0], Columna);

      return Dato;
    }

    public string DameDatoISP(int Identificador, string Columna,bool IncluirInactivos)
    {
      ArrayList Lista = DameISP(Identificador,IncluirInactivos);

      string Dato = DameDato((ArrayList)Lista[0], Columna);

      return Dato;
    }

    /// <summary>
    /// Se le pasa un Id de un pedido detalle y el nombre de una columna y devuelve el dato en formato string.
    /// </summary>
    /// <param name="Identificador">(int) Identificador del pedido detalle que se busca</param>
    /// <param name="Columna">(string) Nombre de la columna a devolver</param>
    /// <returns>(string) Valor del dato de la columna especificada</returns>
    public string DameDatoPedido(int Identificador, string Columna)
    {
      ArrayList Lista = DamePedidoPorDetalle(Identificador);

      string Dato = DameDato((ArrayList)Lista[0], Columna);

      return Dato;
    }

    /// <summary>
    /// Guarda en el registro LOG de BD la operación realizada por cierta entidad. Guarda el identificador de la entidad, el tipo (Operador, UsuarioFinal, etc.)
    /// y la acción además de la fecha y hora.
    /// </summary>
    /// <param name="Identificador">(int) Identificador de la entidad (IdOperador, IdUsuarioFinal, etc.)</param>
    /// <param name="Tipo">(string) Tipo de entidad (Operador, UsuarioFinal, etc.)</param>
    /// <param name="Accion">(string) Acción realizada. Texto libre.</param>
    public void EscribeLOG(int Identificador, string Tipo, string Accion)
    {
      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = Con.CreateCommand();

      try
      {
        Com.CommandType = System.Data.CommandType.StoredProcedure;
        Com.CommandText = "LOG_Insertar";
        Com.CommandTimeout = 300;
        Com.Parameters.Add("@Identificador", SqlDbType.Int).Value = Identificador;
        Com.Parameters.Add("@Tipo", SqlDbType.VarChar, 250).Value = Tipo;
        Com.Parameters.Add("@Accion", SqlDbType.VarChar, -1).Value = Accion;

        Con.Open();

        Com.ExecuteNonQuery();
      }

      finally
      {
        if (Con.State == ConnectionState.Open)
          Con.Close();

        Com.Dispose();
        Con.Dispose();
      }
    }

    /// <summary>
    /// Inserta cabeceras para aumentar la seguridad del site.
    /// </summary>
    public void InsertarCabeceras()
    {
      //Control de cache: Las siguientes tres cabeceras es para el control de cache del site en distintas versiones
      HttpContext.Current.Response.AppendHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
      HttpContext.Current.Response.AppendHeader("Pragma", "no-cache"); // HTTP 1.0.
      HttpContext.Current.Response.AppendHeader("Expires", "0"); // Proxies.
    }

    /// <summary>
    /// Método que recoge el subdominio de la URL y localiza el ISP.
    /// </summary>
    /// <param name="URL">(string) URL a analizar</param>
    /// <returns>(int) IdISP del ISP localizado. Si no encuentra ninguno devolverá 0.</returns>
    public int DameIdISPPorDominio(string URL)
    {
      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      string valor = string.Empty;
      string DominioPrincipal = DameValorConfiguracion("DominioPrincipal");
      string Subdominio = DameSubdominio(URL, DominioPrincipal).ToLower();
      int IdISP = 0;

      try
      {
        if (Subdominio != "" && !(Regex.IsMatch(Subdominio, "[A-Za-z0-9](?:[A-Za-z0-9\\-]{0,61}[A-Za-z0-9])?")))
          throw new Exception("Subdomain not valid");

        SqlCommand Com = Con.CreateCommand();
        Com.CommandType = System.Data.CommandType.StoredProcedure;
        Com.CommandText = "ISP_DameIdISPPorDominio";
        Com.CommandTimeout = 300;
        Com.Parameters.Add("@subdominio", SqlDbType.VarChar, 250).Value = Subdominio;
        Con.Open();

        SqlDataReader Rst = Com.ExecuteReader();

        if (Rst.Read())
          IdISP = Convert.ToInt32(Rst["IdISP"]);

        Com.Dispose();
      }

      finally
      {
        if (Con.State == ConnectionState.Open)
          Con.Close();

        Con.Dispose();
      }

      return IdISP;
    }

    /// <summary>
    /// Carga la hoja de estilos y otros datos de ISP en base al subdominio detectado.
    /// </summary>
    /// <param name="Subdominio">(string) Subdominio del site que la aplicación utilizará para detectar qué ISP es</param>
    /// <returns>(string) CSS URL</returns>
    public string CargarISP(string Subdominio, ref int IdISP)
    {
       SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = Con.CreateCommand();

      string RutaCSS = string.Empty;

      try
      {
        Com.CommandType = System.Data.CommandType.StoredProcedure;
        Com.CommandText = "ISP_DameDatos";
        Com.CommandTimeout = 300;
        Com.Parameters.Add("@Subdominio", SqlDbType.VarChar, 250).Value = Subdominio;
        Con.Open();

        SqlDataReader Rst = Com.ExecuteReader();

        if (Rst.Read())
        {
          IdISP = Convert.ToInt32(Rst["IdISP"]);

          if (Rst["HojaCSS"] != null && Rst["HojaCSS"].ToString() != string.Empty)
            RutaCSS = "~/style/custom/" + Rst["HojaCSS"].ToString().ToLower();
          else
            RutaCSS = "~/style/custom/personalstyle.css";
        }
        else
        {
          IdISP = -1;
          RutaCSS = "~/style/custom/personalstyle.css";
        }
      }

      finally
      {
        if (Con.State == ConnectionState.Open)
          Con.Close();

        Com.Dispose();
        Con.Dispose();
      }

      return RutaCSS;
    }

    /// <summary>
    /// Devuelve la IP del HOST
    /// </summary>
    /// <returns>(string) IP detectada</returns>
    public string DameIP()
    {
      string IP = System.Web.HttpContext.Current.Request.UserHostAddress;

      if (IP == "::1")
      {
        for (int i = 0; i < Dns.GetHostEntry(Dns.GetHostName()).AddressList.Length; i++)
        {
          if (Dns.GetHostEntry(Dns.GetHostName()).AddressList[i].AddressFamily == AddressFamily.InterNetwork && (IP == string.Empty || IP == "::1"))
            IP = Dns.GetHostEntry(Dns.GetHostName()).AddressList[i].ToString();
        }
      }

      return IP;
    }

    /// <summary>
    /// Devuelve los idiomas que tiene configurado un ISP en tres ArrayList embebidos. El primer ArrayList en una lista con tantos registros (ArrayLists de Registro) como 
    /// idiomas tenga especificado el ISP. El ArrayList de Registro contiene una lista de campos (ArrayList de Dupla nombre/valor de campo) de tantos campos como devuelva
    /// la consulta. Cada una de las Duplas es un ArrayList que contiene dos valores: Nombre y Valor.
    /// </summary>
    /// <param name="IdISP">(int) ISP del que queremos que nos devuelva sus idiomas</param>
    /// <returns>(ArrayList) Lista con el IdIdioma, el idioma en inglés, el CultureCode y si es un idioma principal o no</returns>
    public ArrayList DameIdiomasISP(int IdISP)
    {
      ArrayList Lista = new ArrayList();
      ArrayList Registro = new ArrayList();
      ArrayList Dupla = new ArrayList();

      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = Con.CreateCommand();

      try
      {
        Com.CommandType = System.Data.CommandType.StoredProcedure;
        Com.CommandText = "ISP_DameIdiomas";
        Com.CommandTimeout = 300;
        Com.Parameters.Add("@IdISP", SqlDbType.Int).Value = IdISP;
        Con.Open();

        SqlDataReader Rst = Com.ExecuteReader();

        while (Rst.Read())
        {
          Registro = new ArrayList();

          for (int i = 0; i < Rst.FieldCount; i++)
          {
            Dupla = new ArrayList();

            Dupla.Add(Rst.GetName(i));
            Dupla.Add(Rst.GetValue(i));

            if (Dupla.Count > 0)
              Registro.Add(Dupla);
          }

          if (Registro.Count > 0)
            Lista.Add(Registro);
        }

        Rst.Close();
        Rst.Dispose();
      }

      finally
      {
        if (Con.State == ConnectionState.Open)
          Con.Close();

        Com.Dispose();
        Con.Dispose();
      }

      return Lista;
    }

    /// <summary>
    /// Se le pasa un ArrayList con la lista de idiomas (obtenida llamando a DameIdiomasISP(int IdISP) y devuelve la cultura que esté marcada como "Principal".
    /// Si no hubiera ninguna marcada como "Principal" (no debería ocurrir), devuelve la de sistema.
    /// </summary>
    /// <param name="Lista">(ArrayList) Lista de idiomas. Uno de ellos estará marcado como "Principal".</param>
    /// <returns>(string) Cadena de cultura principal. P. Ej. es-ES</returns>
    public string DameCulturaPrincipal(ArrayList Lista)
    {
      string CulturaPrincipal = Thread.CurrentThread.CurrentCulture.Name;

      for (int i = 0; i < Lista.Count; i++)
      {
        if (Convert.ToBoolean(DameDato((ArrayList)Lista[i], "Principal")))
          CulturaPrincipal = DameDato((ArrayList)Lista[i], "CultureCode");
      }

      return CulturaPrincipal;
    }

    /// <summary>
    /// Se le pasa un ArrayList con la lista de idiomas (obtenida llamando a DameIdiomasISP(int IdISP) y devuelve el IdIdioma que esté marcado como "Principal".
    /// </summary>
    /// <param name="Lista">(ArrayList) Lista de idiomas. Uno de ellos estará marcado como "Principal".</param>
    /// <returns>(int) IdIdioma principal. P. Ej. 29</returns>
    public int DameIdiomaPrincipal(ArrayList Lista)
    {
      int IdIdioma = -1;

      for (int i = 0; i < Lista.Count; i++)
      {
        if (Convert.ToBoolean(DameDato((ArrayList)Lista[i], "Principal")))
          IdIdioma = Convert.ToInt32(DameDato((ArrayList)Lista[i], "IdIdioma"));
      }

      return IdIdioma;
    }

    /// <summary>
    /// Le pasas una lista de registros con n campos (un ArrayList dentro de un ArrayList) y te devuelve el campo que le pasas como parámetro de la primera fila.
    /// </summary>
    /// <param name="Registros">(ArrayList) Lista de reigstros con n campos.</param>
    /// <param name="Nombre">(string) Valor del campo siempre de tipo string, luego tendrás que hacer la conversión correspondiente.</param>
    /// <returns></returns>
    public string DameDato(ArrayList Registros, string Nombre)
    {
      string Dato = string.Empty;
      ArrayList Dupla = new ArrayList();
      Dupla = (ArrayList)Registros[0];

      for (int r = 0; r < Registros.Count; r++)
      {
        Dupla = (ArrayList)Registros[r];

        if (Dupla[0].ToString().ToUpper() == Nombre.ToUpper() && Dato == string.Empty)
          Dato = HttpUtility.HtmlEncode(Dupla[1].ToString());//Dato = Dupla[1].ToString().Replace("ç","&ccedil;").Replace("õ","&otilde;");
      }

      return Dato;
    }

    /// <summary>
    /// Devuelve los datos de un usuario final. El primer ArrayList en una lista con tantos registros (ArrayLists de Registro) como 
    /// usuarios devuelva (debería ser sólo uno). El ArrayList de Registro contiene una lista de campos (ArrayList de Dupla nombre/valor de campo) de tantos campos como devuelva
    /// la consulta. Cada una de las Duplas es un ArrayList que contiene dos valores: Nombre y Valor.
    /// </summary>
    /// <param name="IdUsuarioFinal">(int) IdUsuarioFinal que queremos que nos devuelva</param>
    /// <returns>(ArrayList) Lista con los datos del usuario final</returns>
    public ArrayList DameUsuarioFinal(int IdUsuarioFinal,bool? MostrarTodos)
    {
      ArrayList Lista = new ArrayList();
      ArrayList Registro = new ArrayList();
      ArrayList Dupla = new ArrayList();

      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = Con.CreateCommand();

      try
      {
        Com.CommandType = System.Data.CommandType.StoredProcedure;
        Com.CommandText = "Usuarios_DevolverDatos";
        Com.CommandTimeout = 300;
        Com.Parameters.Add("@IdUsuarioFinal", SqlDbType.Int).Value = IdUsuarioFinal;

        if(MostrarTodos!=null)
          Com.Parameters.Add("@Activo", SqlDbType.Bit).Value = false;

        Con.Open();

        SqlDataReader Rst = Com.ExecuteReader();

        while (Rst.Read())
        {
          Registro = new ArrayList();

          for (int i = 0; i < Rst.FieldCount; i++)
          {
            Dupla = new ArrayList();

            Dupla.Add(Rst.GetName(i));
            Dupla.Add(Rst.GetValue(i));

            if (Dupla.Count > 0)
              Registro.Add(Dupla);
          }

          if (Registro.Count > 0)
            Lista.Add(Registro);
        }

        Rst.Close();
        Rst.Dispose();
      }

      finally
      {
        if (Con.State == ConnectionState.Open)
          Con.Close();

        Com.Dispose();
        Con.Dispose();
      }

      return Lista;
    }

    /// <summary>
    /// Devuelve los datos de un operador. El primer ArrayList en una lista con tantos registros (ArrayLists de Registro) como 
    /// operadores (debería ser sólo uno). El ArrayList de Registro contiene una lista de campos (ArrayList de Dupla nombre/valor de campo) de tantos campos como devuelva
    /// la consulta. Cada una de las Duplas es un ArrayList que contiene dos valores: Nombre y Valor.
    /// </summary>
    /// <param name="IdOperador">(int) IdOperador que queremos que nos devuelva</param>
    /// <returns>(ArrayList) Lista con los datos del usuario final</returns>
    public ArrayList DameOperador(int IdOperador)
    {
      ArrayList Lista = new ArrayList();
      ArrayList Registro = new ArrayList();
      ArrayList Dupla = new ArrayList();

      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = Con.CreateCommand();

      try
      {
        Com.CommandType = System.Data.CommandType.StoredProcedure;
        Com.CommandText = "Operadores_DevolverDatos";
        Com.CommandTimeout = 300;
        Com.Parameters.Add("@IdOperador", SqlDbType.Int).Value = IdOperador;
        Con.Open();

        SqlDataReader Rst = Com.ExecuteReader();

        while (Rst.Read())
        {
          Registro = new ArrayList();

          for (int i = 0; i < Rst.FieldCount; i++)
          {
            Dupla = new ArrayList();

            Dupla.Add(Rst.GetName(i));
            Dupla.Add(Rst.GetValue(i));

            if (Dupla.Count > 0)
              Registro.Add(Dupla);
          }

          if (Registro.Count > 0)
            Lista.Add(Registro);
        }

        Rst.Close();
        Rst.Dispose();
      }

      finally
      {
        if (Con.State == ConnectionState.Open)
          Con.Close();

        Com.Dispose();
        Con.Dispose();
      }

      return Lista;
    }

    /// <summary>
    /// Devuelve los datos de un ISP. El primer ArrayList en una lista con tantos registros (ArrayLists de Registro) como 
    /// ISPs devuelva (debería ser sólo uno). El ArrayList de Registro contiene una lista de campos (ArrayList de Dupla nombre/valor de campo) de tantos campos como devuelva
    /// la consulta. Cada una de las Duplas es un ArrayList que contiene dos valores: Nombre y Valor.
    /// </summary>
    /// <param name="IdISP">(int) IdISP que queremos que nos devuelva</param>
    /// <returns>(ArrayList) Lista con los datos del ISP</returns>
    public ArrayList DameISP(int IdISP,bool IncluirInactivos)
    {
      ArrayList Lista = new ArrayList();
      ArrayList Registro = new ArrayList();
      ArrayList Dupla = new ArrayList();

      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = Con.CreateCommand();

      try
      {
        Com.CommandType = System.Data.CommandType.StoredProcedure;
        Com.CommandText = "ISP_DameDatos";
        Com.CommandTimeout = 300;
        Com.Parameters.Add("@IdISP", SqlDbType.Int).Value = IdISP;
        Com.Parameters.Add("@IncluirInactivos", SqlDbType.Bit).Value = IncluirInactivos;
        Con.Open();

        SqlDataReader Rst = Com.ExecuteReader();

        while (Rst.Read())
        {
          Registro = new ArrayList();

          for (int i = 0; i < Rst.FieldCount; i++)
          {
            Dupla = new ArrayList();

            Dupla.Add(Rst.GetName(i));
            Dupla.Add(Rst.GetValue(i));

            if (Dupla.Count > 0)
              Registro.Add(Dupla);
          }

          if (Registro.Count > 0)
            Lista.Add(Registro);
        }

        Rst.Close();
        Rst.Dispose();
      }

      finally
      {
        if (Con.State == ConnectionState.Open)
          Con.Close();

        Com.Dispose();
        Con.Dispose();
      }

      return Lista;
    }

    /// <summary>
    /// Devuelve los datos de un pedido identificándolo por su detalle de pedido.
    /// </summary>
    /// <param name="IdPedidoDetalle">(int) IdPedidoDetalle del que queremos buscar su pedido</param>
    /// <returns>(ArrayList) Lista de campos con sus valores</returns>
    public ArrayList DamePedidoPorDetalle(int IdPedidoDetalle)
    {
      ArrayList Lista = new ArrayList();
      ArrayList Registro = new ArrayList();
      ArrayList Dupla = new ArrayList();

      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = Con.CreateCommand();

      try
      {
        Com.CommandType = System.Data.CommandType.StoredProcedure;
        Com.CommandText = "Pedidos_DamePedido";
        Com.CommandTimeout = 300;
        Com.Parameters.Add("@IdPedidoDetalle", SqlDbType.Int).Value = IdPedidoDetalle;
        Con.Open();

        SqlDataReader Rst = Com.ExecuteReader();

        while (Rst.Read())
        {
          Registro = new ArrayList();

          for (int i = 0; i < Rst.FieldCount; i++)
          {
            Dupla = new ArrayList();

            Dupla.Add(Rst.GetName(i));
            Dupla.Add(Rst.GetValue(i));

            if (Dupla.Count > 0)
              Registro.Add(Dupla);
          }

          if (Registro.Count > 0)
            Lista.Add(Registro);
        }

        Rst.Close();
        Rst.Dispose();
      }

      finally
      {
        if (Con.State == ConnectionState.Open)
          Con.Close();

        Com.Dispose();
        Con.Dispose();
      }

      return Lista;
    }

    /// <summary>
    /// Método que reordena los elementos de un DropDownList de forma alfabética.
    /// </summary>
    /// <param name="DDL">(DropDownList) DropDownList a ordenar</param>
    public void ReordenarDDL(DropDownList DDL)
    {
      List<ListItem> ListadoBackup = new List<ListItem>();

      foreach (ListItem Elemento in DDL.Items)
        ListadoBackup.Add(Elemento);

      DDL.Items.Clear();

      foreach (ListItem Elemento in ListadoBackup.OrderBy(Elem => Elem.Text))
        DDL.Items.Add(Elemento);
    }

    /// <summary>
    /// Devuelve la ruta del site en función del entorno (Local, staging, producción, etc.), del protocolo y del dominio, teniendo en cuenta
    /// que si es localhost le añada el puerto de depuración si este es distinto del 80.
    /// </summary>
    /// <returns>(string) Ruta del site</returns>
    public string DameRuta()
    {
      string HOST = ConfigurationManager.AppSettings["HOST"].ToString();
      string ENTORNO = ConfigurationManager.AppSettings["ENTORNO"].ToString();
      string URL = string.Empty;

      URL = HOST + "://";

      if (HttpContext.Current.Request.Url.Port != 80)
      {
        URL += HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port;
      }
      else
      {
        URL += HttpContext.Current.Request.Url.Host;
      }

      return URL;
    }

    /// <summary>
    /// Devuelve la ruta de la API al punto de entrada pasado por parámetro.
    /// </summary>
    /// <param name="Punto">(string) Punto de entrada: RutaAPI_Login, RutaAPI_NewOrder</param>
    /// <returns>(string) Ruta a ese punto de la API</returns>
    public string DameRutaAPI(string Punto)
    {
      string RUTA = ConfigurationManager.AppSettings["RutaAPI"].ToString();

      if (ConfigurationManager.AppSettings[Punto] != null)
        RUTA += ConfigurationManager.AppSettings[Punto].ToString();
      else
        throw new Exception("The API source does not exist.");

      return RUTA.ToLower();
    }

		/// <summary>
		/// Devuelve las zonas con restricción de visibilidad de productos por ISP.
		/// </summary>
		/// <param name="IdISP">Identificador del ISP</param>
		/// <returns>Lista de zonas por producto e ISP</returns>
		public ArrayList DameZonasProductosISP(int IdISP,string Zona)
		{
			ArrayList Lista = new ArrayList();
			ArrayList Registro = new ArrayList();
			ArrayList Dupla = new ArrayList();

			SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
			SqlCommand Com = Con.CreateCommand();

			try
			{
				Com.CommandType = System.Data.CommandType.StoredProcedure;
				Com.CommandText = "Productos_DameZonasPorISP";
        Com.CommandTimeout = 300;
        Com.Parameters.Add("@IdISP", SqlDbType.Int).Value = IdISP;

				if(Zona!=null && Zona!=string.Empty)
					Com.Parameters.Add("@Zona", SqlDbType.VarChar,500).Value = Zona;

				Con.Open();

				SqlDataReader Rst = Com.ExecuteReader();

				while (Rst.Read())
				{
					Registro = new ArrayList();

					for (int i = 0; i < Rst.FieldCount; i++)
					{
						Dupla = new ArrayList();

						Dupla.Add(Rst.GetName(i));
						Dupla.Add(Rst.GetValue(i));

						if (Dupla.Count > 0)
							Registro.Add(Dupla);
					}

					if (Registro.Count > 0)
						Lista.Add(Registro);
				}

				Rst.Close();
				Rst.Dispose();
			}

			finally
			{
				if (Con.State == ConnectionState.Open)
					Con.Close();

				Com.Dispose();
				Con.Dispose();
			}

			return Lista;


		}

		/// <summary>
		/// Se le pasa un Id de un tipo de operador y el nombre de una columna y devuelve el dato en formato string.
		/// </summary>
		/// <param name="IdTipoOperador">(int) Identificador del operador que se busca</param>
		/// <param name="Campo">(string) Nombre de la columna a devolver</param>
		/// <returns>(string) Valor del dato de la columna especificada</returns>
		public string DameDatoTipoOperador(int IdTipoOperador, string Campo)
		{
			ArrayList Lista = DameTiposOperador(IdTipoOperador);

			string Nombre = string.Empty;
			if(Lista.Count>0)
				Nombre = DameDato((ArrayList)Lista[0], Campo);

			return Nombre;
		}

		/// <summary>
		/// Devuelve los tipos de operadores.
		/// </summary>
		/// <returns>(ArrayList) Lista con los datos de los tipos de operadores</returns>
		public ArrayList DameTiposOperador(int IdTipoOperador)
		{
			ArrayList Lista = new ArrayList();
			ArrayList Registro = new ArrayList();
			ArrayList Dupla = new ArrayList();

			SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
			SqlCommand Com = Con.CreateCommand();

			try
			{
				Com.CommandType = System.Data.CommandType.StoredProcedure;
				Com.CommandText = "Operadores_DevolverOperadoresTipos";
        Com.CommandTimeout = 300;
        Com.Parameters.Add("@IdTipoOperador", SqlDbType.Int).Value = IdTipoOperador;
				Con.Open();

				SqlDataReader Rst = Com.ExecuteReader();

				while (Rst.Read())
				{
					Registro = new ArrayList();

					for (int i = 0; i < Rst.FieldCount; i++)
					{
						Dupla = new ArrayList();

						Dupla.Add(Rst.GetName(i));
						Dupla.Add(Rst.GetValue(i));

						if (Dupla.Count > 0)
							Registro.Add(Dupla);
					}

					if (Registro.Count > 0)
						Lista.Add(Registro);
				}

				Rst.Close();
				Rst.Dispose();
			}

			finally
			{
				if (Con.State == ConnectionState.Open)
					Con.Close();

				Com.Dispose();
				Con.Dispose();
			}

			return Lista;
		}


    //VALIDADORES DE FORMATOS
    public string validarCPF(string CPF_CNPJ)
    {
      string Tipo = string.Empty;
      string ExpresionPersonaFisica = @"^\d{3}\.\d{3}\.\d{3}\-\d{2}$";
      string ExpresionPersonaJuridica = @"^\d{2}\.\d{3}\.\d{3}\/\d{4}\-\d{2}$";
      Regex rgxPersonaFisica = new Regex(ExpresionPersonaFisica);
      Regex rgxPersonaJuridica = new Regex(ExpresionPersonaJuridica);

      if (rgxPersonaFisica.IsMatch(CPF_CNPJ))
        Tipo = "CPF";

      if (rgxPersonaJuridica.IsMatch(CPF_CNPJ))
        Tipo = "CNPJ";

      return Tipo;
    }

    public string validarEmail(string email)
    {
      string Tipo = string.Empty;

      string Expresion = @"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)+$";
      Regex rgx = new Regex(Expresion);

      if (rgx.IsMatch(email))
        Tipo="EMAIL_OK";

      return Tipo;
    }

    public string validarCEP(string CEP)
    {
      string Tipo = string.Empty;

      string Expresion = @"^\d{5}\-\d{3}$";
      Regex rgx = new Regex(Expresion);

      if (rgx.IsMatch(CEP))
        Tipo = "CEP_OK";

      return Tipo;
    }
	}
}
