﻿using System;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Web.Security;
using System.Web.Configuration;
using System.Configuration;
using System.Net.Mail;
using System.Net;

namespace LIVINGSAFE_SiteCallCenter.App_Code
{
  public class cSesiones
  {
    cUtil Utilidad = new cUtil();

    public cSesiones()
    {
    }

    public int InsertarSesion(int Identificador, string Tipo, int IdISP, int Duracion)
    {
      int IdSesion = 0;

      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = new SqlCommand();

      try
      {
        Con.Open();

        Com = Con.CreateCommand();
        Com.CommandText = "Sesiones_InsertarSesion";
        Com.CommandTimeout = 300;
        Com.CommandType = CommandType.StoredProcedure;
        Com.Parameters.Add("@Identificador", SqlDbType.Int).Value = Identificador;
        Com.Parameters.Add("@Tipo", SqlDbType.VarChar, 250).Value = Tipo;
        Com.Parameters.Add("@Duracion", SqlDbType.Int).Value = Duracion;
        Com.Parameters.Add("@IdISP", SqlDbType.Int).Value = IdISP;

        SqlParameter parIdSesion = Com.Parameters.Add("@RETURN_VALUE", SqlDbType.Int);
        parIdSesion.Direction = ParameterDirection.ReturnValue;

        Com.ExecuteNonQuery();

        IdSesion = Convert.ToInt32(parIdSesion.Value);
      }

      finally
      {
        Com.Dispose();

        if (Con.State == ConnectionState.Open)
          Con.Close();

        Con.Dispose();
      }

      return IdSesion;
    }

    public bool ValidarSesion(int Identificador, string Tipo, int IdISP)
    {
      cEncriptacion AES = new cEncriptacion();

      bool EsValida = false;
      int Contador = 0;

      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = new SqlCommand();

      try
      {
        Con.Open();

        Com = Con.CreateCommand();
        Com.CommandText = "Sesiones_ValidarSesion";
        Com.CommandTimeout = 300;
        Com.CommandType = CommandType.StoredProcedure;
        Com.Parameters.Add("@Identificador", SqlDbType.Int).Value = Identificador;
        Com.Parameters.Add("@Tipo", SqlDbType.VarChar, 250).Value = Tipo;
        Com.Parameters.Add("@IdISP", SqlDbType.Int).Value = IdISP;

        SqlParameter parContador = Com.Parameters.Add("@RETURN_VALUE", SqlDbType.Int);
        parContador.Direction = ParameterDirection.ReturnValue;

        Com.ExecuteNonQuery();

        Contador = Convert.ToInt32(parContador.Value);

        if (Contador > 0)
          EsValida = true;
      }

      finally
      {
        Com.Dispose();

        if (Con.State == ConnectionState.Open)
          Con.Close();

        Con.Dispose();
      }

      return EsValida;
    }

    public void CaducarSesion(int Identificador, string Tipo, int IdISP)
    {
      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = new SqlCommand();

      try
      {
        Con.Open();

        Com = Con.CreateCommand();
        Com.CommandText = "Sesiones_CaducarSesion";
        Com.CommandTimeout = 300;
        Com.CommandType = CommandType.StoredProcedure;
        Com.Parameters.Add("@Identificador", SqlDbType.Int).Value = Identificador;
        Com.Parameters.Add("@Tipo", SqlDbType.VarChar, 250).Value = Tipo;
        Com.Parameters.Add("@IdISP", SqlDbType.Int).Value = IdISP;

        Com.ExecuteNonQuery();
      }

      finally
      {
        Com.Dispose();

        if (Con.State == ConnectionState.Open)
          Con.Close();

        Con.Dispose();
      }
    }

    public bool Autenticar(string USR, string PWD, bool Recordar,string OrigenLogin)
    {
      cSesiones Sesiones = new cSesiones();
      string URL = HttpContext.Current.Request.Url.Host;
      int IdISPDetectado = Utilidad.DameIdISPPorDominio(URL);
      int IdSesion = -1;
      bool ActivarDesactivarCAPTCHA = false;

      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = Con.CreateCommand();

      try
      {
        Com.CommandType = System.Data.CommandType.StoredProcedure;
        Com.CommandText = "Operadores_DevolverDatos";
        Com.CommandTimeout = 300;
        Com.Parameters.Add("@Login", SqlDbType.VarChar, 500).Value = USR.Replace("'", "").Replace(" ", "").Trim();
        Com.Parameters.Add("@IdISP", SqlDbType.Int).Value = IdISPDetectado;

        Con.Open();

        SqlDataReader Rst = Com.ExecuteReader();

        if (Rst.Read())
        {
          int IdISPOperador = Convert.ToInt32(Rst["IdISPOperador"]);
          int IdOperador = Convert.ToInt32(Rst["IdOperador"]);
          string Login = Rst["LoginOperador"].ToString();
          string PasswordCifrada = Rst["PasswordOperador"].ToString();
          string TipoUsuario = "Operador";


          cEncriptacion AES = new cEncriptacion(Utilidad.DameClaveCifradoISP(IdISPOperador));
          string SC = PasswordCifrada.Substring(0, 44);
          string Recortado = PasswordCifrada.Substring(SC.Length, PasswordCifrada.Length - SC.Length);
          string Password = AES.DescifrarAES(Recortado, SC);

          //Registramos evento de intento de LOGIN
          Utilidad.EscribeLOG(IdOperador, TipoUsuario, "Intento de LOGIN de operador. Origen: " + OrigenLogin + " (CALLCENTER)");

          if ((USR.Trim().ToLower() == Login.Trim().ToLower()) && (PWD == Password) && IdISPOperador == IdISPDetectado)
          {
            //Identificamos el tipo de usuario aunque debería ser usuario final. La sesión durará lo especificado en la tabla de CONFIGURACION

            int DuracionSesion = Convert.ToInt32(Utilidad.DameValorConfiguracion("DuracionSesion"));
            int DuracionSesionRecuerdame = Convert.ToInt32(Utilidad.DameValorConfiguracion("DuracionSesionRecuerdame"));

            //Registramos LOGIN conseguido
            Utilidad.EscribeLOG(IdOperador, TipoUsuario, "LOGIN de operador. Origen: " + OrigenLogin + " (CALLCENTER)");

            //Creamos el ticket
            FormsAuthenticationTicket ticket = null;

            //Si marca el check de recordar es que quiere que se recuerde su sesión aún cerrando el navegador
            if (Recordar)
            {
              ticket = new FormsAuthenticationTicket(1, IdOperador.ToString(), DateTime.Now, DateTime.Now.AddMinutes(DuracionSesionRecuerdame), Recordar, IdOperador.ToString());
              IdSesion = Sesiones.InsertarSesion(IdOperador, TipoUsuario, IdISPDetectado, DuracionSesionRecuerdame);
            }
            else
            {
              ticket = new FormsAuthenticationTicket(1, IdOperador.ToString(), DateTime.Now, DateTime.Now.AddMinutes(DuracionSesion), Recordar, IdOperador.ToString());
              IdSesion = Sesiones.InsertarSesion(IdOperador, TipoUsuario, IdISPDetectado, DuracionSesion);
            }

            //Creamos una sesión para el usuario final
            string ticketEncriptado = FormsAuthentication.Encrypt(ticket);
            HttpCookie SesionUsuario = new HttpCookie(FormsAuthentication.FormsCookieName, ticketEncriptado);
            //SesionUsuario.HttpOnly = true;

            if (Recordar)
              SesionUsuario.Expires = ticket.Expiration;
            else
              SesionUsuario.Expires = DateTime.MinValue;

            HttpContext.Current.Response.Cookies.Add(SesionUsuario);

            HttpCookie SesionTipo = new HttpCookie("SesionTipo", AES.CifrarAES(IdISPOperador, TipoUsuario));
            HttpContext.Current.Response.Cookies.Add(SesionTipo);
            //SesionTipo.HttpOnly = true;
            HttpContext.Current.Response.Cookies.Add(SesionTipo);

            string LoginCampo = USR.Replace("'", "").Replace(" ", "").Trim();
            string IP = Utilidad.DameIP();
            Sesiones.BorrarIntentoCAPTCHA(LoginCampo, IdISPDetectado, IP);

            HttpContext.Current.Response.Redirect(FormsAuthentication.GetRedirectUrl(IdOperador.ToString(), Recordar));
          }
          else
          {
            string LoginCampo = USR.Replace("'", "").Replace(" ", "").Trim();
            string IP = Utilidad.DameIP();

            Sesiones.InsertarIntentoCAPTCHA(LoginCampo, IdISPDetectado, IP);
            ActivarDesactivarCAPTCHA = true;

            //Registramos intento de LOGIN
            Utilidad.EscribeLOG(IdOperador, TipoUsuario, "LOGIN erróneo de usuario final. Posible contraseña incorrecta (CALLCENTER)");

            throw new Exception("ERRNUM0001");
          }
        }
        else
        {
          string LoginCampo = USR.Replace("'", "").Replace(" ", "").Trim();
          string IP = Utilidad.DameIP();

          Sesiones.InsertarIntentoCAPTCHA(LoginCampo, IdISPDetectado, IP);
          ActivarDesactivarCAPTCHA = true;

          //Registramos intento de LOGIN
          Utilidad.EscribeLOG(-1, Utilidad.DameTipoDeEntidad(USR), "LOGIN erróneo. Puede que el Login no exista para el ISP indicado (ISP: " + IdISPDetectado + ") (CALLCENTER)");

          throw new Exception(Resources.login.ResourceManager.GetString("ERRNUM0002"));
        }
      }

      finally
      {
        if (Con.State == ConnectionState.Open)
          Con.Close();

        Com.Dispose();
        Con.Dispose();
      }

      return ActivarDesactivarCAPTCHA;
    }

    public void CaducaOperador()
    {
      HttpCookie CookieFA = new HttpCookie("formsauth", "");
      CookieFA.Expires = DateTime.Today.AddYears(-1);
      CookieFA.Value = "";
      HttpContext.Current.Response.Cookies.Add(CookieFA);

      //En teoría esta es la misma que la anterior
      HttpCookie cookie1 = new HttpCookie(FormsAuthentication.FormsCookieName, "");
      cookie1.Expires = DateTime.Now.AddYears(-1);
      HttpContext.Current.Response.Cookies.Add(cookie1);

      HttpCookie SesionTipo = new HttpCookie("SesionTipo", "");
      SesionTipo.Expires = DateTime.Today.AddYears(-1);
      SesionTipo.Value = "";
      HttpContext.Current.Response.Cookies.Add(SesionTipo);


      SessionStateSection sessionStateSection = (SessionStateSection)WebConfigurationManager.GetSection("system.web/sessionState");
      HttpCookie cookie3 = new HttpCookie(sessionStateSection.CookieName, "");
      cookie3.Expires = DateTime.Now.AddYears(-1);
      HttpContext.Current.Response.Cookies.Add(cookie3);

      //Aunque no trabajamos con sesiones, la eliminamos.
      HttpContext.Current.Session.Clear();
      HttpContext.Current.Session.Abandon();

      //Forzamos LOG-OUT
      FormsAuthentication.SignOut();

      HttpContext.Current.User = null;
    }

    public void InsertarIntentoCAPTCHA(string Login, int IdISP, string IP)
    {
      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = Con.CreateCommand();

      try
      {
        Con.Open();

        Com = Con.CreateCommand();
        Com.CommandText = "Sesiones_CAPTCHA_InsertarIntento";
        Com.CommandTimeout = 300;
        Com.CommandType = CommandType.StoredProcedure;
        Com.Parameters.Add("@Login", SqlDbType.VarChar, 500).Value = Login;
        Com.Parameters.Add("@IdISP", SqlDbType.Int).Value = IdISP;
        Com.Parameters.Add("@IP", SqlDbType.VarChar, 50).Value = IP;

        Com.ExecuteNonQuery();
      }

      finally
      {
        if (Con.State == ConnectionState.Open)
          Con.Close();

        Com.Dispose();
        Con.Dispose();
      }
    }

    public int ContarIntentosCAPTCHA(string Login, int IdISP, string IP)
    {
      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = Con.CreateCommand();
      int Contador = 0;

      try
      {
        Con.Open();

        Com = Con.CreateCommand();
        Com.CommandText = "Sesiones_CAPTCHA_ContarIntentos";
        Com.CommandTimeout = 300;
        Com.CommandType = CommandType.StoredProcedure;
        Com.Parameters.Add("@Login", SqlDbType.VarChar, 500).Value = Login;
        Com.Parameters.Add("@IdISP", SqlDbType.Int).Value = IdISP;
        Com.Parameters.Add("@IP", SqlDbType.VarChar, 50).Value = IP;
        Com.Parameters.Add("@Tiempo", SqlDbType.Int).Value = Convert.ToInt32(Utilidad.DameValorConfiguracion("TiempoReintentosCAPTCHA"));

        SqlParameter parametro = Com.Parameters.Add("RETURN_VALUE", SqlDbType.Int);
        parametro.Direction = ParameterDirection.ReturnValue;

        Com.ExecuteNonQuery();

        Contador = Convert.ToInt32(Com.Parameters["RETURN_VALUE"].Value);
      }

      finally
      {
        if (Con.State == ConnectionState.Open)
          Con.Close();

        Com.Dispose();
        Con.Dispose();
      }

      return Contador;
    }

    public void BorrarIntentoCAPTCHA(string Login, int IdISP, string IP)
    {
      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = Con.CreateCommand();

      try
      {
        Con.Open();

        Com = Con.CreateCommand();
        Com.CommandText = "Sesiones_CAPTCHA_BorrarIntento";
        Com.CommandTimeout = 300;
        Com.CommandType = CommandType.StoredProcedure;
        Com.Parameters.Add("@Login", SqlDbType.VarChar, 500).Value = Login;
        Com.Parameters.Add("@IdISP", SqlDbType.Int).Value = IdISP;
        Com.Parameters.Add("@IP", SqlDbType.VarChar, 50).Value = IP;

        Com.ExecuteNonQuery();
      }

      finally
      {
        if (Con.State == ConnectionState.Open)
          Con.Close();

        Com.Dispose();
        Con.Dispose();
      }
    }

    public string SolicitarCambioPassword(string Login, int IdISPDetectado)
    {
      SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["LivingSafeCS"].ToString());
      SqlCommand Com = Con.CreateCommand();

      string EMail = string.Empty;
      int IdISP = -1;
      int IdOperador = -1;
      string NombreUsuario = string.Empty;
      string RutaLogo = string.Empty;

      try
      {
        Con.Open();

        Com.CommandType = CommandType.StoredProcedure;
        Com.CommandText = "Operadores_DevolverDatos";
        Com.CommandTimeout = 300;
        Com.Parameters.Add("@Login", SqlDbType.VarChar, 500).Value = Login;
        Com.Parameters.Add("@IdISP", SqlDbType.Int).Value = IdISPDetectado;

        SqlDataReader Rst = Com.ExecuteReader();

        if (Rst.Read())
        {
          EMail = Rst["EmailOperador"].ToString();
          IdISP = Convert.ToInt32(Rst["IdISPOperador"]);
          IdOperador = Convert.ToInt32(Rst["IdOperador"]);
          NombreUsuario = Rst["NombreOperador"].ToString();
          RutaLogo = Rst["Logo"].ToString();

          string[] CadenaLogo = RutaLogo.Split('.');

          string url = ConfigurationManager.AppSettings["HOST"].ToString();


          if (HttpContext.Current.Request.Url.Host.ToLower() == "localhost")
            RutaLogo = url + "://" + HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port + "/images/logos/" + CadenaLogo[0] + "_email." + CadenaLogo[1];
          else
            RutaLogo = url + "://" + HttpContext.Current.Request.Url.Host + "/images/logos/" + CadenaLogo[0] + "_email." + CadenaLogo[1];

          EnviarDatos(IdISP, IdOperador, EMail, NombreUsuario, RutaLogo);

          //Registramos evento de cambio de contraseña
          Utilidad.EscribeLOG(IdOperador, Utilidad.DameTipoDeEntidad(Login), "Solicitud de cambio de contraseña (CALLCENTER)");
        }
        else
        {
          throw new Exception(Resources.login.ResourceManager.GetString("ERRNUM0002"));
        }

        Rst.Dispose();
      }

      finally
      {
        if (Con.State == ConnectionState.Open)
          Con.Close();

        Com.Dispose();
        Con.Dispose();
      }

      return EMail;
    }

    protected void EnviarDatos(int IdISP, int IdUsuarioFinal, string Email, string NombreUsuario, string RutaLogo)
    {
      DateTime fecha = DateTime.Now;
      cEncriptacion AES = new cEncriptacion(Utilidad.DameClaveCifradoISP(IdISP));
      string host = String.Empty;
      string URL = HttpContext.Current.Request.Url.Host;
      string DominioPrincipal = Utilidad.DameValorConfiguracion("DominioPrincipal");

      string url = ConfigurationManager.AppSettings["HOST"].ToString();

      if (Email == null || Email == string.Empty)
        throw new Exception(Resources.login.ResourceManager.GetString("ERRNUM0004"));

      string SC = AES.CrearSecretoCompartido(IdUsuarioFinal.ToString());
      string IdOperadorCifrado = AES.CifrarAES(IdUsuarioFinal.ToString(), SC);
      string IdOperadorCifradoFinal = SC + IdOperadorCifrado;

      SC = AES.CrearSecretoCompartido(IdISP.ToString());
      string IdISPCifrado = AES.CifrarAES(IdISP.ToString(), SC);
      string IdISPCifradoFinal = SC + IdISPCifrado;

      SmtpClient Correo = new SmtpClient(Utilidad.DameValorConfiguracion("ServidorSMTPCorreoSpamina"));
      NetworkCredential Credenciales = new NetworkCredential(Utilidad.DameValorConfiguracion("ServidorSMTPUser"), Utilidad.DameValorConfiguracion("ServidorSMTPPass"));
      Correo.Credentials = Credenciales;

      Correo.Port = Convert.ToInt32(Utilidad.DameValorConfiguracion("ServidorSMTPPuerto"));
      Correo.EnableSsl = true;

      MailMessage Mensaje = new MailMessage();

			//IMPORTANTE: El dominio del e-mail que van en el FROM debe ser el mismo que el dominio del SMTP User de las credenciales.
			Mensaje.From = new System.Net.Mail.MailAddress(Utilidad.DameValorConfiguracion("MailFromCambioPassword"));
      Mensaje.To.Add(Email);
      Mensaje.Subject = Resources.login.ResourceManager.GetString("AsuntoMailCambioPass");
      Mensaje.IsBodyHtml = true;

      if (HttpContext.Current.Request.Url.Port != 80)
        host = HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port;
      else
        host = HttpContext.Current.Request.Url.Host;

      string Enlace = url + "://" + host + "/change-password.aspx?i=" + HttpUtility.UrlEncode(IdOperadorCifradoFinal) + "&p=" + HttpUtility.UrlEncode(IdISPCifradoFinal) + "&pd=" + IdISP.ToString() + "&f=" + fecha.Ticks.ToString();
      string Duracion = Utilidad.DameValorConfiguracion("CaducidadEnlaceCambioPassword");

      Mensaje.Body = GetMessage(NombreUsuario, Enlace, Duracion, RutaLogo);

      Correo.Send(Mensaje);
    }

    protected string GetMessage(string Usuario, string Enlace, string Duracion, string RutaLogo)
    {
      string Msg = String.Empty;
      string ruta = ConfigurationManager.AppSettings["HOST"].ToString() + "://";

      if (HttpContext.Current.Request.Url.Port != 80)
        ruta += HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port;
      else
        ruta += HttpContext.Current.Request.Url.Host;

      string LogoLivingSafe = ruta + "/images/logos/logo_livingsafe_email.png";
      string LogoLivingSafeEmail = ruta + "/images/logos/logo_livingsafe_simbolo.png";
      string FondoCelda = ruta + "/images/email_fondo.jpg";

      Msg = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath(Resources.login.ResourceManager.GetString("RutaPlantillaMailCambioPass")));
      Msg = Msg.Replace("[LOGOLIVINGSAFE]", LogoLivingSafe);
      Msg = Msg.Replace("[LOGOLIVINGSAFEEMAIL]", LogoLivingSafeEmail);
      Msg = Msg.Replace("[LOGOPARTNER]", RutaLogo);
      Msg = Msg.Replace("[USUARIO]", Usuario);
      Msg = Msg.Replace("[ENLACE]", Enlace);
      Msg = Msg.Replace("[DURACION]", Duracion);

      return Msg;
    }
  }
}